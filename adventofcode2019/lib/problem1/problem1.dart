/// https://adventofcode.com/2019, Day 1
///
/// Task: https://adventofcode.com/2019/day/1
/// Date: 2019-12-01
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: 3372463, for 0.01s
/// Phase 2 answer: 5055835, for 0.01s

import 'dart:io';

List<String> readRows() =>
    File('lib/problem1/input.txt').readAsLinesSync().toList();

List<int> parseMasses(List<String> lines) =>
    lines.map((s) => int.parse(s)).toList();

int solvePhase1(List<int> masses) =>
    masses.map((m) => m ~/ 3 - 2).reduce((a, b) => a + b);

int solvePhase2(List<int> masses) {
  int calcFuelNeededFor(int mass) {
    var neededFuel = mass ~/ 3 - 2;
    if (neededFuel <= 0) return 0;
    return neededFuel + calcFuelNeededFor(neededFuel);
  }

  return masses.map((m) => calcFuelNeededFor(m)).reduce((a, b) => a + b);
}

void main() {
  List<int> masses = parseMasses(readRows());

  print('Phase 1: Ans = ${solvePhase1(masses)}');
  print('Phase 2: Ans = ${solvePhase2(masses)}');
}
