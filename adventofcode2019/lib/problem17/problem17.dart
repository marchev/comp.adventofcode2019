import 'dart:io';
import 'dart:math';
import 'dart:convert';

/// https://adventofcode.com/2019, Day 17
///
/// Task: https://adventofcode.com/2019/day/17
/// Date: 2019-12-17
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: 6672, for 0.05s
/// Phase 2 answer: 923017, for 0.2s

List<String> readRows() =>
    File('lib/problem17/input.txt').readAsLinesSync().toList();

List<int> parseOps(String line) => line.split(',').map(int.parse).toList();

class Point {
  final int r, c;

  const Point(this.r, this.c);

  @override
  bool operator ==(Object other) =>
      other is Point && r == other.r && c == other.c;

  @override
  int get hashCode => r * 10000 + c;

  @override
  String toString() => '($r,$c)';
}

class Computer {
  static const memorySize = 100000;

  /// Called when an input is requested.
  final Function _provideInput;

  /// Called when a new output value is emitted.
  final Function _consumeOutput;

  final List<int> _mem;

  /// Instruction pointer.
  var _i = 0;

  /// Relative base (for relative mode params).
  var _relativeBase = 0;

  Computer(List<int> ops, int provideInput(), void consumeOutput(int))
      : _provideInput = provideInput,
        _consumeOutput = consumeOutput,
        _mem = ops
            .toList()
            .followedBy(List.filled(memorySize - ops.length, 0))
            .toList();

  /// Executes the program, requesting inputs and supplying outputs through
  /// callbacks.
  void run() {
    while (_i < _mem.length) {
      _log('op: ${_mem[_i]}');

      var opCode = _mem[_i] % 100;
      var paramModes = _mem[_i] ~/ 100;

      switch (opCode) {
        case 1: // +
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a + b, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 2: // *
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a * b, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 3: // input
          final input = _provideInput();
          _writeValue(input, _mem[_i + 1], 0, paramModes);
          _i += 2;
          break;
        case 4: // output
          final output = _readValue(_mem[_i + 1], 0, paramModes);
          _consumeOutput(output);
          _log('output: $output');
          _i += 2;
          break;
        case 5: // jump-if-true
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          if (a != 0) {
            _i = _readValue(_mem[_i + 2], 1, paramModes);
          } else {
            _i += 3;
          }
          break;
        case 6: // jump-if-false
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          if (a == 0) {
            _i = _readValue(_mem[_i + 2], 1, paramModes);
          } else {
            _i += 3;
          }
          break;
        case 7: // less than
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a < b ? 1 : 0, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 8: // equals
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a == b ? 1 : 0, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 9: // set relative base
          _relativeBase += _readValue(_mem[_i + 1], 0, paramModes);
          _i += 2;
          break;
        case 99:
          _log('normal exit');
          return;
        default:
          throw Exception('Unrecognized instruction $opCode');
      }
    }
  }

  int _readValue(int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        return _mem[paramValue];
      case 1: // immediate mode
        return paramValue;
      case 2: // relative mode
        return _mem[_relativeBase + paramValue];
      default:
        throw AssertionError();
    }
  }

  void _writeValue(int value, int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        _mem[paramValue] = value;
        break;
      case 2: // relative mode
        _mem[_relativeBase + paramValue] = value;
        break;
      default:
        throw AssertionError();
    }
  }

  void _log(String msg) {
//    print('computer: $msg');
  }
}

/// Represents a robot in the grid with its location and direction.
class Robot {
  Point location;
  String dirSymbol;

  Robot(this.location, this.dirSymbol);

  int get dir {
    switch (dirSymbol) {
      case '^':
        return 0;
      case '>':
        return 1;
      case 'v':
        return 2;
      case '<':
        return 3;
      default:
        throw AssertionError();
    }
  }

  set dir(int value) {
    switch (value) {
      case 0:
        dirSymbol = '^';
        break;
      case 1:
        dirSymbol = '>';
        break;
      case 2:
        dirSymbol = 'v';
        break;
      case 3:
        dirSymbol = '<';
        break;
      default:
        throw AssertionError();
    }
  }

  @override
  String toString() => '[robot $dirSymbol at $location]';
}

class Grid {
  static const emptyCell = '.';
  static const scaffoldCell = '#';

  final _g = <Point, String>{};

//  Robot _robot;

  String cell(Point p) => _g[p] ?? emptyCell;

  void setCell(Point p, String value) => _g[p] = value;

//  set robot(Robot robot) => _robot = robot;

  Iterable<Point> get scaffoldPoints => _g.entries
      .where((entry) => entry.value == scaffoldCell)
      .map((entry) => entry.key);

  @override
  String toString() {
    final minR = _g.keys.map((point) => point.r).reduce(min);
    final maxR = _g.keys.map((point) => point.r).reduce(max);
    final minC = _g.keys.map((point) => point.c).reduce(min);
    final maxC = _g.keys.map((point) => point.c).reduce(max);
    final rows = StringBuffer();
    for (int r = minR; r <= maxR; ++r) {
      for (int c = minC; c <= maxC; ++c) {
        rows.write(_g[Point(r, c)] ?? emptyCell);
      }
      rows.writeln();
    }
    return rows.toString();
  }
}

// 0 - up; 1 - right; 2 - down; 3 - left
final dr = <int>[-1, 0, 1, 0];
final dc = <int>[0, 1, 0, -1];

int solvePhase1(List<int> ops) {
  final grid = Grid();
  int outputR = 0, outputC = 0;

  final comp = Computer(
    ops,
    () => throw AssertionError('No input instructions are anticipated'),
    (output) {
      if (output == 10) {
        outputR++;
        outputC = 0;
      } else {
        final point = Point(outputR, outputC);
        final s = AsciiDecoder().convert([output]);
        if (s == '^' || s == 'v' || s == '<' || s == '>') {
          grid.setCell(point, '#');
        } else if (s == 'X') {
          print('robot at $point (out)');
          grid.setCell(point, '.');
        } else {
          grid.setCell(point, s);
        }
        outputC++;
      }
    },
  );
  comp.run();

  var result = 0;
  for (var point in grid.scaffoldPoints) {
    var numScaffoldNeighbors = 0;
    for (var i = 0; i < 4; ++i) {
      final neighbor = Point(point.r + dr[i], point.c + dc[i]);
      numScaffoldNeighbors += grid.cell(neighbor) == '#' ? 1 : 0;
    }
    if (numScaffoldNeighbors == 4) {
      result += point.r * point.c;
    }
  }
  return result;
}

/// Continuous 'F's become a number denoting their count; 'L' and 'R' are kept
/// as they are.
String serializePattern(String pattern) {
  final segments = <String>[];
  var numConsecutiveF = 0;
  for (var s in pattern.split('')) {
    if (s == 'F') {
      numConsecutiveF++;
    } else {
      if (numConsecutiveF > 0) {
        segments.add('$numConsecutiveF');
        numConsecutiveF = 0;
      }
      segments.add('$s');
    }
  }
  if (numConsecutiveF > 0) {
    segments.add('$numConsecutiveF');
  }
  return segments.join(',');
}

String serializePatternSeq(List<int> patternSeq) => patternSeq
    .map((patternIx) => patternIx == 0 ? 'A' : (patternIx == 1 ? 'B' : 'C'))
    .join(',');

class PatternsSolution {
  final List<String> serializedPatterns;
  final String serializedPatternSeq;

  PatternsSolution(List<String> patterns, List<int> patternSeq)
      : serializedPatterns = patterns.map(serializePattern).toList(),
        serializedPatternSeq = serializePatternSeq(patternSeq);

  @override
  String toString() =>
      '[seq: $serializedPatternSeq; patterns: $serializedPatterns]';
}

PatternsSolution findPatterns(
    String seq, int pos, List<String> patterns, List<int> patternSeq) {
  if (pos == seq.length) {
    return PatternsSolution(patterns, patternSeq);
  }
  if (patternSeq.length >= 10) {
    // The serialized pattern seq (which includes commas) would exceed 20 chars.
    return null;
  }
  // First check if any of the patterns can be applied starting from pos.
  for (var i = 0; i < patterns.length; ++i) {
    if (seq.substring(pos, pos + patterns[i].length) == patterns[i]) {
      final solution = findPatterns(
          seq, pos + patterns[i].length, patterns, patternSeq.toList()..add(i));
      if (solution != null) return solution;
    }
  }
  // If there are still undetermined patterns, define a new one and apply it.
  if (patterns.length < 3) {
    for (var len = 1; pos + len <= seq.length; ++len) {
      final newPattern = seq.substring(pos, pos + len);
      if (serializePattern(newPattern).length > 20) break;
      final solution = findPatterns(
          seq,
          pos + len,
          patterns.toList()..add(newPattern),
          patternSeq.toList()..add(patterns.length));
      if (solution != null) return solution;
    }
  }
  return null;
}

/// Populates a sequence of robot moves (L, R, and F ('forward')) - from
/// a given position and direction, to the very last position in the grid.
List<String> populateSequence(Grid grid, Robot robot) {
  final seq = <String>[];
  while (true) {
    final forward = Point(
        robot.location.r + dr[robot.dir], robot.location.c + dc[robot.dir]);
    if (grid.cell(forward) == Grid.scaffoldCell) {
      seq.add('F');
      robot.location = forward;
      continue;
    }

    final left = Point(robot.location.r + dr[(robot.dir + 3) % 4],
        robot.location.c + dc[(robot.dir + 3) % 4]);
    if (grid.cell(left) == Grid.scaffoldCell) {
      seq..add('L')..add('F');
      robot.location = left;
      robot.dir = (robot.dir + 3) % 4;
      continue;
    }

    final right = Point(robot.location.r + dr[(robot.dir + 1) % 4],
        robot.location.c + dc[(robot.dir + 1) % 4]);
    if (grid.cell(right) == Grid.scaffoldCell) {
      seq..add('R')..add('F');
      robot.location = right;
      robot.dir = (robot.dir + 1) % 4;
      continue;
    }

    break;
  }
  return seq;
}

int solvePhase2(List<int> ops) {
  final grid = Grid();
  int outputR = 0, outputC = 0;
  Robot robot;

  final comp = Computer(
    ops,
    () => throw AssertionError('No input instructions are anticipated'),
    (output) {
      if (output == 10) {
        outputR++;
        outputC = 0;
      } else {
        final point = Point(outputR, outputC);
        final s = AsciiDecoder().convert([output]);
        if (s == '^' || s == 'v' || s == '<' || s == '>') {
          grid.setCell(point, '#');
          robot = Robot(point, s);
        } else if (s == 'X') {
          throw AssertionError();
        } else {
          grid.setCell(point, s);
        }
        outputC++;
      }
    },
  );
  comp.run();
//  print(grid);

  final seq = populateSequence(grid, robot);
  final solution = findPatterns(seq.join(''), 0, [], []);
//  print(solution);

  final fullInput = AsciiEncoder().convert([
    solution.serializedPatternSeq,
    solution.serializedPatterns[0],
    solution.serializedPatterns[1],
    solution.serializedPatterns[2],
    'n\n'
  ].join('\n'));
  final modifiedOps = ops.toList()..[0] = 2;
  int lastOutput;
  final comp2 = Computer(
    modifiedOps,
    () {
      var inputIx = 0;
      return () => fullInput[inputIx++];
    }(),
    (output) {
//      print('output: $output');
      lastOutput = output;
    },
  );
  comp2.run();

  return lastOutput;
}

void main() {
  var lines = readRows();
  List<int> ops = parseOps(lines[0]);

  print('Phase 1: Ans = ${solvePhase1(ops)}');
  print('Phase 2: Ans = ${solvePhase2(ops)}');
}
