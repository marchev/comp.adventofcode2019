import 'dart:io';
import 'package:collection/collection.dart';
import 'package:quiver/check.dart';

/// https://adventofcode.com/2019, Day 18
///
/// Task: https://adventofcode.com/2019/day/18
/// Date: 2019-12-18 for part 1, and 2020-12-06 for part 2.
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 6162, for 0.05s
/// Phase 2 answer: 1556, for 1.5s

List<String> readRows() =>
    File('lib/problem18/input.txt').readAsLinesSync().toList();

List<int> parseOps(String line) => line.split(',').map(int.parse).toList();

class Point {
  final int r, c;

  const Point(this.r, this.c);

  @override
  bool operator ==(Object other) =>
      other is Point && r == other.r && c == other.c;

  @override
  int get hashCode => r * 10000 + c;

  @override
  String toString() => '($r,$c)';
}

class ReachableObject {
  final String object;
  final int dist;

  ReachableObject(this.object, this.dist);

  @override
  String toString() => '[$object at $dist]';
}

// 0 - up; 1 - right; 2 - down; 3 - left
// 4 - top-left; 5 - top-right; 6 - bottom-left; 7 - bottom-right
const dr = const <int>[-1, 0, 1, 0, -1, -1, 1, 1];
const dc = const <int>[0, 1, 0, -1, -1, 1, 1, -1];

class Grid {
  static const startCell = '@';
  static const emptyCell = '.';
  static const wallCell = '#';

  final List<List<String>> _g;

  Map<String, Point> _keyToPosition;
  List<String> keys;
  List<Point> startPositions;

  int get rows => _g.length;

  int get cols => _g[0].length;

  Grid(this._g) {
    keys = _g.expand((row) => row).where(isKey).toList();

    _keyToPosition = {};
    startPositions = [];
    for (int r = 0; r < rows; ++r) {
      for (int c = 0; c < cols; ++c) {
        if (_g[r][c] == startCell) {
          // Store the start point and remove it from the grid.
          startPositions.add(Point(r, c));
          _g[r][c] = emptyCell;
        } else if (isKey(_g[r][c])) {
          _keyToPosition[_g[r][c]] = Point(r, c);
        }
      }
    }
  }

  /// Returns a list of reachable objects (doors or keys), for which we
  /// don't have the keys yet.
  List<ReachableObject> findReachableObjects(
      Point fromPosition, int ownedKeys) {
    final reachable = <ReachableObject>[];

    final queue = <Point>[fromPosition];
    final levels = <int>[0];
    final added = queue.toSet();

    for (var queueIx = 0; queueIx < queue.length;) {
      final cur = queue[queueIx];
      final curLevel = levels[queueIx];
      queueIx++;

      for (var k = 0; k < 4; ++k) {
        final next = Point(cur.r + dr[k], cur.c + dc[k]);
        if (added.contains(next)) continue;

        final nextG = _g[next.r][next.c];
        if (nextG == emptyCell || _isDoorOrKeyButHasKey(ownedKeys, nextG)) {
          queue.add(next);
          levels.add(1 + curLevel);
          added.add(next);
        } else if (nextG != wallCell) {
          // A door or key, for which we don't have the key yet.
          reachable.add(ReachableObject(nextG, 1 + curLevel));
        }
      }
    }
    return reachable;
  }

  static bool _isDoorOrKeyButHasKey(int keys, String val) {
    String key = val.toLowerCase(); // The day doors become keys
    if (!isKey(key)) return false;
    return ((keys >> keyId(key)) & 1) == 1;
  }

  static int keyId(String key) {
    checkState(isKey(key));
    return key.codeUnitAt(0) - 'a'.codeUnitAt(0);
  }

  static bool isKey(String val) =>
      'a'.codeUnitAt(0) <= val.codeUnitAt(0) &&
      val.codeUnitAt(0) <= 'z'.codeUnitAt(0);

  static bool isDoor(String val) =>
      'A'.codeUnitAt(0) <= val.codeUnitAt(0) &&
      val.codeUnitAt(0) <= 'Z'.codeUnitAt(0);

  Point getPositionById(int posId) {
    if (0 <= posId && posId < 26) {
      final key = String.fromCharCode('a'.codeUnitAt(0) + posId);
      return _keyToPosition[key];
    } else if (posId < 26 + startPositions.length) {
      return startPositions[posId - 26];
    }
    throw AssertionError();
  }

  @override
  String toString() {
    final str = StringBuffer();
    for (int r = 0; r < rows; ++r) {
      for (int c = 0; c < cols; ++c) {
        str.write(_g[r][c]);
      }
      str.writeln();
    }
    return str.toString();
  }
}

class QueueEntry implements Comparable<QueueEntry> {
  final int state;
  final int dist;

  const QueueEntry(this.state, this.dist);

  @override
  String toString() => '($state,$dist)';

  @override
  int compareTo(QueueEntry other) => dist.compareTo(other.dist);
}

Grid createGridForPhase1(List<String> lines) {
  final g = lines.map((line) => line.split('')).toList();
  return Grid(g);
}

Grid createGridForPhase2(List<String> lines) {
  final g = lines.map((line) => line.split('')).toList();

  final initialStart = Point(g.length ~/ 2, g[0].length ~/ 2);
  checkState(g[initialStart.r][initialStart.c] == Grid.startCell);

  // Modify the grid to meet the constraints of part 2.
  g[initialStart.r][initialStart.c] = Grid.wallCell;
  for (int k = 0; k < 4; ++k) {
    final neighbor = Point(initialStart.r + dr[k], initialStart.c + dc[k]);
    g[neighbor.r][neighbor.c] = Grid.wallCell;
  }
  for (int k = 4; k < 8; ++k) {
    final neighbor = Point(initialStart.r + dr[k], initialStart.c + dc[k]);
    g[neighbor.r][neighbor.c] = Grid.startCell;
  }
  return Grid(g);
}

/// Finds the least number of moves needed to collect all the keys in the given
/// [grid] using Dijkstra's algorithm.
int findShortestPath(Grid grid) {
  // Encode the initial state.
  var initialState = 0; // no keys taken yet
  for (var i = 0; i < grid.startPositions.length; ++i) {
    initialState = initialState * 30 + (26 + i);
  }

  final queue = PriorityQueue<QueueEntry>()..add(QueueEntry(initialState, 0));
  final minDistToState = <int, int>{}..[initialState] = 0;
  final processedStates = Set<int>();

  final withAllKeys =
      grid.keys.map(Grid.keyId).fold(0, (acc, b) => acc | (1 << b));

  while (queue.isNotEmpty) {
    final curEntry = queue.removeFirst();
    if (curEntry.dist != minDistToState[curEntry.state]) continue;
    processedStates.add(curEntry.state);

    // Decode state.
    var tmpState = curEntry.state;
    final curPositionIds = <int>[];
    for (var i = 0; i < grid.startPositions.length; ++i) {
      curPositionIds.insert(0, tmpState % 30);
      tmpState ~/= 30;
    }
    final curKeys = tmpState;

    if (curKeys == withAllKeys) {
      return curEntry.dist;
    }

    for (var cpi = 0; cpi < curPositionIds.length; ++cpi) {
      final curPosition = grid.getPositionById(curPositionIds[cpi]);
      for (final next in grid.findReachableObjects(curPosition, curKeys)) {
        // Allow only going to keys. There is no point in going to doors,
        // given that we cannot unlock them.
        if (Grid.isKey(next.object)) {
          final nextPosId =
              Grid.keyId(next.object); // same as position ID in this case

          // Encode state.
          var nextState = curKeys | (1 << Grid.keyId(next.object));
          for (var i = 0; i < grid.startPositions.length; ++i) {
            nextState =
                nextState * 30 + (i == cpi ? nextPosId : curPositionIds[i]);
          }

          if (processedStates.contains(nextState)) continue;
          final nextDist = curEntry.dist + next.dist;
          if (minDistToState[nextState] == null ||
              minDistToState[nextState] > nextDist) {
            minDistToState[nextState] = nextDist;
            queue.add(QueueEntry(nextState, nextDist));
          }
        }
      }
    }
  }
  return null;
}

int solvePhase1(List<String> lines) =>
    findShortestPath(createGridForPhase1(lines));

int solvePhase2(List<String> lines) =>
    findShortestPath(createGridForPhase2(lines));

void main() {
  final lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
