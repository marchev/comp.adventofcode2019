import 'dart:math';

/// https://adventofcode.com/2019, Day 12
///
/// Task: https://adventofcode.com/2019/day/12
/// Date: 2019-12-15
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Quite nice one. First part is straightforward, but for the second part
/// one should observe that the x, y and z coordinates are independent, so one
/// can simply find the cycles for each of the coordinates independently,
/// and then find the answer as:
///  Ans =  max(the first step at which the position and the acceleration
///             start to repeat | over each coordinate x, y, z)
///       + LCM(length of the repetition cycle | over each coordinate x, y, z).
///
/// Phase 1 answer: 10198, for 0.05s
/// Phase 2 answer: 271442326847376, for 1.3s

class Point {
  int x, y, z;

  Point(this.x, this.y, this.z);

  @override
  bool operator ==(Object other) =>
      other is Point && x == other.x && y == other.y && z == other.z;

  @override
  int get hashCode => (x * 1000 + y) * 1000 + z;

  @override
  String toString() => '($x,$y,$z)';
}

class Moon {
  final Point pos;
  final Point acc;

  Moon(this.pos) : acc = Point(0, 0, 0);

  @override
  String toString() => '[at $pos with acc=$acc]';
}

int potentialEnergy(Moon moon) =>
    moon.pos.x.abs() + moon.pos.y.abs() + moon.pos.z.abs();

int kineticEnergy(Moon moon) =>
    moon.acc.x.abs() + moon.acc.y.abs() + moon.acc.z.abs();

int solvePhase1(List<Moon> moons, [int steps = 1000]) {
  for (int step = 0; step < steps; ++step) {
    // First update the acceleration of all moons, and only then update
    // their positions, since otherwise updating of their positions would
    // interfere with updating of their accelerations.
    for (var moon in moons) {
      moon.acc.x += moons.where((other) => other.pos.x > moon.pos.x).length;
      moon.acc.x -= moons.where((other) => other.pos.x < moon.pos.x).length;
      moon.acc.y += moons.where((other) => other.pos.y > moon.pos.y).length;
      moon.acc.y -= moons.where((other) => other.pos.y < moon.pos.y).length;
      moon.acc.z += moons.where((other) => other.pos.z > moon.pos.z).length;
      moon.acc.z -= moons.where((other) => other.pos.z < moon.pos.z).length;
    }
    for (var moon in moons) {
      moon.pos.x += moon.acc.x;
      moon.pos.y += moon.acc.y;
      moon.pos.z += moon.acc.z;
    }
  }

  return moons
      .map((moon) => potentialEnergy(moon) * kineticEnergy(moon))
      .fold(0, (a, b) => a + b);
}

class Cycle {
  final int start, length;

  const Cycle(this.start, this.length);

  @override
  String toString() => '[cycle: len=$length, start=$start]';
}

int gcd(int a, int b) {
  if (b == 0) return a;
  return gcd(b, a % b);
}

int lcm(List<int> nums) => nums.reduce((a, b) => a * b ~/ gcd(a, b));

/// Finds a cycle only on one of the coordinates.
Cycle findCycle(List<Moon> moons, int getPos(Moon), void addPos(Moon, int),
    int getAcc(Moon), void addAcc(Moon, int)) {
  final seen = <String, int>{};

  for (int step = 0;; ++step) {
    // Serialize
    final key = moons.map((m) => '${getPos(m)}@${getAcc(m)}').join(',');
    final seenAtStep = seen.putIfAbsent(key, () => step);
    if (seenAtStep != step) {
      return Cycle(seenAtStep, step - seenAtStep);
    }

    for (var moon in moons) {
      addAcc(moon, moons.where((other) => getPos(other) > getPos(moon)).length);
      addAcc(
          moon, -moons.where((other) => getPos(other) < getPos(moon)).length);
    }
    for (var moon in moons) {
      addPos(moon, getAcc(moon));
    }
  }
}

int solvePhase2(List<Moon> moons) {
  final cycles = [
    findCycle(
      moons,
      (m) => m.pos.x,
      (m, val) => m.pos.x += val,
      (m) => m.acc.x,
      (m, val) => m.acc.x += val,
    ),
    findCycle(
      moons,
      (m) => m.pos.y,
      (m, val) => m.pos.y += val,
      (m) => m.acc.y,
      (m, val) => m.acc.y += val,
    ),
    findCycle(
      moons,
      (m) => m.pos.z,
      (m, val) => m.pos.z += val,
      (m) => m.acc.z,
      (m, val) => m.acc.z += val,
    ),
  ];
//  print(cycles);
  return cycles.map((cycle) => cycle.start).reduce(max) +
      lcm(cycles.map((cycle) => cycle.length).toList());
}

void main() async {
  final moons = <Moon>[
    Moon(Point(5, 13, -3)),
    Moon(Point(18, -7, 13)),
    Moon(Point(16, 3, 4)),
    Moon(Point(0, 8, 8)),
  ];

  print('Phase 1: Ans = ${solvePhase1(moons)}');
  print('Phase 2: Ans = ${solvePhase2(moons)}');
}
