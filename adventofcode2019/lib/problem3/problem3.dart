/// https://adventofcode.com/2019, Day 3
///
/// Task: https://adventofcode.com/2019/day/3
/// Date: 2019-12-04
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 896, for 0.18s
/// Phase 2 answer: 16524, for 0.21s

import 'dart:io';
import 'dart:math';

List<String> readRows() =>
    File('lib/problem3/input.txt').readAsLinesSync().toList();

List<String> parseDirs(String line) => line.split(',').toList();

// up, down, left, right
final dr = [-1, 1, 0, 0];
final dc = [0, 0, -1, 1];

class Point {
  final int r, c;

  const Point(this.r, this.c);

  @override
  bool operator ==(Object other) =>
      other is Point && r == other.r && c == other.c;

  @override
  get hashCode => (r + 100000) * 200000 + c;

  @override
  String toString() => '($r,$c)';
}

Map<Point, int> generatePoints(List<String> dirs) {
  Map<Point, int> pointToPosition = {};

  var r = 0, c = 0;
  var moves = 0;
  for (var i = 0; i < dirs.length; ++i) {
    var d = dirs[i].substring(0, 1);
    var times = int.parse(dirs[i].substring(1));

    var di;
    switch (d) {
      case 'U':
        di = 0;
        break;
      case 'D':
        di = 1;
        break;
      case 'L':
        di = 2;
        break;
      case 'R':
        di = 3;
        break;
      default:
        throw AssertionError();
    }

    for (int t = 0; t < times; ++t) {
      r += dr[di];
      c += dc[di];
      moves++;

      final p = Point(r, c);
      if (!pointToPosition.containsKey(p)) {
        pointToPosition[p] = moves;
      }
    }
  }
  return pointToPosition;
}

int solvePhase1(List<String> dirs1, List<String> dirs2) {
  var minDist = pow(10, 9);
  var pointToPos1 = generatePoints(dirs1);
  var pointToPos2 = generatePoints(dirs2);
  for (var p in pointToPos2.keys) {
    if (pointToPos1.containsKey(p)) {
      minDist = min(minDist, p.r.abs() + p.c.abs());
    }
  }
  return minDist;
}

int solvePhase2(List<String> dirs1, List<String> dirs2) {
  var minMoves = pow(10, 9);
  var pointToPos1 = generatePoints(dirs1);
  var pointToPos2 = generatePoints(dirs2);
  for (var p in pointToPos2.keys) {
    if (pointToPos1.containsKey(p)) {
      minMoves = min(minMoves, pointToPos1[p] + pointToPos2[p]);
    }
  }
  return minMoves;
}

void main() {
  var lines = readRows();
  List<String> dirs1 = parseDirs(lines[0]);
  List<String> dirs2 = parseDirs(lines[1]);

  print('Phase 1: Ans = ${solvePhase1(dirs1, dirs2)}');
  print('Phase 2: Ans = ${solvePhase2(dirs1, dirs2)}');
}
