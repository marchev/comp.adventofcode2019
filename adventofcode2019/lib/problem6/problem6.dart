/// https://adventofcode.com/2019, Day 6
///
/// Task: https://adventofcode.com/2019/day/6
/// Date: 2019-12-07
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 322508, for 0.03s
/// Phase 2 answer: 496, for 0.03s

import 'dart:io';

List<String> readRows() =>
    File('lib/problem6/input.txt').readAsLinesSync().toList();

class Tree {
  final _childrenOf = Map<String, List<String>>();
  final _parentOf = Map<String, String>();

  Tree(List<String> edges) {
    edges.forEach((edge) {
      final pair = edge.split(')').toList();
      _addEdge(pair[0], pair[1]);
    });
  }

  String parentOf(String node) => _parentOf[node];

  List<String> childrenOf(String node) => _childrenOf[node] ?? [];

  void _addEdge(String from, String to) {
    _parentOf[to] = from;
    if (_childrenOf.containsKey(from)) {
      _childrenOf[from].add(to);
    } else {
      _childrenOf[from] = [to];
    }
  }
}

int solvePhase1(List<String> lines) {
  final tree = Tree(lines);
  final queue = ['COM'];
  final orbits = [0];
  for (var i = 0; i < queue.length; ++i) {
    for (var kid in tree.childrenOf(queue[i])) {
      queue.add(kid);
      orbits.add(1 + orbits[i]);
    }
  }
  return orbits.fold(0, (a, b) => a + b);
}

int solvePhase2(List<String> lines) {
  final tree = Tree(lines);

  const me = 'YOU';
  const santa = 'SAN';

  final myParents = [];
  for (var node = tree.parentOf(me); node != null; node = tree.parentOf(node)) {
    myParents.add(node);
  }

  for (var node = tree.parentOf(santa), levelFromSanta = 0;
      node != null;
      node = tree.parentOf(node), ++levelFromSanta) {
    final levelFromMe = myParents.indexOf(node);
    if (levelFromMe != -1) {
      return levelFromMe + levelFromSanta;
    }
  }
  throw AssertionError();
}

void main() {
  var lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
