import 'dart:io';

import 'package:quiver/check.dart';

/// https://adventofcode.com/2019, Day 14
///
/// Task: https://adventofcode.com/2019/day/14
/// Date: 2019-12-16
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Analysis:
///
/// Phase 1 is solved by backward propagation of the needed chemical quantities
/// in a topological sort. O(C+R) time complexity, where C is number of
/// chemicals and R is number of reactions.
///
/// Phase 2 is based on the same algorithm as phase 1, but instead of simply
/// supplying 1 unit of FUEL, uses binary search to find the largest X,
/// such that X units of FUEL can be produced by <=10^12 units of ORE.
/// Time complexity is O(log(maxFuel) (C+R)).
///
/// Phase 1 answer: 532506, for 0.02s
/// Phase 2 answer: 2595245, for 0.02 s

List<String> readRows() =>
    File('lib/problem14/input.txt').readAsLinesSync().toList();

class Chemical {
  final String name;

  /// How much of this chemical produces the reaction that produces it.
  ///
  /// Each chemical is guaranteed to be obtained by at most one reaction.
  /// The only exception is ORE which is the origin element and there is no
  /// reaction the synthesizes it.
  int reactionOutcomeQty;

  /// How much is needed from this chemical to produces 1+ units of FUEL.
  /// It's being updated at runtime.
  int neededQty = 0;

  /// Number of not yet processed chemicals that this chemical is reagent of.
  /// It's being updated at runtime.
  int numOutputs = 0;

  Chemical(this.name);

  @override
  bool operator ==(other) => name == other.name;

  @override
  int get hashCode => name.hashCode;

  @override
  String toString() =>
      '[chemical $name; reaction produces $reactionOutcomeQty]';
}

/// A chemical of a particular quantity, as it takes part in a particular
/// reaction as a reagent.
class Reagent {
  final Chemical chemical;
  final qty;

  Reagent(this.chemical, this.qty);

  @override
  String toString() => '[reagent: $chemical; needs $qty of it]';
}

class QuantifiedChemical {
  final String name;
  final qty;

  QuantifiedChemical(this.name, this.qty);
}

class Graph {
  final _chemicalByName = <String, Chemical>{};
  final _reagentsByChemical = <Chemical, List<Reagent>>{};

  void addReaction(List<QuantifiedChemical> from, QuantifiedChemical to) {
    final product =
        _chemicalByName.putIfAbsent(to.name, () => Chemical(to.name));
    product.reactionOutcomeQty = to.qty;

    final reagents = _reagentsByChemical.putIfAbsent(product, () => []);
    reagents.addAll(from.map((qc) => Reagent(
        _chemicalByName.putIfAbsent(qc.name, () => Chemical(qc.name))
          ..numOutputs += 1,
        qc.qty)));
  }

  Chemical chemicalByName(String name) => _chemicalByName[name];

  List<Reagent> reagentsByChemical(Chemical c) => _reagentsByChemical[c] ?? [];

  int get numChemicals => _chemicalByName.length;

  Iterable<Chemical> get chemicals => _chemicalByName.values;

  @override
  String toString() =>
      'chemicals: $_chemicalByName\nreagentsByChemical: $_reagentsByChemical';
}

/// Format is a list of:
/// 10 DLSD, 2 SWKHJ, 15 HTSW => 1 TCRNC
Graph parseGraph(List<String> lines) {
  final g = Graph();

  QuantifiedChemical parseQtyChemical(String s) {
    final halves = s.split(' ');
    return QuantifiedChemical(halves[1], int.parse(halves[0]));
  }

  for (var line in lines) {
    final halves = line.split(' => ');
    final from = halves[0]
        .split(', ')
        .map((segment) => parseQtyChemical(segment))
        .toList();
    final to = parseQtyChemical(halves[1]);
    g.addReaction(from, to);
  }
  return g;
}

int computeNeededOre(List<String> lines, int neededFuel) {
  final g = parseGraph(lines);
//  print(g);

  g.chemicalByName('ORE').reactionOutcomeQty = 1;
  g.chemicalByName('FUEL').neededQty = neededFuel;

  final processed = Set<Chemical>();
  while (processed.length < g.numChemicals) {
    // Find a node with not output chemicals
    final Chemical chemical = g.chemicals
        .firstWhere((c) => !processed.contains(c) && c.numOutputs == 0);
    checkState(chemical != null);
    processed.add(chemical);

    final int timesToRunReaction =
        (chemical.neededQty + chemical.reactionOutcomeQty - 1) ~/
            chemical.reactionOutcomeQty;
    for (var reagent in g.reagentsByChemical(chemical)) {
      checkState(!processed.contains(reagent.chemical));
      reagent.chemical.numOutputs--;
      reagent.chemical.neededQty += timesToRunReaction * reagent.qty;
      checkState(reagent.chemical.neededQty >= 0, message: 'overflow');
    }
  }

  return g._chemicalByName['ORE'].neededQty;
}

int solvePhase1(List<String> lines) => computeNeededOre(lines, 1);

/// Uses binary search to find the largest X, such that X units of FUEL
/// can be produced by <=10^12 units of ORE.
int solvePhase2(List<String> lines) {
  const availableOre = 1000000000000;
  var producedFuel = 0;
  for (int m = (1 << 40); m >= 1; m ~/= 2) {
    final neededOre = computeNeededOre(lines, producedFuel + m);
    if (neededOre <= availableOre) {
      producedFuel += m;
    }
  }
  return producedFuel;
}

void main() {
  var lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
