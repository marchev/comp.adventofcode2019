import 'dart:io';

import 'package:quiver/check.dart';

/// https://adventofcode.com/2019, Day 24
///
/// Task: https://adventofcode.com/2019/day/24
/// Date: 2019-12-24
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Analysis:
/// Straightforward state encoding and decoding problem.
///
/// Phase 1 answer: 32506911, for 0.05s
/// Phase 2 answer: 2025, for 0.1s

List<String> readRows() =>
    File('lib/problem24/input.txt').readAsLinesSync().toList();

const size = 5;

/// Converts a given grid to a state.
int encode(List<String> lines) {
  var state = 0;
  for (int i = 0; i < size * size; ++i) {
    if (lines[i ~/ size].substring(i % size, i % size + 1) == '#') {
      state |= 1 << i;
    }
  }
  return state;
}

/// Returns 1 or 0 depending on whether the i-th bit of [state] is set.
int val(int state, int i) =>
    (i < 0 || i >= size * size) ? 0 : ((state >> i) & 1);

void printState(int state) {
  final str = StringBuffer();
  for (int i = 0; i < size * size; ++i) {
    str.write(val(state, i));
    if ((i + 1) % size == 0) str.writeln();
  }
  str.writeln();
  print(str);
}

int solvePhase1(List<String> lines) {
  /// Computes the next state, assuming there is only a single dimension.
  int computeNextState(int state) {
    int countAdjacent(int i) {
      int num = val(state, i + size) + val(state, i - size);
      if (i % size > 0) num += val(state, i - 1);
      if (i % size < size - 1) num += val(state, i + 1);
      return num;
    }

    int nextState = state;
    for (int i = 0; i < size * size; ++i) {
      int numAdjacent = countAdjacent(i);
      if (val(state, i) == 1 && numAdjacent != 1) {
        nextState -= 1 << i;
      } else if (val(state, i) == 0 && (numAdjacent == 1 || numAdjacent == 2)) {
        nextState += 1 << i;
      }
    }
    return nextState;
  }

  final initialState = encode(lines);
  final seen = Set<int>()..add(initialState);

  var curState = initialState;
  for (int step = 1;; ++step) {
    final nextState = computeNextState(curState);
    if (!seen.add(nextState)) {
      return nextState;
    }
    curState = nextState;
  }
}

int countSetBits(int state) {
  int num = 0;
  for (int i = 0; i < size * size; ++i) {
    num += val(state, i);
  }
  return num;
}

int solvePhase2(List<String> lines, int steps) {
  /// Computes the next state, taking into account the previous and next levels.
  int computeStateInNextMinute(int prevState, int curState, int nextState) {
    int countAdjacent(int i) {
      checkArgument(i != 12);

      // Count the filled neighbors in the present level
      int num = val(curState, i + size) + val(curState, i - size);
      if (i % size > 0) num += val(curState, i - 1);
      if (i % size < size - 1) num += val(curState, i + 1);

      // Count the filled neighbors in the prev level
      if (i % size == 0) num += val(prevState, 11);
      if (i % size == size - 1) num += val(prevState, 13);
      if (i < size) num += val(prevState, 7);
      if (i >= (size - 1) * size) num += val(prevState, 17);

      // Count the filled neighbors in the next level
      if (i == 11) {
        num += val(nextState, 0) +
            val(nextState, size) +
            val(nextState, 2 * size) +
            val(nextState, 3 * size) +
            val(nextState, 4 * size);
      }
      if (i == 13) {
        num += val(nextState, size - 1) +
            val(nextState, 2 * size - 1) +
            val(nextState, 3 * size - 1) +
            val(nextState, 4 * size - 1) +
            val(nextState, 5 * size - 1);
      }
      if (i == 7) {
        num += val(nextState, 0) +
            val(nextState, 1) +
            val(nextState, 2) +
            val(nextState, 3) +
            val(nextState, 4);
      }
      if (i == 17) {
        num += val(nextState, 20) +
            val(nextState, 21) +
            val(nextState, 22) +
            val(nextState, 23) +
            val(nextState, 24);
      }

      return num;
    }

    int newState = curState;
    for (int i = 0; i < size * size; ++i) {
      // Skip the middle cell, as it doesn't have semantics. For simplicity,
      // it will always be unset.
      if (i == 12) continue;
      int numAdjacent = countAdjacent(i);
      if (val(curState, i) == 1 && numAdjacent != 1) {
        newState -= 1 << i;
      } else if (val(curState, i) == 0 &&
          (numAdjacent == 1 || numAdjacent == 2)) {
        newState += 1 << i;
      }
    }
    return newState;
  }

  final initialState = encode(lines);

  var curStates = [initialState];
  for (int step = 0; step < steps; ++step) {
    // Each iteration adds two new levels - one at the beginning and one at
    // the end.
    final nextStates = [computeStateInNextMinute(0, 0, curStates[0])];
    for (int i = 0; i < curStates.length; ++i) {
      nextStates.add(computeStateInNextMinute(i == 0 ? 0 : curStates[i - 1],
          curStates[i], i == curStates.length - 1 ? 0 : curStates[i + 1]));
    }
    nextStates.add(computeStateInNextMinute(curStates.last, 0, 0));
    curStates = nextStates;
  }

  return curStates.map((state) => countSetBits(state)).fold(0, (a, b) => a + b);
}

void main() {
  var lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines, 200)}');
}
