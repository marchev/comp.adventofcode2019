/// https://adventofcode.com/2020, Day 22
///
/// Task: https://adventofcode.com/2020/day/22
/// Date: 2020-12-30
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 32815
/// Phase 2 answer: 30695
///
/// Tags: card decks, simulation, recursion

import 'dart:collection';
import 'dart:io';

import 'package:quiver/check.dart';

List<String> readRows() =>
    File('lib/2020/problem22/input.txt').readAsLinesSync().toList();

List<int> createDeck(int player, List<String> lines) {
  int ix = lines.indexOf('Player $player:') + 1;
  final deck = <int>[];
  while (ix < lines.length && lines[ix].isNotEmpty) {
    deck.add(int.parse(lines[ix]));
    ix++;
  }
  return deck;
}

int calculateScore(List<int> deck) {
  int sum = 0;
  for (int i = 1; i <= deck.length; ++i) {
    sum += deck[deck.length - i] * i;
  }
  return sum;
}

int solvePhase1(List<String> lines) {
  final q1 = ListQueue.of(createDeck(1, lines));
  final q2 = ListQueue.of(createDeck(2, lines));

  while (q1.isNotEmpty && q2.isNotEmpty) {
    final t1 = q1.removeFirst();
    final t2 = q2.removeFirst();
    checkState(t1 != t2);
    if (t1 > t2) {
      q1..addLast(t1)..addLast(t2);
    } else {
      q2..addLast(t2)..addLast(t1);
    }
  }

  if (q1.isNotEmpty) {
    return calculateScore(q1.toList());
  } else {
    return calculateScore(q2.toList());
  }
}

class GameOutcome {
  int winner;
  int score;

  GameOutcome(this.winner, this.score);
}

/// Returns who wins this game - 1 or 2, and the score.
GameOutcome playGame(Iterable<int> deck1, Iterable<int> deck2) {
  final q1 = ListQueue.of(deck1);
  final q2 = ListQueue.of(deck2);
  final seenStates = Set<String>();

  while (q1.isNotEmpty && q2.isNotEmpty) {
    final state = q1.join(',') + ' ' + q2.join(',');
    if (!seenStates.add(state)) {
      return GameOutcome(1, null);
    }

    final t1 = q1.removeFirst();
    final t2 = q2.removeFirst();
    checkState(t1 != t2);

    int winner;
    if (t1 <= q1.length && t2 <= q2.length) {
      winner = playGame(q1.take(t1), q2.take(t2)).winner;
    } else {
      winner = t1 > t2 ? 1 : 2;
    }

    if (winner == 1) {
      q1..addLast(t1)..addLast(t2);
    } else {
      q2..addLast(t2)..addLast(t1);
    }
  }

  if (q1.isNotEmpty) {
    return GameOutcome(1, calculateScore(q1.toList()));
  } else {
    return GameOutcome(2, calculateScore(q2.toList()));
  }
}

int solvePhase2(List<String> lines) {
  final outcome = playGame(createDeck(1, lines), createDeck(2, lines));
  return outcome.score;
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
