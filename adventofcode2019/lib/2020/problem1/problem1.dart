/// https://adventofcode.com/2020, Day 1
///
/// Task: https://adventofcode.com/2020/day/1
/// Date: 2020-12-01
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 800139
/// Phase 2 answer: 59885340

import 'dart:io';

List<String> readRows() =>
    File('lib/2020/problem1/input.txt').readAsLinesSync().toList();

List<int> parseNums(List<String> lines) =>
    lines.map((s) => int.parse(s)).toList();

int solvePhase1(List<int> nums) {
  for (var i = 0; i < nums.length; ++i) {
    for (var j = i + 1; j < nums.length; ++j) {
      if (nums[i] + nums[j] == 2020) {
        return nums[i] * nums[j];
      }
    }
  }
  throw AssertionError();
}

int solvePhase2(List<int> nums) {
  for (var i = 0; i < nums.length; ++i) {
    for (var j = i + 1; j < nums.length; ++j) {
      for (var k = j + 1; k < nums.length; ++k) {
        if (nums[i] + nums[j] + nums[k] == 2020) {
          return nums[i] * nums[j] * nums[k];
        }
      }
    }
  }
  throw AssertionError();
}

void main() {
  List<int> nums = parseNums(readRows());

  print('Phase 1: Ans = ${solvePhase1(nums)}');
  print('Phase 2: Ans = ${solvePhase2(nums)}');
}
