/// https://adventofcode.com/2020, Day 21
///
/// Task: https://adventofcode.com/2020/day/21
/// Date: 2020-12-29
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 2410
/// Phase 2 answer: tmp,pdpgm,cdslv,zrvtg,ttkn,mkpmkx,vxzpfp,flnhl

import 'dart:io';

import 'package:quiver/check.dart';

List<String> readRows() =>
    File('lib/2020/problem21/input.txt').readAsLinesSync().toList();

final lineRegEx = RegExp(r'^([^(]+)\(contains ([^)]+)\)$');

int solvePhase1(List<String> lines) {
  final allergenToIngredients = <String, Set<String>>{};
  final ingredientToAppearancesCount = <String, int>{};
  for (final line in lines) {
    final match = lineRegEx.firstMatch(line);
    checkNotNull(match);
    final ingredients = match.group(1).trim().split(' ').toSet();
    final allergens = match.group(2).replaceAll(',', '').trim().split(' ');

    allergens.forEach((allergen) {
      if (allergenToIngredients.containsKey(allergen)) {
        allergenToIngredients[allergen] =
            allergenToIngredients[allergen].intersection(ingredients);
      } else {
        allergenToIngredients[allergen] = ingredients;
      }
    });

    ingredients.forEach((ingredient) {
      ingredientToAppearancesCount.putIfAbsent(ingredient, () => 0);
      ingredientToAppearancesCount[ingredient]++;
    });
  }

  final ingredientsPossiblyWithAllergens =
      allergenToIngredients.values.expand((iterable) => iterable).toSet();
  final allIngredients = ingredientToAppearancesCount.keys.toSet();
  final ingredientsCertainlyWithoutAllergens =
      allIngredients.difference(ingredientsPossiblyWithAllergens);

  return ingredientsCertainlyWithoutAllergens
      .map((i) => ingredientToAppearancesCount[i])
      .fold(0, (acc, a) => acc + a);
}

String solvePhase2(List<String> lines) {
  var allergenToIngredients = <String, Set<String>>{};
  for (final line in lines) {
    final match = lineRegEx.firstMatch(line);
    checkNotNull(match);
    final ingredients = match.group(1).trim().split(' ').toSet();
    final allergens = match.group(2).replaceAll(',', '').trim().split(' ');

    allergens.forEach((allergen) {
      if (allergenToIngredients.containsKey(allergen)) {
        allergenToIngredients[allergen] =
            allergenToIngredients[allergen].intersection(ingredients);
      } else {
        allergenToIngredients[allergen] = ingredients;
      }
    });
  }

  // Determine which ingredient contains each of the allergens one by one.
  final allergenToIngredient = <String, String>{};
  while (allergenToIngredients.isNotEmpty) {
    final withOneIngredientEntry = allergenToIngredients.entries
        .firstWhere((entry) => entry.value.length == 1);
    checkNotNull(withOneIngredientEntry);
    final determinedAllergen = withOneIngredientEntry.key;
    final determinedIngredient = withOneIngredientEntry.value.single;

    allergenToIngredient[determinedAllergen] = determinedIngredient;
    allergenToIngredients.remove(determinedAllergen);

    // Update the map, so that it doesn't contain the already determined
    // ingredient as an option for other allergens.
    allergenToIngredients = Map.fromEntries(allergenToIngredients.entries
        .where((entry) => entry.key != determinedAllergen)
        .map((entry) => MapEntry(entry.key,
            entry.value.difference(Set<String>.of([determinedIngredient])))));
  }

  final sortedEntries = allergenToIngredient.entries.toList()
    ..sort((e1, e2) => e1.key.compareTo(e2.key));
  return sortedEntries.map((entry) => entry.value).join(',');
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
