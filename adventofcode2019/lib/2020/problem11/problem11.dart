/// https://adventofcode.com/2020, Day 11
///
/// Task: https://adventofcode.com/2020/day/11
/// Date: 2020-12-13
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 2263
/// Phase 2 answer: 2002

import 'dart:io';

List<String> readRows() =>
    File('lib/2020/problem11/input.txt').readAsLinesSync().toList();

// 0 - up; 1 - right; 2 - down; 3 - left
// 4 - top-left; 5 - top-right; 6 - bottom-left; 7 - bottom-right
const dr = const <int>[-1, 0, 1, 0, -1, -1, 1, 1];
const dc = const <int>[0, 1, 0, -1, -1, 1, 1, -1];

class Grid {
  static const floorCell = '.';
  static const emptyCell = 'L';
  static const occupiedCell = '#';

  final List<List<String>> g;

  int get rows => g.length;

  int get cols => g[0].length;

  Grid(List<List<String>> rawG)
      : g = List.generate(rawG.length, (r) => List.from(rawG[r]));

  int countOccupiedNextTo(int r, int c) {
    int occupiedCount = 0;
    for (int k = 0; k < dr.length; ++k) {
      final nr = r + dr[k];
      final nc = c + dc[k];
      if (0 <= nr &&
          nr < rows &&
          0 <= nc &&
          nc < cols &&
          g[nr][nc] == occupiedCell) {
        occupiedCount++;
      }
    }
    return occupiedCount;
  }

  int countOccupiedSeenFrom(int r, int c) {
    int occupiedCount = 0;
    for (int k = 0; k < dr.length; ++k) {
      for (int distance = 1;; ++distance) {
        final nr = r + distance * dr[k];
        final nc = c + distance * dc[k];
        if (!(0 <= nr && nr < rows && 0 <= nc && nc < cols)) break;
        if (g[nr][nc] == occupiedCell) {
          occupiedCount++;
          break;
        } else if (g[nr][nc] == emptyCell) {
          break;
        }
      }
    }
    return occupiedCount;
  }

  int countOccupied() =>
      g.expand((row) => row).where((cell) => cell == occupiedCell).length;

  @override
  String toString() {
    final str = StringBuffer();
    for (int r = 0; r < rows; ++r) {
      for (int c = 0; c < cols; ++c) {
        str.write(g[r][c]);
      }
      str.writeln();
    }
    return str.toString();
  }
}

class IterationResult {
  final Grid nextGrid;
  final int changeCount;

  IterationResult(this.nextGrid, this.changeCount);
}

int solvePhase1(List<String> lines) {
  IterationResult next(Grid grid) {
    final nextGrid = Grid(grid.g);
    int changeCount = 0;
    for (int r = 0; r < grid.rows; ++r) {
      for (int c = 0; c < grid.cols; ++c) {
        if (grid.g[r][c] == Grid.emptyCell &&
            grid.countOccupiedNextTo(r, c) == 0) {
          changeCount++;
          nextGrid.g[r][c] = Grid.occupiedCell;
        } else if (grid.g[r][c] == Grid.occupiedCell &&
            grid.countOccupiedNextTo(r, c) >= 4) {
          changeCount++;
          nextGrid.g[r][c] = Grid.emptyCell;
        }
      }
    }
    return IterationResult(nextGrid, changeCount);
  }

  var grid = Grid(lines.map((line) => line.split('')).toList());
  for (int iter = 0; iter < 1000; ++iter) {
    final result = next(grid);
    if (result.changeCount == 0) {
      return result.nextGrid.countOccupied();
    }
    grid = result.nextGrid;
  }
  throw AssertionError();
}

int solvePhase2(List<String> lines) {
  IterationResult next(Grid grid) {
    final nextGrid = Grid(grid.g);
    int changeCount = 0;
    for (int r = 0; r < grid.rows; ++r) {
      for (int c = 0; c < grid.cols; ++c) {
        if (grid.g[r][c] == Grid.emptyCell &&
            grid.countOccupiedSeenFrom(r, c) == 0) {
          changeCount++;
          nextGrid.g[r][c] = Grid.occupiedCell;
        } else if (grid.g[r][c] == Grid.occupiedCell &&
            grid.countOccupiedSeenFrom(r, c) >= 5) {
          changeCount++;
          nextGrid.g[r][c] = Grid.emptyCell;
        }
      }
    }
    return IterationResult(nextGrid, changeCount);
  }

  var grid = Grid(lines.map((line) => line.split('')).toList());
  for (int iter = 0; iter < 10000; ++iter) {
    final result = next(grid);
    if (result.changeCount == 0) {
      return result.nextGrid.countOccupied();
    }
    grid = result.nextGrid;
  }
  throw AssertionError();
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
