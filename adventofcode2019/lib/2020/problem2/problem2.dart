/// https://adventofcode.com/2020, Day 2
///
/// Task: https://adventofcode.com/2020/day/2
/// Date: 2020-12-02
/// Author: Svilen Marchev
///
/// Status: works for both
///
/// Phase 1 answer: 447
/// Phase 2 answer: 249

import 'dart:io';

import 'package:quiver/check.dart';

final policyRegEx = RegExp(r'^(\d+)[-](\d+) ([a-z]): ([a-z]+)$');

List<String> readRows() =>
    File('lib/2020/problem2/input.txt').readAsLinesSync().toList();

bool isPolicy1Valid(String line) {
  final match = policyRegEx.firstMatch(line);
  checkState(match != null);

  int a = int.parse(match.group(1));
  int b = int.parse(match.group(2));
  String ch = match.group(3);
  String pass = match.group(4);

  int occurs = ch.allMatches(pass).length;
  return a <= occurs && occurs <= b;
}

int solvePhase1(List<String> lines) => lines.where(isPolicy1Valid).length;

bool isPolicy2Valid(String line) {
  final match = policyRegEx.firstMatch(line);
  checkState(match != null);

  int a = int.parse(match.group(1));
  int b = int.parse(match.group(2));
  String ch = match.group(3);
  String pass = match.group(4);

  if (b > pass.length) return false;

  String as = String.fromCharCode(pass.codeUnitAt(a - 1));
  String bs = String.fromCharCode(pass.codeUnitAt(b - 1));
  return (as == ch) ^ (bs == ch);
}

int solvePhase2(List<String> lines) => lines.where(isPolicy2Valid).length;

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
