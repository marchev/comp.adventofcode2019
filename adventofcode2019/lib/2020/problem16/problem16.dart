/// https://adventofcode.com/2020, Day 16
///
/// Task: https://adventofcode.com/2020/day/16
/// Date: 2020-12-31, 2021-01-01
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 21996
/// Phase 2 answer: 650080463519
///
/// Checking if numbers are within given ranges.
///
/// Tags: simulation, ranges

import 'dart:io';

import 'package:quiver/check.dart';

List<String> readRows() =>
    File('lib/2020/problem16/input.txt').readAsLinesSync().toList();

class Range {
  final int a;
  final int b;

  Range(this.a, this.b);

  bool contains(int x) => a <= x && x <= b;
}

class Rule {
  final String label;
  final List<Range> ranges;

  Rule(this.label, this.ranges);

  bool accepts(int num) => ranges.any((range) => range.contains(num));
}

List<Rule> createRules(List<String> lines) {
  final rules = <Rule>[];
  for (final line in lines) {
    if (line.isEmpty) break;
    final tokens = line.split(': ');
    final label = tokens[0];
    final rangeStrings = tokens[1].split(' or ');
    final ranges = rangeStrings
        .map((s) => s.split('-').map(int.parse).toList())
        .map((arr) => Range(arr[0], arr[1]))
        .toList();
    rules.add(Rule(label, ranges));
  }
  return rules;
}

List<int> createTicket(String line) => line.split(',').map(int.parse).toList();

bool isNumCoveredByAnyRule(int num, List<Rule> rules) =>
    rules.any((rule) => rule.accepts(num));

int solvePhase1(List<String> lines) {
  final rules = createRules(lines);
  int uncoveredNumsSum = 0;
  for (int i = lines.indexOf('nearby tickets:') + 1; i < lines.length; ++i) {
    final ticket = createTicket(lines[i]);
    for (final num in ticket) {
      if (!isNumCoveredByAnyRule(num, rules)) {
        uncoveredNumsSum += num;
      }
    }
  }
  return uncoveredNumsSum;
}

int solvePhase2(List<String> lines) {
  final rules = createRules(lines);

  final myTicket = createTicket(lines[lines.indexOf('your ticket:') + 1]);
  final fieldCount = myTicket.length;

  final validTickets = <List<int>>[];
  for (int i = lines.indexOf('nearby tickets:') + 1; i < lines.length; ++i) {
    final ticket = createTicket(lines[i]);
    final isValid = ticket.every((num) => isNumCoveredByAnyRule(num, rules));
    if (isValid) validTickets.add(ticket);
  }

  final fieldIdToPossibleRules = <int, Set<Rule>>{};
  for (int fieldId = 0; fieldId < fieldCount; ++fieldId) {
    final possibleRules = Set.of(rules);
    for (final ticket in validTickets) {
      final num = ticket[fieldId];
      for (final rule in rules) {
        if (!rule.accepts(num)) {
          possibleRules.remove(rule);
        }
      }
    }
    fieldIdToPossibleRules[fieldId] = possibleRules;
  }

  final fieldIdToRule = <int, Rule>{};
  while (fieldIdToRule.length < fieldCount) {
    final determinedField = fieldIdToPossibleRules.entries
        .firstWhere((entry) => entry.value.length == 1, orElse: null);
    checkNotNull(determinedField);
    final determinedFieldId = determinedField.key;
    final determinedRule = determinedField.value.single;

    fieldIdToRule[determinedFieldId] = determinedRule;
    fieldIdToPossibleRules.remove(determinedFieldId);
    fieldIdToPossibleRules.forEach(
        (fieldId, possibleRules) => possibleRules.remove(determinedRule));
  }
  // fieldIdToRule.entries
  //     .forEach((entry) => print('${entry.key}: ${entry.value.label}'));

  final departureFieldIds = fieldIdToRule.entries
      .where((fieldIdRule) => fieldIdRule.value.label.startsWith('departure'))
      .map((fieldIdRule) => fieldIdRule.key)
      .toList();
  final departureFieldValues =
      departureFieldIds.map((fieldId) => myTicket[fieldId]).toList();
  return departureFieldValues.reduce((acc, a) => acc * a);
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
