/// https://adventofcode.com/2020, Day 19
///
/// Task: https://adventofcode.com/2020/day/19
/// Date: 2020-12-31
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 178
/// Phase 2 answer: 346
///
/// Tags: dp, formal grammars, language recognition

import 'dart:io';

import 'package:quiver/check.dart';

List<String> readRows() =>
    File('lib/2020/problem19/input.txt').readAsLinesSync().toList();

class Rule {
  final int id;
  final List<List<int>> disjunctions;
  final String letter;

  Rule(this.id, this.disjunctions, this.letter);

  @override
  String toString() => '$id: $disjunctions "$letter"';
}

List<Rule> createRules(List<String> lines) {
  final rules = <Rule>[];
  for (final line in lines) {
    if (line.isEmpty) break;
    final tokens = line.split(':');
    final id = int.parse(tokens.first);
    if (tokens[1].contains('"')) {
      final letter = tokens[1].substring(2, 3);
      rules.add(Rule(id, [], letter));
    } else {
      final disjunctionStrings = tokens[1].trim().split(' | ');
      final disjunctions = disjunctionStrings
          .map((s) => s.split(' ').map(int.parse).toList())
          .toList();
      rules.add(Rule(id, disjunctions, ''));
    }
  }
  return rules;
}

/// Assumes that the rules are consecutive integers, starting from 0.
class GrammarAcceptor {
  final Map<int, Rule> idToRule;
  final List<String> s;

  List<List<List<bool>>> matchesMemo;

  GrammarAcceptor(this.idToRule, String ss) : s = ss.split('');

  bool accepts() {
    matchesMemo = List.generate(
        s.length,
        (i) => List.generate(
            s.length, (j) => List.generate(idToRule.length, (r) => null)));
    return _matches(0, s.length - 1, 0);
  }

  bool _matches(int i, int j, int ruleId) {
    if (i > j) return false;
    final rule = idToRule[ruleId];
    if (i == j && rule.letter.isNotEmpty) {
      return s[i] == rule.letter;
    }
    if (matchesMemo[i][j][ruleId] != null) return matchesMemo[i][j][ruleId];
    bool doesMatch = false;

    for (int di = 0; di < rule.disjunctions.length && !doesMatch; ++di) {
      final disjunction = rule.disjunctions[di];
      checkState(1 <= disjunction.length && disjunction.length <= 3);
      if (disjunction.length == 1) {
        doesMatch |= _matches(i, j, disjunction.first);
      } else if (disjunction.length == 2) {
        for (int k = i; k < j && !doesMatch; ++k) {
          doesMatch |= _matches(i, k, disjunction[0]) &&
              _matches(k + 1, j, disjunction[1]);
        }
      } else {
        for (int k = i; k < j - 1 && !doesMatch; ++k) {
          if (!_matches(i, k, disjunction[0])) continue;
          for (int l = k + 1; l < j && !doesMatch; ++l) {
            doesMatch |= _matches(k + 1, l, disjunction[1]) &&
                _matches(l + 1, j, disjunction[2]);
          }
        }
      }
    }

    matchesMemo[i][j][ruleId] = doesMatch;
    return doesMatch;
  }
}

int solvePhase1(List<String> lines) {
  final rules = createRules(lines);
  final idToRule =
      Map.fromEntries(rules.map((rule) => MapEntry(rule.id, rule)));

  int acceptedCount = 0;
  for (int i = lines.indexOf('') + 1; i < lines.length; ++i) {
    final acceptor = GrammarAcceptor(idToRule, lines[i]);
    if (acceptor.accepts()) {
      acceptedCount++;
    }
  }
  return acceptedCount;
}

int solvePhase2(List<String> lines) {
  final rules = createRules(lines);
  final idToRule =
      Map.fromEntries(rules.map((rule) => MapEntry(rule.id, rule)));

  idToRule[8].disjunctions.add([42, 8]);
  idToRule[11].disjunctions.add([42, 11, 31]);

  int acceptedCount = 0;
  for (int i = lines.indexOf('') + 1; i < lines.length; ++i) {
    final acceptor = GrammarAcceptor(idToRule, lines[i]);
    if (acceptor.accepts()) {
      acceptedCount++;
    }
  }
  return acceptedCount;
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
