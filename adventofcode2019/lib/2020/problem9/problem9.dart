import 'dart:collection';

/// https://adventofcode.com/2020, Day 9
///
/// Task: https://adventofcode.com/2020/day/9
/// Date: 2020-12-13
/// Author: Svilen Marchev
///
/// Status: works for both
///
/// Phase 1 answer: 36845998
/// Phase 2 answer: 4830226

import 'dart:io';

import 'package:quiver/iterables.dart';

List<String> readRows() =>
    File('lib/2020/problem9/input.txt').readAsLinesSync().toList();

Set<int> computeAllSums(Iterable<int> iterable) {
  final list = iterable.toList();
  final set = Set<int>();
  for (var i = 0; i < list.length; ++i) {
    for (var j = i + 1; j < list.length; ++j) {
      set.add(list[i] + list[j]);
    }
  }
  return set;
}

int solvePhase1(List<String> lines, {int lookBackCount = 25}) {
  final nums = lines.map(int.parse).toList();
  final queue = ListQueue<int>.from(nums.take(lookBackCount));
  for (var i = lookBackCount; i < nums.length; ++i) {
    final allSums = computeAllSums(queue);
    if (!allSums.contains(nums[i])) {
      return nums[i];
    }
    queue.removeFirst();
    queue.addLast(nums[i]);
  }
}

int solvePhase2(List<String> lines, {int lookBackCount = 25}) {
  final target = solvePhase1(lines, lookBackCount: lookBackCount);
  final nums = lines.map(int.parse).toList();
  var i = 0, j = 0, sum = 0;
  while (i < nums.length) {
    if (sum == target) {
      final range = nums.sublist(j, i);
      return min<int>(range) + max<int>(range);
    } else if (sum > target) {
      sum -= nums[j++];
    } else {
      sum += nums[i++];
    }
  }
  throw AssertionError();
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
