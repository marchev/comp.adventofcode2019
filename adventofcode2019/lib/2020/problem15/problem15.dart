/// https://adventofcode.com/2020, Day 15
///
/// Task: https://adventofcode.com/2020/day/15
/// Date: 2021-01-31
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 639
/// Phase 2 answer: 266
///
/// Generating a sequence and keeping references to old elements. Runs in
/// O(N) time and memory, where N is the number of elements from the sequence
/// that we generate. Runs in ~4s for phase 2.
///
/// Tags: simulation, sequence, sweeping

int computeTurn(List<int> startingNums, int endTurn) {
  final lastPosOf = <int, int>{};
  for (int i = 1; i < startingNums.length; ++i) {
    lastPosOf[startingNums[i - 1]] = i;
  }
  int last = startingNums.last;
  for (int i = startingNums.length + 1; i <= endTurn; ++i) {
    final cur = lastPosOf.containsKey(last) ? i - 1 - lastPosOf[last] : 0;
    lastPosOf[last] = i - 1;
    last = cur;
  }
  return last;
}

int solvePhase1(List<int> startingNums) => computeTurn(startingNums, 2020);

int solvePhase2(List<int> startingNums) => computeTurn(startingNums, 30000000);

void main() {
  List<int> startingNums = <int>[11, 18, 0, 20, 1, 7, 16];

  print('Phase 1: Ans = ${solvePhase1(startingNums)}');
  print('Phase 2: Ans = ${solvePhase2(startingNums)}');
}
