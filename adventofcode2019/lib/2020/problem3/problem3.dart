/// https://adventofcode.com/2020, Day 3
///
/// Task: https://adventofcode.com/2020/day/3
/// Date: 2020-12-04
/// Author: Svilen Marchev
///
/// Status: works for both
///
/// Phase 1 answer: 159
/// Phase 2 answer: 6419669520

import 'dart:io';

List<String> readRows() =>
    File('lib/2020/problem3/input.txt').readAsLinesSync().toList();

/// Finite number of rows, but columns repeat infinitely.
class Grid {
  final List<String> lines;

  Grid(this.lines);

  int get rows => lines.length;

  String cell(int r, int c) => lines[r][c % lines[0].length];
}

int solvePhase1(List<String> lines) {
  final grid = Grid(lines);
  return List.generate(grid.rows, (r) => grid.cell(r, r * 3))
      .where((cell) => cell == '#')
      .length;
}

int solvePhase2(List<String> lines) {
  final grid = Grid(lines);
  final dr = [1, 1, 1, 1, 2];
  final dc = [1, 3, 5, 7, 1];
  var multiple = 1;
  for (var i = 0; i < dr.length; ++i) {
    var trees = 0;
    for (var r = 0, c = 0; r < grid.rows; r += dr[i], c += dc[i]) {
      trees += grid.cell(r, c) == '#' ? 1 : 0;
    }
    multiple *= trees;
  }
  return multiple;
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
