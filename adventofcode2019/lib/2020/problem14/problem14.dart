/// https://adventofcode.com/2020, Day 14
///
/// Task: https://adventofcode.com/2020/day/14
/// Date: 2021-01-31
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 4886706177792
/// Phase 2 answer: 3348493585827
///
/// Simulates writing to memory, applying a bitmask.
///
/// Tags: simulation

import 'dart:io';

List<String> readRows() =>
    File('lib/2020/problem14/input.txt').readAsLinesSync().toList();

final maskRegEx = RegExp(r'mask = ([X01]{36})');
final memRegEx = RegExp(r'mem\[(\d+)\] = (\d+)');

int solvePhase1(List<String> lines) {
  var mask = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';
  final mem = <int, int>{};
  for (final line in lines) {
    final maskMatch = maskRegEx.firstMatch(line);
    if (maskMatch != null) {
      mask = maskMatch.group(1);
    } else {
      final memMatch = memRegEx.firstMatch(line);
      final address = int.parse(memMatch.group(1));
      final val = int.parse(memMatch.group(2));
      mem[address] = applyMask(mask, val);
    }
  }
  return mem.values.fold(0, (acc, a) => acc + a);
}

int applyMask(String mask, int val) {
  final chars = mask.codeUnits.reversed.toList();
  for (int i = 0; i < chars.length; ++i) {
    if (chars[i] == '1'.codeUnits.first) {
      val |= 1 << i;
    } else if (chars[i] == '0'.codeUnits.first) {
      if (((val >> i) & 1) == 1) {
        val -= 1 << i;
      }
    }
  }
  return val;
}

int solvePhase2(List<String> lines) {
  var mask = '000000000000000000000000000000000000';
  final mem = <int, int>{};
  for (final line in lines) {
    final maskMatch = maskRegEx.firstMatch(line);
    if (maskMatch != null) {
      mask = maskMatch.group(1);
    } else {
      final memMatch = memRegEx.firstMatch(line);
      final address = int.parse(memMatch.group(1));
      final val = int.parse(memMatch.group(2));
      assignToMemoryForPart2(address, mask, mem, val);
    }
  }
  return mem.values.fold(0, (acc, a) => acc + a);
}

void assignToMemoryForPart2(
    int originalAddress, String maskString, Map<int, int> mem, int value) {
  final maskChars = maskString.codeUnits.reversed.toList();

  void recurse(int address, int i) {
    if (i == maskChars.length) {
      mem[address] = value;
      return;
    }
    if (maskChars[i] == 'X'.codeUnits.first) {
      recurse(address, i + 1);
      recurse(address + (1 << i), i + 1);
    } else if (maskChars[i] == '1'.codeUnits.first) {
      recurse(address + (1 << i), i + 1);
    } else {
      final iThBit = (originalAddress >> i) & 1;
      recurse(address + (iThBit << i), i + 1);
    }
  }

  recurse(0, 0);
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
