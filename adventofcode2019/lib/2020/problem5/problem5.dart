/// https://adventofcode.com/2020, Day 5
///
/// Task: https://adventofcode.com/2020/day/5
/// Date: 2020-12-05
/// Author: Svilen Marchev
///
/// Status: works for both
///
/// Phase 1 answer: 842
/// Phase 2 answer: 617

import 'dart:io';

import 'package:quiver/check.dart';
import 'package:quiver/iterables.dart';

List<String> readRows() =>
    File('lib/2020/problem5/input.txt').readAsLinesSync().toList();

int findNum(String commands) {
  var l = 0;
  var r = (1 << commands.length) - 1;
  for (final c in commands.split('')) {
    if (c == 'L' || c == 'F') {
      r -= (r - l + 1) ~/ 2;
    } else {
      l += (r - l + 1) ~/ 2;
    }
  }
  checkState(l == r);
  return l;
}

int solvePhase1(List<String> lines) {
  return max(lines.map((line) {
    final row = findNum(line.substring(0, 7));
    final col = findNum(line.substring(7));
    return row * 8 + col;
  }));
}

int solvePhase2(List<String> lines) {
  final seats = Set.from(lines.map((line) {
    final row = findNum(line.substring(0, 7));
    final col = findNum(line.substring(7));
    return row * 8 + col;
  }));
  for (var id = 8; id < 127 * 8; ++id) {
    if (!seats.contains(id) && seats.containsAll([id - 1, id + 1])) {
      return id;
    }
  }
  throw AssertionError();
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
