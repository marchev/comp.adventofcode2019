/// https://adventofcode.com/2020, Day 10
///
/// Task: https://adventofcode.com/2020/day/10
/// Date: 2020-12-10
/// Author: Svilen Marchev
///
/// Status: works for both
///
/// Phase 1 answer: 1885
/// Phase 2 answer: 2024782584832

import 'dart:io';

import 'package:quiver/check.dart';

List<String> readRows() =>
    File('lib/2020/problem10/input.txt').readAsLinesSync().toList();

int solvePhase1(List<String> lines) {
  final nums = lines.map(int.parse).toList();
  nums.sort();
  final diffs = <int>[0, 0, 0, 0];
  for (var i = 0; i < nums.length - 1; ++i) {
    diffs[nums[i + 1] - nums[i]]++;
  }
  diffs[nums[0]]++;
  diffs[3]++;
  return diffs[1] * diffs[3];
}

int solvePhase2(List<String> lines) {
  var nums = lines.map(int.parse).toList();
  nums.sort();
  nums = [0] + nums + [nums.last + 3];

  final f = List.filled(nums.length, 0);
  f[0] = 1;
  for (var i = 1; i < nums.length; ++i) {
    for (var j = i - 1; j >= 0 && nums[i] - nums[j] <= 3; --j) {
      f[i] += f[j];
    }
  }
  return f[nums.length - 1];
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
