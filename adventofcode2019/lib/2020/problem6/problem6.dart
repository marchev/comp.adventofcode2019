/// https://adventofcode.com/2020, Day 6
///
/// Task: https://adventofcode.com/2020/day/6
/// Date: 2020-12-06
/// Author: Svilen Marchev
///
/// Status: works for both
///
/// Phase 1 answer: 7283
/// Phase 2 answer: 3520

import 'dart:io';

import 'package:quiver/check.dart';

List<String> readRows() =>
    File('lib/2020/problem6/input.txt').readAsLinesSync().toList();

class Group {
  final Map<String, int> _questionToNumAnswers = {};
  int _numPeople = 0;

  void addPerson(List<String> questions) {
    _numPeople++;
    for (final question in Set.of(questions)) {
      _questionToNumAnswers.putIfAbsent(question, () => 0);
      _questionToNumAnswers[question]++;
    }
  }

  int get numPeople => _numPeople;

  int get numAnsweredByAnyone => _questionToNumAnswers.length;

  int get numAnsweredByEveryone => _questionToNumAnswers.entries
      .where((entry) => entry.value == _numPeople)
      .length;
}

List<Group> convertToGroups(List<String> lines) {
  List<Group> allGroups = [];
  var currentGroup = Group();
  for (final line in lines) {
    if (line == '') {
      if (currentGroup.numPeople > 0) {
        allGroups.add(currentGroup);
        currentGroup = Group();
      }
      continue;
    }
    currentGroup.addPerson(line.split(''));
  }
  if (currentGroup.numPeople > 0) {
    allGroups.add(currentGroup);
  }
  return allGroups;
}

int solvePhase1(List<String> lines) => convertToGroups(lines)
    .fold(0, (acc, group) => acc + group.numAnsweredByAnyone);

int solvePhase2(List<String> lines) => convertToGroups(lines)
    .fold(0, (acc, group) => acc + group.numAnsweredByEveryone);

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
