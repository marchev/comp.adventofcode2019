/// https://adventofcode.com/2020, Day 17
///
/// Task: https://adventofcode.com/2020/day/17
/// Date: 2020-12-31
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 426
/// Phase 2 answer: 1892
///
/// Tags: game of life, 3d grid, 4d grid

import 'dart:io';

List<String> readRows() =>
    File('lib/2020/problem17/input.txt').readAsLinesSync().toList();

/// 4-dimensional point.
class Point {
  final int x, y, z, w;

  const Point(this.x, this.y, this.z, this.w);

  @override
  bool operator ==(Object other) =>
      other is Point &&
      x == other.x &&
      y == other.y &&
      z == other.z &&
      w == other.w;

  @override
  int get hashCode => ((x * 1000 + y) * 1000 + z) * 1000 + w;

  @override
  String toString() => '($x,$y,$z,$w)';
}

/// Works in 3D and 4D.
int countActiveNeighbors(Point p, Set<Point> activePoints,
    {bool in4D = false}) {
  int activeCount = 0;
  for (int offX = -1; offX <= 1; ++offX) {
    for (int offY = -1; offY <= 1; ++offY) {
      for (int offZ = -1; offZ <= 1; ++offZ) {
        for (int offW = in4D ? -1 : 0; offW <= (in4D ? 1 : 0); ++offW) {
          if (offX == 0 && offY == 0 && offZ == 0 && offW == 0) continue;
          final q = Point(p.x + offX, p.y + offY, p.z + offZ, p.w + offW);
          if (activePoints.contains(q)) {
            activeCount++;
          }
        }
      }
    }
  }
  return activeCount;
}

Set<Point> executeCycle(Set<Point> activePoints, {bool in4D = false}) {
  final newActivePoints = Set<Point>();
  for (final p in activePoints) {
    final activeCount = countActiveNeighbors(p, activePoints, in4D: in4D);
    if (activeCount == 2 || activeCount == 3) {
      newActivePoints.add(p);
    }

    // Check if any of its inactive neighbors should be activated.
    for (int offX = -1; offX <= 1; ++offX) {
      for (int offY = -1; offY <= 1; ++offY) {
        for (int offZ = -1; offZ <= 1; ++offZ) {
          for (int offW = in4D ? -1 : 0; offW <= (in4D ? 1 : 0); ++offW) {
            if (offX == 0 && offY == 0 && offZ == 0 && offW == 0) continue;
            final q = Point(p.x + offX, p.y + offY, p.z + offZ, p.w + offW);
            if (activePoints.contains(q)) continue;
            if (countActiveNeighbors(q, activePoints, in4D: in4D) == 3) {
              newActivePoints.add(q);
            }
          }
        }
      }
    }
  }
  return newActivePoints;
}

Set<Point> findInitialActivePoints(List<String> lines) {
  final activePoints = Set<Point>();
  for (int y = 0; y < lines.length; ++y) {
    for (int x = 0; x < lines[y].length; ++x) {
      if (lines[y].substring(x, x + 1) == '#') {
        activePoints.add(Point(x, y, 0, 0));
      }
    }
  }
  return activePoints;
}

int solvePhase1(List<String> lines) {
  var activePoints = findInitialActivePoints(lines);
  for (int cycle = 0; cycle < 6; ++cycle) {
    activePoints = executeCycle(activePoints, in4D: false);
  }
  return activePoints.length;
}

int solvePhase2(List<String> lines) {
  var activePoints = findInitialActivePoints(lines);
  for (int cycle = 0; cycle < 6; ++cycle) {
    activePoints = executeCycle(activePoints, in4D: true);
  }
  return activePoints.length;
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
