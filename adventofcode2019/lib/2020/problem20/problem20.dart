/// https://adventofcode.com/2020, Day 20
///
/// Task: https://adventofcode.com/2020/day/20
/// Date: 2020-12-23, 2020-12-28
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 13224049461431
/// Phase 2 answer: 2231

import 'dart:io';
import 'dart:math';

import 'package:quiver/check.dart';
import 'package:quiver/collection.dart';

List<String> readRows() =>
    File('lib/2020/problem20/input.txt').readAsLinesSync().toList();

final tileHeaderRegEx = RegExp(r'Tile (\d+):');

List<List<String>> copy(List<List<String>> arr) =>
    List.generate(arr.length, (r) => List<String>.from(arr[r]));

class Tile {
  final List<List<String>> g;
  final int id;

  int get n => g.length;

  Tile(List<List<String>> rawG, this.id) : g = copy(rawG);

  @override
  String toString() {
    final str = StringBuffer();
    for (int r = 0; r < n; ++r) {
      for (int c = 0; c < n; ++c) {
        str.write(g[r][c]);
      }
      str.writeln();
    }
    return str.toString();
  }

  @override
  int get hashCode => id;
}

List<Tile> createTiles(List<String> lines) {
  final tiles = <Tile>[];
  for (int li = 0; li < lines.length; li += 12) {
    final match = tileHeaderRegEx.firstMatch(lines[li]);
    checkNotNull(match);
    final id = int.parse(match.group(1));
    final rows = lines.sublist(li + 1, li + 11);
    tiles.add(Tile(rows.map((row) => row.split('')).toList(), id));
  }
  return tiles;
}

String reverse(String s) => s.split('').reversed.join();

int solvePhase1(List<String> lines) {
  final tiles = createTiles(lines);

  final edgeToTiles = ListMultimap<String, Tile>();
  for (final tile in tiles) {
    final topEdge = tile.g.first.join();
    final bottomEdge = tile.g.last.join();
    final leftEdge = tile.g.map((row) => row.first).join();
    final rightEdge = tile.g.map((row) => row.last).join();

    final edges = <String>[
      topEdge,
      reverse(topEdge),
      bottomEdge,
      reverse(bottomEdge),
      leftEdge,
      reverse(leftEdge),
      rightEdge,
      reverse(rightEdge)
    ];
    edges.forEach((edge) => edgeToTiles.add(edge, tile));
  }

  // Some analysis
  edgeToTiles.forEachKey((edge, tiles) {
    if (tiles.length > 2) print('! edge $edge: ${tiles.length}');
  });

  final tileToNeighbors = SetMultimap<Tile, Tile>();
  edgeToTiles.forEachKey((edge, tiles) {
    checkState(tiles.length <= 2);
    if (tiles.length == 2) {
      tileToNeighbors
        ..add(tiles.first, tiles.last)
        ..add(tiles.last, tiles.first);
    }
  });

  // Analysis
  // tileToNeighbors.forEachKey((tile, neighbors) {
  //   print('> tile ${tile.id}: ${neighbors.map((t) => t.id).toList()}');
  // });

  // Corners have exactly 2 neighbors
  final cornerTiles = tileToNeighbors
      .asMap()
      .entries
      .where((entry) => entry.value.length <= 2)
      .map((entry) => entry.key)
      .toList();
  checkState(cornerTiles.length == 4);
  // print('corners: ${cornerTiles.map((c) => c.id)}');

  final product =
      cornerTiles.map((tile) => tile.id).reduce((value, id) => value * id);
  return product;
}

/// Represents the order in which tiles should appear.
class TileMatrix {
  final tileMatrix = <List<Tile>>[];
  final tiles = Set<Tile>();

  /// For each matrix cell, hold the expanded grid in the rotation it should be.
  final expandedMatrix = <List<List<List<String>>>>[];

  /// The size of the tile matrix.
  ///
  /// Assumes the matrix is a square.
  int get size => tileMatrix.length;

  void set(int r, int c, Tile tile) {
    while (tileMatrix.length <= r) tileMatrix.add([]);
    while (tileMatrix[r].length <= c) tileMatrix[r].add(null);
    tileMatrix[r][c] = tile;
    tiles.add(tile);
  }

  Tile get(int r, int c) => tileMatrix[r][c];

  bool isAdded(Tile tile) => tiles.contains(tile);

  void setExpanded(int r, int c, List<List<String>> grid) {
    while (expandedMatrix.length <= r) expandedMatrix.add([]);
    while (expandedMatrix[r].length <= c) expandedMatrix[r].add(null);
    expandedMatrix[r][c] = copy(grid);
  }

  List<List<String>> expand() {
    final g = List.generate(size * 8, (r) => List<String>.filled(size * 8, ''));
    for (int r = 0; r < size; ++r) {
      for (int c = 0; c < size; ++c) {
        final expanded = expandedMatrix[r][c];
        for (int i = 0; i < 8; ++i) {
          for (int j = 0; j < 8; ++j) {
            g[r * 8 + i][c * 8 + j] = expanded[i + 1][j + 1];
          }
        }
      }
    }
    return g;
  }

  @override
  String toString() {
    final str = StringBuffer();
    for (int r = 0; r < tileMatrix.length; ++r) {
      str.writeln(tileMatrix[r].map((tile) => tile.id));
    }
    return str.toString();
  }
}

class TileEdge {
  final Tile tile;
  final String edge;

  TileEdge(this.tile, this.edge);
}

List<List<String>> flipHorizontally(List<List<String>> g) {
  return List.generate(g.length, (r) => List.from(g[r].reversed));
}

List<List<String>> flipVertically(List<List<String>> g) {
  return List.generate(g.length, (r) => List.from(g[g.length - 1 - r]));
}

List<List<String>> transpose(List<List<String>> g0) {
  final g = List.generate(g0.length, (r) => List.filled(g0[0].length, ''));
  for (int r = 0; r < g.length; ++r) {
    for (int c = 0; c < g[0].length; ++c) {
      g[r][c] = g0[c][r];
    }
  }
  return g;
}

List<List<String>> rotate(List<List<String>> g) {
  return flipVertically(transpose(g));
}

int countCoveredBySeaMonsters(List<List<String>> g) {
  const monster = <String>[
    '                  # ',
    '#    ##    ##    ###',
    ' #  #  #  #  #  #   ',
  ];
  final coveredPositions = Set<int>();
  for (int r = 0; r < g.length - monster.length; ++r) {
    nextColumn:
    for (int c = 0; c < g[0].length - monster[0].length; ++c) {
      for (int i = 0; i < monster.length; ++i) {
        for (int j = 0; j < monster[0].length; ++j) {
          if (monster[i][j] == '#' && g[r + i][c + j] != '#') {
            continue nextColumn;
          }
        }
      }
      // A new monster found
      for (int i = 0; i < monster.length; ++i) {
        for (int j = 0; j < monster[0].length; ++j) {
          if (monster[i][j] == '#') {
            final encoded = (r + i) * g[0].length + (c + j);
            coveredPositions.add(encoded);
          }
        }
      }
    }
  }
  return coveredPositions.length;
}

int solvePhase2(List<String> lines) {
  final tiles = createTiles(lines);

  final edgeToTiles = ListMultimap<String, Tile>();
  for (final tile in tiles) {
    final topEdge = tile.g.first.join();
    final bottomEdge = tile.g.last.join();
    final leftEdge = tile.g.map((row) => row.first).join();
    final rightEdge = tile.g.map((row) => row.last).join();

    final edges = <String>[
      topEdge,
      reverse(topEdge),
      bottomEdge,
      reverse(bottomEdge),
      leftEdge,
      reverse(leftEdge),
      rightEdge,
      reverse(rightEdge)
    ];
    edges.forEach((edge) => edgeToTiles.add(edge, tile));
  }

  final tileToNeighbors = SetMultimap<Tile, Tile>();
  final tileToNeighborEdges = SetMultimap<Tile, TileEdge>();
  edgeToTiles.forEachKey((edge, tiles) {
    checkState(tiles.length <= 2);
    if (tiles.length == 2) {
      tileToNeighbors
        ..add(tiles.first, tiles.last)
        ..add(tiles.last, tiles.first);
      tileToNeighborEdges
        ..add(tiles.first, TileEdge(tiles.last, edge))
        ..add(tiles.last, TileEdge(tiles.first, edge));
    }
  });

  // Corners have exactly 2 neighbors
  final cornerTiles = tileToNeighbors
      .asMap()
      .entries
      .where((entry) => entry.value.length <= 2)
      .map((entry) => entry.key)
      .toList();
  checkState(cornerTiles.length == 4);
  // print('corners: ${cornerTiles.map((c) => c.id)}');

  final topLeftCornerTile = cornerTiles.first;
  final matrix = TileMatrix();
  matrix.set(0, 0, topLeftCornerTile);
  matrix.set(0, 1, tileToNeighbors[topLeftCornerTile].first);
  matrix.set(1, 0, tileToNeighbors[topLeftCornerTile].last);

  // First part: Place the top left corner and its neighbors (you can designate
  // one random corner as the top left one), and then determine the rest
  // of matrix, sequentially adding one reverse diagonal at a time. Stop when
  // all diagonals are determined until the main reverse diagonal.
  //
  // Note: The implementation assumes that the matrix is a square and its size
  // is at least 2.
  int r = 2;
  for (; !cornerTiles.contains(matrix.get(r - 1, 0)); ++r) {
    final candidates = tileToNeighbors[matrix.get(r - 1, 0)]
        .where((tile) => !matrix.isAdded(tile))
        .toList();
    final candidatesUp = tileToNeighbors[matrix.get(r - 2, 1)]
        .where((tile) => !matrix.isAdded(tile))
        .toList();
    checkState(candidates.length == 2);
    checkState(candidatesUp.length == 2);

    matrix.set(
        r, 0, candidates.where((tile) => !candidatesUp.contains(tile)).single);
    matrix.set(r - 1, 1,
        candidates.where((tile) => candidatesUp.contains(tile)).single);

    for (int i = r - 2; i >= 0; --i) {
      final tile = tileToNeighbors[matrix.get(i, r - i - 1)]
          .where((tile) => !matrix.isAdded(tile))
          .single;
      matrix.set(i, r - i, tile);
    }
  }
  // print(matrix);

  // Second part: Determine all diagonals below the main reverse one.
  for (int c = 1; c < r; ++c) {
    for (int rr = r - 1, cc = c; cc < r; --rr, ++cc) {
      final tile = tileToNeighbors[matrix.get(rr, cc - 1)]
          .where((tile) => !matrix.isAdded(tile))
          .single;
      matrix.set(rr, cc, tile);
    }
  }
  // print(matrix);

  // Determine the orientation of each tile inside the matrix.
  //
  // We know which edges connect each pair of tiles, so we sequentially
  // go through each matrix tile and rotate and flip it in all possible ways
  // and pick an orientation that satisfies all the constraints.
  for (int r = 0; r < matrix.size; ++r) {
    nextTile:
    for (int c = 0; c < matrix.size; ++c) {
      final tile = matrix.get(r, c);
      final neighborEdges = tileToNeighborEdges.asMap()[tile];

      var grid = matrix.get(r, c).g;
      for (int flip = 0; flip < 3; ++flip) {
        if (flip == 1) {
          grid = flipHorizontally(grid);
        } else if (flip == 2) {
          grid = flipVertically(grid);
        }
        for (int rotation = 0; rotation < 4; ++rotation) {
          // Try out this orientation.

          final topEdge = grid.first.join();
          final bottomEdge = grid.last.join();
          final leftEdge = grid.map((row) => row.first).join();
          final rightEdge = grid.map((row) => row.last).join();

          // Whether this particular orientation is aligned with all the
          // neighboring tiles.
          var isAligned = true;

          // Check alignment with neighbor on the top
          if (r > 0) {
            final neighbor = matrix.get(r - 1, c);
            final commonEdges = neighborEdges
                .where((e) => e.tile == neighbor)
                .map((e) => e.edge)
                .toList();
            if (!commonEdges.contains(topEdge)) isAligned = false;
          }
          // Check alignment with neighbor on the bottom
          if (r < matrix.size - 1) {
            final neighbor = matrix.get(r + 1, c);
            final commonEdges = neighborEdges
                .where((e) => e.tile == neighbor)
                .map((e) => e.edge)
                .toList();
            if (!commonEdges.contains(bottomEdge)) isAligned = false;
          }
          // Check alignment with neighbor on the left
          if (c > 0) {
            final neighbor = matrix.get(r, c - 1);
            final commonEdges = neighborEdges
                .where((e) => e.tile == neighbor)
                .map((e) => e.edge)
                .toList();
            if (!commonEdges.contains(leftEdge)) isAligned = false;
          }
          // Check alignment with neighbor on the right
          if (c < matrix.size - 1) {
            final neighbor = matrix.get(r, c + 1);
            final commonEdges = neighborEdges
                .where((e) => e.tile == neighbor)
                .map((e) => e.edge)
                .toList();
            if (!commonEdges.contains(rightEdge)) isAligned = false;
          }

          if (isAligned) {
            matrix.setExpanded(r, c, grid);
            continue nextTile;
          }

          grid = rotate(grid);
        }
        if (flip == 1) {
          grid = flipHorizontally(grid);
        } else if (flip == 2) {
          grid = flipVertically(grid);
        }
      }
    }
  }

  // Generate the expanded grid and start flipping and rotating it. For each
  // rotation, check for monsters in it. When you find a rotation with monsters
  // in it, compute and return the answer.
  var grid = matrix.expand();
  // grid.map((row) => row.join()).forEach((line) => print(line));
  for (int flip = 0; flip < 3; ++flip) {
    if (flip == 1) {
      grid = flipHorizontally(grid);
    } else if (flip == 2) {
      grid = flipVertically(grid);
    }
    for (int rotation = 0; rotation < 4; ++rotation) {
      // Try out this rotation.

      final coveredCount = countCoveredBySeaMonsters(grid);
      if (coveredCount > 0) {
        final total =
            grid.expand((row) => row).where((cell) => cell == '#').length;
        return total - coveredCount;
      }

      grid = rotate(grid);
    }
    if (flip == 1) {
      grid = flipHorizontally(grid);
    } else if (flip == 2) {
      grid = flipVertically(grid);
    }
  }
  throw AssertionError();
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
