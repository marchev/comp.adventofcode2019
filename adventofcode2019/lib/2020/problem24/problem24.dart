/// https://adventofcode.com/2020, Day 24
///
/// Task: https://adventofcode.com/2020/day/24
/// Date: 2020-12-30
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 549
/// Phase 2 answer: 4147
///
/// Tags: hexagonal grid, game of life

import 'dart:io';

List<String> readRows() =>
    File('lib/2020/problem24/input.txt').readAsLinesSync().toList();

Set<String> findBlackCells(List<String> lines) {
  final blackCells = Set<String>();
  for (final line in lines) {
    int r = 0;
    int d = 0;
    for (int i = 0; i < line.length; ++i) {
      if (line[i] == 'e') {
        d++;
      } else if (line[i] == 'w') {
        d--;
      } else if (line[i] == 'n') {
        r--;
        i++;
        d += line[i] == 'e' ? 1 : 0;
      } else if (line[i] == 's') {
        r++;
        i++;
        d += line[i] == 'e' ? 0 : -1;
      } else {
        throw AssertionError();
      }
    }

    final key = '${r}_$d';
    if (blackCells.contains(key)) {
      blackCells.remove(key);
    } else {
      blackCells.add(key);
    }
  }
  return blackCells;
}

int solvePhase1(List<String> lines) => findBlackCells(lines).length;

// 0 - up; 1 - right; 2 - down; 3 - left; 4 - top-right; 5 - bottom-left
const dr = const <int>[-1, 0, 1, 0, -1, 1];
const dc = const <int>[0, 1, 0, -1, 1, -1];

int countBlackNeighbors(Set<String> blackCells, int r, int c) {
  int count = 0;
  for (int i = 0; i < dr.length; ++i) {
    int nr = r + dr[i];
    int nc = c + dc[i];
    final key = '${nr}_$nc';
    if (blackCells.contains(key)) count++;
  }
  return count;
}

Set<String> findBlackCellsAfterADay(Set<String> blackCells) {
  final newBlackCells = Set<String>();
  for (final blackCell in blackCells) {
    final rc = blackCell.split('_').map(int.parse).toList();
    final r = rc[0];
    final c = rc[1];

    // Check if the black cell should be flipped.
    final blackNeighborsCount = countBlackNeighbors(blackCells, r, c);
    if (blackNeighborsCount != 0 && blackNeighborsCount <= 2) {
      newBlackCells.add(blackCell);
    }

    // Check if any of the white neighbors of the black cell should be flipped.
    for (int i = 0; i < dr.length; ++i) {
      int nr = r + dr[i];
      int nc = c + dc[i];
      final key = '${nr}_$nc';
      // If the neighbor is white, check if it should be flipped.
      if (!blackCells.contains(key)) {
        final blackNeighborsCount = countBlackNeighbors(blackCells, nr, nc);
        if (blackNeighborsCount == 2) {
          newBlackCells.add(key);
        }
      }
    }
  }
  return newBlackCells;
}

int solvePhase2(List<String> lines) {
  var blackCells = findBlackCells(lines);
  for (int i = 0; i < 100; ++i) {
    blackCells = findBlackCellsAfterADay(blackCells);
  }
  return blackCells.length;
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
