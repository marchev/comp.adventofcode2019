/// https://adventofcode.com/2020, Day 8
///
/// Task: https://adventofcode.com/2020/day/8
/// Date: 2021-01-31
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 1394
/// Phase 2 answer: 1626
///
/// Execute a series of instructions and find a cycle.
///
/// Tags: simulation

import 'dart:io';

List<String> readRows() =>
    File('lib/2020/problem8/input.txt').readAsLinesSync().toList();

class Outcome {
  final bool finished;
  final int acc;

  Outcome(this.finished, this.acc);
}

Outcome compute(List<String> lines, int flippedIndex) {
  final executed = Set<int>();
  int cur = 0;
  int acc = 0;
  while (cur < lines.length) {
    if (!executed.add(cur)) {
      return Outcome(false, acc);
    }

    final tokens = lines[cur].split(' ');
    final command = tokens.first;
    final value = int.parse(tokens.last);

    if (command == 'acc') {
      acc += value;
      cur++;
    } else if ((command == 'nop' && flippedIndex != cur) ||
        (command == 'jmp' && flippedIndex == cur)) {
      cur++;
    } else {
      cur += value;
    }
  }
  return Outcome(true, acc);
}

int solvePhase1(List<String> lines) => compute(lines, -1).acc;

int solvePhase2(List<String> lines) {
  for (int i = 0; i < lines.length; ++i) {
    if (!lines[i].startsWith('acc')) {
      final outcome = compute(lines, i);
      if (outcome.finished) {
        return outcome.acc;
      }
    }
  }
  throw AssertionError();
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
