/// https://adventofcode.com/2020, Day 13
///
/// Task: https://adventofcode.com/2020/day/13
/// Date: 2021-01-31
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 2382
/// Phase 2 answer: 906332393333683
///
/// Generates a bunch of congruences, that are then solved using the Chinese
/// remainder theorem. Those could also be solved externally, using a solver
/// like https://www.dcode.fr/chinese-remainder
///
/// Tags: math, Chinese remainder theorem

import 'dart:io';

import 'package:quiver/check.dart';
import 'package:quiver/iterables.dart';

List<String> readRows() =>
    File('lib/2020/problem13/input.txt').readAsLinesSync().toList();

int solvePhase1(List<String> lines) {
  final desiredDeparture = int.parse(lines.first);
  final ids =
      lines.last.split(',').where((id) => id != 'x').map(int.parse).toList();

  final nextDepartures =
      ids.map((id) => (((desiredDeparture - 1) ~/ id) + 1) * id).toList();
  final earliestDeparture = min(nextDepartures);
  final nextBusId = ids[nextDepartures.indexOf(earliestDeparture)];
  return (earliestDeparture - desiredDeparture) * nextBusId;
}

int solvePhase2(List<String> lines) {
  final ids =
      lines.last.split(',').map((id) => id == 'x' ? 1 : int.parse(id)).toList();
  print('Congruences to solve using the Chinese remainder theorem:');
  final rems = <int>[];
  final mods = <int>[];
  for (int i = 0; i < ids.length; ++i) {
    if (ids[i] != 1) {
      final mod = ids[i];
      final rem = (mod - i % mod) % mod;
      checkState(0 <= rem && rem < mod);
      print('x === $rem (mod $mod)');
      rems.add(rem);
      mods.add(mod);
    }
  }
  return solveCongruencesUsingChineseRemainderTheorem(rems, mods);
}

/// Uses the method, described in "Existence (constructive proof)" in
/// https://en.wikipedia.org/wiki/Chinese_remainder_theorem#Proof
int solveCongruencesUsingChineseRemainderTheorem(
    List<int> rems, List<int> mods) {
  checkState(rems.length >= 2);

  BigInt x = BigInt.from(rems.first);
  BigInt mod = BigInt.from(mods.first);
  for (int i = 1; i < rems.length; ++i) {
    final bezoutCoefs = computeBezoutCoefficients(mod, BigInt.from(mods[i]));
    x = x * BigInt.from(mods[i]) * bezoutCoefs.last +
        BigInt.from(rems[i]) * mod * bezoutCoefs.first;
    mod *= BigInt.from(mods[i]);

    x = (x % mod + mod) % mod;
    checkState(BigInt.zero <= x && x < mod);
  }
  return x.toInt();
}

/// Uses https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
List<BigInt> computeBezoutCoefficients(BigInt a, BigInt b) {
  BigInt oldR = a, r = b;
  BigInt oldS = BigInt.one, s = BigInt.zero;
  BigInt oldT = BigInt.zero, t = BigInt.one;

  while (r != BigInt.zero) {
    final quotient = oldR ~/ r;

    final savedOldR = oldR;
    oldR = r;
    r = savedOldR - quotient * r;

    final savedOldS = oldS;
    oldS = s;
    s = savedOldS - quotient * s;

    final savedOldT = oldT;
    oldT = t;
    t = savedOldT - quotient * t;
  }

  // "Bézout coefficients:", (old_s, old_t)
  // "greatest common divisor:", old_r
  // "quotients by the gcd:", (t, s)
  return [oldS, oldT];
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
