/// https://adventofcode.com/2020, Day 25
///
/// Task: https://adventofcode.com/2020/day/25
/// Date: 2021-03-14
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 7936032
/// Phase 2 answer: nothing to solve
///
/// Simple math.
///
/// Tags: math, exponents, handshake

const mod = 20201227;

int log7(int n, int mod) {
  int cur = 1;
  for (int i = 0; i < mod; ++i) {
    if (cur == n) return i;
    cur = (cur * 7) % mod;
  }
  throw AssertionError();
}

int solvePhase1(int publicKey1, int publicKey2) {
  final d2 = log7(publicKey2, mod);
  return publicKey1.modPow(d2, mod);
}

void main() {
  print('Phase 1: Ans = ${solvePhase1(11349501, 5107328)}');
  print('Phase 2: Ans = N/A (nothing to solve)');
}
