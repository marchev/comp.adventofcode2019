/// https://adventofcode.com/2020, Day 4
///
/// Task: https://adventofcode.com/2020/day/4
/// Date: 2020-12-04
/// Author: Svilen Marchev
///
/// Status: works for both
///
/// Phase 1 answer: 208
/// Phase 2 answer: 167

import 'dart:io';

import 'package:quiver/check.dart';

final tokenRegEx = RegExp(r'^([^:]+):(.+)$');

final yearRegEx = RegExp(r'^\d{4}$');
final heightRegEx = RegExp(r'^(\d{2,4})(cm|in)$');
final hairRegEx = RegExp(r'^#[0-9a-f]{6}$');
final eyeRegEx = RegExp(r'^amb|blu|brn|gry|grn|hzl|oth$');
final passportIdRegEx = RegExp(r'^\d{9}$');

bool validateByr(String s) {
  if (!yearRegEx.hasMatch(s)) return false;
  final y = int.parse(s);
  return 1920 <= y && y <= 2002;
}

bool validateIyr(String s) {
  if (!yearRegEx.hasMatch(s)) return false;
  final y = int.parse(s);
  return 2010 <= y && y <= 2020;
}

bool validateEyr(String s) {
  if (!yearRegEx.hasMatch(s)) return false;
  final y = int.parse(s);
  return 2020 <= y && y <= 2030;
}

bool validateHgt(String s) {
  final match = heightRegEx.firstMatch(s);
  if (match == null) return false;
  int value = int.parse(match.group(1));
  if (match.group(2) == 'cm') {
    return 150 <= value && value <= 193;
  } else {
    return 59 <= value && value <= 76;
  }
}

bool validateHcl(String s) => hairRegEx.hasMatch(s);

bool validateEcl(String s) => eyeRegEx.hasMatch(s);

bool validatePid(String s) => passportIdRegEx.hasMatch(s);

List<String> readRows() =>
    File('lib/2020/problem4/input.txt').readAsLinesSync().toList();

class Passport {
  final Map<String, String> pairs = {};

  void add(String key, String value) => pairs[key] = value;

  String get(String key) => pairs[key];

  int get length => pairs.length;
}

List<Passport> convertToPassports(List<String> lines) {
  List<Passport> allPassports = [];
  var currentPassport = Passport();
  for (final line in lines) {
    if (line == '') {
      if (currentPassport.length > 0) {
        allPassports.add(currentPassport);
        currentPassport = Passport();
      }
      continue;
    }
    line.split(' ').where((token) => token.isNotEmpty).forEach((token) {
      final match = tokenRegEx.firstMatch(token);
      checkState(match != null);
      currentPassport.add(match.group(1), match.group(2));
    });
  }
  if (currentPassport.length > 0) {
    allPassports.add(currentPassport);
  }
  return allPassports;
}

int solvePhase1(List<String> lines) {
  return convertToPassports(lines).where((passport) {
    return ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
        .map((key) => passport.get(key))
        .every((value) => value != null);
  }).length;
}

int solvePhase2(List<String> lines) {
  return convertToPassports(lines).where((passport) {
    return [
      ['byr', validateByr],
      ['iyr', validateIyr],
      ['eyr', validateEyr],
      ['hgt', validateHgt],
      ['hcl', validateHcl],
      ['ecl', validateEcl],
      ['pid', validatePid],
    ].map((keyAndValidator) {
      final value = passport.get(keyAndValidator[0]);
      if (value == null) return false;
      return (keyAndValidator[1] as Function)(value);
    }).every((value) => value);
  }).length;
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
