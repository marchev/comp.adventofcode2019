/// https://adventofcode.com/2020, Day 7
///
/// Task: https://adventofcode.com/2020/day/7
/// Date: 2021-02-21
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 268
/// Phase 2 answer: 7867
///
/// Traverse a graph, computing and memoizing a function for each node.
///
/// Tags: graph traversal, DP, memoization

import 'dart:io';

import 'package:quiver/check.dart';

List<String> readRows() =>
    File('lib/2020/problem7/input.txt').readAsLinesSync().toList();

const specialBag = 'shiny gold';

final bagRegExp = RegExp(r'(\w+ \w+) bags?[.,]? ?');
final countedBagRegExp = RegExp(r'(\d+) (\w+ \w+) bags?[.,]? ?');

class Bag {
  final String name;
  final List<QuantifiedBag> quantifiedBags = [];

  Bag(this.name);

  void addContainedBag(String bagName, int count) =>
      quantifiedBags.add(QuantifiedBag(bagName, count));
}

class QuantifiedBag {
  final String bagName;
  final int count;

  QuantifiedBag(this.bagName, this.count);
}

Bag createBag(String line) {
  final halves = line.split(' contain ');
  final containerBagName = bagRegExp.firstMatch(halves.first).group(1);
  final bag = Bag(containerBagName);
  if (halves.last != 'no other bags.') {
    for (final m in countedBagRegExp.allMatches(halves.last)) {
      final containedBagCount = int.parse(m.group(1));
      final containedBagName = m.group(2);
      bag.addContainedBag(containedBagName, containedBagCount);
    }
  }
  return bag;
}

bool doesBagContainSpecialBag(
    Bag currentBag, Map<String, Bag> bagByName, Map<String, int> bagStatus) {
  checkState(bagStatus[currentBag.name] != 2, message: 'graph has cycles');
  if (bagStatus.containsKey(currentBag.name)) {
    return bagStatus[currentBag.name] == 0 ? false : true;
  }
  bool doesContain = false;
  if (currentBag.name == specialBag) {
    doesContain = true;
  } else {
    bagStatus[currentBag.name] = 2; // mark as added to the search tree
    for (final childQuantifiedBag in currentBag.quantifiedBags) {
      final childBag = bagByName[childQuantifiedBag.bagName];
      doesContain = doesBagContainSpecialBag(childBag, bagByName, bagStatus);
      if (doesContain) break;
    }
  }
  bagStatus[currentBag.name] = doesContain ? 1 : 0;
  return doesContain;
}

int solvePhase1(List<String> lines) {
  final bags = lines.map(createBag).toList();
  final bagByName = Map.fromIterables(bags.map((bag) => bag.name), bags);
  final bagStatus = <String, int>{};
  return bags
      .where((bag) =>
          bag.name != specialBag &&
          doesBagContainSpecialBag(bag, bagByName, bagStatus))
      .length;
}

int countTotalBags(Bag currentBag, Map<String, Bag> bagByName,
    Map<String, int> subtreeBagCount) {
  checkState(subtreeBagCount[currentBag.name] != -1,
      message: 'graph has cycles');
  if (subtreeBagCount.containsKey(currentBag.name)) {
    return subtreeBagCount[currentBag.name];
  }
  subtreeBagCount[currentBag.name] = -1; // mark as being added to the traversal
  int totalBags = 1;
  for (final childQuantifiedBag in currentBag.quantifiedBags) {
    final childBag = bagByName[childQuantifiedBag.bagName];
    totalBags += childQuantifiedBag.count *
        countTotalBags(childBag, bagByName, subtreeBagCount);
  }
  subtreeBagCount[currentBag.name] = totalBags;
  return totalBags;
}

int solvePhase2(List<String> lines) {
  final bags = lines.map(createBag).toList();
  final bagByName = Map.fromIterables(bags.map((bag) => bag.name), bags);
  final subtreeBagCount = <String, int>{};
  return countTotalBags(bagByName[specialBag], bagByName, subtreeBagCount) - 1;
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
