/// https://adventofcode.com/2020, Day 23
///
/// Task: https://adventofcode.com/2020/day/23
/// Date: 2020-12-30
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 54327968
/// Phase 2 answer: 157410423276
///
/// Run time for phase 2: ~8s.

import 'dart:collection';
import 'package:quiver/check.dart';

class Cup extends LinkedListEntry<Cup> {
  int id;

  Cup(this.id);
}

/// Represents a configuration of cups.
class Config {
  /// The order of cups. The "current" one is always the first in the list.
  final cups = LinkedList<Cup>();
  final cupIdToCup = <int, Cup>{};
  final maxCupId;

  Config(List<int> order) : maxCupId = order.length {
    cups.addAll(order.map((id) => Cup(id)));
    cupIdToCup.addEntries(cups.map((cup) => MapEntry(cup.id, cup)));
  }

  void makeMove() {
    final currentCup = cups.first;

    final slice = <Cup>[
      currentCup.next,
      currentCup.next.next,
      currentCup.next.next.next,
    ];
    slice.forEach((cup) => cup.unlink());

    var destinationId = currentCup.id - 1;
    final sliceIds = slice.map((cup) => cup.id).toList();
    while (true) {
      if (destinationId == 0) destinationId = maxCupId;
      if (!sliceIds.contains(destinationId)) break;
      destinationId--;
    }

    final destinationCup = cupIdToCup[destinationId];
    checkNotNull(destinationCup);
    slice.reversed.forEach((cupToAdd) => destinationCup.insertAfter(cupToAdd));

    final newCurrentCup = currentCup.next ?? cups.first;

    // Ensure the "current" is always the first in the list.
    while (newCurrentCup != cups.first) {
      final toMove = cups.first;
      toMove.unlink();
      cups.add(toMove);
    }
  }

  List<int> orderStartingFrom1() {
    final cup1 = cupIdToCup[1];
    final order = <int>[];
    var iter = cup1.next ?? cups.first;
    while (iter.id != 1) {
      order.add(iter.id);
      iter = iter.next ?? cups.first;
    }
    return order;
  }
}

String solvePhase1(String input, {int moveCount = 100}) {
  final order = input.codeUnits
      .map((codeUnit) => codeUnit - '0'.codeUnits.first)
      .toList();
  final c = Config(order);
  for (int i = 0; i < moveCount; ++i) {
    c.makeMove();
  }
  return c.orderStartingFrom1().join();
}

int solvePhase2(String prefixInput) {
  final order = prefixInput.codeUnits
      .map((codeUnit) => codeUnit - '0'.codeUnits.first)
      .toList();
  while (order.length < 1000000) {
    order.add(order.length + 1);
  }

  final c = Config(order);
  for (int i = 0; i < 10000000; ++i) {
    c.makeMove();
  }
  final finalOrder = c.orderStartingFrom1();
  return finalOrder[0] * finalOrder[1];
}

void main() {
  print('Phase 1: Ans = ${solvePhase1('538914762')}');
  print('Phase 2: Ans = ${solvePhase2('538914762')}');
}
