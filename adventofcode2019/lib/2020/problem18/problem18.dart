/// https://adventofcode.com/2020, Day 18
///
/// Task: https://adventofcode.com/2020/day/18
/// Date: 2020-12-31
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 1451467526514
/// Phase 2 answer: 224973686321527
///
/// Tags: evaluating expressions

import 'dart:collection';
import 'dart:io';

import 'package:quiver/check.dart';

List<String> readRows() =>
    File('lib/2020/problem18/input.txt').readAsLinesSync().toList();

bool isDigit(String c) =>
    '0'.codeUnitAt(0) <= c.codeUnitAt(0) &&
    c.codeUnitAt(0) <= '9'.codeUnitAt(0);

int evaluate(String line, Map<String, int> weight) {
  line = line.replaceAll(' ', '');
  final tokens = line.split('');

  final numsStack = ListQueue<int>();
  final exprStack = ListQueue<String>();

  void executeOperation() {
    int n2 = numsStack.removeLast();
    int n1 = numsStack.removeLast();
    String expr = exprStack.removeLast();
    numsStack.addLast(expr == '*' ? n1 * n2 : n1 + n2);
  }

  for (int i = 0; i < tokens.length; ++i) {
    final t = tokens[i];
    if (isDigit(t)) {
      numsStack.addLast(int.parse(t));
    } else if (t == '(') {
      exprStack.addLast(t);
    } else if (t == '+' || t == '*') {
      while (exprStack.isNotEmpty &&
          (exprStack.last == '+' || exprStack.last == '*') &&
          weight[exprStack.last] <= weight[t]) {
        executeOperation();
      }
      exprStack.addLast(t);
    } else if (t == ')') {
      while (exprStack.isNotEmpty && exprStack.last != '(') {
        executeOperation();
      }
      checkState(exprStack.isNotEmpty);
      exprStack.removeLast();
    } else {
      throw AssertionError();
    }
  }
  while (exprStack.isNotEmpty) {
    executeOperation();
  }
  checkState(exprStack.isEmpty);
  checkState(numsStack.length == 1);
  return numsStack.first;
}

int solvePhase1(List<String> lines) {
  final weight = <String, int>{'+': 1, '*': 1};
  return lines
      .map((line) => evaluate(line, weight))
      .fold(0, (acc, e) => acc + e);
}

int solvePhase2(List<String> lines) {
  final weight = <String, int>{'+': 1, '*': 2};
  return lines
      .map((line) => evaluate(line, weight))
      .fold(0, (acc, e) => acc + e);
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
