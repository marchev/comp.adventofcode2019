/// https://adventofcode.com/2020, Day 12
///
/// Task: https://adventofcode.com/2020/day/12
/// Date: 2021-01-31
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 415
/// Phase 2 answer: 29401
///
/// Simulation of instructions.
///
/// Tags: simulation

import 'dart:io';

import 'package:quiver/check.dart';

List<String> readRows() =>
    File('lib/2020/problem12/input.txt').readAsLinesSync().toList();

/// Directions, counter-clockwise: E, N, W, S
const dx = const <int>[1, 0, -1, 0];
const dy = const <int>[0, 1, 0, -1];

int solvePhase1(List<String> lines) {
  int x = 0, y = 0, dir = 0;
  for (final line in lines) {
    final code = line.substring(0, 1);
    final offset = int.parse(line.substring(1));
    switch (code) {
      case 'E':
        x += offset;
        break;
      case 'W':
        x -= offset;
        break;
      case 'N':
        y += offset;
        break;
      case 'S':
        y -= offset;
        break;
      case 'L':
        dir = (dir + offset ~/ 90) % 4;
        break;
      case 'R':
        dir = (dir + 3 * (offset ~/ 90)) % 4;
        break;
      case 'F':
        x += dx[dir] * offset;
        y += dy[dir] * offset;
        break;
      default:
        throw AssertionError();
    }
  }
  return x.abs() + y.abs();
}

int solvePhase2(List<String> lines) {
  int x = 0, y = 0; // coordinates of the ship
  int ox = 10, oy = 1; // coordinates of the waypoint, relative to the ship
  for (final line in lines) {
    final code = line.substring(0, 1);
    final offset = int.parse(line.substring(1));
    switch (code) {
      case 'E':
        ox += offset;
        break;
      case 'W':
        ox -= offset;
        break;
      case 'N':
        oy += offset;
        break;
      case 'S':
        oy -= offset;
        break;
      case 'L':
        for (int degrees = 0; degrees < offset; degrees += 90) {
          final savedOx = ox;
          ox = -oy;
          oy = savedOx;
        }
        break;
      case 'R':
        for (int degrees = 0; degrees < offset; degrees += 90) {
          final savedOx = ox;
          ox = oy;
          oy = -savedOx;
        }
        break;
      case 'F':
        x += ox * offset;
        y += oy * offset;
        break;
      default:
        throw AssertionError();
    }
  }
  return x.abs() + y.abs();
}

void main() {
  List<String> lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
