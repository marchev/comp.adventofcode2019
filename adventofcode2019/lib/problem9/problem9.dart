import 'dart:async';
import 'dart:io';
import 'dart:math';

/// https://adventofcode.com/2019, Day 9
///
/// Task: https://adventofcode.com/2019/day/9
/// Date: 2019-12-09
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 3780860499, for 0.05s
/// Phase 2 answer: 33343, for 0.1s

List<String> readRows() =>
    File('lib/problem9/input.txt').readAsLinesSync().toList();

List<int> parseOps(String line) => line.split(',').map(int.parse).toList();

Stream<T> flattenStreams<T>(List<Stream<T>> source) async* {
  for (var stream in source) yield* stream;
}

Stream<T> prependToStream<T>(T first, Stream<T> rest) => flattenStreams([
      Stream.fromIterable([first]),
      rest
    ]);

class Computer {
  static const memorySize = 100000;

  final int id;
  final List<int> _mem;
  Stream<int> _input;
  final _output = StreamController<int>.broadcast();

  /// Instruction pointer.
  var _i = 0;

  /// Relative base (for relative mode params).
  var _relativeBase = 0;

  Computer(this.id, List<int> ops, input, int config)
      : _mem = ops
            .toList()
            .followedBy(List.filled(memorySize - ops.length, 0))
            .toList(),
        _input = prependToStream(config, input);

  Stream<int> get output => _output.stream;

  void addToInput(Stream<int> more) => _input = flattenStreams([_input, more]);

  /// Runs the program until it halts.
  Future<void> run() async {
    await for (var input in _input) {
      _log('received input $input');
      _onInputProvided(input);
    }
  }

  /// Executes instructions and eventually consumes the [input].
  ///
  /// Stops if another input needs to be provided, or if the program exits.
  void _onInputProvided(int input) {
    var isInputConsumed = false;
    while (_i < _mem.length) {
      _log('op: ${_mem[_i]}');

      var opCode = _mem[_i] % 100;
      var paramModes = _mem[_i] ~/ 100;

      switch (opCode) {
        case 1: // +
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a + b, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 2: // *
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a * b, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 3: // input
          if (isInputConsumed) return; // another input is needed
          isInputConsumed = true;
          _writeValue(input, _mem[_i + 1], 0, paramModes);
          _i += 2;
          break;
        case 4: // output
          final output = _readValue(_mem[_i + 1], 0, paramModes);
          _output.add(output);
          _log('output: $output');
          _i += 2;
          break;
        case 5: // jump-if-true
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          if (a != 0) {
            _i = _readValue(_mem[_i + 2], 1, paramModes);
          } else {
            _i += 3;
          }
          break;
        case 6: // jump-if-false
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          if (a == 0) {
            _i = _readValue(_mem[_i + 2], 1, paramModes);
          } else {
            _i += 3;
          }
          break;
        case 7: // less than
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a < b ? 1 : 0, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 8: // equals
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a == b ? 1 : 0, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 9: // set relative base
          _relativeBase += _readValue(_mem[_i + 1], 0, paramModes);
          _i += 2;
          break;
        case 99:
          _log('normal exit');
          _output.close();
          return;
        default:
          _log('ABNORMAL EXIT');
          _output.close();
          return;
      }
    }
  }

  int _readValue(int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        return _mem[paramValue];
      case 1: // immediate mode
        return paramValue;
      case 2: // relative mode
        return _mem[_relativeBase + paramValue];
      default:
        throw AssertionError();
    }
  }

  void _writeValue(int value, int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        _mem[paramValue] = value;
        break;
      case 2: // relative mode
        _mem[_relativeBase + paramValue] = value;
        break;
      default:
        throw AssertionError();
    }
  }

  void _log(String msg) {
//    print('$id: $msg');
  }
}

Future<String> computeForConfig(List<int> ops, int config) async {
  var input = Stream.fromIterable([0]);
  final computer = Computer(0, ops, input, config);
  computer.run();
  return await computer.output.join(',');
}

Future<String> solvePhase1(List<int> ops) async => computeForConfig(ops, 1);

Future<String> solvePhase2(List<int> ops) async => computeForConfig(ops, 2);

void main() async {
  var lines = readRows();
  List<int> ops = parseOps(lines[0]);

  print('Phase 1: Ans = ${await solvePhase1(ops)}');
  print('Phase 2: Ans = ${await solvePhase2(ops)}');
}
