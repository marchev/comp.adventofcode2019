import 'dart:io';

/// https://adventofcode.com/2019, Day 8
///
/// Task: https://adventofcode.com/2019/day/8
/// Date: 2019-12-08
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 2080, for 0.01s
/// Phase 2 answer: AURCY, for 0.01s

List<String> readRows() =>
    File('lib/problem8/input.txt').readAsLinesSync().toList();

const rows = 6;
const cols = 25;
const area = rows * cols;

List<int> decodePicAt(String serialized, int start) =>
    serialized.substring(start, start + area).split('').map(int.parse).toList();

int solvePhase1(String serialized) {
  int countPixels(List<int> pic, int pixelValue) =>
      pic.where((px) => px == pixelValue).length;

  var picWithLeast0s = decodePicAt(serialized, 0);
  for (int i = area; i < serialized.length; i += area) {
    final pic = decodePicAt(serialized, i);
    if (countPixels(picWithLeast0s, 0) > countPixels(pic, 0)) {
      picWithLeast0s = pic;
    }
  }
  return countPixels(picWithLeast0s, 1) * countPixels(picWithLeast0s, 2);
}

void solvePhase2(String serialized) {
  var decoded = decodePicAt(serialized, 0);

  void applyLayer(List<int> layer) {
    for (int r = 0; r < rows; ++r) {
      for (int c = 0; c < cols; ++c) {
        if (decoded[r * cols + c] == 2) {
          decoded[r * cols + c] = layer[r * cols + c];
        }
      }
    }
  }

  for (int i = area; i < serialized.length; i += area) {
    final layer = decodePicAt(serialized, i);
    applyLayer(layer);
  }

  for (int r = 0; r < rows; ++r) {
    print(decoded
        .getRange(r * cols, r * cols + cols)
        .join(' ')
        .replaceAll('0', ' '));
  }
}

void main() async {
  var lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines[0])}');

  print('Phase 2: decoded image:');
  solvePhase2(lines[0]);
}
