import 'dart:convert';
import 'dart:io';
import 'dart:math';

/// https://adventofcode.com/2019, Day 25
///
/// Task: https://adventofcode.com/2019/day/25
/// Date: 2019-12-25
/// Author: Svilen Marchev
///
/// Status: works for phase 1
///
/// Analysis:
///
/// Phase 1: I explored the map manually and created a visual map, using an
/// interactive prompt with the Intcode program. This was a good idea,
/// since the map was small enough and I avoided parsing free text and
/// edge case handling like items that don't let you move, kill the program,
/// or enter infinite loop. My strategy was to collect all collectible items
/// and by the end try out all possible subsets of them.
///
/// Phase 1 answer: 196872, for 1.0s
/// Phase 2 answer: ?, for 0.?s

List<String> readRows() =>
    File('lib/problem25/input.txt').readAsLinesSync().toList();

List<int> parseOps(String line) => line.split(',').map(int.parse).toList();

class Computer {
  static const memorySize = 10000;

  /// Called when an input is requested.
  final Function _provideInput;

  /// Called when a new output value is emitted.
  final Function _consumeOutput;

  final List<int> _mem;

  /// Instruction pointer.
  var _i = 0;

  /// Relative base (for relative mode params).
  var _relativeBase = 0;

  Computer(List<int> ops, int provideInput(), void consumeOutput(int))
      : _provideInput = provideInput,
        _consumeOutput = consumeOutput,
        _mem = ops
            .toList()
            .followedBy(List.filled(memorySize - ops.length, 0))
            .toList();

  /// Executes the program, requesting inputs and supplying outputs through
  /// callbacks.
  void run() {
    while (_i < _mem.length) {
      _log('op: ${_mem[_i]}');

      var opCode = _mem[_i] % 100;
      var paramModes = _mem[_i] ~/ 100;

      switch (opCode) {
        case 1: // +
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a + b, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 2: // *
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a * b, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 3: // input
          final input = _provideInput();
          if (input == null) {
            _log('exiting since null was inputted');
            return;
          }
          _writeValue(input, _mem[_i + 1], 0, paramModes);
          _i += 2;
          break;
        case 4: // output
          final output = _readValue(_mem[_i + 1], 0, paramModes);
          _consumeOutput(output);
          _log('output: $output');
          _i += 2;
          break;
        case 5: // jump-if-true
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          if (a != 0) {
            _i = _readValue(_mem[_i + 2], 1, paramModes);
          } else {
            _i += 3;
          }
          break;
        case 6: // jump-if-false
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          if (a == 0) {
            _i = _readValue(_mem[_i + 2], 1, paramModes);
          } else {
            _i += 3;
          }
          break;
        case 7: // less than
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a < b ? 1 : 0, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 8: // equals
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a == b ? 1 : 0, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 9: // set relative base
          _relativeBase += _readValue(_mem[_i + 1], 0, paramModes);
          _i += 2;
          break;
        case 99:
          _log('normal exit');
          return;
        default:
          throw Exception('Unrecognized instruction $opCode');
      }
    }
  }

  int _readValue(int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        return _mem[paramValue];
      case 1: // immediate mode
        return paramValue;
      case 2: // relative mode
        return _mem[_relativeBase + paramValue];
      default:
        throw AssertionError();
    }
  }

  void _writeValue(int value, int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        _mem[paramValue] = value;
        break;
      case 2: // relative mode
        _mem[_relativeBase + paramValue] = value;
        break;
      default:
        throw AssertionError();
    }
  }

  void _log(String msg) {
//    print('computer: $msg');
  }
}

Iterable<int> translateCommandsToIntcodeInput(Iterable<String> commands) sync* {
  yield* commands.map((c) => c + '\n').expand((c) => c.codeUnits);
}

/// Starts taking commands from stdin, forwarding them to the Intcode program.
Iterable<int> startInteractivePrompt() sync* {
  while (true) {
    print('Please input command:');
    final line = stdin.readLineSync(encoding: Encoding.getByName('utf-8'));
    yield* translateCommandsToIntcodeInput([line]);
  }
}

int solvePhase1(List<int> ops) {
  final codeRegEx = RegExp('\\d{3,}');

  var outputBuffer = StringBuffer();
  String lastRoom;
  int code;

  /// Populated manually, by exploring the map in an interactive mode.
  final commandsToCollectItemsAndReachExit = [
    // Take all items from the south and go back to start
    'south', // -> passages
    'take festive hat',
    'north',

    // Take all items from the west and go back to start
    'west',
    'south',
    'take pointer',
    'south', // -> Hot Chocolate Fountain
    'take prime number',
    'west',
    'take coin',
    'east',
    'north', // -> storage
    'north', // -> arcade
    'east', // -> start

    // Take all items from west-south and go back to start
    'east', // -> kitchen
    'south', // -> sick bay
    'south', // -> crew quarters
    'take space heater',
    'south', // -> halodeck
    'take astrolabe',
    'north',
    'north',
    'north', // -> kitchen
    'west', // -> start

    // Take all items from west-north and exit
    'east', // -> kitchen
    'north', // -> gift wrapping
    'take wreath',
    'north', // -> observatory
    'west', // -> hallway
    'take dehydrated water',
    'inv',
    'north', // warp drive maintenance
    'east', // security
    'south', // target!
  ];

  final inventory = commandsToCollectItemsAndReachExit
      .where((item) => item.startsWith('take '))
      .map((item) => item.substring('take '.length))
      .toList();

  Iterable<int> generateInputs() sync* {
    yield* translateCommandsToIntcodeInput(commandsToCollectItemsAndReachExit);

    for (int b = 0; b < 1 << inventory.length; ++b) {
      // Drop everything before picking only a few selected items, determined
      // by the bitmask.
      yield* translateCommandsToIntcodeInput(
          inventory.map((item) => 'drop $item'));

      for (int i = 0; i < inventory.length; ++i) {
        if ((b >> i) & 1 > 0) {
          yield* translateCommandsToIntcodeInput(['take ${inventory[i]}']);
        }
      }
      yield* translateCommandsToIntcodeInput(['south']);
    }
  }

  final comp = Computer(ops, () {
    final inputsIterator = generateInputs().iterator;
    return () {
      if (!inputsIterator.moveNext()) return null;
      return inputsIterator.current;
    };
  }(), (outputCharCode) {
    final output = String.fromCharCode(outputCharCode);
    if (output == '\n') {
      final line = outputBuffer.toString();
      if (line.isEmpty) return;
      if (line.startsWith('== ')) {
        lastRoom = line.substring('== '.length, line.length - ' =='.length);
      }
      if (lastRoom == 'Pressure-Sensitive Floor') {
        final matches = codeRegEx.allMatches(line);
        if (matches.isNotEmpty) {
          code = int.parse(matches.first.group(0));
        }
      }

//      print('out: $line');
      outputBuffer.clear();
    } else {
      outputBuffer.write(output);
    }
  });
  comp.run();

  return code;
}

void main() {
  var lines = readRows();
  List<int> ops = parseOps(lines[0]);

  print('Phase 1: Ans = ${solvePhase1(ops)}');
}
