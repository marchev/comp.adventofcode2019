/// https://adventofcode.com/2019, Day 2
///
/// Task: https://adventofcode.com/2019/day/2
/// Date: 2019-12-02
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: 3058646, for 0.01s
/// Phase 2 answer: 8976, for 0.01s

import 'dart:io';

List<String> readRows() =>
    File('lib/problem2/input.txt').readAsLinesSync().toList();

List<int> parseNums(List<String> lines) =>
    lines[0].split(',').map(int.parse).toList();

void runProgram(List<int> nums) {
  for (int i = 0; i < nums.length; i += 4) {
    if (nums[i] == 99) {
      break;
    } else if (nums[i] == 1) {
      nums[nums[i + 3]] = nums[nums[i + 1]] + nums[nums[i + 2]];
    } else if (nums[i] == 2) {
      nums[nums[i + 3]] = nums[nums[i + 1]] * nums[nums[i + 2]];
    } else {
      throw AssertionError();
    }
  }
}

void runProgramWithInputs(List<int> nums, int a, int b) {
  nums[1] = a;
  nums[2] = b;
  runProgram(nums);
}

int solvePhase1(List<int> nums) {
  var tmp = List.of(nums);
  runProgramWithInputs(tmp, 12, 2);
  return tmp[0];
}

int solvePhase2(List<int> nums) {
  for (int i = 0; i < 100; ++i) {
    for (int j = 0; j < 100; ++j) {
      var tmp = List.of(nums);
      runProgramWithInputs(tmp, i, j);
      if (tmp[0] == 19690720) {
        return 100 * i + j;
      }
    }
  }
  throw AssertionError();
}

void main() {
  List<int> nums = parseNums(readRows());

  print('Phase 1: Ans = ${solvePhase1(nums)}');
  print('Phase 2: Ans = ${solvePhase2(nums)}');
}
