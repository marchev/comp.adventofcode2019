import 'dart:io';
import 'dart:math';

/// https://adventofcode.com/2019, Day 10
///
/// Task: https://adventofcode.com/2019/day/10
/// Date: 2019-12-10
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 221, for 0.25s
/// Phase 2 answer: 806, for 0.25s

List<String> readRows() =>
    File('lib/problem10/input.txt').readAsLinesSync().toList();

class Point {
  final int x, y;

  const Point(this.x, this.y);

  @override
  bool operator ==(Object other) =>
      other is Point && x == other.x && y == other.y;

  @override
  String toString() => '($x,$y)';
}

List<Point> extractPoints(List<String> lines) {
  final points = <Point>[];
  for (var y = 0; y < lines.length; ++y) {
    var row = lines[y].split('').toList();
    for (var x = 0; x < row.length; ++x) {
      if (row[x] == '#') {
        points.add(Point(x, lines.length - 1 - y));
      }
    }
  }
  return points;
}

class ChosenPoint {
  final Point chosenPoint;
  final int numVisible;

  const ChosenPoint(this.chosenPoint, this.numVisible);
}

bool noPointBetween(Point p1, Point p2, List<Point> points) {
  // Find general form of the line determined by p1 and p2.
  final a = p2.y - p1.y;
  final b = p1.x - p2.x;
  final c = a * p1.x + b * p1.y;

  for (var p in points) {
    if (p == p1 || p == p2) continue;
    if (a * p.x + b * p.y == c) {
      if (min(p1.x, p2.x) <= p.x &&
          p.x <= max(p1.x, p2.x) &&
          min(p1.y, p2.y) <= p.y &&
          p.y <= max(p1.y, p2.y)) {
        return false;
      }
    }
  }
  return true;
}

ChosenPoint solvePhase1(List<String> lines) {
  final points = extractPoints(lines);

  Point chosenPoint;
  int numVisibleFromChosenPoint = 0;

  for (var p1 in points) {
    var numVisible = 0;
    for (var p2 in points) {
      if (p1 == p2) continue;
      if (noPointBetween(p1, p2, points)) {
        numVisible++;
      }
    }
    if (numVisibleFromChosenPoint < numVisible) {
      numVisibleFromChosenPoint = numVisible;
      chosenPoint = p1;
    }
  }
  return ChosenPoint(chosenPoint, numVisibleFromChosenPoint);
}

List<Point> computeOrderOfRemoval(List<String> lines) {
  final points = extractPoints(lines);

  final center = solvePhase1(lines).chosenPoint;
//  print('chosenPoint: $center');
  points.remove(center);

  points.sort((a, b) {
    if (a.x - center.x >= 0 && b.x - center.x < 0) return -1;
    if (a.x - center.x < 0 && b.x - center.x >= 0) return 1;
    if (a.x - center.x == 0 && b.x - center.x == 0) {
      if (a.y >= center.y && b.y >= center.y) return a.y.compareTo(b.y);
      if (a.y <= center.y && b.y <= center.y) return -a.y.compareTo(b.y);
      return a.y.compareTo(b.y);
    }

    // Compute the cross product of vectors (center -> a) x (center -> b).
    int det = (a.x - center.x) * (b.y - center.y) -
        (b.x - center.x) * (a.y - center.y);
    if (det < 0) return -1;
    if (det > 0) return 1;

    // Points a and b are on the same line from the center.
    // Check which point is closer to the center.
    int d1 = (a.x - center.x) * (a.x - center.x) +
        (a.y - center.y) * (a.y - center.y);
    int d2 = (b.x - center.x) * (b.x - center.x) +
        (b.y - center.y) * (b.y - center.y);
    return d1.compareTo(d2);
  });
//  print('order: $points');

  final removed = <Point>[];
  while (points.length > 0) {
    final toRemove = <Point>[];
    for (final p in points) {
      if (noPointBetween(center, p, points)) {
        toRemove.add(p);
      }
    }
//    print('removing: $toRemove');
    removed.addAll(toRemove);
    toRemove.forEach(points.remove);
  }
  return removed.map((p) => Point(p.x, lines.length - 1 - p.y)).toList();
}

int solvePhase2(List<String> lines, [int numToRemove = 200]) {
  final removed =
      computeOrderOfRemoval(lines).map((p) => p.x * 100 + p.y).toList();
  return removed[numToRemove - 1];
}

void main() {
  var lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines).numVisible}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
