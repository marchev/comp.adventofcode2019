import 'dart:io';
import 'dart:math';

import 'package:quiver/check.dart';
import 'package:quiver/iterables.dart' hide min;

/// https://adventofcode.com/2019, Day 16
///
/// Task: https://adventofcode.com/2019/day/16
/// Date: 2019-12-16
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Analysis:
///
/// Phase 1 is a straightforward matrix multiplication, and I expected that
/// phase 2 would ask for fast matrix multiplication, but surprisingly that
/// was not the case.
///
/// In phase 2 I made use of the special form of the pattern matrix - blocks
/// of zeros, ones, zeros, and minus ones, which can be interpreted as blocks
/// of input elements that need to be ignored, included, or excluded in the
/// corresponding sum.
///
/// Suppose that we want to calculate a given step with a given input and want
/// to produce its output. For every outputIx=0,1,...,|input|*10000-1, we have
/// an "inclusion-exclusion pattern" of length (outputIx+1)*4.
/// So for every block of consecutive (outputIx+1)*4 elements we can easily
/// add the sums of all included and excluded elements of that block. For that,
/// we would simply need to compute accumulated sums over the input at the
/// beginning of the step. And, of course, we should keep in mind that the
/// first such block is of size (outputIx+1)*4-1 (because the first element
/// is repeated only outputIx times).
///
/// Let N=|input|*10000, i.e. the size of the expanded input, which is
/// N=~6.5*10^6. With this algorithm each step has complexity:
///   O(each step) =
///   O(N/4 + N/8 + N/12 + ...+ N/(4N)) =
///   O(N/4 (1 + 1/2 + 1/3 + ... + 1/N)) =
///   O((1.6 * 10^6) * 16.2) =
///   O(26 * 10^6)
/// So the total time complexity is:
///   O(steps) * O(each step) =
///   O(100 * (26 * 10^6)) =
///   O(26 * 10^8)
///
/// Memory complexity is O(N).
///
/// Phase 1 answer: 10189359, for 0.3s
/// Phase 2 answer: 80722126, for ~1:14 min

List<String> readRows() =>
    File('lib/problem16/input.txt').readAsLinesSync().toList();

List<int> parseInput(String line) => line.split('').map(int.parse).toList();

class Matrix {
  final List<List<int>> a;

  Matrix(this.a);

  int get rows => a.length;

  int get cols => a[0].length;

  factory Matrix.empty(int rows, int cols) =>
      Matrix(List.generate(rows, (i) => List.filled(cols, 0)));

  Matrix multiplyMod(Matrix other, int mod) {
    checkArgument(cols == other.rows, message: 'cannot multiply matrices');
    final product = Matrix.empty(rows, other.cols);
    for (var i = 0; i < rows; ++i) {
      for (var j = 0; j < other.cols; ++j) {
        var val = 0;
        for (var k = 0; k < cols; ++k) {
          val += a[i][k] * other.a[k][j];
        }
        // Dart's modulo operation gives non-negative results, so in order
        // to get the least significant digit, we need the absolute value.
        product.a[i][j] = val.abs() % mod;
      }
    }
    return product;
  }

  @override
  String toString() => a.map((row) => '$row').toList().join('\n');
}

Iterable<int> generateBase(int outputIx) sync* {
  final pattern = [0, 1, 0, -1];
  // How many reps at a time of each pattern num.
  final reps = outputIx + 1;
  // The first element is a special case - it needs to emitted reps-1 times.
  for (var i = 1; i < reps; ++i) yield pattern[0];
  for (var patternIx = 1;; patternIx = (patternIx + 1) % pattern.length) {
    for (var i = 0; i < reps; ++i) yield pattern[patternIx];
  }
}

String solvePhase1(List<int> input, int steps) {
  final baseMatrix = Matrix.empty(input.length, input.length);
  for (int col = 0; col < input.length; ++col) {
    final colList = generateBase(col).take(input.length).toList();
    for (int row = 0; row < input.length; ++row) {
      baseMatrix.a[row][col] = colList[row];
    }
  }

  var inputMatrix = Matrix([input]);
  for (var step = 0; step < steps; ++step) {
    inputMatrix = inputMatrix.multiplyMod(baseMatrix, 10);
  }
  return inputMatrix.a[0].take(8).join('');
}

String solvePhase2(List<int> inputPeriod, int steps) {
  const inputReps = 10000;
  final n = inputReps * inputPeriod.length;

  var input = cycle(inputPeriod).take(n).toList(growable: false);
  var output = List.filled(n, 0, growable: false);

  final acc = List.filled(n, 0, growable: false);
  int sumInputElements(int startIx, int num) {
    if (startIx >= acc.length) return 0;
    final endIx = min(startIx + num - 1, acc.length - 1);
    return acc[endIx] - (startIx > 0 ? acc[startIx - 1] : 0);
  }

  for (var step = 0; step < steps; ++step) {
    print('Step $step');

    acc[0] = input[0];
    for (var i = 1; i < input.length; ++i) acc[i] = acc[i - 1] + input[i];

    for (var outputIx = 0; outputIx < n; ++outputIx) {
      var val = 0;

      // Reps of each of the nums from the pattern.
      final reps = outputIx + 1;
      // Size of the pattern, considering the reps of its individual nums.
      final patternLength = 4 * reps;

      val += sumInputElements(reps - 1, reps); // the block of 1-s
      val -= sumInputElements(3 * reps - 1, reps); // the block of -1-s
      for (var i = patternLength - 1; i < n; i += patternLength) {
        val += sumInputElements(i + reps, reps); // the block of 1-s
        val -= sumInputElements(i + 3 * reps, reps); // the block of -1-s
      }

      // Dart's modulo operation gives non-negative results, so in order
      // to get the least significant digit, we need the absolute value.
      output[outputIx] = val.abs() % 10;
    }

    final tmp = input;
    input = output;
    output = tmp;
  }

  final messageStartIx = inputPeriod.take(7).reduce((a, b) => a * 10 + b);
  return input.skip(messageStartIx).take(8).join('');
}

void main() {
  var lines = readRows();
  List<int> input = parseInput(lines[0]);

  print('Phase 1: Ans = ${solvePhase1(input, 100)}');
  print('Phase 2: Ans = ${solvePhase2(input, 100)}');
}
