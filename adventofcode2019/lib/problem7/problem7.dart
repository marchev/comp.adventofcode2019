import 'dart:async';
import 'dart:io';
import 'dart:math';

/// https://adventofcode.com/2019, Day 7
///
/// Task: https://adventofcode.com/2019/day/7
/// Date: 2019-12-07
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 101490, for 0.08s
/// Phase 2 answer: 61019896, for 0.12s

List<String> readRows() =>
    File('lib/problem7/input.txt').readAsLinesSync().toList();

List<int> parseOps(String line) => line.split(',').map(int.parse).toList();

Stream<T> flattenStreams<T>(List<Stream<T>> source) async* {
  for (var stream in source) yield* stream;
}

Stream<T> prependToStream<T>(T first, Stream<T> rest) => flattenStreams([
      Stream.fromIterable([first]),
      rest
    ]);

class Computer {
  final int id;
  final List<int> _mem;
  Stream<int> _input;
  final _output = StreamController<int>.broadcast();

  /// Instruction pointer.
  var i = 0;

  Computer(this.id, List<int> ops, input, int config)
      : _mem = ops.toList(),
        _input = prependToStream(config, input);

  Stream<int> get output => _output.stream;

  void addToInput(Stream<int> more) => _input = flattenStreams([_input, more]);

  /// Runs the program until it halts.
  Future<void> run() async {
    await for (var input in _input) {
      _log('received input $input');
      _onInputProvided(input);
    }
  }

  /// Executes instructions and eventually supplies the [input].
  ///
  /// Stops if another input needs to be provided, or if the program exits.
  void _onInputProvided(int input) {
    var isInputConsumed = false;
    while (i < _mem.length) {
      _log('op: ${_mem[i]}');

      var opCode = _mem[i] % 100;
      var paramModes = _mem[i] ~/ 100;

      switch (opCode) {
        case 1: // +
          var a = _readValue(_mem[i + 1], 0, paramModes);
          var b = _readValue(_mem[i + 2], 1, paramModes);
          _mem[_mem[i + 3]] = a + b;
          i += 4;
          break;
        case 2: // *
          var a = _readValue(_mem[i + 1], 0, paramModes);
          var b = _readValue(_mem[i + 2], 1, paramModes);
          _mem[_mem[i + 3]] = a * b;
          i += 4;
          break;
        case 3: // input
          if (isInputConsumed) return; // another input is needed
          isInputConsumed = true;
          _mem[_mem[i + 1]] = input;
          i += 2;
          break;
        case 4: // output
          final output = _readValue(_mem[i + 1], 0, paramModes);
          _output.add(output);
          _log('output: $output');
          i += 2;
          break;
        case 5: // jump-if-true
          var a = _readValue(_mem[i + 1], 0, paramModes);
          if (a != 0) {
            i = _readValue(_mem[i + 2], 1, paramModes);
          } else {
            i += 3;
          }
          break;
        case 6: // jump-if-false
          var a = _readValue(_mem[i + 1], 0, paramModes);
          if (a == 0) {
            i = _readValue(_mem[i + 2], 1, paramModes);
          } else {
            i += 3;
          }
          break;
        case 7: // less than
          var a = _readValue(_mem[i + 1], 0, paramModes);
          var b = _readValue(_mem[i + 2], 1, paramModes);
          _mem[_mem[i + 3]] = a < b ? 1 : 0;
          i += 4;
          break;
        case 8: // equals
          var a = _readValue(_mem[i + 1], 0, paramModes);
          var b = _readValue(_mem[i + 2], 1, paramModes);
          _mem[_mem[i + 3]] = a == b ? 1 : 0;
          i += 4;
          break;
        case 99:
          _log('normal exit');
          _output.close();
          return;
        default:
          _log('ABNORMAL EXIT');
          _output.close();
          return;
      }
    }
  }

  int _readValue(int paramValue, int paramIx, int paramModes) {
    var isImmediateMode = paramModes ~/ pow(10, paramIx) % 10 == 1;
    return isImmediateMode ? paramValue : _mem[paramValue];
  }

  void _log(String msg) {
//    print('$id: $msg');
  }
}

Future<int> computeForConfig(List<int> ops, List<int> config) async {
  final computers = <Computer>[];
  var nextInput = Stream.fromIterable([0]);
  for (var i = 0; i < config.length; ++i) {
    final c = Computer(i, ops, nextInput, config[i]);
    computers.add(c);
    nextInput = c.output;
  }

  computers.forEach((c) => c.run());
  return await computers.last.output.first;
}

Future<int> computeForLoopConfig(List<int> ops, List<int> config) async {
  final computers = <Computer>[];
  var nextInput = Stream.fromIterable([0]);
  for (var i = 0; i < config.length; ++i) {
    final c = Computer(i, ops, nextInput, config[i]);
    computers.add(c);
    nextInput = c.output;
  }
  computers.first.addToInput(computers.last.output);

  computers.forEach((c) => c.run());
  return await computers.last.output.last;
}

bool nextPermutation(List<int> perm) {
  var i = perm.length - 2;
  while (i >= 0 && perm[i] > perm[i + 1]) i--;
  if (i < 0) return false;

  var j = perm.length - 1;
  while (j > i && perm[j] < perm[i]) --j;

  var tmp = perm[i];
  perm[i] = perm[j];
  perm[j] = tmp;

  for (var a = i + 1, b = perm.length - 1; a < b; ++a, --b) {
    var tmp = perm[a];
    perm[a] = perm[b];
    perm[b] = tmp;
  }
  return true;
}

Future<int> solvePhase1(List<int> ops) async {
  final perm = [0, 1, 2, 3, 4];
  int maxValue;
  do {
//    print('Computing config $perm');
    var value = await computeForConfig(ops, perm);
    maxValue = max(maxValue ?? value, value);
  } while (nextPermutation(perm));
  return maxValue;
}

Future<int> solvePhase2(List<int> ops) async {
  final perm = [5, 6, 7, 8, 9];
  int maxValue;
  do {
//    print('Computing config $perm');
    var value = await computeForLoopConfig(ops, perm);
    maxValue = max(maxValue ?? value, value);
  } while (nextPermutation(perm));
  return maxValue;
}

void main() async {
  var lines = readRows();
  List<int> ops = parseOps(lines[0]);

  print('Phase 1: Ans = ${await solvePhase1(ops)}');
  print('Phase 2: Ans = ${await solvePhase2(ops)}');
}
