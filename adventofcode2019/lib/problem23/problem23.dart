import 'dart:collection';
import 'dart:io';
import 'dart:math' hide min;
import 'package:quiver/iterables.dart';

/// https://adventofcode.com/2019, Day 23
///
/// Task: https://adventofcode.com/2019/day/23
/// Date: 2019-12-26
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Analysis:
/// Simple simulation of a network, running Intcode computers.
///
/// Phase 1 answer: 21089, for 0.1s
/// Phase 2 answer: 16658, for 0.8s

List<String> readRows() =>
    File('lib/problem23/input.txt').readAsLinesSync().toList();

List<int> parseOps(String line) => line.split(',').map(int.parse).toList();

class Computer {
  static const memorySize = 5000;

  /// Called when an input is requested.
  final Function _provideInput;

  /// Called when a new output value is emitted.
  final Function _consumeOutput;

  final List<int> _mem;

  /// Instruction pointer.
  var _i = 0;

  /// Relative base (for relative mode params).
  var _relativeBase = 0;

  Computer(List<int> ops, int provideInput(), void consumeOutput(int))
      : _provideInput = provideInput,
        _consumeOutput = consumeOutput,
        _mem = ops
            .toList()
            .followedBy(List.filled(memorySize - ops.length, 0))
            .toList();

  /// Executes the next instruction of the program. Requests inputs and
  /// supplies outputs through callbacks.
  void runInstruction() {
    _log('op: ${_mem[_i]}');

    var opCode = _mem[_i] % 100;
    var paramModes = _mem[_i] ~/ 100;

    switch (opCode) {
      case 1: // +
        var a = _readValue(_mem[_i + 1], 0, paramModes);
        var b = _readValue(_mem[_i + 2], 1, paramModes);
        _writeValue(a + b, _mem[_i + 3], 2, paramModes);
        _i += 4;
        break;
      case 2: // *
        var a = _readValue(_mem[_i + 1], 0, paramModes);
        var b = _readValue(_mem[_i + 2], 1, paramModes);
        _writeValue(a * b, _mem[_i + 3], 2, paramModes);
        _i += 4;
        break;
      case 3: // input
        final input = _provideInput();
        if (input == null) {
          _log('exiting since null was inputted');
          return;
        }
        _writeValue(input, _mem[_i + 1], 0, paramModes);
        _i += 2;
        break;
      case 4: // output
        final output = _readValue(_mem[_i + 1], 0, paramModes);
        _consumeOutput(output);
        _log('output: $output');
        _i += 2;
        break;
      case 5: // jump-if-true
        var a = _readValue(_mem[_i + 1], 0, paramModes);
        if (a != 0) {
          _i = _readValue(_mem[_i + 2], 1, paramModes);
        } else {
          _i += 3;
        }
        break;
      case 6: // jump-if-false
        var a = _readValue(_mem[_i + 1], 0, paramModes);
        if (a == 0) {
          _i = _readValue(_mem[_i + 2], 1, paramModes);
        } else {
          _i += 3;
        }
        break;
      case 7: // less than
        var a = _readValue(_mem[_i + 1], 0, paramModes);
        var b = _readValue(_mem[_i + 2], 1, paramModes);
        _writeValue(a < b ? 1 : 0, _mem[_i + 3], 2, paramModes);
        _i += 4;
        break;
      case 8: // equals
        var a = _readValue(_mem[_i + 1], 0, paramModes);
        var b = _readValue(_mem[_i + 2], 1, paramModes);
        _writeValue(a == b ? 1 : 0, _mem[_i + 3], 2, paramModes);
        _i += 4;
        break;
      case 9: // set relative base
        _relativeBase += _readValue(_mem[_i + 1], 0, paramModes);
        _i += 2;
        break;
      case 99:
        _log('normal exit');
        return;
      default:
        throw Exception('Unrecognized instruction $opCode');
    }
  }

  int _readValue(int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        return _mem[paramValue];
      case 1: // immediate mode
        return paramValue;
      case 2: // relative mode
        return _mem[_relativeBase + paramValue];
      default:
        throw AssertionError();
    }
  }

  void _writeValue(int value, int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        _mem[paramValue] = value;
        break;
      case 2: // relative mode
        _mem[_relativeBase + paramValue] = value;
        break;
      default:
        throw AssertionError();
    }
  }

  void _log(String msg) {
//    print('computer: $msg');
  }
}

class Nic {
  final _queueByAddress = <int, Queue<int>>{};
  final _numFailedReceivesAfterLastSend = <int, int>{};
  int natX, natY;

  void send(int address, int x, int y) {
    if (address == 255) {
      // Only remember the last values
      natX = x;
      natY = y;
    } else {
      _queueByAddress.putIfAbsent(address, () => Queue())..add(x)..add(y);
      _numFailedReceivesAfterLastSend[address] = 0;
    }
  }

  int receive(int address) {
    final q = _queueByAddress[address];
    if (q == null || q.isEmpty) {
      _numFailedReceivesAfterLastSend[address] =
          1 + (_numFailedReceivesAfterLastSend[address] ?? 0);
      return -1;
    }
    return q.removeFirst();
  }

  int numPending(int address) {
    final q = _queueByAddress[address];
    if (q == null) return 0;
    return q.length;
  }

  bool isIdle() =>
      _queueByAddress.values.every((q) => q.isEmpty) &&
      (min(_numFailedReceivesAfterLastSend.values) ?? 0) > 1;
}

Computer createComputer(List<int> ops, int address, Nic nic) {
  Iterable<int> generateInputs() sync* {
    yield address;
    while (nic.numPending(255) < 2) {
      yield nic.receive(address);
    }
  }

  final outputs = <int>[];
  return Computer(
    ops,
    () {
      final inputsIterator = generateInputs().iterator;
      return () {
        if (!inputsIterator.moveNext()) return null;
        return inputsIterator.current;
      };
    }(),
    (output) {
      outputs.add(output);
      if (outputs.length == 3) {
        nic.send(outputs[0], outputs[1], outputs[2]);
        outputs.clear();
      }
    },
  );
}

int solvePhase1(List<int> ops) {
  final nic = Nic();
  final comps = <Computer>[];
  for (int i = 0; i < 50; ++i) {
    comps.add(createComputer(ops, i, nic));
  }

  while (nic.natY == null) {
    for (var c in comps) {
      c.runInstruction();
    }
  }
  return nic.natY;
}

int solvePhase2(List<int> ops) {
  final nic = Nic();
  final comps = <Computer>[];
  for (int i = 0; i < 50; ++i) {
    comps.add(createComputer(ops, i, nic));
  }

  int lastNatY;
  while (true) {
    for (var c in comps) {
      c.runInstruction();
    }

    if (nic.isIdle()) {
      nic.send(0, nic.natX, nic.natY);

      if (lastNatY == nic.natY) {
        return nic.natY;
      }
      lastNatY = nic.natY;
    }
  }
}

void main() {
  var lines = readRows();
  List<int> ops = parseOps(lines[0]);

  print('Phase 1: Ans = ${solvePhase1(ops)}');
  print('Phase 2: Ans = ${solvePhase2(ops)}');
}
