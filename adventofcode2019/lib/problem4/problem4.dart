/// https://adventofcode.com/2019, Day 4
///
/// Task: https://adventofcode.com/2019/day/4
/// Date: 2019-12-05
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 1033, for 0.07s
/// Phase 2 answer: 670, for 0.07s

const start = 254032;
const end = 789860;

int solvePhase1() {
  var count = 0;
  for (var i = start; i <= end; ++i) {
    var digs = i.toString().split('');
    var hasPairOfSameDigs = false;
    var isIncreasing = true;
    for (var k = 1; k < digs.length; ++k) {
      hasPairOfSameDigs |= digs[k - 1] == digs[k];
      isIncreasing &= digs[k - 1].compareTo(digs[k]) <= 0;
    }
    if (hasPairOfSameDigs && isIncreasing) {
      count++;
    }
  }
  return count;
}

int solvePhase2() {
  var count = 0;
  for (var i = start; i <= end; ++i) {
    var digs = i.toString().split('');
    var hasPairOfSameDigs = false; // but only two consecutive reps, not more
    var isIncreasing = true;
    for (var k = 1; k < digs.length; ++k) {
      hasPairOfSameDigs |= digs[k - 1] == digs[k] &&
          (k == 1 || digs[k - 2] != digs[k]) &&
          (k == digs.length - 1 || digs[k + 1] != digs[k]);
      isIncreasing &= digs[k - 1].compareTo(digs[k]) <= 0;
    }
    if (hasPairOfSameDigs && isIncreasing) {
      count++;
    }
  }
  return count;
}

void main() {
  print('Phase 1: Ans = ${solvePhase1()}');
  print('Phase 2: Ans = ${solvePhase2()}');
}
