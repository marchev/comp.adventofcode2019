import 'dart:io';
import 'dart:math';

/// https://adventofcode.com/2019, Day 13
///
/// Task: https://adventofcode.com/2019/day/13
/// Date: 2019-12-15
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 205, for 0.1s
/// Phase 2 answer: 10292, for 0.1s

List<String> readRows() =>
    File('lib/problem13/input.txt').readAsLinesSync().toList();

List<int> parseOps(String line) => line.split(',').map(int.parse).toList();

class Point {
  final int r, c;

  const Point(this.r, this.c);

  @override
  bool operator ==(Object other) =>
      other is Point && r == other.r && c == other.c;

  @override
  int get hashCode => r * 10000 + c;

  @override
  String toString() => '($r,$c)';
}

class Computer {
  static const memorySize = 100000;

  /// Called when an input is requested.
  final Function _provideInput;

  /// Called when a new output value is emitted.
  final Function _consumeOutput;

  final List<int> _mem;

  /// Instruction pointer.
  var _i = 0;

  /// Relative base (for relative mode params).
  var _relativeBase = 0;

  Computer(List<int> ops, int provideInput(), void consumeOutput(int))
      : _provideInput = provideInput,
        _consumeOutput = consumeOutput,
        _mem = ops
            .toList()
            .followedBy(List.filled(memorySize - ops.length, 0))
            .toList();

  /// Executes the program, requesting inputs and supplying outputs through
  /// callbacks.
  void run() {
    while (_i < _mem.length) {
      _log('op: ${_mem[_i]}');

      var opCode = _mem[_i] % 100;
      var paramModes = _mem[_i] ~/ 100;

      switch (opCode) {
        case 1: // +
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a + b, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 2: // *
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a * b, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 3: // input
          final input = _provideInput();
          _writeValue(input, _mem[_i + 1], 0, paramModes);
          _i += 2;
          break;
        case 4: // output
          final output = _readValue(_mem[_i + 1], 0, paramModes);
          _consumeOutput(output);
          _log('output: $output');
          _i += 2;
          break;
        case 5: // jump-if-true
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          if (a != 0) {
            _i = _readValue(_mem[_i + 2], 1, paramModes);
          } else {
            _i += 3;
          }
          break;
        case 6: // jump-if-false
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          if (a == 0) {
            _i = _readValue(_mem[_i + 2], 1, paramModes);
          } else {
            _i += 3;
          }
          break;
        case 7: // less than
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a < b ? 1 : 0, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 8: // equals
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a == b ? 1 : 0, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 9: // set relative base
          _relativeBase += _readValue(_mem[_i + 1], 0, paramModes);
          _i += 2;
          break;
        case 99:
          _log('normal exit');
          return;
        default:
          throw Exception('Unrecognized instruction $opCode');
      }
    }
  }

  int _readValue(int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        return _mem[paramValue];
      case 1: // immediate mode
        return paramValue;
      case 2: // relative mode
        return _mem[_relativeBase + paramValue];
      default:
        throw AssertionError();
    }
  }

  void _writeValue(int value, int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        _mem[paramValue] = value;
        break;
      case 2: // relative mode
        _mem[_relativeBase + paramValue] = value;
        break;
      default:
        throw AssertionError();
    }
  }

  void _log(String msg) {
//    print('computer: $msg');
  }
}

int solvePhase1(List<int> ops) {
  final grid = <Point, String>{};
  var outputIx = 0;
  int outputR, outputC;

  final comp = Computer(
    ops,
    () => throw AssertionError('No input instructions are anticipated'),
    (output) {
      if (outputIx % 3 == 0) {
        outputC = output;
      } else if (outputIx % 3 == 1) {
        outputR = output;
      } else {
        final p = Point(outputR, outputC);
        switch (output) {
          case 0:
            grid[p] = ' '; // empty
            break;
          case 1:
            grid[p] = 'W'; // wall
            break;
          case 2:
            grid[p] = 'B'; // block
            break;
          case 3:
            grid[p] = 'P'; // paddle
            break;
          case 4:
            grid[p] = 'X'; // ball
            break;
          default:
            throw AssertionError();
        }
      }
      outputIx++;
    },
  );

  comp.run();
  printGrid(grid);

  return grid.values.where((value) => value == 'B').length;
}

int solvePhase2(List<int> ops) {
  final grid = <Point, String>{};
  var outputIx = 0;
  int outputR, outputC;
  Point paddlePos;
  Point ballPos;
  int score;

  // Corrupt memory, in order to play for free.
  ops = ops.toList()..[0] = 2;

  final comp = Computer(
    ops,
    () {
      if (paddlePos == null || ballPos == null) return 0;
      // Try to always position the paddle below the ball, so that the ball can
      // bounce off of it.
      return (ballPos.c - paddlePos.c).sign;
    },
    (output) {
      if (outputIx % 3 == 0) {
        outputC = output;
      } else if (outputIx % 3 == 1) {
        outputR = output;
      } else {
        final p = Point(outputR, outputC);
        if (p.c == -1 && p.r == 0) {
          // Update the score.
          score = output;
        } else {
          // Update tile.
          switch (output) {
            case 0:
              grid[p] = ' '; // empty
              break;
            case 1:
              grid[p] = 'W'; // wall
              break;
            case 2:
              grid[p] = 'B'; // block
              break;
            case 3:
              grid[p] = 'P'; // paddle
              paddlePos = p;
              break;
            case 4:
              grid[p] = 'X'; // ball
              ballPos = p;
              break;
            default:
              throw AssertionError();
          }
        }
      }
      outputIx++;
    },
  );

  comp.run();
  printGrid(grid);

  return score;
}

void printGrid(Map<Point, String> grid) {
  final minR = grid.keys.map((point) => point.r).reduce(min);
  final maxR = grid.keys.map((point) => point.r).reduce(max);
  final minC = grid.keys.map((point) => point.c).reduce(min);
  final maxC = grid.keys.map((point) => point.c).reduce(max);
  for (int r = minR; r <= maxR; ++r) {
    final row = StringBuffer();
    for (int c = minC; c <= maxC; ++c) {
      row.write(grid[Point(r, c)] ?? '.');
    }
    print(row);
  }
}

void main() {
  var lines = readRows();
  List<int> ops = parseOps(lines[0]);

  print('Phase 1: Ans = ${solvePhase1(ops)}');
  print('Phase 2: Ans = ${solvePhase2(ops)}');
}
