import 'dart:io';
import 'dart:math';
import 'package:quiver/check.dart';

/// https://adventofcode.com/2019, Day 19
///
/// Task: https://adventofcode.com/2019/day/19
/// Date: 2019-12-19
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Analysis:
///
/// Phase 1:
/// Simple Intcode program simulation.
///
/// Phase 2:
/// Observe that the beam at each row starts at >= column than it starts at
/// the previous row, and ends at >= column than the previous row ends.
/// This observation leads to a dramatic improvement of the algorithm
/// for finding the beam coverage at each row.
///
/// However, note that it's possible that no integer coordinates are covered
/// by the beam at a given row, even though there are such covered at the
/// previous row. This is a special case for the algorithm, since it relies
/// on knowing the exact start and end columns for each row.
///
/// To mitigate that, we will find the first row that has 2+ consecutive
/// filled cells and will start from it. It can be proven that if a row has 2+
/// consecutive filled cells, the next row would have at least one filled
/// cells, i.e. it wouldn't be empty. (Basically, if the beam is wide enough
/// to cover two consecutive integer coordinates at row R, then it would be
/// certainly wide enough to cover at least 1 integer coordinate at row R+1.)
///
/// Phase 1 answer: 156, for 0.2s
/// Phase 2 answer: 2610980, for 0.1s

List<String> readRows() =>
    File('lib/problem19/input.txt').readAsLinesSync().toList();

List<int> parseOps(String line) => line.split(',').map(int.parse).toList();

class Computer {
  static const memorySize = 500;

  /// Called when an input is requested.
  final Function _provideInput;

  /// Called when a new output value is emitted.
  final Function _consumeOutput;

  final List<int> _mem;

  /// Instruction pointer.
  var _i = 0;

  /// Relative base (for relative mode params).
  var _relativeBase = 0;

  Computer(List<int> ops, int provideInput(), void consumeOutput(int))
      : _provideInput = provideInput,
        _consumeOutput = consumeOutput,
        _mem = ops
            .toList()
            .followedBy(List.filled(memorySize - ops.length, 0))
            .toList();

  /// Executes the program, requesting inputs and supplying outputs through
  /// callbacks.
  void run() {
    while (_i < _mem.length) {
      _log('op: ${_mem[_i]}');

      var opCode = _mem[_i] % 100;
      var paramModes = _mem[_i] ~/ 100;

      switch (opCode) {
        case 1: // +
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a + b, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 2: // *
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a * b, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 3: // input
          final input = _provideInput();
          _writeValue(input, _mem[_i + 1], 0, paramModes);
          _i += 2;
          break;
        case 4: // output
          final output = _readValue(_mem[_i + 1], 0, paramModes);
          _consumeOutput(output);
          _log('output: $output');
          _i += 2;
          break;
        case 5: // jump-if-true
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          if (a != 0) {
            _i = _readValue(_mem[_i + 2], 1, paramModes);
          } else {
            _i += 3;
          }
          break;
        case 6: // jump-if-false
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          if (a == 0) {
            _i = _readValue(_mem[_i + 2], 1, paramModes);
          } else {
            _i += 3;
          }
          break;
        case 7: // less than
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a < b ? 1 : 0, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 8: // equals
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a == b ? 1 : 0, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 9: // set relative base
          _relativeBase += _readValue(_mem[_i + 1], 0, paramModes);
          _i += 2;
          break;
        case 99:
          _log('normal exit');
          return;
        default:
          throw Exception('Unrecognized instruction $opCode');
      }
    }
  }

  int _readValue(int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        return _mem[paramValue];
      case 1: // immediate mode
        return paramValue;
      case 2: // relative mode
        return _mem[_relativeBase + paramValue];
      default:
        throw AssertionError();
    }
  }

  void _writeValue(int value, int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        _mem[paramValue] = value;
        break;
      case 2: // relative mode
        _mem[_relativeBase + paramValue] = value;
        break;
      default:
        throw AssertionError();
    }
  }

  void _log(String msg) {
//    print('computer: $msg');
  }
}

bool isCellFilled(List<int> ops, int r, int c) {
  bool isFilled;
  final comp = Computer(
    ops,
    () {
      var inputIx = 0;
      return () => (inputIx++ == 0) ? c : r;
    }(),
    (output) {
      isFilled = output == 1;
    },
  );
  comp.run();
  return isFilled;
}

void printBeam(List<int> ops, int rows, int cols) {
  for (int r = 0; r < rows; ++r) {
    final str = StringBuffer();
    for (int c = 0; c < cols; ++c) {
      final isFilled = isCellFilled(ops, r, c);
      str.write(isFilled ? '#' : '.');
    }
    print(str);
  }
}

int solvePhase1(List<int> ops) {
  var numFilled = 0;
  for (int r = 0; r < 50; ++r) {
    for (int c = 0; c < 50; ++c) {
      numFilled += isCellFilled(ops, r, c) ? 1 : 0;
    }
  }
  return numFilled;
}

List<int> findFirstRowWithTwoConsecutiveFilledCells(List<int> ops) {
  const searchSpace = 50;

  var lastStartCol = 0;
  for (int r = 0; r < searchSpace; ++r) {
    var c = lastStartCol;
    while (c < searchSpace && !isCellFilled(ops, r, c)) ++c;
    if (c < searchSpace) {
      var numConsecutive = 1;
      while (isCellFilled(ops, r, c + numConsecutive)) ++numConsecutive;
      if (numConsecutive > 1) return [r, c, c + numConsecutive - 1];
      lastStartCol = c;
    }
  }
  throw AssertionError('consider increasing the searchSpace');
}

int solvePhase2(List<int> ops, int squareSize) {
  checkArgument(squareSize >= 2);
//  printBeam(ops, 50, 50);

  // It can be proven that if a row has 2+ consecutive filled cells, then the
  // next row would have at least one fulled cells, i.e. it wouldn't be empty.
  // This helps us apply a more efficient algorithm for finding the beam
  // spread in a given row - by using the spread information computed already
  // for the previous row.
  final start = findFirstRowWithTwoConsecutiveFilledCells(ops);
  final startR = start[0];
  final rowBeamStart = List.filled(startR + 1, 0, growable: true)
    ..[startR] = start[1];
  final rowBeamEnd = List.filled(startR + 1, 0, growable: true)
    ..[startR] = start[2];

  for (var r = startR + 1;; ++r) {
    // Find the first occupied cell in row r.
    var startCol = rowBeamStart[r - 1];
    while (!isCellFilled(ops, r, startCol)) ++startCol;

    // Find the last occupied cell in row r.
    var endCol = max(startCol, rowBeamEnd[r - 1]);
    while (isCellFilled(ops, r, endCol + 1)) ++endCol;

    final squareFirstR = r - squareSize + 1;
    if (squareFirstR >= startR) {
      final maxSquareSize = rowBeamEnd[squareFirstR] - startCol + 1;
      if (maxSquareSize >= squareSize) {
        return 10000 * startCol + squareFirstR;
      }
    }

    rowBeamStart.add(startCol);
    rowBeamEnd.add(endCol);
  }
}

void main() {
  var lines = readRows();
  List<int> ops = parseOps(lines[0]);

  print('Phase 1: Ans = ${solvePhase1(ops)}');
  print('Phase 2: Ans = ${solvePhase2(ops, 100)}');
}
