/// https://adventofcode.com/2019, Day 5
///
/// Task: https://adventofcode.com/2019/day/5
/// Date: 2019-12-05
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 2845163, for 0.05s
/// Phase 2 answer: 9436229, for 0.05s

import 'dart:io';
import 'dart:math';

List<String> readRows() =>
    File('lib/problem5/input.txt').readAsLinesSync().toList();

List<int> parseOps(String line) => line.split(',').map(int.parse).toList();

int solvePhase1(List<int> mem) {
  int readValue(int paramValue, int paramIx, int paramModes) {
    var isImmediateMode = paramModes ~/ pow(10, paramIx) % 10 == 1;
    return isImmediateMode ? paramValue : mem[paramValue];
  }

  int lastOutput;
  for (var i = 0; i < mem.length;) {
    print('op: ${mem[i]}');
    var opCode = mem[i] % 100;
    var paramModes = mem[i] ~/ 100;

    switch (opCode) {
      case 1:
        var a = readValue(mem[i + 1], 0, paramModes);
        var b = readValue(mem[i + 2], 1, paramModes);
        mem[mem[i + 3]] = a + b;
        i += 4;
        break;
      case 2:
        var a = readValue(mem[i + 1], 0, paramModes);
        var b = readValue(mem[i + 2], 1, paramModes);
        mem[mem[i + 3]] = a * b;
        i += 4;
        break;
      case 3:
        mem[mem[i + 1]] = 1; // always input 1
        i += 2;
        break;
      case 4:
        lastOutput = readValue(mem[i + 1], 0, paramModes);
        print('OUT: $lastOutput');
        i += 2;
        break;
      case 99:
        print('EXIT');
        return lastOutput;
      default:
        print('abnormal exit');
        return null;
    }
  }
}

int solvePhase2(List<int> mem, [int input = 5]) {
  int readValue(int paramValue, int paramIx, int paramModes) {
    var isImmediateMode = paramModes ~/ pow(10, paramIx) % 10 == 1;
    return isImmediateMode ? paramValue : mem[paramValue];
  }

  int lastOutput;
  for (var i = 0; i < mem.length;) {
    print('op: ${mem[i]}');
    var opCode = mem[i] % 100;
    var paramModes = mem[i] ~/ 100;

    switch (opCode) {
      case 1:
        var a = readValue(mem[i + 1], 0, paramModes);
        var b = readValue(mem[i + 2], 1, paramModes);
        mem[mem[i + 3]] = a + b;
        i += 4;
        break;
      case 2:
        var a = readValue(mem[i + 1], 0, paramModes);
        var b = readValue(mem[i + 2], 1, paramModes);
        mem[mem[i + 3]] = a * b;
        i += 4;
        break;
      case 3:
        mem[mem[i + 1]] = input; // always input the same number
        i += 2;
        break;
      case 4:
        lastOutput = readValue(mem[i + 1], 0, paramModes);
        print('OUT: $lastOutput');
        i += 2;
        break;
      case 5: // jump-if-true
        var a = readValue(mem[i + 1], 0, paramModes);
        if (a != 0) {
          i = readValue(mem[i + 2], 1, paramModes);
        } else {
          i += 3;
        }
        break;
      case 6: // jump-if-false
        var a = readValue(mem[i + 1], 0, paramModes);
        if (a == 0) {
          i = readValue(mem[i + 2], 1, paramModes);
        } else {
          i += 3;
        }
        break;
      case 7: // less than
        var a = readValue(mem[i + 1], 0, paramModes);
        var b = readValue(mem[i + 2], 1, paramModes);
        mem[mem[i + 3]] = a < b ? 1 : 0;
        i += 4;
        break;
      case 8: // equals
        var a = readValue(mem[i + 1], 0, paramModes);
        var b = readValue(mem[i + 2], 1, paramModes);
        mem[mem[i + 3]] = a == b ? 1 : 0;
        i += 4;
        break;
      case 99:
        print('EXIT');
        return lastOutput;
      default:
        print('abnormal exit');
        return null;
    }
  }
}

void main() {
  var lines = readRows();
  List<int> ops = parseOps(lines[0]);

  print('Phase 1: Ans = ${solvePhase1(ops.toList())}');
  print('Phase 2: Ans = ${solvePhase2(ops.toList())}');
}
