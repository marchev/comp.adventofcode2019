import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:quiver/check.dart';

/// https://adventofcode.com/2019, Day 11
///
/// Task: https://adventofcode.com/2019/day/11
/// Date: 2019-12-14
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 2720, for 0.05s
/// Phase 2 answer: JZPJRAGJ, for 0.05s

List<String> readRows() =>
    File('lib/problem11/input.txt').readAsLinesSync().toList();

List<int> parseOps(String line) => line.split(',').map(int.parse).toList();

class Point {
  final int r, c;

  const Point(this.r, this.c);

  @override
  bool operator ==(Object other) =>
      other is Point && r == other.r && c == other.c;

  @override
  int get hashCode => r * 10000 + c;

  @override
  String toString() => '($r,$c)';
}

class Computer {
  static const memorySize = 100000;

  final List<int> _mem;

  /// Where the output is published.
  ///
  /// Having the stream synchronous ensures that the listeners are executed
  /// right away.
  final _output = StreamController<int>(sync: true);

  /// Instruction pointer.
  var _i = 0;

  /// Relative base (for relative mode params).
  var _relativeBase = 0;

  Computer(List<int> ops)
      : _mem = ops
            .toList()
            .followedBy(List.filled(memorySize - ops.length, 0))
            .toList();

  Stream<int> get output => _output.stream;

  /// Executes instructions and eventually consumes the [input].
  ///
  /// Stops if another input needs to be provided, or if the program exits.
  void onInputProvided(int input) {
    var isInputConsumed = false;
    while (_i < _mem.length) {
//      _log('op: ${_mem[_i]}');

      var opCode = _mem[_i] % 100;
      var paramModes = _mem[_i] ~/ 100;

      switch (opCode) {
        case 1: // +
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a + b, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 2: // *
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a * b, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 3: // input
          if (isInputConsumed) return; // another input is needed
          isInputConsumed = true;
          _writeValue(input, _mem[_i + 1], 0, paramModes);
          _i += 2;
          break;
        case 4: // output
          final output = _readValue(_mem[_i + 1], 0, paramModes);
          _output.add(output);
          _log('output: $output');
          _i += 2;
          break;
        case 5: // jump-if-true
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          if (a != 0) {
            _i = _readValue(_mem[_i + 2], 1, paramModes);
          } else {
            _i += 3;
          }
          break;
        case 6: // jump-if-false
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          if (a == 0) {
            _i = _readValue(_mem[_i + 2], 1, paramModes);
          } else {
            _i += 3;
          }
          break;
        case 7: // less than
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a < b ? 1 : 0, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 8: // equals
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a == b ? 1 : 0, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 9: // set relative base
          _relativeBase += _readValue(_mem[_i + 1], 0, paramModes);
          _i += 2;
          break;
        case 99:
          _log('normal exit');
          _output.close();
          return;
        default:
          _log('ABNORMAL EXIT');
          _output.close();
          return;
      }
    }
  }

  int _readValue(int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        return _mem[paramValue];
      case 1: // immediate mode
        return paramValue;
      case 2: // relative mode
        return _mem[_relativeBase + paramValue];
      default:
        throw AssertionError();
    }
  }

  void _writeValue(int value, int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        _mem[paramValue] = value;
        break;
      case 2: // relative mode
        _mem[_relativeBase + paramValue] = value;
        break;
      default:
        throw AssertionError();
    }
  }

  void _log(String msg) {
//    print('computer: $msg');
  }
}

// 0 - up; 1 - right; 2 - down; 3 - left
final dr = <int>[-1, 0, 1, 0];
final dc = <int>[0, 1, 0, -1];

void runOnGrid(List<int> ops, Map<Point, String> grid) {
  var dir = 0;
  var pos = Point(0, 0);

  final comp = Computer(ops);
  var completed = false;

  var outputIx = 0;
  comp.output.listen((output) {
    if (outputIx % 2 == 0) {
      grid[pos] = output == 0 ? '.' : '#';
//      print('output listener: changing to $pos to ${grid[pos]}');
    } else {
      dir = output == 0 ? (dir - 1 + 4) % 4 : (dir + 1) % 4;
//      print('output listener: changing dir to $dir');
    }
    outputIx++;
  }, onDone: () => completed = true);

  for (int step = 0; !completed && step < 100000; ++step) {
    final curValue = (grid[pos] ?? '.') == '.' ? 0 : 1;
//    print('$step: at pos $pos: adding input value $curValue');

    // Feed the input value. If the computer is to produce an output for it,
    // the output callback will be called *synchronously*, so after this line
    // we can assume that if an output is to be produced, it is both produced
    // and consumed, updating the grid and the current direction.
    comp.onInputProvided(curValue);

    // The current direction has already been updated if an output was produced,
    // so we just step forward in that direction.
    pos = Point(pos.r + dr[dir], pos.c + dc[dir]);
  }
  checkState(completed, message: 'Consider increasing the steps limit');
}

int solvePhase1(List<int> ops) {
  final grid = <Point, String>{};
  runOnGrid(ops, grid);
  return grid.keys.length;
}

void solvePhase2(List<int> ops) {
  final grid = <Point, String>{};
  grid[Point(0, 0)] = '#';
  runOnGrid(ops, grid);

  final minR = grid.keys.map((point) => point.r).reduce(min);
  final maxR = grid.keys.map((point) => point.r).reduce(max);
  final minC = grid.keys.map((point) => point.c).reduce(min);
  final maxC = grid.keys.map((point) => point.c).reduce(max);
  for (int r = minR; r <= maxR; ++r) {
    final row = StringBuffer();
    for (int c = minC; c <= maxC; ++c) {
      row.write(grid[Point(r, c)] ?? '.');
    }
    print(row);
  }
}

void main() async {
  var lines = readRows();
  List<int> ops = parseOps(lines[0]);

  print('Phase 1: Ans = ${solvePhase1(ops)}');

  print('Phase 2 output:');
  solvePhase2(ops);
}
