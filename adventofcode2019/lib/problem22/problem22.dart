import 'dart:io';

import 'package:quiver/check.dart';

/// https://adventofcode.com/2019, Day 22
///
/// Task: https://adventofcode.com/2019/day/22
/// Date: 2019-12-28
/// Author: Svilen Marchev
///
/// Status: works for phase 1
///
/// Analysis:
/// Phase 1: I simulate all card operations, but I keep track only of the
/// position of a single card. This way the time complexity of each operation
/// is O(1).
///
/// Phase 1 answer: 3293, for 0.05s
/// Phase 2 answer: not 110914658417241 (should be smaller), for ?s
///                 not  52903856187612 (should be >)
///                      56362550850882 ??

List<String> readRows() =>
    File('lib/problem22/input.txt').readAsLinesSync().toList();

List<int> parseOps(String line) => line.split(',').map(int.parse).toList();

int solvePhase1(List<String> lines, int numCards, int trackedCard) {
  int pos = trackedCard;
  for (var line in lines) {
    if (line == 'deal into new stack') {
      pos = numCards - 1 - pos;
    } else if (line.startsWith('cut ')) {
      final cut = int.parse(line.substring('cut '.length));
      if (cut >= 0) {
        if (pos >= cut) {
          pos -= cut;
        } else {
          pos += numCards - cut;
        }
      } else {
        if (pos >= numCards + cut) {
          pos -= numCards + cut;
        } else {
          pos += cut.abs();
        }
      }
    } else if (line.startsWith('deal with increment ')) {
      final increment =
          int.parse(line.substring('deal with increment '.length));
      pos = (pos * increment) % numCards;
    } else {
      throw AssertionError('unrecognized command $line');
    }
  }
  return pos;
}

int applyCommands(List<String> lines, int numCards, int pos, bool reversed) {
  final numCardsBigInt = BigInt.from(numCards);
  final order = reversed ? lines.reversed : lines;
  for (var line in order) {
    if (line == 'deal into new stack') {
      pos = numCards - 1 - pos;
    } else if (line.startsWith('cut ')) {
      var cut = int.parse(line.substring('cut '.length));
      if (reversed) cut = -cut;

      if (cut >= 0) {
        if (pos >= cut) {
          pos -= cut;
        } else {
          pos += numCards - cut;
        }
      } else {
        if (pos >= numCards + cut) {
          pos -= numCards + cut;
        } else {
          pos += cut.abs();
        }
      }
    } else if (line.startsWith('deal with increment ')) {
      var increment = int.parse(line.substring('deal with increment '.length));
      if (reversed) increment = increment.modInverse(numCards);

//      pos = (pos * increment) % numCards;
      pos = ((BigInt.from(pos) * BigInt.from(increment)) % numCardsBigInt)
          .toInt();
      checkState(pos >= 0);
    } else {
      throw AssertionError('unrecognized command $line');
    }
  }
  return pos;
}

int solvePhase2(List<String> lines, int numCards, int numReps, int askedPos) {
  var pos = askedPos;
  final positionToSeenAtStep = <int, int>{}..[pos] = 0;

  for (int step = 1; step <= numReps; ++step) {
    pos = applyCommands(lines, numCards, pos, true);
    if (step % 100000 == 0) print('step=$step');

    final seenAtStep = positionToSeenAtStep.putIfAbsent(pos, () => step);
    if (seenAtStep != step) {
      final cycleLength = step - seenAtStep;
      print('cycle of len $cycleLength!');
//      final numRemainingReps = (numReps - seenAtStep) % cycleLength;
      final numRemainingReps = (numReps - step) % cycleLength;
      print(
          'step=$step seenAtStep=$seenAtStep numRemainingReps=$numRemainingReps');
//      if (numRemainingReps ==0) break;
      for (int k = 0; k < numRemainingReps; ++k) {
        pos = applyCommands(lines, numCards, pos, true);
      }
      break;
    }
  }

  return pos;
}

void main() {
  var lines = readRows();

//  print('Phase 1: Ans = ${solvePhase1(lines, 10007, 2019)}');
  print('Phase 2: Ans = ${solvePhase2(lines, 119315717514047, 101741582076661, 2020)}');
}
