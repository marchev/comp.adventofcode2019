import 'dart:io';
import 'dart:math';
import 'package:quiver/check.dart';

/// https://adventofcode.com/2019, Day 15
///
/// Task: https://adventofcode.com/2019/day/15
/// Date: 2019-12-21
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Analysis:
///
/// Phase 1: i-DFS (iterated DFS): We define a function that tells whether
/// the target is reachable within a given number of steps from the origin,
/// and then we use binary search to find the min value of steps needed.
/// 
/// Phase 2: Traverse the whole grid using DFS, so that you get full picture
/// of the grid. Then traverse it using BFS, starting from the point where
/// the oxygen is, and return the largest traversal level.
///
/// Phase 1 answer: 254, for 0.1s
/// Phase 2 answer: 268, for 0.03s

List<String> readRows() =>
    File('lib/problem15/input.txt').readAsLinesSync().toList();

List<int> parseOps(String line) => line.split(',').map(int.parse).toList();

class Point {
  final int r, c;

  const Point(this.r, this.c);

  @override
  bool operator ==(Object other) =>
      other is Point && r == other.r && c == other.c;

  @override
  int get hashCode => r * 10000 + c;

  @override
  String toString() => '($r,$c)';
}

class Computer {
  static const memorySize = 2000;

  /// Called when an input is requested.
  final Function _provideInput;

  /// Called when a new output value is emitted.
  final Function _consumeOutput;

  final List<int> _mem;

  /// Instruction pointer.
  var _i = 0;

  /// Relative base (for relative mode params).
  var _relativeBase = 0;

  Computer(List<int> ops, int provideInput(), void consumeOutput(int))
      : _provideInput = provideInput,
        _consumeOutput = consumeOutput,
        _mem = ops
            .toList()
            .followedBy(List.filled(memorySize - ops.length, 0))
            .toList();

  /// Executes the program, requesting inputs and supplying outputs through
  /// callbacks.
  void run() {
    while (_i < _mem.length) {
      _log('op: ${_mem[_i]}');

      var opCode = _mem[_i] % 100;
      var paramModes = _mem[_i] ~/ 100;

      switch (opCode) {
        case 1: // +
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a + b, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 2: // *
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a * b, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 3: // input
          final input = _provideInput();
          if (input == null) {
            _log('exiting since null was inputted');
            return;
          }
          _writeValue(input, _mem[_i + 1], 0, paramModes);
          _i += 2;
          break;
        case 4: // output
          final output = _readValue(_mem[_i + 1], 0, paramModes);
          _consumeOutput(output);
          _log('output: $output');
          _i += 2;
          break;
        case 5: // jump-if-true
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          if (a != 0) {
            _i = _readValue(_mem[_i + 2], 1, paramModes);
          } else {
            _i += 3;
          }
          break;
        case 6: // jump-if-false
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          if (a == 0) {
            _i = _readValue(_mem[_i + 2], 1, paramModes);
          } else {
            _i += 3;
          }
          break;
        case 7: // less than
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a < b ? 1 : 0, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 8: // equals
          var a = _readValue(_mem[_i + 1], 0, paramModes);
          var b = _readValue(_mem[_i + 2], 1, paramModes);
          _writeValue(a == b ? 1 : 0, _mem[_i + 3], 2, paramModes);
          _i += 4;
          break;
        case 9: // set relative base
          _relativeBase += _readValue(_mem[_i + 1], 0, paramModes);
          _i += 2;
          break;
        case 99:
          _log('normal exit');
          return;
        default:
          throw Exception('Unrecognized instruction $opCode');
      }
    }
  }

  int _readValue(int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        return _mem[paramValue];
      case 1: // immediate mode
        return paramValue;
      case 2: // relative mode
        return _mem[_relativeBase + paramValue];
      default:
        throw AssertionError();
    }
  }

  void _writeValue(int value, int paramValue, int paramIx, int paramModes) {
    switch (paramModes ~/ pow(10, paramIx) % 10) {
      case 0: // position mode
        _mem[paramValue] = value;
        break;
      case 2: // relative mode
        _mem[_relativeBase + paramValue] = value;
        break;
      default:
        throw AssertionError();
    }
  }

  void _log(String msg) {
//    print('computer: $msg');
  }
}

// 0 - up; 1 - down; 2 - left; 3 - right;
final dr = <int>[-1, 1, 0, 0];
final dc = <int>[0, 0, -1, 1];

int oppositeDirectionOf(int k) => k == 0 || k == 2 ? k + 2 : k;

void printGrid(Map<Point, String> grid) {
  final minR = grid.keys.map((point) => point.r).reduce(min);
  final maxR = grid.keys.map((point) => point.r).reduce(max);
  final minC = grid.keys.map((point) => point.c).reduce(min);
  final maxC = grid.keys.map((point) => point.c).reduce(max);
  for (int r = minR; r <= maxR; ++r) {
    final row = StringBuffer();
    for (int c = minC; c <= maxC; ++c) {
      row.write(grid[Point(r, c)] ?? ' ');
    }
    print(row);
  }
}

/// Returns whether the target point is reachable within [maxSteps] steps
/// from the starting position.
bool canReachInGivenSteps(
    List<int> ops, int maxSteps, Map<Point, String> grid) {
  final start = Point(0, 0);
  grid[start] = '.';

  var found = false;
  final added = Set<Point>();
  int lastOutput;

  /// Generates input instructions that walk through the grid in a DFS order,
  /// with a limit on the max traversal depth.
  Iterable<int> generateInputs(Point cur, int remainingLevels) sync* {
    if (remainingLevels <= 0) return;
    if (found) return;
    added.add(cur);
    for (int k = 0; k < 4; ++k) {
      final next = Point(cur.r + dr[k], cur.c + dc[k]);
      if (added.contains(next) || grid[next] == '#') continue;
      yield k + 1;
      final output = lastOutput;
      if (output == 0) {
        grid[next] = '#'; // wall
      } else if (output == 1) {
        grid[next] = '.'; // empty
        yield* generateInputs(next, remainingLevels - 1);

        // Move in the opposite direction, in order to restore the previous
        // position. This causes an output to be produced, but we ignore it.
        yield oppositeDirectionOf(k);
      } else {
        found = true;
      }
    }
  }

  final comp = Computer(
    ops,
    () {
      final inputsIterator = generateInputs(start, maxSteps).iterator;
      return () {
        if (found || !inputsIterator.moveNext()) return null;
        return inputsIterator.current;
      };
    }(),
    (output) {
      lastOutput = output;
    },
  );
  comp.run();

  return found;
}

Point traverse(List<int> ops, Map<Point, String> grid) {
  final start = Point(0, 0);
  grid[start] = '.';

  final added = Set<Point>();
  int lastOutput;
  Point target;

  /// Generates input instructions that walk through the grid in a DFS order.
  Iterable<int> generateInputs(Point cur) sync* {
    added.add(cur);
    for (int k = 0; k < 4; ++k) {
      final next = Point(cur.r + dr[k], cur.c + dc[k]);
      if (added.contains(next) || grid[next] == '#') continue;
      yield k + 1;
      final output = lastOutput;
      if (output == 0) {
        grid[next] = '#'; // wall
      } else if (output == 1 || output == 2) {
        grid[next] = '.'; // empty
        if (output == 2) target = next;
        yield* generateInputs(next);

        // Move in the opposite direction, in order to restore the previous
        // position. This causes an output to be produced, but we ignore it.
        yield oppositeDirectionOf(k);
      }
    }
  }

  final comp = Computer(
    ops,
    () {
      final inputsIterator = generateInputs(start).iterator;
      return () {
        if (!inputsIterator.moveNext()) return null;
        return inputsIterator.current;
      };
    }(),
    (output) {
      lastOutput = output;
    },
  );
  comp.run();

  return target;
}

int solvePhase1(List<int> ops) {
  // This is reused in the checks below - not necessary, but reduces the number
  // of inputs and outputs that need to be exchanged with the Intcode program.
  final grid = <Point, String>{};

  var maxStepsAtWhichNoReachable = 0;
  for (var b = 1 << 8; b > 0; b >>= 1) {
    if (!canReachInGivenSteps(ops, maxStepsAtWhichNoReachable + b, grid)) {
      maxStepsAtWhichNoReachable += b;
    }
  }
  final reachable =
      canReachInGivenSteps(ops, maxStepsAtWhichNoReachable + 1, grid);
  checkState(reachable, message: 'increase upper limit');
//  printGrid(grid);
  return maxStepsAtWhichNoReachable + 1;
}

int solvePhase2(List<int> ops) {
  final grid = <Point, String>{};

  final start = traverse(ops, grid);
  final queue = [start];
  final levels = {start: 0};
  var queueIx = 0;

  while (queueIx < queue.length) {
    final cur = queue[queueIx++];
    for (int k = 0; k < 4; ++k) {
      final next = Point(cur.r + dr[k], cur.c + dc[k]);
      checkState(grid.containsKey(next));
      if (levels.containsKey(next) || grid[next] == '#') continue;
      queue.add(next);
      levels[next] = 1 + levels[cur];
    }
  }
  return levels[queue.last];
}

void main() {
  var lines = readRows();
  List<int> ops = parseOps(lines[0]);

  print('Phase 1: Ans = ${solvePhase1(ops)}');
  print('Phase 2: Ans = ${solvePhase2(ops)}');
}
