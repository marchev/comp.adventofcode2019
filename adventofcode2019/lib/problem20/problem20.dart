import 'dart:io';
import 'package:collection/collection.dart';

/// https://adventofcode.com/2019, Day 20
///
/// Task: https://adventofcode.com/2019/day/20
/// Date: 2019-12-20, 2019-12-25
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: 596, for 0.05s
/// Phase 2 answer: 7610, for 4s

List<String> readRows() =>
    File('lib/problem20/input.txt').readAsLinesSync().toList();

List<int> parseOps(String line) => line.split(',').map(int.parse).toList();

class Point {
  final int r, c;

  const Point(this.r, this.c);

  @override
  bool operator ==(Object other) =>
      other is Point && r == other.r && c == other.c;

  @override
  int get hashCode => r * 10000 + c;

  @override
  String toString() => '($r,$c)';
}

class ReachablePortal {
  final String label;
  final int dist;
  final bool isInnerPortal;

  ReachablePortal(this.label, this.dist, this.isInnerPortal);

  @override
  String toString() => '[$label at $dist; isInnerPortal=$isInnerPortal]';
}

// 0 - up; 1 - right; 2 - down; 3 - left
final dr = <int>[-1, 0, 1, 0];
final dc = <int>[0, 1, 0, -1];

class Edge {
  final String toNode;
  final int dist;

  Edge(this.toNode, this.dist);

  @override
  String toString() => '[to: $toNode, dist: $dist]';
}

class Graph {
  final _outcomingEdges = <String, List<Edge>>{};

  List<Edge> outcomingEdges(String node) => _outcomingEdges[node] ?? [];

  void addEdge(String from, String to, int dist) =>
      _outcomingEdges.putIfAbsent(from, () => []).add(Edge(to, dist));

  @override
  String toString() => '$_outcomingEdges';
}

class Grid {
  final List<List<String>> _g;

  Grid(List<String> lines) : _g = lines.map((line) => line.split('')).toList();

  int get rows => _g.length;

  int get cols => _g[0].length;

  String cell(int r, int c) =>
      (r < 0 || r >= rows || c < 0 || c >= cols) ? ' ' : _g[r][c];

  Graph buildGraph() {
    final graph = Graph();
    for (int r = 0; r < rows; ++r) {
      for (int c = 0; c < cols; ++c) {
        final p = Point(r, c);
        final fromNode = getAssociatedNode(p);
        if (fromNode != null) {
          final reachableNodes = _findReachableNodes(p);
//          print('reachable from ($r,$c): $reachable');
          for (var reachableNode in reachableNodes) {
//            if (fromNode == reachableNode.label) continue;
            final toNode =
                '${reachableNode.label}-${reachableNode.isInnerPortal ? "I" : "E"}';
            graph.addEdge(fromNode, toNode, reachableNode.dist);
          }
          graph.addEdge(
              fromNode, '$fromNode-${_isInnerPortal(p) ? "E" : "I"}', 1);
        }
      }
    }

    return graph;
  }

  /// Creates a node for the purposes of the graph that we build here.
  ///
  /// Each portal label is mapped to two nodes - intern and extern,
  /// and either '-I' or '-E' is appended to the label to build the node,
  /// e.g. 'XP-I' and 'XP-E' for the 'XP' label. Returns null if no label
  /// is associated to the given location.
  String getAssociatedNode(Point p) {
    if (cell(p.r, p.c) != '.') return null;
    for (int k = 0; k < 4; ++k) {
      final nr = p.r + dr[k];
      final nc = p.c + dc[k];
      final nnr = nr + dr[k];
      final nnc = nc + dc[k];
      if (isLetter(cell(nr, nc)) && isLetter(cell(nnr, nnc))) {
        final label = (k == 0 || k == 3)
            ? cell(nnr, nnc) + cell(nr, nc)
            : cell(nr, nc) + cell(nnr, nnc);
        return '$label-${_getPortalType(p)}';
      }
    }
    return null;
  }

  /// Returns 'I' for 'intern' or 'E' for 'extern'.
  String _getPortalType(Point p) => _isInnerPortal(p) ? 'I' : 'E';

  /// Returns a list of reachable nodes.
  List<ReachablePortal> _findReachableNodes(Point start) {
    final reachable = <ReachablePortal>[];

    final queue = [start];
    final distances = [0];
    final added = queue.toSet();

    for (var queueIx = 0; queueIx < queue.length;) {
      final pos = queue[queueIx];
      final dist = distances[queueIx];
      queueIx++;

      final node = getAssociatedNode(pos);
      if (node != null) {
        reachable.add(ReachablePortal(node, dist, _isInnerPortal(pos)));
      }

      for (int k = 0; k < 4; ++k) {
        final nextPos = Point(pos.r + dr[k], pos.c + dc[k]);
        if (added.contains(nextPos)) continue;

        if (cell(nextPos.r, nextPos.c) == '.') {
          queue.add(nextPos);
          distances.add(1 + dist);
          added.add(nextPos);
        }
      }
    }
    return reachable;
  }

  bool _isInnerPortal(Point cur) =>
      !(cur.r == 2 || cur.r == rows - 3 || cur.c == 2 || cur.c == cols - 3);

  static bool isLetter(String val) =>
      'A'.codeUnitAt(0) <= val.codeUnitAt(0) &&
      val.codeUnitAt(0) <= 'Z'.codeUnitAt(0);

  @override
  String toString() {
    final str = StringBuffer();
    for (int r = 0; r < rows; ++r) {
      for (int c = 0; c < cols; ++c) {
        str.write(_g[r][c]);
      }
      str.writeln();
    }
    return str.toString();
  }
}

class QueueEntry implements Comparable<QueueEntry> {
  final String portal;
  final int dist;

  const QueueEntry(this.portal, this.dist);

  @override
  String toString() => '($portal,$dist)';

  @override
  int compareTo(QueueEntry other) => dist.compareTo(other.dist);
}

int solvePhase1(List<String> lines) {
  final grid = Grid(lines);
  final graph = grid.buildGraph();
  print(graph);

  const initialPortal = 'AA-E';
  const endPortal = 'ZZ-E';

  final queue = PriorityQueue<QueueEntry>()..add(QueueEntry(initialPortal, 0));
  final minDistToPortal = <String, int>{}..[initialPortal] = 0;
  final processedPortals = Set<String>();

  while (queue.isNotEmpty) {
    final curEntry = queue.removeFirst();
    if (curEntry.dist != minDistToPortal[curEntry.portal]) continue;
    processedPortals.add(curEntry.portal);

    if (curEntry.portal == endPortal) {
      return curEntry.dist;
    }

    for (Edge edge in graph.outcomingEdges(curEntry.portal)) {
      final nextDist = curEntry.dist + edge.dist;
      if (minDistToPortal[edge.toNode] == null ||
          minDistToPortal[edge.toNode] > nextDist) {
        minDistToPortal[edge.toNode] = nextDist;
        queue.add(QueueEntry(edge.toNode, nextDist));
      }
    }
  }
  throw AssertionError('no path found');
}

int solvePhase2(List<String> lines) {
  final grid = Grid(lines);
  final graph = grid.buildGraph();
  print(graph);

  const initialPortal = 'AA-E-1';
  const endPortal = 'ZZ-E-1';

  final queue = PriorityQueue<QueueEntry>()..add(QueueEntry(initialPortal, 0));
  final minDistToNode = <String, int>{}..[initialPortal] = 0;
  final processedPortals = Set<String>();

  while (queue.isNotEmpty) {
    final curEntry = queue.removeFirst();
    if (curEntry.dist != minDistToNode[curEntry.portal]) continue;
    processedPortals.add(curEntry.portal);

    if (curEntry.portal == endPortal) {
      return curEntry.dist;
    }

    final curNode = curEntry.portal.substring(0, 4);
    final curLabel = curEntry.portal.substring(0, 2);
    final curIsInner = curEntry.portal.substring(3, 4) == 'I';
    final curLevel = int.parse(curEntry.portal.substring(5));
    print('cur: $curEntry ($curLabel,$curLevel,$curIsInner)');

    if (curLevel == 1 && !curIsInner && curLabel != 'AA' && curLabel != 'ZZ') {
      print(' -> invalid, skipping');
      continue;
    }
    if (curLevel > 1 && !curIsInner && (curLabel == 'AA' || curLabel == 'ZZ')) {
      print(' -> invalid, skipping');
      continue;
    }

    for (Edge edge in graph.outcomingEdges(curNode)) {
      final nextDist = curEntry.dist + edge.dist;
      final nextLabel = edge.toNode.substring(0, 2);
      final nextIsInner = edge.toNode.substring(3, 4) == 'I';
      final nextLevel =
          curLevel + (curLabel != nextLabel ? 0 : (nextIsInner ? -1 : 1));
      final nextNode = '${edge.toNode}-$nextLevel';
      if (nextLevel <= 0) continue;

      if (minDistToNode[nextNode] == null ||
          minDistToNode[nextNode] > nextDist) {
        minDistToNode[nextNode] = nextDist;
        queue.add(QueueEntry(nextNode, nextDist));
        print(' -> update $nextNode to $nextDist');
      }
    }
  }
  throw AssertionError('no path found');
}

void main() {
  var lines = readRows();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
