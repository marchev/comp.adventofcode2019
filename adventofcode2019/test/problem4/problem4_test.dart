import 'package:adventofcode2019/problem4/problem4.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given input', () {
      expect(solvePhase1(), equals(1033));
    });
  });

  group('phase 2', () {
    test('with given input', () {
      expect(solvePhase2(), equals(670));
    });
  });
}
