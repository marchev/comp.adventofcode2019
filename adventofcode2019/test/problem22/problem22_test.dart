import 'package:adventofcode2019/problem22/problem22.dart';
import 'package:test/test.dart';

List<int> convertToCardDeck(List<int> cardToPosition) {
  final deck = List.filled(cardToPosition.length, 0);
  for (var card = 0; card < cardToPosition.length; ++card) {
    deck[cardToPosition[card]] = card;
  }
  return deck;
}

void main() {
//  group('phase 1', () {
//    test('with given case 1', () {
//      List<String> lines = [
//        'deal with increment 7',
//        'deal into new stack',
//        'deal into new stack',
//      ];
//
//      final cardToPosition =
//          List.generate(10, (i) => solvePhase1(lines, 10, i));
//
//      expect(convertToCardDeck(cardToPosition),
//          equals([0, 3, 6, 9, 2, 5, 8, 1, 4, 7]));
//    });
//
//    test('with given case 2', () {
//      List<String> lines = [
//        'cut 6',
//        'deal with increment 7',
//        'deal into new stack',
//      ];
//
//      final cardToPosition =
//          List.generate(10, (i) => solvePhase1(lines, 10, i));
//
//      expect(convertToCardDeck(cardToPosition),
//          equals([3, 0, 7, 4, 1, 8, 5, 2, 9, 6]));
//    });
//
//    test('with given case 3', () {
//      List<String> lines = [
//        'deal into new stack',
//        'cut -2',
//        'deal with increment 7',
//        'cut 8',
//        'cut -4',
//        'deal with increment 7',
//        'cut 3',
//        'deal with increment 9',
//        'deal with increment 3',
//        'cut -1',
//      ];
//
//      final cardToPosition =
//          List.generate(10, (i) => solvePhase1(lines, 10, i));
//
//      expect(convertToCardDeck(cardToPosition),
//          equals([9, 2, 5, 8, 1, 4, 7, 0, 3, 6]));
//    });
//
//    test('with file input', () {
//      final lines = readRows();
//
//      expect(solvePhase1(lines, 10007, 2019), equals(3293));
//    });
//  });

  group('phase 2', () {
    test('applyCommands with given case 1', () {
      List<String> lines = [
        'deal into new stack',
        'cut -2',
        'deal with increment 7',
        'cut 8',
        'cut -4',
        'deal with increment 7',
        'cut 3',
        'deal with increment 9',
        'deal with increment 3',
        'cut -1',
      ];

      final positionToCard =
          List.generate(10, (i) => applyCommands(lines, 10, i, true));

      expect(positionToCard, equals([9, 2, 5, 8, 1, 4, 7, 0, 3, 6]));
    });

    test('applyCommands applied multiple times', () {
      List<String> lines = [
        'deal into new stack',
        'cut -2',
        'deal with increment 7',
        'cut 8',
        'cut -4',
        'deal with increment 7',
        'cut 3',
        'deal with increment 9',
        'deal with increment 3',
        'cut -1',
      ];

      final numCards = 10;
      final numReps = 10000;
      for (int i = 0; i < numCards; ++i) {
        int pos = i;
        for (int k = 0; k < numReps; ++k) {
          pos = applyCommands(lines, numCards, pos, false);
        }

        for (int k = 0; k < numReps; ++k) {
          pos = applyCommands(lines, numCards, pos, true);
        }

        expect(pos, i, reason: 'mismatch for i=$i');
      }
    });

    test('with file input but less reps', () {
      final lines = readRows();

//      expect(solvePhase2(lines, 119315717514047, 3000000, 2020),
//          equals(27931873322770));
      expect(solvePhase2(lines, 119315717514047, 30000000, 2020),
          equals(42575528725679));
    });

    test('with file input, against phase 1', () {
      final lines = readRows();
      // cycle lens for:
      // 102103 - cyc len: 3003, k=34
      // 101281 - 33760, k=3
      // 95087 - 47543, k=2
      final numCards = 95087;
      final numReps = 300000;//300000;

      final cardAtPos2020 = solvePhase2(lines, numCards, numReps, 2020);

      int pos = cardAtPos2020;
      for (int k = 0; k < numReps; ++k) {
        pos = applyCommands(lines, numCards, pos, false);
      }
      expect(pos, equals(2020));
    });

    test('with file input', () {
      final lines = readRows();

      expect(solvePhase2(lines, 119315717514047, 101741582076661, 2020),
          equals(-1));
    });
  });
}
