import 'package:adventofcode2019/problem2/problem2.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case 1', () {
      final nums = [1, 0, 0, 0, 99];
      runProgram(nums);
      expect(nums, equals([2, 0, 0, 0, 99]));
    });

    test('with given case 2', () {
      final nums = [2, 3, 0, 3, 99];
      runProgram(nums);
      expect(nums, equals([2, 3, 0, 6, 99]));
    });

    test('with given case 3', () {
      final nums = [2, 4, 4, 5, 99, 0];
      runProgram(nums);
      expect(nums, equals([2, 4, 4, 5, 99, 9801]));
    });

    test('with given case 4', () {
      final nums = [1, 1, 1, 4, 99, 5, 6, 0, 99];
      runProgram(nums);
      expect(nums, equals([30, 1, 1, 4, 2, 5, 6, 0, 99]));
    });

    test('with file input', () {
      expect(solvePhase1(parseNums(readRows())), equals(3058646));
    });
  });

  group('phase 2', () {
    test('with file input', () {
      expect(solvePhase2(parseNums(readRows())), equals(8976));
    });
  });
}
