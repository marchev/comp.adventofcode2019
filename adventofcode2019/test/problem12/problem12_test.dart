import 'package:adventofcode2019/problem12/problem12.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with file input', () {
      final moons = <Moon>[
        Moon(Point(5, 13, -3)),
        Moon(Point(18, -7, 13)),
        Moon(Point(16, 3, 4)),
        Moon(Point(0, 8, 8)),
      ];

      expect(solvePhase1(moons), equals(10198));
    });
  });

  group('phase 2', () {
    test('with given case 1', () {
      final moons = <Moon>[
        Moon(Point(-1, 0, 2)),
        Moon(Point(2, -10, -7)),
        Moon(Point(4, -8, 8)),
        Moon(Point(3, 5, -1)),
      ];

      expect(solvePhase2(moons), equals(2772));
    });

    test('with given case 2', () {
      final moons = <Moon>[
        Moon(Point(-8, -10, 0)),
        Moon(Point(5, 5, 10)),
        Moon(Point(2, -7, 3)),
        Moon(Point(9, -8, -3)),
      ];

      expect(solvePhase2(moons), equals(4686774924));
    });

    test('with file input', () {
      final moons = <Moon>[
        Moon(Point(5, 13, -3)),
        Moon(Point(18, -7, 13)),
        Moon(Point(16, 3, 4)),
        Moon(Point(0, 8, 8)),
      ];

      expect(solvePhase2(moons), equals(271442326847376));
    });
  });
}
