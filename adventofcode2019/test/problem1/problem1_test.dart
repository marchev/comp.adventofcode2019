import 'package:adventofcode2019/problem1/problem1.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      List<int> masses = [12, 14, 1969, 100756];
      expect(solvePhase1(masses), equals(2 + 2 + 654 + 33583));
    });

    test('with file input', () {
      expect(solvePhase1(parseMasses(readRows())), equals(3372463));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      List<int> masses = [12, 14, 1969, 100756];
      expect(solvePhase2(masses), equals(2 + 2 + 966 + 50346));
    });

    test('with file input', () {
      expect(solvePhase2(parseMasses(readRows())), equals(5055835));
    });
  });
}
