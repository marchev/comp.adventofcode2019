import 'package:adventofcode2019/problem6/problem6.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case 1', () {
      final lines = [
        'COM)B',
        'B)C',
        'C)D',
        'D)E',
        'E)F',
        'B)G',
        'G)H',
        'D)I',
        'E)J',
        'J)K',
        'K)L',
      ];

      expect(solvePhase1(lines), equals(42));
    });

    test('with file input', () {
      final lines = readRows();

      expect(solvePhase1(lines), equals(322508));
    });
  });

  group('phase 2', () {
    test('with given case 1', () {
      final lines = [
        'COM)B',
        'B)C',
        'C)D',
        'D)E',
        'E)F',
        'B)G',
        'G)H',
        'D)I',
        'E)J',
        'J)K',
        'K)L',
        'K)YOU',
        'I)SAN',
      ];

      expect(solvePhase2(lines), equals(4));
    });

    test('with file input', () {
      final lines = readRows();

      expect(solvePhase2(lines), equals(496));
    });
  });
}
