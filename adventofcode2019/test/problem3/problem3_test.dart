import 'package:adventofcode2019/problem3/problem3.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case 1', () {
      final dirs1 = ['R8', 'U5', 'L5', 'D3'];
      final dirs2 = ['U7', 'R6', 'D4', 'L4'];

      expect(solvePhase1(dirs1, dirs2), equals(6));
    });

    test('with given case 2', () {
      final dirs1 = [
        'R75',
        'D30',
        'R83',
        'U83',
        'L12',
        'D49',
        'R71',
        'U7',
        'L72'
      ];
      final dirs2 = ['U62', 'R66', 'U55', 'R34', 'D71', 'R55', 'D58', 'R83'];

      expect(solvePhase1(dirs1, dirs2), equals(159));
    });

    test('with file input', () {
      final lines = readRows();
      final dirs1 = parseDirs(lines[0]);
      final dirs2 = parseDirs(lines[1]);

      expect(solvePhase1(dirs1, dirs2), equals(896));
    });
  });

  group('phase 2', () {
    test('with given case 1', () {
      final dirs1 = [
        'R75',
        'D30',
        'R83',
        'U83',
        'L12',
        'D49',
        'R71',
        'U7',
        'L72'
      ];
      final dirs2 = ['U62', 'R66', 'U55', 'R34', 'D71', 'R55', 'D58', 'R83'];

      expect(solvePhase2(dirs1, dirs2), equals(610));
    });

    test('with file input', () {
      final lines = readRows();
      final dirs1 = parseDirs(lines[0]);
      final dirs2 = parseDirs(lines[1]);

      expect(solvePhase2(dirs1, dirs2), equals(16524));
    });
  });
}
