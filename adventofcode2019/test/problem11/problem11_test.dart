import 'package:adventofcode2019/problem11/problem11.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with file input', () {
      final lines = readRows();
      final ops = parseOps(lines[0]);

      expect(solvePhase1(ops), equals(2720));
    });
  });

  group('phase 2', () {
    test('with file input', () {
      final lines = readRows();
      final ops = parseOps(lines[0]);

      solvePhase2(ops);
    });
  });
}
