import 'package:adventofcode2019/problem18/problem18.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case 1', () {
      List<String> lines = [
        '#########',
        '#b.A.@.a#',
        '#########',
      ];

      expect(solvePhase1(lines), equals(8));
    });

    test('with given case 2', () {
      List<String> lines = [
        '########################',
        '#f.D.E.e.C.b.A.@.a.B.c.#',
        '######################.#',
        '#d.....................#',
        '########################',
      ];

      expect(solvePhase1(lines), equals(86));
    });

    test('with given case 3', () {
      List<String> lines = [
        '########################',
        '#...............b.C.D.f#',
        '#.######################',
        '#.....@.a.B.c.d.A.e.F.g#',
        '########################',
      ];

      expect(solvePhase1(lines), equals(132));
    });

    test('with given case 4', () {
      List<String> lines = [
        '#################',
        '#i.G..c...e..H.p#',
        '########.########',
        '#j.A..b...f..D.o#',
        '########@########',
        '#k.E..a...g..B.n#',
        '########.########',
        '#l.F..d...h..C.m#',
        '#################',
      ];

      expect(solvePhase1(lines), equals(136));
    });

    test('with given case 5', () {
      List<String> lines = [
        '########################',
        '#@..............ac.GI.b#',
        '###d#e#f################',
        '###A#B#C################',
        '###g#h#i################',
        '########################',
      ];

      expect(solvePhase1(lines), equals(81));
    });

    test('with file input', () {
      final lines = readRows();

      expect(solvePhase1(lines), equals(6162));
    });
  });

  group('phase 2', () {
    test('with given case 1', () {
      List<String> lines = [
        '#######',
        '#a.#Cd#',
        '##...##',
        '##.@.##',
        '##...##',
        '#cB#Ab#',
        '#######',
      ];

      expect(solvePhase2(lines), equals(8));
    });

    test('with given case 2', () {
      List<String> lines = [
        '###############',
        '#d.ABC.#.....a#',
        '######...######',
        '######.@.######',
        '######...######',
        '#b.....#.....c#',
        '###############',
      ];

      expect(solvePhase2(lines), equals(24));
    });

    test('with given case 3', () {
      List<String> lines = [
        '#############',
        '#DcBa.#.GhKl#',
        '#.###...#I###',
        '#e#d#.@.#j#k#',
        '###C#...###J#',
        '#fEbA.#.FgHi#',
        '#############',
      ];

      expect(solvePhase2(lines), equals(32));
    });

    test('with given case 4', () {
      List<String> lines = [
        '#############',
        '#g#f.D#..h#l#',
        '#F###e#E###.#',
        '#dCba...BcIJ#',
        '#####.@.#####',
        '#nK.L...G...#',
        '#M###N#H###.#',
        '#o#m..#i#jk.#',
        '#############',
      ];

      expect(solvePhase2(lines), equals(72));
    });

    test('with file input', () {
      final lines = readRows();

      expect(solvePhase2(lines), equals(1556));
    });
  });
}
