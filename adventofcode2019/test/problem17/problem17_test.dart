import 'package:adventofcode2019/problem17/problem17.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with file input', () {
      final lines = readRows();
      final ops = parseOps(lines[0]);

      expect(solvePhase1(ops), equals(6672));
    });
  });

  group('phase 2', () {
    test('with file input', () {
      final lines = readRows();
      final ops = parseOps(lines[0]);

      expect(solvePhase2(ops), equals(923017));
    });
  });
}
