import 'package:adventofcode2019/problem15/problem15.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with file input', () {
      final lines = readRows();
      final ops = parseOps(lines[0]);

      expect(solvePhase1(ops), equals(254));
    });
  });

  group('phase 2', () {
    test('with file input', () {
      final lines = readRows();
      final ops = parseOps(lines[0]);

      expect(solvePhase2(ops), equals(268));
    });
  });
}
