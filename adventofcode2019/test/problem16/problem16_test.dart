import 'package:adventofcode2019/problem16/problem16.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case 1', () {
      List<int> input = parseInput('12345678');

      expect(solvePhase1(input, 1), equals('48226158'));
      expect(solvePhase1(input, 2), equals('34040438'));
      expect(solvePhase1(input, 3), equals('03415518'));
      expect(solvePhase1(input, 4), equals('01029498'));
    });

    test('with file input', () {
      var lines = readRows();
      List<int> input = parseInput(lines[0]);

      expect(solvePhase1(input, 100), equals('10189359'));
    });
  });

  group('phase 2', () {
    test('with given case 1', () {
      List<int> input = parseInput('03036732577212944063491565474664');

      expect(solvePhase2(input, 100), equals('84462026'));
    });

    test('with given case 2', () {
      List<int> input = parseInput('02935109699940807407585447034323');

      expect(solvePhase2(input, 100), equals('78725270'));
    });

    test('with given case 3', () {
      List<int> input = parseInput('03081770884921959731165446850517');

      expect(solvePhase2(input, 100), equals('53553731'));
    });

    test('with file input', () {
      var lines = readRows();
      List<int> input = parseInput(lines[0]);

      expect(solvePhase2(input, 100), equals('80722126'));
    });
  });
}
