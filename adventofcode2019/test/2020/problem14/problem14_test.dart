import 'package:adventofcode2019/2020/problem14/problem14.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      expect(
          solvePhase1(<String>[
            'mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X',
            'mem[8] = 11',
            'mem[7] = 101',
            'mem[8] = 0',
          ]),
          equals(165));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(4886706177792));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      expect(
          solvePhase2(<String>[
            'mask = 000000000000000000000000000000X1001X',
            'mem[42] = 100',
            'mask = 00000000000000000000000000000000X0XX',
            'mem[26] = 1',
          ]),
          equals(208));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(3348493585827));
    });
  });
}
