import 'package:adventofcode2019/2020/problem17/problem17.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      expect(
          solvePhase1(<String>[
            '.#.',
            '..#',
            '###',
          ]),
          equals(112));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(426));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      expect(
          solvePhase2(<String>[
            '.#.',
            '..#',
            '###',
          ]),
          equals(848));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(1892));
    });
  });
}
