import 'package:adventofcode2019/2020/problem3/problem3.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      List<String> lines = [
        '..##.......',
        '#...#...#..',
        '.#....#..#.',
        '..#.#...#.#',
        '.#...##..#.',
        '..#.##.....',
        '.#.#.#....#',
        '.#........#',
        '#.##...#...',
        '#...##....#',
        '.#..#...#.#',
      ];
      expect(solvePhase1(lines), equals(7));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(159));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      List<String> lines = [
        '..##.......',
        '#...#...#..',
        '.#....#..#.',
        '..#.#...#.#',
        '.#...##..#.',
        '..#.##.....',
        '.#.#.#....#',
        '.#........#',
        '#.##...#...',
        '#...##....#',
        '.#..#...#.#',
      ];
      expect(solvePhase2(lines), equals(336));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(6419669520));
    });
  });
}
