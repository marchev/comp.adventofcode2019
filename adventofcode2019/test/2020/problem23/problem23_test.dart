import 'package:adventofcode2019/2020/problem23/problem23.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case with 10 moves', () {
      expect(solvePhase1('389125467', moveCount: 10), equals('92658374'));
    });

    test('with given case with 100 moves', () {
      expect(solvePhase1('389125467', moveCount: 100), equals('67384529'));
    });

    test('with real input', () {
      expect(solvePhase1('538914762', moveCount: 100), equals('54327968'));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      expect(solvePhase2('389125467'), equals(149245887792));
    });

    test('with real input', () {
      expect(solvePhase2('538914762'), equals(157410423276));
    });
  });
}
