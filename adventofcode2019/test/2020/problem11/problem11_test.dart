import 'package:adventofcode2019/2020/problem11/problem11.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      List<String> lines = [
        'L.LL.LL.LL',
        'LLLLLLL.LL',
        'L.L.L..L..',
        'LLLL.LL.LL',
        'L.LL.LL.LL',
        'L.LLLLL.LL',
        '..L.L.....',
        'LLLLLLLLLL',
        'L.LLLLLL.L',
        'L.LLLLL.LL',
      ];
      expect(solvePhase1(lines), equals(37));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(2263));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      List<String> lines = [
        'L.LL.LL.LL',
        'LLLLLLL.LL',
        'L.L.L..L..',
        'LLLL.LL.LL',
        'L.LL.LL.LL',
        'L.LLLLL.LL',
        '..L.L.....',
        'LLLLLLLLLL',
        'L.LLLLLL.L',
        'L.LLLLL.LL',
      ];
      expect(solvePhase2(lines), equals(26));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(2002));
    });
  });
}
