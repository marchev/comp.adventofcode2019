import 'package:adventofcode2019/2020/problem2/problem2.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      List<String> lines = [
        '1-3 a: abcde',
        '1-3 b: cdefg',
        '2-9 c: ccccccccc',
      ];
      expect(solvePhase1(lines), equals(2));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(447));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      List<String> lines = [
        '1-3 a: abcde',
        '1-3 b: cdefg',
        '2-9 c: ccccccccc',
      ];
      expect(solvePhase2(lines), equals(1));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(249));
    });
  });
}
