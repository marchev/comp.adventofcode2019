import 'package:adventofcode2019/2020/problem24/problem24.dart';
import 'package:test/test.dart';

final lines = <String>[
  'sesenwnenenewseeswwswswwnenewsewsw',
  'neeenesenwnwwswnenewnwwsewnenwseswesw',
  'seswneswswsenwwnwse',
  'nwnwneseeswswnenewneswwnewseswneseene',
  'swweswneswnenwsewnwneneseenw',
  'eesenwseswswnenwswnwnwsewwnwsene',
  'sewnenenenesenwsewnenwwwse',
  'wenwwweseeeweswwwnwwe',
  'wsweesenenewnwwnwsenewsenwwsesesenwne',
  'neeswseenwwswnwswswnw',
  'nenwswwsewswnenenewsenwsenwnesesenew',
  'enewnwewneswsewnwswenweswnenwsenwsw',
  'sweneswneswneneenwnewenewwneswswnese',
  'swwesenesewenwneswnwwneseswwne',
  'enesenwswwswneneswsenwnewswseenwsese',
  'wnwnesenesenenwwnenwsewesewsesesew',
  'nenewswnwewswnenesenwnesewesw',
  'eneswnwswnwsenenwnwnwwseeswneewsenese',
  'neswnwewnwnwseenwseesewsenwsweewe',
  'wseweeenwnesenwwwswnew',
];

void main() {
  group('phase 1', () {
    test('with given case', () {
      expect(solvePhase1(lines), equals(10));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(549));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      expect(solvePhase2(lines), equals(2208));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(4147));
    });
  });
}
