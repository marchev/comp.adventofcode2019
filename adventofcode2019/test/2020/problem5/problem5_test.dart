import 'package:adventofcode2019/2020/problem5/problem5.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given single cases', () {
      expect(solvePhase1(['FBFBBFFRLR']), equals(357));
      expect(solvePhase1(['BFFFBBFRRR']), equals(567));
      expect(solvePhase1(['FFFBBBFRRR']), equals(119));
      expect(solvePhase1(['BBFFBBFRLL']), equals(820));
    });

    test('with given multiple cases', () {
      List<String> lines = [
        'FBFBBFFRLR',
        'BFFFBBFRRR',
        'FFFBBBFRRR',
        'BBFFBBFRLL'
      ];
      expect(solvePhase1(lines), equals(820));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(842));
    });
  });

  group('phase 2', () {
    test('with file input', () {
      expect(solvePhase2(readRows()), equals(617));
    });
  });
}
