import 'package:adventofcode2019/2020/problem25/problem25.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      expect(
          solvePhase1(5764801, 17807724),
          equals(14897079));
    });

    test('with real input', () {
      expect(solvePhase1(11349501, 5107328), equals(7936032));
    });
  });

  // group('phase 2', () {
  //   test('with given case', () {
  //     expect(
  //         solvePhase2(<String>[
  //           'nop +0',
  //           'acc +1',
  //           'jmp +4',
  //           'acc +3',
  //           'jmp -3',
  //           'acc -99',
  //           'acc +1',
  //           'jmp -4',
  //           'acc +6',
  //         ]),
  //         equals(8));
  //   });
  //
  //   test('with file input', () {
  //     expect(solvePhase2(readRows()), equals(1626));
  //   });
  // });
}
