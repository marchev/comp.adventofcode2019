import 'package:adventofcode2019/2020/problem10/problem10.dart';
import 'package:test/test.dart';

void main() {
  const List<String> case1Lines = [
    '16',
    '10',
    '15',
    '5',
    '1',
    '11',
    '7',
    '19',
    '6',
    '12',
    '4',
  ];
  const List<String> case2Lines = [
    '28',
    '33',
    '18',
    '42',
    '31',
    '14',
    '46',
    '20',
    '48',
    '47',
    '24',
    '23',
    '49',
    '45',
    '19',
    '38',
    '39',
    '11',
    '1',
    '32',
    '25',
    '35',
    '8',
    '17',
    '7',
    '9',
    '4',
    '2',
    '34',
    '10',
    '3',
  ];

  group('phase 1', () {
    test('with given case 1', () {
      expect(solvePhase1(case1Lines), equals(35));
    });

    test('with given case 2', () {
      expect(solvePhase1(case2Lines), equals(220));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(1885));
    });
  });

  group('phase 2', () {
    test('with given case 1', () {
      expect(solvePhase2(case1Lines), equals(8));
    });

    test('with given case 2', () {
      expect(solvePhase2(case2Lines), equals(19208));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(2024782584832));
    });
  });
}
