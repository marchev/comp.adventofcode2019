import 'package:adventofcode2019/2020/problem22/problem22.dart';
import 'package:test/test.dart';

final lines = <String>[
  'Player 1:',
  '9',
  '2',
  '6',
  '3',
  '1',
  '',
  'Player 2:',
  '5',
  '8',
  '4',
  '7',
  '10',
];

void main() {
  group('phase 1', () {
    test('with given case', () {
      expect(solvePhase1(lines), equals(306));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(32815));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      expect(solvePhase2(lines), equals(291));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(30695));
    });
  });
}
