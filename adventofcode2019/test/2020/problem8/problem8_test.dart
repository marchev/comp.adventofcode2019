import 'package:adventofcode2019/2020/problem8/problem8.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      expect(
          solvePhase1(<String>[
            'nop +0',
            'acc +1',
            'jmp +4',
            'acc +3',
            'jmp -3',
            'acc -99',
            'acc +1',
            'jmp -4',
            'acc +6',
          ]),
          equals(5));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(1394));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      expect(
          solvePhase2(<String>[
            'nop +0',
            'acc +1',
            'jmp +4',
            'acc +3',
            'jmp -3',
            'acc -99',
            'acc +1',
            'jmp -4',
            'acc +6',
          ]),
          equals(8));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(1626));
    });
  });
}
