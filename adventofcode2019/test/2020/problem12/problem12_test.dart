import 'package:adventofcode2019/2020/problem12/problem12.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      expect(
          solvePhase1(<String>[
            'F10',
            'N3',
            'F7',
            'R90',
            'F11',
          ]),
          equals(25));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(415));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      expect(
          solvePhase2(<String>[
            'F10',
            'N3',
            'F7',
            'R90',
            'F11',
          ]),
          equals(286));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(29401));
    });
  });
}
