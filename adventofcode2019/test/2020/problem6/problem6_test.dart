import 'package:adventofcode2019/2020/problem6/problem6.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      List<String> lines = [
        'abc',
        '',
        'a',
        'b',
        'c',
        '',
        'ab',
        'ac',
        '',
        'a',
        'a',
        'a',
        'a',
        '',
        'b',
      ];
      expect(solvePhase1(lines), equals(11));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(7283));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      List<String> lines = [
        'abc',
        '',
        'a',
        'b',
        'c',
        '',
        'ab',
        'ac',
        '',
        'a',
        'a',
        'a',
        'a',
        '',
        'b',
      ];
      expect(solvePhase2(lines), equals(6));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(3520));
    });
  });
}
