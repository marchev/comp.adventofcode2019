import 'package:adventofcode2019/2020/problem7/problem7.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      expect(
          solvePhase1(<String>[
            'light red bags contain 1 bright white bag, 2 muted yellow bags.',
            'dark orange bags contain 3 bright white bags, 4 muted yellow bags.',
            'bright white bags contain 1 shiny gold bag.',
            'muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.',
            'shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.',
            'dark olive bags contain 3 faded blue bags, 4 dotted black bags.',
            'vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.',
            'faded blue bags contain no other bags.',
            'dotted black bags contain no other bags.',
          ]),
          equals(4));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(268));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      expect(
          solvePhase2(<String>[
            'light red bags contain 1 bright white bag, 2 muted yellow bags.',
            'dark orange bags contain 3 bright white bags, 4 muted yellow bags.',
            'bright white bags contain 1 shiny gold bag.',
            'muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.',
            'shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.',
            'dark olive bags contain 3 faded blue bags, 4 dotted black bags.',
            'vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.',
            'faded blue bags contain no other bags.',
            'dotted black bags contain no other bags.',
          ]),
          equals(32));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(7867));
    });
  });
}
