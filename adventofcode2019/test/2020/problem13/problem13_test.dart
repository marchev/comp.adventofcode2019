import 'package:adventofcode2019/2020/problem13/problem13.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      expect(
          solvePhase1(<String>[
            '939',
            '7,13,x,x,59,x,31,19',
          ]),
          equals(295));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(2382));
    });
  });

  group('phase 2', () {
    test('with given case 1', () {
      expect(
          solvePhase2(<String>['939', '7,13,x,x,59,x,31,19']), equals(1068781));
    });

    test('with given case 2', () {
      expect(solvePhase2(<String>['939', '17,x,13,19']), equals(3417));
    });

    test('with given case 3', () {
      expect(solvePhase2(<String>['939', '67,7,59,61']), equals(754018));
    });

    test('with given case 4', () {
      expect(solvePhase2(<String>['939', '67,x,7,59,61']), equals(779210));
    });

    test('with given case 5', () {
      expect(solvePhase2(<String>['939', '67,7,x,59,61']), equals(1261476));
    });

    test('with given case 6', () {
      expect(
          solvePhase2(<String>['939', '1789,37,47,1889']), equals(1202161486));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(906332393333683));
    });
  });
}
