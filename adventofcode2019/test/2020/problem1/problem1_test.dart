import 'package:adventofcode2019/2020/problem1/problem1.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      List<int> nums = [1721, 979, 366, 299, 675, 1456];
      expect(solvePhase1(nums), equals(514579));
    });

    test('with file input', () {
      expect(solvePhase1(parseNums(readRows())), equals(800139));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      List<int> nums = [1721, 979, 366, 299, 675, 1456];
      expect(solvePhase2(nums), equals(241861950));
    });

    test('with file input', () {
      expect(solvePhase2(parseNums(readRows())), equals(59885340));
    });
  });
}
