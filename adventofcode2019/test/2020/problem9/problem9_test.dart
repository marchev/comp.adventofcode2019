import 'package:adventofcode2019/2020/problem9/problem9.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      List<String> lines = [
        '35',
        '20',
        '15',
        '25',
        '47',
        '40',
        '62',
        '55',
        '65',
        '95',
        '102',
        '117',
        '150',
        '182',
        '127',
        '219',
        '299',
        '277',
        '309',
        '576',
      ];
      expect(solvePhase1(lines, lookBackCount: 5), equals(127));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(36845998));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      List<String> lines = [
        '35',
        '20',
        '15',
        '25',
        '47',
        '40',
        '62',
        '55',
        '65',
        '95',
        '102',
        '117',
        '150',
        '182',
        '127',
        '219',
        '299',
        '277',
        '309',
        '576',
      ];
      expect(solvePhase2(lines, lookBackCount: 5), equals(62));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(4830226));
    });
  });
}
