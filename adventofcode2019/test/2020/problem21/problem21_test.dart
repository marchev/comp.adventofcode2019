import 'package:adventofcode2019/2020/problem21/problem21.dart';
import 'package:test/test.dart';

final lines = <String>[
  'mxmxvkd kfcds sqjhc nhms (contains dairy, fish)',
  'trh fvjkl sbzzf mxmxvkd (contains dairy)',
  'sqjhc fvjkl (contains soy)',
  'sqjhc mxmxvkd sbzzf (contains fish)',
];

void main() {
  group('phase 1', () {
    test('with given case', () {
      expect(solvePhase1(lines), equals(5));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(2410));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      expect(solvePhase2(lines), equals('mxmxvkd,sqjhc,fvjkl'));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()),
          equals('tmp,pdpgm,cdslv,zrvtg,ttkn,mkpmkx,vxzpfp,flnhl'));
    });
  });
}
