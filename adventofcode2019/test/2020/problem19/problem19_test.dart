import 'package:adventofcode2019/2020/problem19/problem19.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      expect(
          solvePhase1(<String>[
            '0: 4 1 5',
            '1: 2 3 | 3 2',
            '2: 4 4 | 5 5',
            '3: 4 5 | 5 4',
            '4: "a"',
            '5: "b"',
            '',
            'ababbb',
            'bababa',
            'abbbab',
            'aaabbb',
            'aaaabbb',
          ]),
          equals(2));
    });

    test('with given case (simplified rules)', () {
      expect(
          solvePhase1(<String>[
            '0: 4 1',
            '1: 2 3 | 3 2',
            '2: 4 4 | 5 5',
            '3: 4 5 | 5 4',
            '4: "a"',
            '5: "b"',
            '',
            'ababb',
            'babab',
            'abbba',
            'aaabb',
            'aaaabb',
          ]),
          equals(2));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(178));
    });
  });

  group('phase 2', () {
    test('with file input', () {
      expect(solvePhase2(readRows()), equals(346));
    });
  });
}
