import 'package:adventofcode2019/2020/problem15/problem15.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given cases', () {
      expect(solvePhase1(<int>[0, 3, 6]), equals(436));
      expect(solvePhase1(<int>[1, 3, 2]), equals(1));
      expect(solvePhase1(<int>[2, 1, 3]), equals(10));
      expect(solvePhase1(<int>[1, 2, 3]), equals(27));
      expect(solvePhase1(<int>[2, 3, 1]), equals(78));
      expect(solvePhase1(<int>[3, 2, 1]), equals(438));
      expect(solvePhase1(<int>[3, 1, 2]), equals(1836));
    });

    test('with file input', () {
      expect(solvePhase1(<int>[11, 18, 0, 20, 1, 7, 16]), equals(639));
    });
  });

  group('phase 2', () {
    test('with given case 1', () {
      expect(solvePhase2(<int>[0, 3, 6]), equals(175594));
    });

    test('with given case 2', () {
      expect(solvePhase2(<int>[1, 3, 2]), equals(2578));
    });

    test('with given case 3', () {
      expect(solvePhase2(<int>[2, 1, 3]), equals(3544142));
    });

    test('with given case 4', () {
      expect(solvePhase2(<int>[1, 2, 3]), equals(261214));
    });

    test('with given case 5', () {
      expect(solvePhase2(<int>[2, 3, 1]), equals(6895259));
    });
    test('with given case 6', () {
      expect(solvePhase2(<int>[3, 2, 1]), equals(18));
    });

    test('with given case 7', () {
      expect(solvePhase2(<int>[3, 1, 2]), equals(362));
    });

    test('with file input', () {
      expect(solvePhase2(<int>[11, 18, 0, 20, 1, 7, 16]), equals(266));
    });
  });
}
