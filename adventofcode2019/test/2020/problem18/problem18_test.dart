import 'package:adventofcode2019/2020/problem18/problem18.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      expect(
          solvePhase1(<String>[
            '2 * 3 + (4 * 5)',
            '5 + (8 * 3 + 9 + 3 * 4 * 3)',
            '5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))',
            '((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2',
          ]),
          equals(26 + 437 + 12240 + 13632));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(1451467526514));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      expect(
          solvePhase2(<String>[
            '1 + (2 * 3) + (4 * (5 + 6))',
            '2 * 3 + (4 * 5)',
            '5 + (8 * 3 + 9 + 3 * 4 * 3)',
            '5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))',
            '((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2',
          ]),
          equals(51 + 46 + 1445 + 669060 + 23340));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(224973686321527));
    });
  });
}
