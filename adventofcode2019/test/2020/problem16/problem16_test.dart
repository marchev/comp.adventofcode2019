import 'package:adventofcode2019/2020/problem16/problem16.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      expect(
          solvePhase1(<String>[
            'class: 1-3 or 5-7',
            'row: 6-11 or 33-44',
            'seat: 13-40 or 45-50',
            '',
            'your ticket:',
            '7,1,14',
            '',
            'nearby tickets:',
            '7,3,47',
            '40,4,50',
            '55,2,20',
            '38,6,12',
          ]),
          equals(71));
    });

    test('with file input', () {
      expect(solvePhase1(readRows()), equals(21996));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      expect(
          solvePhase2(<String>[
            'departure class: 0-1 or 4-19',
            'departure row: 0-5 or 8-19',
            'departure seat: 0-13 or 16-19',
            'another: 80-90 or 95-100',
            '',
            'your ticket:',
            '11,12,13,90',
            '',
            'nearby tickets:',
            '3,9,18,85',
            '15,1,5,100',
            '5,14,9,96',
          ]),
          equals(11 * 12 * 13));
    });

    test('with file input', () {
      expect(solvePhase2(readRows()), equals(650080463519));
    });
  });
}
