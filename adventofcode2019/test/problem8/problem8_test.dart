import 'package:adventofcode2019/problem8/problem8.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with file input', () async {
      final lines = readRows();

      expect(solvePhase1(lines[0]), equals(2080));
    });
  });

  group('phase 2', () {
    test('with file input', () async {
      final lines = readRows();

      // No assertions applicable, but make sure it doesn't throw
      try {
        solvePhase2(lines[0]);
      } catch (e) {
        fail(e);
      }
    });
  });
}
