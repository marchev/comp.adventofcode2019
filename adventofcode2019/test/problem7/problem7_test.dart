import 'package:adventofcode2019/problem7/problem7.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case 1', () async {
      final ops = parseOps('3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0');

      expect(await computeForConfig(ops, [4, 3, 2, 1, 0]), equals(43210));
      expect(await solvePhase1(ops), equals(43210));
    });

    test('with given case 2', () async {
      final ops = parseOps(
          '3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0');

      expect(await computeForConfig(ops, [0, 1, 2, 3, 4]), equals(54321));
      expect(await solvePhase1(ops), equals(54321));
    });

    test('with given case 3', () async {
      final ops = parseOps(
          '3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0');

      expect(await computeForConfig(ops, [1, 0, 4, 3, 2]), equals(65210));
      expect(await solvePhase1(ops), equals(65210));
    });

    test('with file input', () async {
      final lines = readRows();
      final ops = parseOps(lines[0]);

      expect(await solvePhase1(ops), equals(101490));
    });
  });

  group('phase 2', () {
    test('with given case 1', () async {
      final ops = parseOps(
          '3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5');

      expect(
          await computeForLoopConfig(ops, [9, 8, 7, 6, 5]), equals(139629729));
      expect(await solvePhase2(ops), equals(139629729));
    });

    test('with given case 2', () async {
      final ops = parseOps(
          '3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54, -5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10');

      expect(await computeForLoopConfig(ops, [9, 7, 8, 5, 6]), equals(18216));
      expect(await solvePhase2(ops), equals(18216));
    });

    test('with file input', () async {
      final lines = readRows();
      final ops = parseOps(lines[0]);

      expect(await solvePhase2(ops), equals(61019896));
    });
  });
}
