import 'package:adventofcode2019/problem5/problem5.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with file input', () {
      final lines = readRows();
      final ops = parseOps(lines[0]);

      expect(solvePhase1(ops), equals(2845163));
    });
  });

  group('phase 2', () {
    test('with given case 1 (positional,=8)', () {
      final ops = parseOps('3,9,8,9,10,9,4,9,99,-1,8');
      expect(solvePhase2(ops, 7), equals(0));
      expect(solvePhase2(ops, 8), equals(1));
    });

    test('with given case 2 (positional,<8)', () {
      final ops = parseOps('3,9,7,9,10,9,4,9,99,-1,8');
      expect(solvePhase2(ops, 7), equals(1));
      expect(solvePhase2(ops, 8), equals(0));
    });

    test('with given case 3 (immediate,=8)', () {
      final ops = parseOps('3,3,1108,-1,8,3,4,3,99');
      expect(solvePhase2(ops, 7), equals(0));
      expect(solvePhase2(ops, 8), equals(1));
    });

    test('with given case 4 (immediate,<8)', () {
      final ops = parseOps('3,3,1107,-1,8,3,4,3,99');
      expect(solvePhase2(ops, 7), equals(1));
      expect(solvePhase2(ops, 8), equals(0));
    });

    test('with given case: jumps', () {
      final ops = parseOps(
          '3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99');
      expect(solvePhase2(ops, 7), equals(999));
      expect(solvePhase2(ops, 8), equals(1000));
      expect(solvePhase2(ops, 9), equals(1001));
    });

    test('with file input', () {
      final lines = readRows();
      final ops = parseOps(lines[0]);

      expect(solvePhase2(ops), equals(9436229));
    });
  });
}
