import 'package:adventofcode2019/problem10/problem10.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case 1', () {
      final lines = [
        '......#.#.',
        '#..#.#....',
        '..#######.',
        '.#.#.###..',
        '.#..#.....',
        '..#....#.#',
        '#..#....#.',
        '.##.#..###',
        '##...#..#.',
        '.#....####',
      ];

      expect(solvePhase1(lines).numVisible, equals(33));
    });

    test('with given case 2', () {
      final lines = [
        '#.#...#.#.',
        '.###....#.',
        '.#....#...',
        '##.#.#.#.#',
        '....#.#.#.',
        '.##..###.#',
        '..#...##..',
        '..##....##',
        '......#...',
        '.####.###.',
      ];

      expect(solvePhase1(lines).numVisible, equals(35));
    });

    test('with given case 3', () {
      final lines = [
        '.#..#..###',
        '####.###.#',
        '....###.#.',
        '..###.##.#',
        '##.##.#.#.',
        '....###..#',
        '..#.#..#.#',
        '#..#.#.###',
        '.##...##.#',
        '.....#.#..',
      ];

      expect(solvePhase1(lines).numVisible, equals(41));
    });

    test('with given case 4', () {
      final lines = [
        '.#..##.###...#######',
        '##.############..##.',
        '.#.######.########.#',
        '.###.#######.####.#.',
        '#####.##.#.##.###.##',
        '..#####..#.#########',
        '####################',
        '#.####....###.#.#.##',
        '##.#################',
        '#####.##.###..####..',
        '..######..##.#######',
        '####.##.####...##..#',
        '.#####..#.######.###',
        '##...#.##########...',
        '#.##########.#######',
        '.####.#.###.###.#.##',
        '....##.##.###..#####',
        '.#.#.###########.###',
        '#.#.#.#####.####.###',
        '###.##.####.##.#..##',
      ];

      expect(solvePhase1(lines).numVisible, equals(210));
    });

    test('with file input', () {
      final lines = readRows();

      expect(solvePhase1(lines).numVisible, equals(221));
    });
  });

  group('phase 2', () {
    test('with given case 1', () {
      final lines = [
        '.#....#####...#..',
        '##...##.#####..##',
        '##...#...#.#####.',
        '..#.....#...###..',
        '..#.#.....#....##',
      ];

      final order = computeOrderOfRemoval(lines);

      // Layer 1
      expect(order.getRange(0, 9), <Point>[
        Point(8, 1),
        Point(9, 0),
        Point(9, 1),
        Point(10, 0),
        Point(9, 2),
        Point(11, 1),
        Point(12, 1),
        Point(11, 2),
        Point(15, 1),
      ]);
      // Layer 2
      expect(order.getRange(9, 18), <Point>[
        Point(12, 2),
        Point(13, 2),
        Point(14, 2),
        Point(15, 2),
        Point(12, 3),
        Point(16, 4),
        Point(15, 4),
        Point(10, 4),
        Point(4, 4),
      ]);
      // Layer 3
      expect(order.getRange(18, 27), <Point>[
        Point(2, 4),
        Point(2, 3),
        Point(0, 2),
        Point(1, 2),
        Point(0, 1),
        Point(1, 1),
        Point(5, 2),
        Point(1, 0),
        Point(5, 1),
      ]);
      // Layer 4
      expect(order.getRange(27, 36), <Point>[
        Point(6, 1),
        Point(6, 0),
        Point(7, 0),
        Point(8, 0),
        Point(10, 1),
        Point(14, 0),
        Point(16, 1),
        Point(13, 3),
        Point(14, 3),
      ]);
    });

    test('with given case 2', () {
      final lines = [
        '.#..##.###...#######',
        '##.############..##.',
        '.#.######.########.#',
        '.###.#######.####.#.',
        '#####.##.#.##.###.##',
        '..#####..#.#########',
        '####################',
        '#.####....###.#.#.##',
        '##.#################',
        '#####.##.###..####..',
        '..######..##.#######',
        '####.##.####...##..#',
        '.#####..#.######.###',
        '##...#.##########...',
        '#.##########.#######',
        '.####.#.###.###.#.##',
        '....##.##.###..#####',
        '.#.#.###########.###',
        '#.#.#.#####.####.###',
        '###.##.####.##.#..##',
      ];

      expect(solvePhase2(lines, 1), equals(1112));
      expect(solvePhase2(lines, 2), equals(1201));
      expect(solvePhase2(lines, 3), equals(1202));
      expect(solvePhase2(lines, 10), equals(1208));
      expect(solvePhase2(lines, 20), equals(1600));
      expect(solvePhase2(lines, 50), equals(1609));
      expect(solvePhase2(lines, 100), equals(1016));
      expect(solvePhase2(lines, 199), equals(906));
      expect(solvePhase2(lines, 200), equals(802));
      expect(solvePhase2(lines, 201), equals(1009));
      expect(solvePhase2(lines, 299), equals(1101));
    });

    test('with file input', () {
      final lines = readRows();

      expect(solvePhase2(lines), equals(806));
    });
  });
}
