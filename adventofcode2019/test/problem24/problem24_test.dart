import 'package:adventofcode2019/problem24/problem24.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case 1', () {
      List<String> lines = [
        '....#',
        '#..#.',
        '#..##',
        '..#..',
        '#....',
      ];

      expect(solvePhase1(lines), equals(2129920));
    });

    test('with file input', () {
      final lines = readRows();

      expect(solvePhase1(lines), equals(32506911));
    });
  });

  group('phase 2', () {
    test('with given case 1', () {
      List<String> lines = [
        '....#',
        '#..#.',
        '#.?##',
        '..#..',
        '#....',
      ];

      expect(solvePhase2(lines, 10), equals(99));
    });

    test('with file input', () {
      final lines = readRows();

      expect(solvePhase2(lines, 200), equals(2025));
    });
  });
}
