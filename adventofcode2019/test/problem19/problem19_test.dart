import 'package:adventofcode2019/problem19/problem19.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with file input', () {
      final lines = readRows();
      final ops = parseOps(lines[0]);

      expect(solvePhase1(ops), equals(156));
    });
  });

  group('phase 2', () {
    test('with file input', () {
      final lines = readRows();
      final ops = parseOps(lines[0]);

      expect(solvePhase2(ops, 100), equals(2610980));
    });
  });
}
