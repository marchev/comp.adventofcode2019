import 'package:adventofcode2019/problem9/problem9.dart';
import 'package:test/test.dart';

void main() {
  group('phase 1', () {
    test('with given case 1', () async {
      final ops =
          parseOps('109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99');

      expect(await computeForConfig(ops, 1),
          equals('109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99'));
    });

    test('with given case 2', () async {
      final ops = parseOps('1102,34915192,34915192,7,4,7,99,0');

      expect(await computeForConfig(ops, 1), equals('1219070632396864'));
    });

    test('with given case 3', () async {
      final ops = parseOps('104,1125899906842624,99');

      expect(await computeForConfig(ops, 1), equals('1125899906842624'));
    });

    test('with file input', () async {
      final lines = readRows();
      final ops = parseOps(lines[0]);

      expect(await solvePhase1(ops), equals('3780860499'));
    });
  });

  group('phase 2', () {
    test('with file input', () async {
      final lines = readRows();
      final ops = parseOps(lines[0]);

      expect(await solvePhase2(ops), equals('33343'));
    });
  });
}
