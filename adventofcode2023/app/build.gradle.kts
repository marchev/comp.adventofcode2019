/*
 * For more details take a look at the 'Building Java & JVM projects' chapter in the Gradle
 * User Manual available at https://docs.gradle.org/8.0.2/userguide/building_java_projects.html
 */

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    // Apply the application plugin to add support for building a CLI application in Java.
    application
    kotlin("jvm") version "2.1.10" // Kotlin version to use
}

group = "me.marchev"
version = "1.0-SNAPSHOT"

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()
}

dependencies {
    // The Kotlin test library
    testImplementation(kotlin("test"))
    // testImplementation(kotlin("test-junit"))

    // Use JUnit Jupiter for testing.
    // testImplementation("org.junit.jupiter:junit-jupiter:5.9.1")

    // This dependency is used by the application.
    // implementation("com.google.guava:guava:31.1-jre")
}

// tasks.named<Test>("test") {
//     // Use JUnit Platform for unit tests.
//     useJUnitPlatform()
// }

tasks.test {
    useJUnitPlatform() // JUnitPlatform for tests.
    //useJUnit()
}

// kotlin { // Extension for easy setup
//     jvmToolchain(17) // Target version of generated JVM bytecode.
// }

//tasks.withType<KotlinCompile>() {
//    kotlinOptions.jvmTarget = "21"
//}

// This is needed (at least) for Problem 21 of 2023.
// Docs: https://docs.gradle.org/current/dsl/org.gradle.api.tasks.testing.Test.html
tasks.withType<Test> {
    // Set heap size for the test JVM(s)
    minHeapSize = "128m"
    maxHeapSize = "2048m"
}

application {
    // Define the main class for the application.
    mainClass.set("Day23Kt")
}
