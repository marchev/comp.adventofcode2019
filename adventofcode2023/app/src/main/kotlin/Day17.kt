import java.util.PriorityQueue

/**
 * https://adventofcode.com/2023, Day 17
 *
 * Task: https://adventofcode.com/2023/day/17/
 * Date: 2024-03-17
 * Author: Svilen Marchev
 *
 * Status: works for both parts
  *
 * Phase 1 answer: 1044
 * Phase 2 answer: 1227
 *
 * Tags: dijkstra, graph theory, shortest path, expanded graph
 * Niceness: 7/10
 *
 * Part 1: Dijkstra in an extended graph. Each node is determined by: position in the grid,
 * the direction from which we landed in that position, and how many times did we go in that same
 * direction consecutively so far.
 *
 * Part 2: Basically same solution as part 1.
 */

object Day17 {
  // Order is crucial: U, R, D, L
  val dr = listOf(-1,0,1,0)
  val dc = listOf(0,1,0,-1)

  data class State(val dist: Int, val r: Int, val c: Int, val lastDir: Int, val timesInDir: Int)

  private fun computeShortestPath(lines: List<String>, minConsecutiveMoves: Int, maxConsecutiveMoves: Int): Int {
    val grid = lines.map { line -> line.toCharArray().map { it.digitToInt() }.toList() }.toList()
    val n = grid.size
    val m = grid.first().size

    val minDist = Array(n) { Array(m) { Array(4) { IntArray(maxConsecutiveMoves+1) {Int.MAX_VALUE} }}}
    minDist[0][0][1][0] = 0
    val queue = PriorityQueue(n*m*4*maxConsecutiveMoves, compareBy<State> { it.dist })
    queue.add(State(0, 0, 0, 1, 0))

    while (queue.isNotEmpty()) {
      val cur = queue.poll()
      if (cur.dist != minDist[cur.r][cur.c][cur.lastDir][cur.timesInDir]) continue
      if (cur.r == n-1 && cur.c == m-1 && cur.timesInDir >= minConsecutiveMoves) return cur.dist

      for (offset in -1..1) {
        if (offset != 0 && cur.timesInDir < minConsecutiveMoves) continue
        val newDir = (cur.lastDir+offset+4)%4
        val newR = cur.r + dr[newDir]
        val newC = cur.c + dc[newDir]
        val newTimesInDir = if (cur.lastDir == newDir) cur.timesInDir+1 else 1
        if (newR in 0 until n && newC in 0 until m && newTimesInDir <= maxConsecutiveMoves) {
          val newDist = cur.dist + grid[newR][newC]
          if (minDist[newR][newC][newDir][newTimesInDir] > newDist) {
            minDist[newR][newC][newDir][newTimesInDir] = newDist
            queue.add(State(newDist, newR, newC, newDir, newTimesInDir))
          }
        }
      }
    }

    throw IllegalStateException("Expected to find a solution but failed to")
  }

  fun solvePart1(lines: List<String>): Int {
    return computeShortestPath(lines, 0, 3)
  }

  fun solvePart2(lines: List<String>): Int {
    return computeShortestPath(lines, 4, 10)
  }
}

fun main() {
  val lines = readLines("Day17")
  println("Part 1 solution = ${Day17.solvePart1(lines)}")
  println("Part 2 solution = ${Day17.solvePart2(lines)}")
}
