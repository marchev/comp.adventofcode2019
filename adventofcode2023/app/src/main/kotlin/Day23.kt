import kotlin.text.substring

/**
 * https://adventofcode.com/2023, Day 23
 *
 * Task: https://adventofcode.com/2023/day/23/
 * Date: 2025-02-09
 * Author: Svilen Marchev
 *
 * Status: works for both parts (but is slow for part 2)
 *
 * Phase 1 answer: 2330
 * Phase 2 answer: 6518 (runs for 6m on my Macbook M1)
 *
 * Tags: simulation, backtrack, longest route
 * Niceness: 5/10
 *
 * Part 1: Just a backtrack. Could be optimized, e.g. the graph could be compressed before the backtrack.
 * 
 * Part 2: Same backtrack could be reused, but it is slow (6 min). Compressing the graph becomes essential.
 * Can be combined with further improvements, e.g. avoiding branching when walking at parimeter cells
 * (e.g. when being on the left border, it doesn't make sense to move up, as that would be dead end;
 * when being on the top border, it doesn't make sense to move left, etc.).
 */

/*
From https://www.reddit.com/r/adventofcode/comments/18oy4pc/comment/kfyvp2g/

I discovered a neat way to make part two faster with a simple heuristic.

After shrinking the input graph to only the junctions, start and end the graph looks like (leaving out weights for brevity):

Start -  a - b - c - d - e
         |   |   |   |   | \
         f - A - B - C - D - g
         |   |   |   |   |   |
         h - E - F - G - H - k
         |   |   |   |   |   |
         m - K - M - N - P - n
         |   |   |   |   |   |
         p - Q - R - S - T - q
           \ |   |   |   |   |
             r - s - t - u - v - End
This graph is almost exactly a square grid graph. In fact we could add top right and bottom left corners by adding two dummy nodes to transform it into a square grid.

This means the problem is a self avoiding rook walk. The number of possible walks for different n is given by OEIS A007764. For n = 6 the value is 1262816.

However it's tricky to find the walks exactly with a DFS and a lot of paths end up in dead ends. We can eliminate most dead ends with the key insight that if we reach a node on the perimeter then we should only move down or to the right. For example if we reach node k then moving to g would make no sense, trapping us in the top section of the graph.

We can implement this with a simple rule. If a node has 3 or fewer connections then it's on the perimeter. If a connection is to another perimeter node then it should be directed. This transforms the graph to:

Start →  a → b → c → d → e
         ↓   |   |   |   | ↘
         f - A - B - C - D - g
         ↓   |   |   |   |   ↓
         h - E - F - G - H - k
         ↓   |   |   |   |   ↓
         m - K - M - N - P - n
         ↓   |   |   |   |   ↓
         p - Q - R - S - T - q
           ↘ |   |   |   |   ↓
             r → s → t → u → v → End  
This heuristic gave me a 6x speedup, reducing my single threaded run time from 90ms to 15ms. It explored a total of 6055627 paths, so still higher than the minimum but much improved over a brute force search. We can also "compress" the start and end nodes to shrink the graph slightly:

Start → b → c → d → e
    ↓   |   |   |   | ↘
    f - A - B - C - D - g
    ↓   |   |   |   |   ↓
    h - E - F - G - H - k
    ↓   |   |   |   |   ↓
    m - K - M - N - P - n
    ↓   |   |   |   |   ↓
    p - Q - R - S - T - q
      ↘ |   |   |   |   ↓
        r → s → t → u → End
Storing the visited state as a bitmask and adding multithreading (as exploring paths is independent) further dropped the runtime to 3.2 ms.

---
This is certainly a very useful insight to consider. Moreover, this is not merely due to some consistent feature in the input data given, but is rather forced by the shape of the problem as described - I start touching the upper side of the maze, and if I ever touch a side of the maze again (whether left, right, top or bottom) then I divide the maze into two parts; at that point, I can check whether I am moving into the part which contains the exit or not (and if not, I can abandon that path).

Effectively, then, any path that touches the edge of the maze becomes perforce directional - you can only ever reach the end by going one way along it.
*/

object Day23 {
  // right, down, left, up
  val dr = listOf(0,1,0,-1)
  val dc = listOf(1,0,-1,0)

  fun findLongestPath(r: Int, c: Int, grid: List<String>, visited: Array<BooleanArray>): Int {
    val numRows = grid.size
    val numCols = grid.first().length

    if (r == numRows-1 && c == numCols-2) {
      return 0
    }
    visited[r][c] = true
    var longestRoute = -1_000_000

    if (grid[r][c] == '>') {
      val nr = r
      val nc = c + 1
      if (nr in grid.indices && nc in grid.first().indices
          && grid[nr][nc] != '#' && !visited[nr][nc]) {
        longestRoute = Math.max(longestRoute, 1 + findLongestPath(nr, nc, grid, visited))
      }
    } else if (grid[r][c] == 'v') {
      val nr = r + 1
      val nc = c
      if (nr in grid.indices && nc in grid.first().indices
          && grid[nr][nc] != '#' && !visited[nr][nc]) {
        longestRoute = Math.max(longestRoute, 1 + findLongestPath(nr, nc, grid, visited))
      }
    } else {
      for (k in dr.indices) {
        val nr = r + dr[k]
        val nc = c + dc[k]
        if (nr in grid.indices && nc in grid.first().indices
            && grid[nr][nc] != '#' && !visited[nr][nc]) {
          longestRoute = Math.max(longestRoute, 1 + findLongestPath(nr, nc, grid, visited))
        }
      }
    }

    visited[r][c] = false
    return longestRoute
  }

  fun solvePart1(grid: List<String>): Int {
    val numRows = grid.size
    val numCols = grid.first().length
    val visited = Array(numRows) { BooleanArray(numCols) }

    return findLongestPath(0, 1, grid, visited)
  }

  fun isEndCellReachable(r: Int, c: Int, grid: List<String>, visited: Array<BooleanArray>): Boolean {
    if (grid[r][c] == '#') return false
    if (r == grid.indices.last && c == grid.first().indices.last-1) return true
    visited[r][c] = true
    for (k in dr.indices) {
      val nr = r + dr[k]
      val nc = c + dc[k]
      if (nr in grid.indices && nc in grid.first().indices
          && grid[nr][nc] != '#' && !visited[nr][nc]) {
        if (isEndCellReachable(nr, nc, grid, visited)) {
          return true
        }
      }
    }
    return false
  }

  data class Cell(val r: Int, val c: Int)

  fun buildReverseTopologicalSort(r: Int, c: Int, grid: List<String>, visited: Array<BooleanArray>, topoSort: MutableList<Cell>) {
    visited[r][c] = true
    for (k in dr.indices) {
      val nr = r + dr[k]
      val nc = c + dc[k]
      if (nr in grid.indices && nc in grid.first().indices
          && grid[nr][nc] != '#' && !visited[nr][nc]) {
        buildReverseTopologicalSort(nr, nc, grid, visited, topoSort)
      }
    }
    topoSort.add(Cell(r,c))
  }

  /** Solves part 2 using a naive backtrack. Takes 6-7 mins on a Macbook Pro M1. */
  fun solvePart2Slow(grid: List<String>): Int {
    val numRows = grid.size
    val numCols = grid.first().length
    val visited = Array(numRows) { BooleanArray(numCols) }
    val gridWithoutDirections = List(numRows) { r -> grid[r].replace('>', '.').replace('v', '.') }

    return findLongestPath(0, 1, gridWithoutDirections, visited)
  }

  fun computeMaxDistanceBetweenDividingCells(r: Int, c: Int, source: Cell, target: Cell, grid: List<String>, visited: Array<BooleanArray>, dividingCells: Set<Cell>): Int? {
    if (r == target.r && c == target.c) return 0
    if ((r != source.r || c != source.c) && dividingCells.contains(Cell(r, c))) return null
    visited[r][c] = true
    var longestRoute: Int? = null
    for (k in dr.indices) {
      val nr = r + dr[k]
      val nc = c + dc[k]
      if (nr in grid.indices && nc in grid.first().indices
          && grid[nr][nc] != '#' && !visited[nr][nc]) {
        val dist = computeMaxDistanceBetweenDividingCells(nr, nc, source, target, grid, visited, dividingCells)
        if (dist != null) {
          longestRoute = Math.max(longestRoute ?: 0, 1 + dist)
        }
      }
    }
    visited[r][c] = false
    return longestRoute
  }

  fun printGrid(grid: List<String>) {
    grid.forEach { row -> println(row) }
    println()
  }

  /**
   * Solves part 2 by finding all dividing nodes, computing the max distance between each pair of
   * consecutive dividing nodes in the topological order, and summing them together to find the
   * final answer.
   * 
   * The efficiency depends on how many dividing nodes the graph has, which in this case is not many,
   * hence it's not an improvement over a naive backtrack.
   */
  fun solvePart2Slow2(gridWithDirections: List<String>): Int {
    val numRows = gridWithDirections.size
    val numCols = gridWithDirections.first().length
    val grid = List(numRows) { r -> gridWithDirections[r].replace('>', '.').replace('v', '.') }
    var visited = Array(numRows) { BooleanArray(numCols) }

    // Find all dividing cells (i.e. individual cells that when removed break the connectivity of
    // the grid).
    val dividingCells = mutableSetOf<Cell>()
    for (r in grid.indices) {
      for (c in grid.first().indices) {
        if (grid[r][c] == '.') {
          val gridWithBlockedCell = List(numRows) { rr -> if (rr != r) grid[rr] else grid[rr].replaceRange(c, c+1, "#") }
          visited.forEach { row -> row.fill(false) }
          if (!isEndCellReachable(0, 1, gridWithBlockedCell, visited)) {
            dividingCells.add(Cell(r, c))
          }
        }
      }
    }

    // Build topological sort
    visited.forEach { row -> row.fill(false) }
    val topoSort = mutableListOf<Cell>()
    buildReverseTopologicalSort(0, 1, grid, visited, topoSort)
    topoSort.reverse()
    check(topoSort.first().r == 0 && topoSort.first().c == 1)

    // Find the longest path between each pair of consecutive dividing nodes and sum the together. This give the answer. 
    //
    // This implementation has the downside that when computing max distance between B and C, the backtrack explores
    // redundantly also paths to the previous dividing node A in the topological order. This effectively doubles the
    // runtime.
    check(dividingCells.contains(topoSort.first()))
    var i = 0
    var totalMaxDist = 0
    while (i < topoSort.size) {
      var j = i + 1
      while (j < topoSort.size && !dividingCells.contains(topoSort[j])) j += 1
      if (j < topoSort.size) {
        print("Computing distance between ${topoSort[i]} and ${topoSort[j]}... ")
        visited.forEach { row -> row.fill(false) }
        val d = computeMaxDistanceBetweenDividingCells(topoSort[i].r, topoSort[i].c, topoSort[i], topoSort[j], grid, visited, dividingCells)
        totalMaxDist += d!!
        println(d)
      }
      i = j
    }
    return totalMaxDist
  }

  fun solvePart2(grid: List<String>): Int = solvePart2Slow(grid)
}

fun main() {
  val lines = readLines("Day23")

//   val lines =
//     listOf(
// "#.#####################",
// "#.......#########...###",
// "#######.#########.#.###",
// "###.....#.>.>.###.#.###",
// "###v#####.#v#.###.#.###",
// "###.>...#.#.#.....#...#",
// "###v###.#.#.#########.#",
// "###...#.#.#.......#...#",
// "#####.#.#.#######.#.###",
// "#.....#.#.#.......#...#",
// "#.#####.#.#.#########v#",
// "#.#...#...#...###...>.#",
// "#.#.#v#######v###.###v#",
// "#...#.>.#...>.>.#.###.#",
// "#####v#.#.###v#.#.###.#",
// "#.....#...#...#.#.#...#",
// "#.#########.###.#.#.###",
// "#...###...#...#...#.###",
// "###.###.#.###v#####v###",
// "#...#...#.#.>.>.#.>.###",
// "#.###.###.#.###.#.#v###",
// "#.....###...###...#...#",
// "#####################.#"
//       )

  println("Part 1 solution = ${Day23.solvePart1(lines)}")
  println("Part 2 solution = ${Day23.solvePart2(lines)}")
}
