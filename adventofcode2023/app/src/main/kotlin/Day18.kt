import kotlin.AssertionError

/**
 * https://adventofcode.com/2023, Day 18
 *
 * Task: https://adventofcode.com/2023/day/18/
 * Date: 2024-01-22, 2024-02-11
 * Author: Svilen Marchev
 *
 * Status: works for both parts
 *
 * Phase 1 answer: 67891
 * Phase 2 answer: 94116351948493
 *
 * Tags: sweeping, polygon, computational geometry, grid, bfs
 * Niceness: 10/10
 *
 * Part 1 analysis:
 * ----------------
 * We make the assumption that the trench's sides don't touch, i.e. there is a gap of at least
 * one between any two trench sides.
 *
 * With this assumption it could be solved by simulation - just add all points of the trench
 * in a set. Then place the whole trench in a bounding box and start a BFS from each point
 * of the bounding box (each bounding box point is outside the trench), marking each point
 * that gets reached. Then the answer is: area-of-the-bounding-box - number-of-points-reached-from-the-BFSs.
 *
 * Part 2 analysis:
 * -----------------
 * Part 2 is way more interesting.
 *
 * We make the assumption that there are no consecutive horizontal and no consecutive vertical
 * segments (it's not difficult to work around that if this doesn't hold, though). Also,
 * we assume segments do not overlap.
 *
 * For part 2 we work with segments instead of points as in part 2. We build a list of segments,
 * and, for convenience, to each segment we add references to the next and to the prev segment
 * from the trench's definition.
 *
 * The critical observation is that while the distances are quite large, the total number of segments
 * is small and so the covered length changes relatively rare when you go down the y-axis. I.e.
 * the covered length at a given Y would be the same as at Y-1, unless there are segments that
 * start or end at Y or Y-1. This means that we could compute the covered length only at a handful
 * of y-coordinates, and reuse those numbers for the rest of y-coordinates.
 *
 * This observation allows a sweeping algorithm on the y-axis:
 * {@code
 * 1) Let C be a sorted list of all unique coordinates: the y of each top-left point of a segment,
 * and the y+1 of each bottom-right point of a segment.
 * 2) Let Area = 0 be the total covered area.
 * 3) Let LastY = 0 be the previous processed Y.
 * 4) Let CurrentlyCovered = 0 is the covered length at each y-coordinate between LastY and Y (exclusive).
 * 5) For each point Y in C:
 * - Area += (Y-LastY) * CurrentlyCovered
 * - CurrentlyCovered = ComputeCoveredLengthAtY(Y)
 * - LastY = Y
 * 6) Return Area
 * }
 *
 * Now the tricky part is computing the covered length at a given y-coordinate. For that we use
 * the observation that if we point a laser beam from far far to the left
 * (where we would certainly be outside the trench) to the trench, with the first time
 * we intersect the trench we would "get inside it", with the second time we would go outside it,
 * and so on this process will alternate, and at the end we would always end up outside it.
 *
 * This allows a sweeping algorithm, in which we sweep left-to-right through all segments that
 * intersect with the given y-coordinate. Each time we find a range that falls inside
 * the trench, we count it into the answer. The difficulty stems from handling a few cases: it can happen that
 * the laser beam intersects a vertical segment (not on its ends, but inside it) or a horizontal segment.
 * Handling a vertical segment (crossed not at its ends) is trivial, as it merely flips
 * the in-out state, but handling a horizontal segment can have 2 sub-cases: the two
 * neighboring segments are both above/below the Y coordinate (in which case the segment does
 * not flip the in-out state), OR one of them is above and the other and below (which flips
 * the state). (Note that we don't need to handle crossing vertical segments *at their ends*,
 * as that is covered by handling the horizontal segments case.)
 *
 * The time complexity is O(N*N*LogN), where N is the number of segments.
 */

object Day18 {
  val dr = listOf(0,1,0,-1)
  val dc = listOf(1,0,-1,0)

  data class Position(val r: Int, val c: Int)

  fun solvePart1(lines: List<String>): Int {
    //"R 6 (#70c710)"
    var r = 0
    var c = 0
    val trench = mutableSetOf(Position(r,c))
    for (line in lines) {
      val tokens = line.split(' ')
      val dir = tokens[0]
      val dirIx = when (dir) {
        "R" -> 0
        "D" -> 1
        "L" -> 2
        "U" -> 3
        else -> throw AssertionError()
      }
      val dist = tokens[1].toInt()
      for (k in 0 until dist) {
        r += dr[dirIx]
        c += dc[dirIx]
        trench.add(Position(r, c))
      }
    }

    val minR = trench.minOf { it.r }
    val maxR = trench.maxOf { it.r }
    val minC = trench.minOf { it.c }
    val maxC = trench.maxOf { it.c }
    val outerBorderMinR = minR - 1
    val outerBorderMaxR = maxR + 1
    val outerBorderMinC = minC - 1
    val outerBorderMaxC = maxC + 1

    // BFS from a position we are certain is outside
    val startPos = Position(outerBorderMinR, outerBorderMinC)
    val queue = mutableListOf(startPos)
    var qIx = 0
    val added = mutableSetOf(startPos)
    while (qIx < queue.size) {
      val curPos = queue[qIx]
      qIx += 1
      for (k in dr.indices) {
        val nr = curPos.r + dr[k]
        val nc = curPos.c + dc[k]
        val newPos = Position(nr, nc)
        if (nr in outerBorderMinR..outerBorderMaxR && nc in outerBorderMinC..outerBorderMaxC && !trench.contains(newPos) && !added.contains(newPos)) {
          queue.add(newPos)
          added.add(newPos)
        }
      }
    }

    val insideArea = (outerBorderMaxR - outerBorderMinR+1) * (outerBorderMaxC - outerBorderMinC+1) - queue.size
    return insideArea

//    val spanR = trench.maxOf { it.r } - trench.minOf { it.r }
//    val spanC = trench.maxOf { it.c } - trench.minOf { it.c }
//    println("$spanR $spanC")
//
//    return trench.size
  }

  data class Point(val r: Long, val c: Long)

  data class Segment(val tl: Point, val br: Point) {
    /** Next in the order in which segments are defined */
    lateinit var next: Segment
    /** Prev in the order in which segments are defined */
    lateinit var prev: Segment

    fun isHorizontal(): Boolean = tl.r == br.r
  }

  fun createNormalizedSegment(p1: Point, p2: Point): Segment {
    if (p1.r > p2.r || p1.c > p2.c) {
      return Segment(p2, p1)
    }
    return Segment(p1, p2)
  }

  fun verifySegmentsAssumption(segments: List<Segment>) {
    for (i in segments.indices) {
      if (!(segments[i].isHorizontal() xor segments[(i+1) % segments.size].isHorizontal())) {
        throw AssertionError("There are consecutive horizontal or consecutive vertical segments")
      }
    }
  }

  fun populateSegmentsUsingPart1Strategy(lines: List<String>): List<Segment> {
    val segments = mutableListOf<Segment>()
    var curPoint = Point(0,0)
    for (line in lines) {
      val tokens = line.split(' ')
      val dir = tokens[0]
      val dirIx = when (dir) {
        "R" -> 0
        "D" -> 1
        "L" -> 2
        "U" -> 3
        else -> throw AssertionError()
      }
      val dist = tokens[1].toInt()
      val nextPoint = Point(curPoint.r + dr[dirIx] * dist, curPoint.c + dc[dirIx]* dist)
      segments.add(createNormalizedSegment(curPoint, nextPoint))
      curPoint = nextPoint
    }
//    for (s in segments) println(s)
    for (i in segments.indices) {
      segments[i].next = segments[(i+1)%segments.size]
      segments[i].prev = segments[(i+segments.size-1)%segments.size]
    }
    return segments
  }

  fun populateSegmentsUsingPart2Strategy(lines: List<String>): List<Segment> {
    val segments = mutableListOf<Segment>()
    var curPoint = Point(0,0)
    for (line in lines) {
      val tokens = line.split(' ')
      val dirIx = tokens[2][7].code - '0'.code
      val dist = tokens[2].substring(2, tokens[2].length-2).toLong(16)
      val nextPoint = Point(curPoint.r + dr[dirIx] * dist, curPoint.c + dc[dirIx]* dist)
      segments.add(createNormalizedSegment(curPoint, nextPoint))
      curPoint = nextPoint
    }
    for (i in segments.indices) {
      segments[i].next = segments[(i+1)%segments.size]
      segments[i].prev = segments[(i+segments.size-1)%segments.size]
    }
    return segments
  }

  fun computeCoveredAreaAtGivenRow(r: Long, segments: List<Segment>): Long {
    val segmentsToSweep = segments
      .filter {
        (it.isHorizontal() && it.tl.r == r) || (!it.isHorizontal() && r in it.tl.r+1..<it.br.r)}
      .sortedBy { it.tl.c }

    var isIn = false
    var lastEnteredIn: Long? = null
    var coveredLength = 0L

    fun emitCoveredLength(a: Long, b: Long) {
      coveredLength += (b-a+1)
    }

    for (s in segmentsToSweep) {
      if (s.isHorizontal()) {
        val isPrevAbove = s.prev.tl.r < r
        val isNextAbove = s.next.tl.r < r
        if (isPrevAbove xor isNextAbove) {
          // Case: we cross a zigzag-shaped boundary. It is treated similarly to the vertical-shaped case.
          if (isIn) {
            emitCoveredLength(lastEnteredIn!!, s.br.c)
            isIn = false
            lastEnteredIn = null
          } else {
            isIn = true
            lastEnteredIn = s.tl.c
          }
        } else {
          // Case: we cross a U-shaped boundary (or П-shaped). We treat it as it were just a
          // single point that doesn't change the in-out state.
          if (!isIn) {
            emitCoveredLength(s.tl.c, s.br.c)
          }
        }
      } else {
        // Case: we cross a vertical-shaped boundary. We just flip the in-out state.
        if (isIn) {
          emitCoveredLength(lastEnteredIn!!, s.tl.c)
          isIn = false
          lastEnteredIn = null
        } else {
          isIn = true
          lastEnteredIn = s.tl.c
        }
      }
    }
    if (isIn) throw AssertionError("Boundary was not closed when sweeping at row $r")

    return coveredLength
  }

  fun solvePart2(lines: List<String>): Long {
    val segments = populateSegmentsUsingPart2Strategy(lines)
    verifySegmentsAssumption(segments)

    val rCoordinatesToSweep = segments.flatMap { listOf(it.tl.r, it.br.r+1) }.toSet().toList().sorted()
    var lastR = rCoordinatesToSweep.first()
    var coveredAreaAtLastR = 0L
    var totalCoveredArea = 0L
    for (r in rCoordinatesToSweep) {
      totalCoveredArea += (r - lastR) * coveredAreaAtLastR
      coveredAreaAtLastR = computeCoveredAreaAtGivenRow(r, segments)
      lastR = r
    }
    return totalCoveredArea
  }
}

fun main() {
  val lines = readLines("Day18")
  println("Part 1 solution = ${Day18.solvePart1(lines)}")
  println("Part 2 solution = ${Day18.solvePart2(lines)}")
}
