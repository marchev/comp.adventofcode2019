/**
 * https://adventofcode.com/2023, Day 21
 *
 * Task: https://adventofcode.com/2023/day/21/
 * Date: 2024-01-08
 * Author: Svilen Marchev
 *
 * Status: works for part 1
 *
 * Phase 1 answer: 3751
 * Phase 2 answer: ?
 *
 * Tags: graph theory, matrix multiplication
 * Niceness: 7/10
 *
 * A key observation is that the path lengths are a power of 2, so we could use matrix
 * multiplication on the adjacency matrix to get the final answer. Complications comes from
 * the fact that there are plenty of nodes (~17000), so we need a more compact representation
 * of the graph.
 */

object Day21 {
  val dr = listOf(0,1,0,-1)
  val dc = listOf(1,0,-1,0)

  // Uses an adjacency matrix, which takes too much memory for ~17000 nodes.
  fun solvePart1Slow(grid: List<String>): Int {
    val numRows = grid.size
    val numCols = grid.first().length

    // Build graph
    val numNodes = numCols * numRows
    var graph = Array(numNodes) { BooleanArray(numNodes) }
    var startNode = -1
    for (r in grid.indices) {
      for (c in grid[r].indices) {
        if (grid[r][c] == '#') continue

        val nodeA = r*numCols + c

        if (grid[r][c] == 'S') {
          startNode = nodeA
        }

        for (k in dr.indices) {
          val nr = r + dr[k]
          val nc = c + dc[k]
          if (nr in grid.indices && nc in grid.first().indices && grid[nr][nc] != '#') {
            val nodeB = nr*numCols + nc
            graph[nodeA][nodeB] = true
          }
        }
      }
    }


    for (power in 1..6) {
      var graphNext = Array(numNodes) { BooleanArray(numNodes) }
      for (i in graph.indices) {
        for (j in graph.indices) {
          for (k in graph.indices) {
            if (graph[i][k] && graph[k][j]) {
              graphNext[i][j] = true
              break
            }
          }
        }
      }

      graph = graphNext
    }

    var numReachable = 0
    for (j in graph.indices) {
      if (graph[startNode][j]) {
        numReachable += 1
      }
    }
    return numReachable
  }

  // Uses a more compressed representation of the graph and is thus faster and more compact.
  fun solvePart1(grid: List<String>, lengthPowerOf2: Int = 6): Int {
    val numRows = grid.size
    val numCols = grid.first().length

    // Build graph
    val numNodes = numCols * numRows
    var graph = Array(numNodes) { HashSet<Int>() }
    var startNode = -1
    for (r in grid.indices) {
      for (c in grid[r].indices) {
        if (grid[r][c] == '#') continue

        val nodeA = r*numCols + c

        if (grid[r][c] == 'S') {
          startNode = nodeA
        }

        for (k in dr.indices) {
          val nr = r + dr[k]
          val nc = c + dc[k]
          if (nr in grid.indices && nc in grid.first().indices && grid[nr][nc] != '#') {
            val nodeB = nr*numCols + nc
            graph[nodeA].add(nodeB)
          }
        }
      }
    }

    for (power in 1..lengthPowerOf2) {
      println("raising to 2^$power")
      var graphNext = Array(numNodes) { HashSet<Int>() }
      for (i in graph.indices) {
        // Avoid some unnecessary computation, thus reducing memory
        if (power == lengthPowerOf2 && i != startNode) continue

        for (k in graph[i]) {
          for (j in graph[k]) {
            graphNext[i].add(j)
          }
        }
      }
      graph = graphNext
    }

    return graph[startNode].size
  }

  fun solvePart2(lines: List<String>): Int {
    return 0
  }
}

fun main() {
  val lines = readLines("Day21")
  println("Part 1 solution = ${Day21.solvePart1(lines)}")
  println("Part 2 solution = ${Day21.solvePart2(lines)}")
}
