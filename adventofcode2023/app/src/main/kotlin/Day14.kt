/**
 * https://adventofcode.com/2023, Day 14
 *
 * Task: https://adventofcode.com/2023/day/14/
 * Date: 2023-12-14
 * Author: Svilen Marchev
 *
 * Status: works for both parts
 * Niceness: 7/10
 *
 * Phase 1 answer: 108813
 * Phase 2 answer: 104533
 *
 * Tags: grid, simulation, inner loop optimization, memoization, finding loops in sequences, encoding, greedy, sweeping
 *
 * For part 1: I use a greedy sweeping algorithm when simulating the tilt, which effectively cuts the inner loop.
 * This way the time is O(N^2) instead of O(N^3) as it would be should the simulation be implemented naively.
 *
 * For part 2: The idea is to start simulating all tilts and cycles, but to keep track of when things start to repeat.
 * We then just cut it short and that turns out to be rather soon (after cycle 97 for the real input).
 */

object Day14 {
  fun gridifyLines(lines: List<String>): List<MutableList<Char>> {
    return lines.map { it.toCharArray().toMutableList() }
  }

  fun computeLoad(grid: List<MutableList<Char>>): Int {
    var sum = 0
    for (r in grid.indices) {
      for (c in grid[r].indices) {
        sum += if (grid[r][c] == 'O') (grid.size - r) else 0
      }
    }
    return sum
  }

  fun solvePart1(lines: List<String>): Int {
    val grid = gridifyLines(lines)
    tiltNorth(grid)
    return computeLoad(grid)
  }

  fun tiltNorth(grid: List<MutableList<Char>>) {
    val numRows = grid.size
    val numCols = grid.first().size
    for (c in 0 until numCols) {
      // Invariants for {nextSwapRow}:
      // - it points to an empty cell, and there are no #-cells between the
      // currently examined row and it
      // - it points to a non-empty cell (# or O), but that cell is always as close to the
      // currently examined row as possible (i.e. either equal to it or one less)
      var nextSwapRow = 0
      for (r in 1 until numRows) {
        if (grid[nextSwapRow][c] != '.') nextSwapRow += 1

        if (grid[r][c] == 'O') {
          if (grid[nextSwapRow][c] == '.') {
            grid[nextSwapRow][c] = 'O'
            grid[r][c] = '.'
            nextSwapRow += 1
            while (nextSwapRow < r && grid[nextSwapRow][c] != '.') nextSwapRow += 1
          }
        } else if (grid[r][c] == '#') {
          nextSwapRow = r
        }
      }
    }
  }

  fun tiltSouth(grid: List<MutableList<Char>>) {
    val numRows = grid.size
    val numCols = grid.first().size
    for (c in 0 until numCols) {
      // Invariants for {nextSwapRow}:
      // - it points to an empty cell, and there are no #-cells between the
      // currently examined row and it
      // - it points to a non-empty cell (# or O), but that cell is always as close to the
      // currently examined row as possible (i.e. either equal to it or plus one)
      var nextSwapRow = numRows - 1
      for (r in numRows - 2 downTo 0) {
        if (grid[nextSwapRow][c] != '.') nextSwapRow -= 1

        if (grid[r][c] == 'O') {
          if (grid[nextSwapRow][c] == '.') {
            grid[nextSwapRow][c] = 'O'
            grid[r][c] = '.'
            nextSwapRow -= 1
            while (nextSwapRow > r && grid[nextSwapRow][c] != '.') nextSwapRow -= 1
          }
        } else if (grid[r][c] == '#') {
          nextSwapRow = r
        }
      }
    }
  }

  fun tiltWest(grid: List<MutableList<Char>>) {
    val numRows = grid.size
    val numCols = grid.first().size
    for (r in 0 until numRows) {
      var nextSwapCol = 0
      for (c in 1 until numCols) {
        if (grid[r][nextSwapCol] != '.') nextSwapCol += 1

        if (grid[r][c] == 'O') {
          if (grid[r][nextSwapCol] == '.') {
            grid[r][nextSwapCol] = 'O'
            grid[r][c] = '.'
            nextSwapCol += 1
            while (nextSwapCol < c && grid[r][nextSwapCol] != '.') nextSwapCol += 1
          }
        } else if (grid[r][c] == '#') {
          nextSwapCol = c
        }
      }
    }
  }

  fun tiltEast(grid: List<MutableList<Char>>) {
    val numRows = grid.size
    val numCols = grid.first().size
    for (r in 0 until numRows) {
      var nextSwapCol = numCols - 1
      for (c in numCols - 2 downTo 0) {
        if (grid[r][nextSwapCol] != '.') nextSwapCol -= 1

        if (grid[r][c] == 'O') {
          if (grid[r][nextSwapCol] == '.') {
            grid[r][nextSwapCol] = 'O'
            grid[r][c] = '.'
            nextSwapCol -= 1
            while (nextSwapCol > c && grid[r][nextSwapCol] != '.') nextSwapCol -= 1
          }
        } else if (grid[r][c] == '#') {
          nextSwapCol = c
        }
      }
    }
  }

  fun printGrid(grid: List<MutableList<Char>>) {
    for (r in grid.indices) {
      println(grid[r].joinToString(separator = ""))
    }
    println()
  }

  fun solvePart2(lines: List<String>): Int {
    val grid = gridifyLines(lines)

    fun performCycle() {
      tiltNorth(grid)
      tiltWest(grid)
      tiltSouth(grid)
      tiltEast(grid)
    }

    val seenAfterCycle = mutableMapOf<String, Int>()
    val numCycles = 1000000000
    var cycle = 1
    while (cycle <= numCycles) {
      performCycle()
      val key = grid.flatten().joinToString(separator = "")
      val cycleLastSeen = seenAfterCycle.putIfAbsent(key, cycle)
      if (cycleLastSeen != null) {
        val loopLen = cycle - cycleLastSeen
        val k = (numCycles - cycle) / loopLen
        cycle += k * loopLen + 1
        break
      }
      cycle += 1
    }
    while (cycle <= numCycles) {
      performCycle()
      cycle += 1
    }
    return computeLoad(grid)
  }
}

fun main() {
  val lines = readLines("Day14")
  println("Part 1 solution = ${Day14.solvePart1(lines)}")
  println("Part 2 solution = ${Day14.solvePart2(lines)}")
}
