/**
 * https://adventofcode.com/2023, Day 11
 *
 * Task: https://adventofcode.com/2023/day/11/
 * Date: 2023-12-12
 * Author: Svilen Marchev
 *
 * Status: works for both parts
 * Niceness: 6/10
 *
 * Phase 1 answer: 9591768
 * Phase 2 answer: 746962097860
 *
 * Tags: counting, manhattan distance, grid
 */

object Day11 {
  data class Position(val r: Int, val c: Int)

  fun computeDistance(grid: List<String>, expansionMultiplier: Int): Long {
    val galaxies = mutableListOf<Position>()
    val hasGalaxiesInRow = Array(grid.size) { false }
    val hasGalaxiesInCol = Array(grid.first().length) { false }
    for (r in grid.indices) {
      for (c in grid[r].indices) {
        if (grid[r][c] == '#') {
          galaxies.add(Position(r, c))
          hasGalaxiesInRow[r] = true
          hasGalaxiesInCol[c] = true
        }
      }
    }

    var sum = 0L
    for (i in galaxies.indices) {
      for (j in i + 1 until galaxies.size) {
        var dist = 0
        for (c in Math.min(galaxies[i].c, galaxies[j].c)..Math.max(galaxies[i].c, galaxies[j].c)) {
          dist += if (hasGalaxiesInCol[c]) 1 else expansionMultiplier
        }
        for (r in Math.min(galaxies[i].r, galaxies[j].r)..Math.max(galaxies[i].r, galaxies[j].r)) {
          dist += if (hasGalaxiesInRow[r]) 1 else expansionMultiplier
        }
        dist -= 2 // account for double counting
        // println("${galaxies[i]} ${galaxies[j]}: $dist")
        sum += dist
      }
    }
    return sum
  }

  fun solvePart1(grid: List<String>): Long = computeDistance(grid, 2)
  fun solvePart2(grid: List<String>): Long = computeDistance(grid, 1000000)
}

fun main() {
  val lines = readLines("Day11")
  println("Part 1 solution = ${Day11.solvePart1(lines)}")
  println("Part 2 solution = ${Day11.solvePart2(lines)}")
}
