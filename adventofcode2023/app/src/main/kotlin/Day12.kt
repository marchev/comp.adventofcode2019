/**
 * https://adventofcode.com/2023, Day 12
 *
 * Task: https://adventofcode.com/2023/day/12/
 * Date: 2023-12-15
 * Author: Svilen Marchev
 *
 * Status: works for both parts
 *
 * Phase 1 answer: 7110
 * Phase 2 answer: 1566786613613
 *
 * Tags: DP
 * Niceness: 8/10
 *
 * For both parts:
 *
 * Each task is solved in time O(GN^2), where G is the number of groups and N is the length of
 * the template, but this could be optimized to O(GN) with some preprocessing and careful
 * optimization of the inner loop.
 *
 * We define a function f(i,g) which gives the number of arrangements in which the template
 * is "consumed" up to the i-th char, all groups up to g are matched, group g _ends_ in the i-th char,
 * and the i+1-th char is a non-# char (since otherwise the i-th char cannot finish any group).
 * Then the final answer is given by sum{f(i,G) | i = 1,...,N, and all chars in tmpl[i+1..N]
 * are non-#}.
 */
object Day12 {
  data class Description(val template: String, val groups: List<Int>)

  fun parseDescription(line: String): Description {
    val tokens = line.split(' ', ',')
    return Description(tokens.first(), tokens.drop(1).map { it.toInt() })
  }

  fun hasWorkingCellsBetween(a: Int, b: Int, tmpl: String) =
    tmpl.subSequence(a, b + 1).any { it == '.' }

  fun computeTask(desc: Description): Long {
    // Add some extra content to the template and the groups, so that we make our life easier
    // with filling up the starting values for the DP function below.
    val tmpl = "#." + desc.template
    val groups = listOf(1).plus(desc.groups)
    val n = tmpl.length
    val numGroups = groups.size

    val f = Array(n) { LongArray(numGroups) }
    f[0][0] = 1
    for (g in 1 until numGroups) {
      for (i in groups[g] - 1 until n) {
        if ((i - groups[g] < 0 || tmpl[i - groups[g]] != '#')
          && (i == n - 1 || tmpl[i + 1] != '#')
          && !hasWorkingCellsBetween(i - groups[g] + 1, i, tmpl)
        ) {
          for (j in i - groups[g] - 1 downTo 0) {
            f[i][g] += f[j][g - 1]
            if (tmpl[j] == '#') break
          }
        }
      }
    }

    var sum = 0L
    for (i in n - 1 downTo 0) {
      sum += f[i][numGroups - 1]
      if (tmpl[i] == '#') break
    }
    return sum
  }

  fun solvePart1(lines: List<String>): Long {
    return lines.sumOf { computeTask(parseDescription(it)) }
  }

  fun solvePart2(lines: List<String>): Long {
    fun unfold(desc: Description): Description {
      return Description(
        Array(5) { desc.template }.joinToString("?"),
        List(5) { desc.groups.toList() }.flatten()
      )
    }
    return lines.sumOf { computeTask(unfold(parseDescription(it))) }
  }
}

fun main() {
  val lines = readLines("Day12")
  println("Part 1 solution = ${Day12.solvePart1(lines)}")
  println("Part 2 solution = ${Day12.solvePart2(lines)}")
}
