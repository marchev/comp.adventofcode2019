/**
 * https://adventofcode.com/2023, Day 13
 *
 * Task: https://adventofcode.com/2023/day/13/
 * Date: 2023-12-13
 * Author: Svilen Marchev
 *
 * Status: works for both parts
 *
 * Phase 1 answer: 27300
 * Phase 2 answer: 29276
 *
 * Tags: grid
 * Niceness: 7/10
 *
 * For part 2, instead of flipping every single character individually and running the matching
 * algorithm on the modified grid, we just modify the matching algorithm to report a match only if the total number
 * of differing chars is a particular number. For part 1 this number is 0 (since we want exact
 * matching there), and for part 2 it's 1 (since there we must flip _exactly_ one char).
 *
 * Time for both parts is O(G N^3), where G is the number of grids and N is a grid's size.
 */

object Day13 {

  fun extractGrids(lines: List<String>): List<List<String>> {
    val grids = mutableListOf<List<String>>()
    var curGrid = mutableListOf<String>()
    for (line in lines) {
      if (line.isEmpty()) {
        if (curGrid.isNotEmpty()) {
          grids.add(curGrid)
          curGrid = mutableListOf()
        }
      } else {
        curGrid.add(line)
      }
    }
    if (curGrid.isNotEmpty()) grids.add(curGrid)
    return grids
  }

  fun findHorizontalMirror(grid: List<String>, desiredDiffs: Int): Int {
    for (i in 1 until grid.size) {
      var a = i - 1
      var b = i
      var diffs = 0
      while (a >= 0 && b < grid.size) {
        diffs += grid[i].indices.count { grid[a][it] != grid[b][it] }
        if (diffs > desiredDiffs) break
        a -= 1
        b += 1
      }
      if (diffs == desiredDiffs) {
        return i
      }
    }
    return 0
  }

  fun findVerticalMirror(grid: List<String>, desiredDiffs: Int): Int {
    val numCols = grid.first().length
    for (i in 1 until numCols) {
      var a = i - 1
      var b = i
      var diffs = 0
      while (a >= 0 && b < numCols) {
        diffs += grid.indices.count { grid[it][a] != grid[it][b] }
        if (diffs > desiredDiffs) break
        a -= 1
        b += 1
      }
      if (diffs == desiredDiffs) {
        return i
      }
    }
    return 0
  }

  fun solvePart1(lines: List<String>): Int {
    val grids = extractGrids(lines)
    var sum = 0
    for (grid in grids) {
      sum += findVerticalMirror(grid, 0)
      sum += 100 * findHorizontalMirror(grid, 0)
    }
    return sum
  }

  fun solvePart2(lines: List<String>): Int {
    val grids = extractGrids(lines)
    var sum = 0
    for (grid in grids) {
      sum += findVerticalMirror(grid, 1)
      sum += 100 * findHorizontalMirror(grid, 1)
    }
    return sum
  }
}

fun main() {
  val lines = readLines("Day13")
  println("Part 1 solution = ${Day13.solvePart1(lines)}")
  println("Part 2 solution = ${Day13.solvePart2(lines)}")
}
