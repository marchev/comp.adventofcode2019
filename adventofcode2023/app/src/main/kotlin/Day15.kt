/**
 * https://adventofcode.com/2023, Day 15
 *
 * Task: https://adventofcode.com/2023/day/15/
 * Date: 2023-12-16, 2024-01-07
 * Author: Svilen Marchev
 *
 * Status: works for both parts
 *
 * Phase 1 answer: 510273
 * Phase 2 answer: 212449
 *
 * Tags: hash table, data structures
 * Niceness: 6/10
 *
 * The problem basically asks for a classic implementation of a hash table.
 * My implementation even uses hand-crafted single-linked lists. :)
 */

object Day15 {
  fun computeHash(s: String): Int {
    var hash = 0
    for (c in s) {
      hash += c.code
      hash = (hash * 17) % 256
    }
    return hash
  }

  fun solvePart1(lines: List<String>): Int {
    val tokens = lines.first().split(',')
    return tokens.sumOf { computeHash(it) }
  }

  class HashTable {
    class Node(
      val label: String,
      var value: Int,
      var next: Node?
    )

    val hsize = 256
    val buckets = Array(hsize) { Node("", -1, null) }

    fun remove(label: String) {
      val slot = computeHash(label)
      var curNode: Node? = buckets[slot]
      while (curNode!!.next != null) {
        if (curNode.next!!.label == label) {
          curNode.next = curNode.next!!.next
          break
        }
        curNode = curNode.next
      }
    }

    fun update(label: String, value: Int) {
      val slot = computeHash(label)
      var curNode: Node? = buckets[slot]
      while (curNode!!.next != null) {
        if (curNode.next!!.label == label) {
          curNode.next!!.value = value
          return
        }
        curNode = curNode.next
      }

      val newNode = Node(label, value, null)
      curNode.next = newNode
    }
  }


  fun solvePart2(lines: List<String>): Int {
    val ht = HashTable()
    val tokens = lines.first().split(',')
    for (token in tokens) {
      if (token.contains('-')) {
        val label = token.substring(0, token.length - 1)
        ht.remove(label)
      } else {
        val label = token.substring(0, token.length - 2)
        val value = token.substring(token.length - 1).toInt()
        ht.update(label, value)
      }
    }

    var score = 0
    for (i in ht.buckets.indices) {
      if (ht.buckets[i].next != null) {
//        print("box $i: ")
        var curNode = ht.buckets[i].next
        var curSlot = 1
        while (curNode != null) {
//          print("[${curNode!!.label} ${curNode!!.value}]")
          score += (i + 1) * curSlot * curNode.value
          curNode = curNode.next
          curSlot += 1
        }
//        println()
      }
    }

    return score
  }
}

fun main() {
  val lines = readLines("Day15")
  println("Part 1 solution = ${Day15.solvePart1(lines)}")
  println("Part 2 solution = ${Day15.solvePart2(lines)}")
}
