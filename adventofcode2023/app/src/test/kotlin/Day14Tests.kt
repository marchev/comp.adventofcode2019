import kotlin.test.Test
import kotlin.test.assertEquals

class Day14Tests {
  private val sampleInput =
    listOf(
      "O....#....",
      "O.OO#....#",
      ".....##...",
      "OO.#O....O",
      ".O.....O#.",
      "O.#..O.#.#",
      "..O..#O..O",
      ".......O..",
      "#....###..",
      "#OO..#....",
    )

  @Test
  fun testPart1_sample() {
    val ans = Day14.solvePart1(sampleInput)
    assertEquals(136, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day14")
    val ans = Day14.solvePart1(lines)
    assertEquals(108813, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day14.solvePart2(sampleInput)
    assertEquals(64, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day14")
    val ans = Day14.solvePart2(lines)
    assertEquals(104533, ans)
  }
}
