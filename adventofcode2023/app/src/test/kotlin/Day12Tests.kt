import kotlin.test.Test
import kotlin.test.assertEquals

class Day12Tests {
  private val sampleInput =
    listOf(
      "???.### 1,1,3",
      ".??..??...?##. 1,1,3",
      "?#?#?#?#?#?#?#? 1,3,1,6",
      "????.#...#... 4,1,1",
      "????.######..#####. 1,6,5",
      "?###???????? 3,2,1",
    )

  @Test
  fun testPart1_sample0() {
    val ans = Day12.solvePart1(sampleInput.subList(0, 1))
    assertEquals(1, ans)
  }

  @Test
  fun testPart1_sample1() {
    val ans = Day12.solvePart1(sampleInput.subList(1, 2))
    assertEquals(4, ans)
  }

  @Test
  fun testPart1_sample5() {
    val ans = Day12.solvePart1(sampleInput.subList(5, 6))
    assertEquals(10, ans)
  }

  @Test
  fun testPart1_myTest0() {
    val ans = Day12.solvePart1(listOf(".??#..#??.?? 3,1"))
    assertEquals(1, ans)
  }

  @Test
  fun testPart1_sampleFull() {
    val ans = Day12.solvePart1(sampleInput)
    assertEquals(21, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day12")
    val ans = Day12.solvePart1(lines)
    assertEquals(7110, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day12.solvePart2(sampleInput)
    assertEquals(525152, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day12")
    val ans = Day12.solvePart2(lines)
    assertEquals(1566786613613, ans)
  }
}
