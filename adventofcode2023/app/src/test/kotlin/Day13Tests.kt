import kotlin.test.Test
import kotlin.test.assertEquals

class Day13Tests {
  private val sampleInput =
      listOf(
              "#.##..##.",
              "..#.##.#.",
              "##......#",
              "##......#",
              "..#.##.#.",
              "..##..##.",
              "#.#.##.#.",
              "",
              "#...##..#",
              "#....#..#",
              "..##..###",
              "#####.##.",
              "#####.##.",
              "..##..###",
              "#....#..#",
      )

  @Test
  fun testPart1_sample() {
    val ans = Day13.solvePart1(sampleInput)
    assertEquals(405, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day13")
    val ans = Day13.solvePart1(lines)
    assertEquals(27300, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day13.solvePart2(sampleInput)
    assertEquals(400, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day13")
    val ans = Day13.solvePart2(lines)
    assertEquals(29276, ans)
  }
}
