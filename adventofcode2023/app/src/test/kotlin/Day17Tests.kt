import kotlin.test.Test
import kotlin.test.assertEquals

class Day17Tests {
  private val sampleInput =
    listOf(
      "2413432311323",
      "3215453535623",
      "3255245654254",
      "3446585845452",
      "4546657867536",
      "1438598798454",
      "4457876987766",
      "3637877979653",
      "4654967986887",
      "4564679986453",
      "1224686865563",
      "2546548887735",
      "4322674655533",
    )

  @Test
  fun testPart1_sample() {
    val ans = Day17.solvePart1(sampleInput)
    assertEquals(102, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day17")
    val ans = Day17.solvePart1(lines)
    assertEquals(1044, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day17.solvePart2(sampleInput)
    assertEquals(94, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day17")
    val ans = Day17.solvePart2(lines)
    assertEquals(1227, ans)
  }
}
