import kotlin.test.Test
import kotlin.test.assertEquals

class Day23Tests {
  private val sampleInput =
    listOf(
"#.#####################",
"#.......#########...###",
"#######.#########.#.###",
"###.....#.>.>.###.#.###",
"###v#####.#v#.###.#.###",
"###.>...#.#.#.....#...#",
"###v###.#.#.#########.#",
"###...#.#.#.......#...#",
"#####.#.#.#######.#.###",
"#.....#.#.#.......#...#",
"#.#####.#.#.#########v#",
"#.#...#...#...###...>.#",
"#.#.#v#######v###.###v#",
"#...#.>.#...>.>.#.###.#",
"#####v#.#.###v#.#.###.#",
"#.....#...#...#.#.#...#",
"#.#########.###.#.#.###",
"#...###...#...#...#.###",
"###.###.#.###v#####v###",
"#...#...#.#.>.>.#.>.###",
"#.###.###.#.###.#.#v###",
"#.....###...###...#...#",
"#####################.#",
      )

  @Test
  fun testPart1_sample1() {
    val ans = Day23.solvePart1(sampleInput)
    assertEquals(94, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day23")
    val ans = Day23.solvePart1(lines)
    assertEquals(2330, ans)
  }

 @Test
 fun testPart2_sample() {
   val ans = Day23.solvePart2(sampleInput)
   assertEquals(154, ans)
 }

 @Test
 fun testPart2_real() {
   val lines = readLines("Day23")
   val ans = Day23.solvePart2(lines)
   assertEquals(6518, ans)
 }
}
