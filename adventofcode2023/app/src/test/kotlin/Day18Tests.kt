import kotlin.test.Test
import kotlin.test.assertEquals

class Day18Tests {
  private val sampleInput =
    listOf(
      "R 6 (#70c710)",
      "D 5 (#0dc571)",
      "L 2 (#5713f0)",
      "D 2 (#d2c081)",
      "R 2 (#59c680)",
      "D 2 (#411b91)",
      "L 5 (#8ceee2)",
      "U 2 (#caa173)",
      "L 1 (#1b58a2)",
      "U 2 (#caa171)",
      "R 2 (#7807d2)",
      "U 3 (#a77fa3)",
      "L 2 (#015232)",
      "U 2 (#7a21e3)",
    )

  @Test
  fun testPart1_sample() {
    val ans = Day18.solvePart1(sampleInput)
    assertEquals(62, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day18")
    val ans = Day18.solvePart1(lines)
    assertEquals(67891, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day18.solvePart2(sampleInput)
    assertEquals(952408144115L, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day18")
    val ans = Day18.solvePart2(lines)
    assertEquals(94116351948493L, ans)
  }
}
