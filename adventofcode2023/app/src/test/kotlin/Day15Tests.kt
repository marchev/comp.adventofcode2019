import kotlin.test.Test
import kotlin.test.assertEquals

class Day15Tests {
  private val sampleInput =
    listOf(
      "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7",
    )

  @Test
  fun testPart1_sample() {
    val ans = Day15.solvePart1(sampleInput)
    assertEquals(1320, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day15")
    val ans = Day15.solvePart1(lines)
    assertEquals(510273, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day15.solvePart2(sampleInput)
    assertEquals(145, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day15")
    val ans = Day15.solvePart2(lines)
    assertEquals(212449, ans)
  }
}
