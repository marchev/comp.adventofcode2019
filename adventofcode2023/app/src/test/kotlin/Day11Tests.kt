import kotlin.test.Test
import kotlin.test.assertEquals

class Day11Tests {
  private val sampleInput =
      listOf(
          "...#......",
          ".......#..",
          "#.........",
          "..........",
          "......#...",
          ".#........",
          ".........#",
          "..........",
          ".......#..",
          "#...#.....",
      )

  @Test
  fun testPart1_sample() {
    val ans = Day11.solvePart1(sampleInput)
    assertEquals(374, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day11")
    val ans = Day11.solvePart1(lines)
    assertEquals(9591768, ans)
  }

  @Test
  fun testComputeDistance_sample1() {
    val ans = Day11.computeDistance(sampleInput, 10)
    assertEquals(1030, ans)
  }

  @Test
  fun testComputeDistance_sample2() {
    val ans = Day11.computeDistance(sampleInput, 100)
    assertEquals(8410, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day11.solvePart2(sampleInput)
    assertEquals(82000210, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day11")
    val ans = Day11.solvePart2(lines)
    assertEquals(746962097860, ans)
  }
}
