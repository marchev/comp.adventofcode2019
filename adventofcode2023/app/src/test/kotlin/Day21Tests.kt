import kotlin.test.Test
import kotlin.test.assertEquals

class Day21Tests {
  private val sampleInput =
    listOf(
      "...........",
      ".....###.#.",
      ".###.##..#.",
      "..#.#...#..",
      "....#.#....",
      ".##..S####.",
      ".##..#...#.",
      ".......##..",
      ".##.#.####.",
      ".##..##.##.",
      "...........",
      )

  @Test
  fun testPart1_sample1() {
    val ans = Day21.solvePart1(sampleInput, lengthPowerOf2 = 1)
    assertEquals(4, ans)
  }

  @Test
  fun testPart1_sample2() {
    val ans = Day21.solvePart1(sampleInput, lengthPowerOf2 = 2)
    assertEquals(9, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day21")
    val ans = Day21.solvePart1(lines)
    assertEquals(3751, ans)
  }

//  @Test
//  fun testPart2_sample() {
//    val ans = Day21.solvePart2(sampleInput)
//    assertEquals(145, ans)
//  }
//
//  @Test
//  fun testPart2_real() {
//    val lines = readLines("Day21")
//    val ans = Day21.solvePart2(lines)
//    assertEquals(212449, ans)
//  }
}
