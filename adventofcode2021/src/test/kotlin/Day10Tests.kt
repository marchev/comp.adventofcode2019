import kotlin.test.Test
import kotlin.test.assertEquals

class Day10Tests {
  private val sampleInput = listOf(
    "[({(<(())[]>[[{[]{<()<>>",
    "[(()[<>])]({[<{<<[]>>(",
    "{([(<{}[<>[]}>{[]{[(<()>",
    "(((({<>}<{<{<>}{[]{[]{}",
    "[[<[([]))<([[{}[[()]]]",
    "[{[{({}]{}}([{[{{{}}([]",
    "{<[[]]>}<{[{[{[]{()[[[]",
    "[<(<(<(<{}))><([]([]()",
    "<{([([[(<>()){}]>(<<{{",
    "<{([{{}}[<[[[<>{}]]]>[]]",
  )

  @Test
  fun testPart1_sample() {
    val ans = Day10.solvePart1(sampleInput)
    assertEquals(26397, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day10")
    val ans = Day10.solvePart1(lines)
    assertEquals(316851, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day10.solvePart2(sampleInput)
    assertEquals(288957, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day10")
    val ans = Day10.solvePart2(lines)
    assertEquals(2182912364L, ans)
  }
}
