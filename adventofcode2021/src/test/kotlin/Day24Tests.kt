import kotlin.test.Test
import kotlin.test.assertEquals

class Day24Tests {
  private val sampleInput = listOf(
    ""
  )

  @Test
  fun testPart1_sample() {
    val ans = Day24.solvePart1(sampleInput)
    assertEquals(-3, ans)
  }

//  @Test
//  fun testPart1_real() {
//    val lines = readLines("Day24")
//    val ans = Day24.solvePart1(lines)
//    assertEquals(-1, ans)
//  }
}
