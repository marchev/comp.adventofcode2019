import kotlin.test.Test
import kotlin.test.assertEquals

class Day21Tests {
  private val sampleInput = listOf(
    "Player 1 starting position: 4",
    "Player 2 starting position: 8",
  )

  @Test
  fun testPart1_sample() {
    val ans = Day21.solvePart1(sampleInput)
    assertEquals(739785, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day21")
    val ans = Day21.solvePart1(lines)
    assertEquals(913560, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day21.solvePart2(sampleInput)
    assertEquals(444356092776315L, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day21")
    val ans = Day21.solvePart2(lines)
    assertEquals(110271560863819L, ans)
  }
}
