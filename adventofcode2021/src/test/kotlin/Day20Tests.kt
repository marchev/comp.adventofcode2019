import kotlin.test.Test
import kotlin.test.assertEquals

class Day20Tests {
  private val sampleInput = listOf(
    "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#",
    "",
    "#..#.",
    "#....",
    "##..#",
    "..#..",
    "..###",
  )

  @Test
  fun testPart1_sample() {
    val ans = Day20.solvePart1(sampleInput)
    assertEquals(35, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day20")
    val ans = Day20.solvePart1(lines)
    assertEquals(5619, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day20.solvePart2(sampleInput)
    assertEquals(3351, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day20")
    val ans = Day20.solvePart2(lines)
    assertEquals(20122, ans)
  }
}
