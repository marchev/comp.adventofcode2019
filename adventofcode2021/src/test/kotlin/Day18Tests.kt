import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class Day18Tests {
  @Test
  fun decodeTree_withSamples_works() {
    for (encoded in listOf(
      "[1,2]",
      "[[1,2],3]",
      "[9,[8,7]]",
      "[[1,9],[8,5]]",
      "[[[[1,2],[3,4]],[[5,6],[7,8]]],9]",
      "[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]",
      "[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]",
      "[15,2021]", // Multidigit numbers
    )) {
      val tree = decode(encoded)
      assertEquals(encoded, encode(tree))
    }
  }

  @Test
  fun classReduceOperation_reduceTreeExplode_explodes() {
    data class TestCase(val encodedInitialTree: String, val encodedExpectedTree: String)

    listOf(
      TestCase("[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]"),
      TestCase("[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]"),
      TestCase("[[6,[5,[4,[3,2]]]],1]", "[[6,[5,[7,0]]],3]"),
      TestCase("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"),
      TestCase("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"),
    ).forEach { test ->
      val initialTree = decode(test.encodedInitialTree)

      assertTrue { Day18.ReduceOperation(initialTree).reduceTreeExplode() }
      assertEquals(test.encodedExpectedTree, encode(initialTree))
    }
  }

  @Test
  fun classReduceOperation_reduceTreeExplode_doesNotExplode() {
    listOf(
      "[1,2]",
      "[16,2]",
      "[[3,[2,[8,0]]],[9,[5,[7,0]]]]",
      "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]",
    ).forEach { encodedInitialTree ->
      val tree = decode(encodedInitialTree)

      assertFalse { Day18.ReduceOperation(tree).reduceTreeExplode() }
      assertEquals(encodedInitialTree, encode(tree))
    }
  }

  @Test
  fun classReduceOperation_reduceTreeSplit_splits() {
    data class TestCase(val encodedInitialTree: String, val encodedExpectedTree: String)

    listOf(
      TestCase("[10,4]", "[[5,5],4]"),
      TestCase("[4,10]", "[4,[5,5]]"),
      TestCase("[11,4]", "[[5,6],4]"),
      TestCase("[11,12]", "[[5,6],12]"),
    ).forEach { test ->
      val initialTree = decode(test.encodedInitialTree)

      assertTrue { Day18.ReduceOperation(initialTree).reduceTreeSplit() }
      assertEquals(test.encodedExpectedTree, encode(initialTree))
    }
  }

  @Test
  fun classReduceOperation_reduceTreeSplit_doesNotSplit() {
    listOf("[1,4]", "[[[[[9,8],1],2],3],4]").forEach { encodedInitialTree ->
      val initialTree = decode(encodedInitialTree)

      assertFalse { Day18.ReduceOperation(initialTree).reduceTreeSplit() }
      assertEquals(encodedInitialTree, encode(initialTree))
    }
  }

  @Test
  fun addTrees_withSample() {
    val tree1 = decode("[[[[4,3],4],4],[7,[[8,4],9]]]")
    val tree2 = decode("[1,1]")

    val treeSum = Day18.addTrees(tree1, tree2)

    assertEquals("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", encode(treeSum))
  }

  @Test
  fun sumEncodedTrees_sample1() {
    val tree = Day18.sumEncodedTrees(listOf("[1,1]", "[2,2]", "[3,3]", "[4,4]"))
    assertEquals("[[[[1,1],[2,2]],[3,3]],[4,4]]", encode(tree))
  }

  @Test
  fun sumEncodedTrees_sample2() {
    val tree = Day18.sumEncodedTrees(listOf("[1,1]", "[2,2]", "[3,3]", "[4,4]", "[5,5]"))
    assertEquals("[[[[3,0],[5,3]],[4,4]],[5,5]]", encode(tree))
  }

  @Test
  fun sumEncodedTrees_sample3() {
    val tree = Day18.sumEncodedTrees(listOf("[1,1]", "[2,2]", "[3,3]", "[4,4]", "[5,5]", "[6,6]"))
    assertEquals("[[[[5,0],[7,4]],[5,5]],[6,6]]", encode(tree))
  }

  @Test
  fun sumEncodedTrees_sample4() {
    val tree = Day18.sumEncodedTrees(listOf(
      "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]",
      "[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]",
      "[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]",
      "[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]",
      "[7,[5,[[3,8],[1,4]]]]",
      "[[2,[2,2]],[8,[8,1]]]",
      "[2,9]",
      "[1,[[[9,3],9],[[9,0],[0,7]]]]",
      "[[[5,[7,4]],7],1]",
      "[[[[4,2],2],6],[8,7]]",
    ))
    assertEquals("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]", encode(tree))
  }

  @Test
  fun calculateMagnitude_samples() {
    data class TestCase(val encodedTree: String, val expectedMagnitude: Int)

    listOf(
      TestCase("[[1,2],[[3,4],5]]", 143),
      TestCase("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", 1384),
      TestCase("[[[[1,1],[2,2]],[3,3]],[4,4]]", 445),
      TestCase("[[[[3,0],[5,3]],[4,4]],[5,5]]", 791),
      TestCase("[[[[5,0],[7,4]],[5,5]],[6,6]]", 1137),
      TestCase("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]", 3488),
    ).forEach { test ->
      val tree = decode(test.encodedTree)
      assertEquals(test.expectedMagnitude, Day18.calculateMagnitude(tree))
    }
  }

  @Test
  fun testPart1_sample() {
    val ans = Day18.solvePart1(listOf(
      "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]",
      "[[[5,[2,8]],4],[5,[[9,9],0]]]",
      "[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]",
      "[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]",
      "[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]",
      "[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]",
      "[[[[5,4],[7,7]],8],[[8,3],8]]",
      "[[9,3],[[9,9],[6,[4,9]]]]",
      "[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]",
      "[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]",
    ))
    assertEquals(4140, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day18")
    val ans = Day18.solvePart1(lines)
    assertEquals(2541, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day18.solvePart2(listOf(
      "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]",
      "[[[5,[2,8]],4],[5,[[9,9],0]]]",
      "[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]",
      "[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]",
      "[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]",
      "[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]",
      "[[[[5,4],[7,7]],8],[[8,3],8]]",
      "[[9,3],[[9,9],[6,[4,9]]]]",
      "[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]",
      "[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]",
    ))
    assertEquals(3993, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day18")
    val ans = Day18.solvePart2(lines)
    assertEquals(4647, ans)
  }

  private fun encode(root: Day18.Node): String = root.toString()
  private fun decode(encoded: String) = Day18.decodeTree(Day18.TreeStream(encoded), null)
}
