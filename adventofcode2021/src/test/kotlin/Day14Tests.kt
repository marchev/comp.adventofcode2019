import kotlin.test.Test
import kotlin.test.assertEquals

class Day14Tests {
  private val sampleInput = listOf(
    "NNCB",
    "",
    "CH -> B",
    "HH -> N",
    "CB -> H",
    "NH -> C",
    "HB -> C",
    "HC -> B",
    "HN -> C",
    "NN -> C",
    "BH -> H",
    "NC -> B",
    "NB -> B",
    "BN -> B",
    "BB -> N",
    "BC -> B",
    "CC -> N",
    "CN -> C",
  )

  @Test
  fun testPart1_sample() {
    val ans = Day14.solvePart1(sampleInput)
    assertEquals(1588, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day14")
    val ans = Day14.solvePart1(lines)
    assertEquals(2937, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day14.solvePart2(sampleInput)
    assertEquals(2188189693529L, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day14")
    val ans = Day14.solvePart2(lines)
    assertEquals(3390034818249, ans)
  }
}
