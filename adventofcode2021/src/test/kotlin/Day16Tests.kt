import kotlin.test.Test
import kotlin.test.assertEquals

class Day16Tests {
  @Test
  fun parsePackage_parsesLiteral() {
    val stream = Day16.BinaryStream(Day16.hexToBinary("D2FE28"))
    val ans = Day16.parsePackage(stream)
    assertEquals(ans, Day16.Package(6, 4, 2021, emptyList()))
  }

  @Test
  fun parsePackage_parsesLengthType0() {
    val stream = Day16.BinaryStream(Day16.hexToBinary("38006F45291200"))
    val ans = Day16.parsePackage(stream)
    assertEquals(ans,
                 Day16.Package(1,
                               6,
                               1,
                               listOf(
                                 Day16.Package(6, 4, 10, emptyList()),
                                 Day16.Package(2, 4, 20, emptyList()),
                               )))
  }

  @Test
  fun parsePackage_parsesLengthType1() {
    val stream = Day16.BinaryStream(Day16.hexToBinary("EE00D40C823060"))
    val ans = Day16.parsePackage(stream)
    assertEquals(ans,
                 Day16.Package(7,
                               3,
                               3,
                               listOf(
                                 Day16.Package(2, 4, 1, emptyList()),
                                 Day16.Package(4, 4, 2, emptyList()),
                                 Day16.Package(1, 4, 3, emptyList()),
                               )))
  }

  @Test
  fun testPart1_sample1() {
    val ans = Day16.solvePart1(listOf("8A004A801A8002F478"))
    assertEquals(16, ans)
  }

  @Test
  fun testPart1_sample2() {
    val ans = Day16.solvePart1(listOf("620080001611562C8802118E34"))
    assertEquals(12, ans)
  }

  @Test
  fun testPart1_sample3() {
    val ans = Day16.solvePart1(listOf("C0015000016115A2E0802F182340"))
    assertEquals(23, ans)
  }

  @Test
  fun testPart1_sample4() {
    val ans = Day16.solvePart1(listOf("A0016C880162017C3686B18A3D4780"))
    assertEquals(31, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day16")
    val ans = Day16.solvePart1(lines)
    assertEquals(923, ans)
  }

  @Test
  fun testPart2_sample_type0Sum() {
    val ans = Day16.solvePart2(listOf("C200B40A82"))
    assertEquals(3, ans)
  }

  @Test
  fun testPart2_sample_type1Product() {
    val ans = Day16.solvePart2(listOf("04005AC33890"))
    assertEquals(54, ans)
  }

  @Test
  fun testPart2_sample_type2Min() {
    val ans = Day16.solvePart2(listOf("880086C3E88112"))
    assertEquals(7, ans)
  }

  @Test
  fun testPart2_sample_type3Max() {
    val ans = Day16.solvePart2(listOf("CE00C43D881120"))
    assertEquals(9, ans)
  }

  @Test
  fun testPart2_sample_type4Value() {
    val ans = Day16.solvePart2(listOf("D2FE28"))
    assertEquals(2021, ans)
  }

  @Test
  fun testPart2_sample_type5GreaterThan() {
    val ans = Day16.solvePart2(listOf("F600BC2D8F"))
    assertEquals(0, ans)
  }

  @Test
  fun testPart2_sample_type6LessThan() {
    val ans = Day16.solvePart2(listOf("D8005AC2A8F0"))
    assertEquals(1, ans)
  }

  @Test
  fun testPart2_sample_type7EqualTo() {
    val ans = Day16.solvePart2(listOf("9C005AC2F8F0"))
    assertEquals(0, ans)
  }

  @Test
  fun testPart2_sample_nested() {
    val ans = Day16.solvePart2(listOf("9C0141080250320F1802104A08"))
    assertEquals(1, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day16")
    val ans = Day16.solvePart2(lines)
    assertEquals(258888628940L, ans)
  }
}
