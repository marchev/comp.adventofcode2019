import kotlin.test.Test
import kotlin.test.assertEquals

class Day15Tests {
  private val sampleInput = listOf(
    "1163751742",
    "1381373672",
    "2136511328",
    "3694931569",
    "7463417111",
    "1319128137",
    "1359912421",
    "3125421639",
    "1293138521",
    "2311944581",
  )

  @Test
  fun testPart1_sample() {
    val ans = Day15.solvePart1(sampleInput)
    assertEquals(40, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day15")
    val ans = Day15.solvePart1(lines)
    assertEquals(656, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day15.solvePart2(sampleInput)
    assertEquals(315, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day15")
    val ans = Day15.solvePart2(lines)
    assertEquals(2979, ans)
  }
}
