import kotlin.test.Test
import kotlin.test.assertEquals

class Day23Tests {
  private val sampleInput = listOf(
    "#############",
    "#...........#",
    "###B#C#B#D###",
    "  #A#D#C#A#",
    "  #########",
  )

  @Test
  fun findNextStatesByMovingFromHallwayToRooms_sample1() {
    val curState =
      Day23.StateWithDistance(Day23.State(".....D.D.A.", listOf("A.", "BB", "CC", "..")), 10)
    val expectedNextStates =
      listOf(Day23.StateWithDistance(Day23.State(".....D...A.", listOf("A.", "BB", "CC", "D.")),
                                     3010))
    assertEquals(expectedNextStates, Day23.findNextStatesByMovingFromHallwayToRooms(curState))
  }

  @Test
  fun findNextStatesByMovingFromHallwayToRooms_sample2() {
    val curState =
      Day23.StateWithDistance(Day23.State(".....D...A.", listOf("A.", "BB", "CC", "D.")),
                              10)
    val expectedNextStates =
      listOf(Day23.StateWithDistance(Day23.State(".........A.", listOf("A.", "BB", "CC", "DD")),
                                     4010))
    assertEquals(expectedNextStates, Day23.findNextStatesByMovingFromHallwayToRooms(curState))
  }

  @Test
  fun findNextStatesByMovingFromHallwayToRooms_sample3() {
    val curState =
      Day23.StateWithDistance(Day23.State(".........A.", listOf("A.", "BB", "CC", "DD")),
                              0)
    val expectedNextStates =
      listOf(Day23.StateWithDistance(Day23.State("...........", listOf("AA", "BB", "CC", "DD")),
                                     8))
    assertEquals(expectedNextStates, Day23.findNextStatesByMovingFromHallwayToRooms(curState))
  }

  @Test
  fun findNextStatesByMovingFromHallwayToRooms_terminal_returnsNoNextSteps() {
    val curState =
      Day23.StateWithDistance(Day23.State("...........", listOf("AA", "BB", "CC", "DD")), 0)
    assertEquals(listOf(), Day23.findNextStatesByMovingFromHallwayToRooms(curState))
  }

  @Test
  fun findNextStatesByMovingFromHallwayToRooms_deadEndPosition_returnsNoNextSteps() {
    val curState =
      Day23.StateWithDistance(Day23.State("A.........D", listOf("D.", "BB", "CC", "A.")), 0)
    assertEquals(listOf(), Day23.findNextStatesByMovingFromHallwayToRooms(curState))
  }

  @Test
  fun findNextStatesByMovingFromHallwayToRooms_multipleNextStates() {
    val curState =
      Day23.StateWithDistance(Day23.State("A.........D", listOf("A.", "BB", "CC", "D.")),
                              0)
    val expectedNextStates = listOf(
      Day23.StateWithDistance(Day23.State("..........D", listOf("AA", "BB", "CC", "D.")), 3),
      Day23.StateWithDistance(Day23.State("A..........", listOf("A.", "BB", "CC", "DD")), 3000),
    )
    assertEquals(expectedNextStates, Day23.findNextStatesByMovingFromHallwayToRooms(curState))
  }

  @Test
  fun findNextStatesByMovingFromRoomsToHallway_onlyOnePieceCanMove() {
    val curState =
      Day23.StateWithDistance(Day23.State(".....D.....", listOf("A.", "BB", "CC", "AD")), 0)
    val expectedNextStates = listOf(
      Day23.StateWithDistance(Day23.State(".....D.D...", listOf("A.", "BB", "CC", "A.")), 2000),
      Day23.StateWithDistance(Day23.State(".....D...D.", listOf("A.", "BB", "CC", "A.")), 2000),
      Day23.StateWithDistance(Day23.State(".....D....D", listOf("A.", "BB", "CC", "A.")), 3000),
    )
    assertEquals(expectedNextStates, Day23.findNextStatesByMovingFromRoomsToHallway(curState))
  }

  @Test
  fun findNextStatesByMovingFromRoomsToHallway_noneCanMove() {
    val curState =
      Day23.StateWithDistance(Day23.State("........A.", listOf("A.", "BB", "CC", "DD")), 0)
    assertEquals(listOf(), Day23.findNextStatesByMovingFromRoomsToHallway(curState))
  }

  @Test
  fun findNextStatesByMovingFromRoomsToHallway_multiplePiecesCanMove() {
    val curState =
      Day23.StateWithDistance(Day23.State("...B.......", listOf("AB", "DC", "C.", "AD")), 0)
    val expectedNextStates = listOf(
      Day23.StateWithDistance(Day23.State("B..B.......", listOf("A.", "DC", "C.", "AD")), 30),
      Day23.StateWithDistance(Day23.State(".B.B.......", listOf("A.", "DC", "C.", "AD")), 20),
      Day23.StateWithDistance(Day23.State("...B.C.....", listOf("AB", "D.", "C.", "AD")), 200),
      Day23.StateWithDistance(Day23.State("...B...C...", listOf("AB", "D.", "C.", "AD")), 400),
      Day23.StateWithDistance(Day23.State("...B.....C.", listOf("AB", "D.", "C.", "AD")), 600),
      Day23.StateWithDistance(Day23.State("...B......C", listOf("AB", "D.", "C.", "AD")), 700),
      Day23.StateWithDistance(Day23.State("...B.D.....", listOf("AB", "DC", "C.", "A.")), 4000),
      Day23.StateWithDistance(Day23.State("...B...D...", listOf("AB", "DC", "C.", "A.")), 2000),
      Day23.StateWithDistance(Day23.State("...B.....D.", listOf("AB", "DC", "C.", "A.")), 2000),
      Day23.StateWithDistance(Day23.State("...B......D", listOf("AB", "DC", "C.", "A.")), 3000),
    )
    assertEquals(expectedNextStates, Day23.findNextStatesByMovingFromRoomsToHallway(curState))
  }

  @Test
  fun testPart1_sample() {
    val ans = Day23.solvePart1(sampleInput)
    assertEquals(12521, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day23")
    val ans = Day23.solvePart1(lines)
    assertEquals(16489, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day23.solvePart2(sampleInput)
    assertEquals(44169, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day23")
    val ans = Day23.solvePart2(lines)
    assertEquals(43413, ans)
  }
}
