import kotlin.test.Test
import kotlin.test.assertEquals

internal class Day1Tests {
  @Test
  fun testPhase1_sample1() {
    val ans = Day1.solvePart1(listOf(199, 200, 208, 210, 200, 207, 240, 269, 260, 263))
    assertEquals(7, ans)
  }

  @Test
  fun testPhase1_real() {
    val input = readLines("Day1").map { it.toInt() }
    val ans = Day1.solvePart1(input)
    assertEquals(1451, ans)
  }

  @Test
  fun testPhase2_sample1() {
    val ans = Day1.solvePart2(listOf(199, 200, 208, 210, 200, 207, 240, 269, 260, 263))
    assertEquals(5, ans)
  }

  @Test
  fun testPhase2_real() {
    val input = readLines("Day1").map { it.toInt() }
    val ans = Day1.solvePart2(input)
    assertEquals(1395, ans)
  }
}