import kotlin.test.Test
import kotlin.test.assertEquals

internal class Day2Tests {

  private val sampleInstructions = listOf(
    Day2.Instruction(Day2.InstructionType.Forward, 5),
    Day2.Instruction(Day2.InstructionType.Down, 5),
    Day2.Instruction(Day2.InstructionType.Forward, 8),
    Day2.Instruction(Day2.InstructionType.Up, 3),
    Day2.Instruction(Day2.InstructionType.Down, 8),
    Day2.Instruction(Day2.InstructionType.Forward, 2),
  )

  @Test
  fun testPhase1_real() {
    val ans = Day2.solvePart1(Day2.readInstructions())
    assertEquals(1580000, ans)
  }

  @Test
  fun testPhase2_sample() {
    val ans = Day2.solvePart2(sampleInstructions)
    assertEquals(900, ans)
  }

  @Test
  fun testPhase2_real() {
    val ans = Day2.solvePart2(Day2.readInstructions())
    assertEquals(1251263225, ans)
  }
}
