import kotlin.test.Test
import kotlin.test.assertEquals

class Day7Tests {
  private val sampleInput = listOf(
    "16,1,2,0,4,2,7,1,2,14"
  )

  @Test
  fun testPart1_sample() {
    val ans = Day7.solvePart1(sampleInput)
    assertEquals(37, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day7")
    val ans = Day7.solvePart1(lines)
    assertEquals(352331, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day7.solvePart2(sampleInput)
    assertEquals(168, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day7")
    val ans = Day7.solvePart2(lines)
    assertEquals(99266250, ans)
  }
}
