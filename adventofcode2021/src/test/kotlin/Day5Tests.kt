import kotlin.test.Test
import kotlin.test.assertEquals

internal class Day5Tests {

  private val sampleInput = listOf(
    "0,9 -> 5,9",
    "8,0 -> 0,8",
    "9,4 -> 3,4",
    "2,2 -> 2,1",
    "7,0 -> 7,4",
    "6,4 -> 2,0",
    "0,9 -> 2,9",
    "3,4 -> 1,4",
    "0,0 -> 8,8",
    "5,5 -> 8,2",
  )

  @Test
  fun testPart1_sample() {
    val ans = Day5.solvePart1(sampleInput)
    assertEquals(5, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day5")
    val ans = Day5.solvePart1(lines)
    assertEquals(5145, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day5.solvePart2(sampleInput)
    assertEquals(12, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day5")
    val ans = Day5.solvePart2(lines)
    assertEquals(16518, ans)
  }
}
