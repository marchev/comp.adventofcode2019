import kotlin.test.Test
import kotlin.test.assertEquals

class Day11Tests {
  private val sampleInput = listOf(
    "5483143223",
    "2745854711",
    "5264556173",
    "6141336146",
    "6357385478",
    "4167524645",
    "2176841721",
    "6882881134",
    "4846848554",
    "5283751526",
  )

  @Test
  fun iterate_sample() {
    val ans = Day11.iterate(listOf(
      listOf(1, 1, 1, 1, 1),
      listOf(1, 9, 9, 9, 1),
      listOf(1, 9, 1, 9, 1),
      listOf(1, 9, 9, 9, 1),
      listOf(1, 1, 1, 1, 1),
    ))
    assertEquals(listOf(
      listOf(3, 4, 5, 4, 3),
      listOf(4, 0, 0, 0, 4),
      listOf(5, 0, 0, 0, 5),
      listOf(4, 0, 0, 0, 4),
      listOf(3, 4, 5, 4, 3),
    ), ans)
  }

  @Test
  fun testPart1_sample() {
    val ans = Day11.solvePart1(sampleInput)
    assertEquals(1656, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day11")
    val ans = Day11.solvePart1(lines)
    assertEquals(1608, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day11.solvePart2(sampleInput)
    assertEquals(195, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day11")
    val ans = Day11.solvePart2(lines)
    assertEquals(214, ans)
  }
}
