import kotlin.test.Test
import kotlin.test.assertEquals

class Day9Tests {
  private val sampleInput = listOf(
    "2199943210",
    "3987894921",
    "9856789892",
    "8767896789",
    "9899965678",
  )

  @Test
  fun testPart1_sample() {
    val ans = Day9.solvePart1(sampleInput)
    assertEquals(15, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day9")
    val ans = Day9.solvePart1(lines)
    assertEquals(591, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day9.solvePart2(sampleInput)
    assertEquals(1134, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day9")
    val ans = Day9.solvePart2(lines)
    assertEquals(1113424, ans)
  }
}
