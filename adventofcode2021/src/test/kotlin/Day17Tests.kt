import kotlin.test.Test
import kotlin.test.assertEquals

class Day17Tests {
  private val sampleInput = listOf(
    "target area: x=20..30, y=-10..-5"
  )

  @Test
  fun testPart1_sample() {
    val ans = Day17.solvePart1(sampleInput)
    assertEquals(45, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day17")
    val ans = Day17.solvePart1(lines)
    assertEquals(2701, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day17.solvePart2(sampleInput)
    assertEquals(112, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day17")
    val ans = Day17.solvePart2(lines)
    assertEquals(1070, ans)
  }
}
