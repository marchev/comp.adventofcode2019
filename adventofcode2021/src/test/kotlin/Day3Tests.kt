import kotlin.test.Test
import kotlin.test.assertEquals

internal class Day3Tests {

  private val sampleInput = listOf(
    "00100",
    "11110",
    "10110",
    "10111",
    "10101",
    "01111",
    "00111",
    "11100",
    "10000",
    "11001",
    "00010",
    "01010",
  )

  @Test
  fun testPart1_sample() {
    val ans = Day3.solvePart1(sampleInput)
    assertEquals(198, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day3")
    val ans = Day3.solvePart1(lines)
    assertEquals(2967914, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day3.solvePart2(sampleInput)
    assertEquals(230, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day3")
    val ans = Day3.solvePart2(lines)
    assertEquals(7041258, ans)
  }
}
