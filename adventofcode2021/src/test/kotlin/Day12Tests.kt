import kotlin.test.Test
import kotlin.test.assertEquals

class Day12Tests {
  private val sampleInput1 = listOf(
    "start-A",
    "start-b",
    "A-c",
    "A-b",
    "b-d",
    "A-end",
    "b-end",
  )

  private val sampleInput2 = listOf(
    "dc-end",
    "HN-start",
    "start-kj",
    "dc-start",
    "dc-HN",
    "LN-dc",
    "HN-end",
    "kj-sa",
    "kj-HN",
    "kj-dc",
  )

  private val sampleInput3 = listOf(
    "fs-end",
    "he-DX",
    "fs-he",
    "start-DX",
    "pj-DX",
    "end-zg",
    "zg-sl",
    "zg-pj",
    "pj-he",
    "RW-he",
    "fs-DX",
    "pj-RW",
    "zg-RW",
    "start-pj",
    "he-WI",
    "zg-he",
    "pj-fs",
    "start-RW",
  )

  @Test
  fun testPart1_sample1() {
    val ans = Day12.solvePart1(sampleInput1)
    assertEquals(10, ans)
  }

  @Test
  fun testPart1_sample2() {
    val ans = Day12.solvePart1(sampleInput2)
    assertEquals(19, ans)
  }

  @Test
  fun testPart1_sample3() {
    val ans = Day12.solvePart1(sampleInput3)
    assertEquals(226, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day12")
    val ans = Day12.solvePart1(lines)
    assertEquals(5104, ans)
  }

  @Test
  fun testPart2_sample1() {
    val ans = Day12.solvePart2(sampleInput1)
    assertEquals(36, ans)
  }

  @Test
  fun testPart2_sample2() {
    val ans = Day12.solvePart2(sampleInput2)
    assertEquals(103, ans)
  }

  @Test
  fun testPart2_sample3() {
    val ans = Day12.solvePart2(sampleInput3)
    assertEquals(3509, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day12")
    val ans = Day12.solvePart2(lines)
    assertEquals(149220, ans)
  }
}
