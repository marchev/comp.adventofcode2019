import kotlin.test.Test
import kotlin.test.assertEquals

internal class Day6Tests {

  private val sampleInput = listOf(
    "3,4,3,1,2",
  )

  @Test
  fun testPart1_sample() {
    val ans = Day6.solvePart1(sampleInput)
    assertEquals(5934, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day6")
    val ans = Day6.solvePart1(lines)
    assertEquals(374994, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day6.solvePart2(sampleInput)
    assertEquals(26984457539L, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day6")
    val ans = Day6.solvePart2(lines)
    assertEquals(ans, 1686252324092L)
  }
}
