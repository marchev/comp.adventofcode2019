import kotlin.test.Test
import kotlin.test.assertEquals

class Day13Tests {
  private val sampleInput = listOf(
    "6,10",
    "0,14",
    "9,10",
    "0,3",
    "10,4",
    "4,11",
    "6,0",
    "6,12",
    "4,1",
    "0,13",
    "10,12",
    "3,4",
    "3,0",
    "8,4",
    "1,10",
    "2,14",
    "8,10",
    "9,0",
    "",
    "fold along y=7",
    "fold along x=5",
  )

  @Test
  fun testPart1_real() {
    val lines = readLines("Day13")
    val ans = Day13.solvePart1(lines)
    assertEquals(-1, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day13")
    Day13.solvePart2(lines)
  }
}
