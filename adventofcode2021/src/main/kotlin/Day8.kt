/// https://adventofcode.com/2021, Day 8
///
/// Task: https://adventofcode.com/2021/day/8/
/// Date: 2022-08-14
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 318
/// Phase 2 answer: 996280
///
/// Tags: simulation, permutations, encoding

object Day8 {
  fun solvePart1(lines: List<String>): Int {
    var count = 0
    for (line in lines) {
      val tokens = line.split(' ')
      val outputs = tokens.drop(11)
      for (output in outputs) {
        val len = output.length
        count += if (len == 2 || len == 4 || len == 7 || len == 3) 1 else 0
      }
    }
    return count
  }

  fun nextPermutation(list: MutableList<Int>) {
    var i = list.size - 2
    while (i >= 0 && list[i] > list[i + 1]) i -= 1

    if (i >= 0) {
      var j = list.size - 1
      while (j > i && list[j] < list[i]) j -= 1
      // Swap i and j
      val tmp = list[i]
      list[i] = list[j]
      list[j] = tmp
    }

    // Reverse all after i
    var a = i + 1
    var b = list.size - 1
    while (a < b) {
      val tmp = list[a]
      list[a] = list[b]
      list[b] = tmp
      a += 1
      b -= 1
    }
  }

  val digitToSegments =
    listOf("abcefg", "cf", "acdeg", "acdfg", "bcdf", "abdfg", "abdefg", "acf", "abcdefg", "abcdfg")

  fun decodePattern(inputs: List<String>, patterns: List<String>): Int {
    val perm = mutableListOf(0, 1, 2, 3, 4, 5, 6)
    var i = 0
    while (i < 5040) { // 7!
      var allMapToDigits = true
      for (input in inputs) {
        val remappedInput = input.map { 'a' + perm[it - 'a'] }.sorted().joinToString("")
        if (!digitToSegments.contains(remappedInput)) {
          allMapToDigits = false
          break
        }
      }
      if (allMapToDigits) {
        var value = 0
        for (pattern in patterns) {
          val remappedPattern = pattern.map { 'a' + perm[it - 'a'] }.sorted().joinToString("")
          val digit = digitToSegments.indexOf(remappedPattern)
          assert(digit >= 0)
          value = 10 * value + digit
        }
        return value
      }

      nextPermutation(perm)
      i += 1
    }

    throw IllegalArgumentException("The input is incorrect")
  }

  fun solvePart2(lines: List<String>): Int {
    var sum = 0
    for (line in lines) {
      val tokens = line.split(' ')
      sum += decodePattern(tokens.take(10), tokens.drop(11))
    }
    return sum
  }
}

fun main() {
  val lines = readLines("Day8")
  println("Part 1 solution = ${Day8.solvePart1(lines)}")
  println("Part 2 solution = ${Day8.solvePart2(lines)}")
}
