import java.io.File

/**
 * Reads lines from the given input txt file.
 */
fun readLines(filename: String) = File("src/main/resources", "$filename.txt").readLines()
