/// https://adventofcode.com/2021, Day 4
///
/// Task: https://adventofcode.com/2021/day/4/
/// Date: 2021-12-05
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 49686
/// Phase 2 answer: 26878
///
/// Tags: simulation, grids

object Day4 {
  fun solvePart1(lines: List<String>): Int {
    val drawnNumbers = lines.first().split(',').map { it.toInt() }.toList()
    val cards = readCards(lines.subList(2, lines.size))

    for (drawnNum in drawnNumbers) {
      for (card in cards) {
        card.markIfAvailable(drawnNum)
        if (card.isWinning()) {
          return drawnNum * card.unmarkedNumbers().sum()
        }
      }
    }
    throw AssertionError();
  }

  fun solvePart2(lines: List<String>): Int {
    val drawnNumbers = lines.first().split(',').map { it.toInt() }.toList()
    val cards = readCards(lines.subList(2, lines.size))

    val winning = mutableSetOf<Card>()
    for (drawnNum in drawnNumbers) {
      for (card in cards) {
        card.markIfAvailable(drawnNum)
        if (card.isWinning() && card !in winning) {
          winning += card
          if (winning.size == cards.size) {
            return drawnNum * card.unmarkedNumbers().sum()
          }
        }
      }
    }
    throw AssertionError();
  }

  private fun readCards(lines: List<String>): List<Card> {
    val cards = mutableListOf<Card>()
    var i = 0
    while (i <= lines.size - 5) {
      val cardMatrix = Array(5) {
        val row = lines[i].split(' ').filter { it.isNotEmpty() }.map { it.toInt() }.toIntArray()
        i += 1
        row
      }
      cards += Card(cardMatrix)
      i += 1 // skip empty line
    }
    return cards.toList()
  }

  class Card(private val m: Array<IntArray>) {
    private val marked = Array(m.size) { BooleanArray(m.first().size) }

    fun markIfAvailable(num: Int): Unit {
      val coords = findNumber(num)
      if (coords != null) {
        marked[coords.first][coords.second] = true
      }
    }

    fun isWinning(): Boolean {
      for (r in m.indices) {
        if (marked[r].all { it }) {
          return true
        }
      }
      for (c in m.indices) {
        if (marked.map { it[c] }.all { it }) {
          return true
        }
      }
      return false
    }

    fun unmarkedNumbers(): List<Int> {
      val unmarkedNums = mutableListOf<Int>()
      for (r in m.indices) {
        for (c in m.indices) {
          if (!marked[r][c]) {
            unmarkedNums += m[r][c]
          }
        }
      }
      return unmarkedNums
    }

    private fun findNumber(num: Int): Pair<Int, Int>? {
      for (r in m.indices) {
        for (c in m.indices) {
          if (m[r][c] == num) {
            return Pair(r, c)
          }
        }
      }
      return null;
    }

    override fun toString(): String {
      val builder = StringBuilder()
      for (row in m) {
        row.joinTo(builder, " ")
        builder.appendLine()
      }
      return builder.toString()
    }
  }
}

fun main() {
  val lines = readLines("Day4")
  println("Part 1 solution = ${Day4.solvePart1(lines)}")
  println("Part 2 solution = ${Day4.solvePart2(lines)}")
}
