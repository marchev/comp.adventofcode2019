import java.util.PriorityQueue
import kotlin.streams.toList

/**
https:adventofcode.com/2021, Day 15

Task: https:adventofcode.com/2021/day/15/
Date: 2021-12-25
Author: Svilen Marchev

Status: works for both parts

Phase 1 answer: 656
Phase 2 answer: 2979

Tags: graph theory, dijkstra, shortest path, grid
 */

object Day15 {
  private val dr = intArrayOf(0, 1, 0, -1)
  private val dc = intArrayOf(1, 0, -1, 0)

  fun solvePart1(lines: List<String>): Int {
    val grid = readGrid(lines)
    return findShortestPath(grid)
  }

  fun solvePart2(lines: List<String>): Int {
    val tile = readGrid(lines)
    val n = tile.size
    val grid = List(n * 5) { MutableList(n * 5) { 0 } }
    for (r in grid.indices) {
      for (c in grid.indices) {
        val inc = r / n + c / n
        grid[r][c] = (tile[r % n][c % n] - 1 + inc) % 9 + 1
      }
    }
    return findShortestPath(grid)
  }


  private fun findShortestPath(grid: List<List<Int>>): Int {
    val n = grid.size
    val dist = List(n) { MutableList(n) { Int.MAX_VALUE } }
    val queue = PriorityQueue<NodeWithDistance>(n * n) { n1, n2 -> n1.dist - n2.dist }

    dist[0][0] = 0
    queue.add(NodeWithDistance(0, 0, 0))

    while (!queue.isEmpty()) {
      val cur = queue.poll()
      if (cur.dist != dist[cur.r][cur.c]) {
        continue
      }
      if (cur.r == n - 1 && cur.c == n - 1) {
        return cur.dist
      }
      for (i in dr.indices) {
        val nr = dr[i] + cur.r
        val nc = dc[i] + cur.c
        if (nr in 0 until n && nc in 0 until n && dist[nr][nc] > cur.dist + grid[nr][nc]) {
          dist[nr][nc] = cur.dist + grid[nr][nc]
          queue.add(NodeWithDistance(nr, nc, dist[nr][nc]))
        }
      }
    }
    throw AssertionError()
  }

  data class NodeWithDistance(val r: Int, val c: Int, val dist: Int)

  private fun readGrid(lines: List<String>): List<List<Int>> {
    return lines.map { line ->
      line.chars().map { it - '0'.code }.toList()
    }
  }
}

fun main() {
  val lines = readLines("Day15")
  println("Part 1 solution = ${Day15.solvePart1(lines)}")
  println("Part 2 solution = ${Day15.solvePart2(lines)}")
}
