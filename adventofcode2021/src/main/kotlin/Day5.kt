import java.util.regex.Pattern
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sign

/// https://adventofcode.com/2021, Day 5
///
/// Task: https://adventofcode.com/2021/day/5/
/// Date: 2021-12-05
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 5145
/// Phase 2 answer: 16518
///
/// Tags: geometry, grid

object Day5 {
  fun solvePart1(lines: List<String>): Int {
    val segments = readSegments(lines)
    val numCovers = Array(1024) { IntArray(1024) }

    for (segment in segments) {
      if (segment.c1 == segment.c2 || segment.r1 == segment.r2) {
        for (r in min(segment.r1, segment.r2)..max(segment.r1, segment.r2)) {
          for (c in min(segment.c1, segment.c2)..max(segment.c1, segment.c2)) {
            numCovers[r][c] += 1
          }
        }
      }
    }
    return numCovers.sumOf { row -> row.count { it > 1 } }
  }

  fun solvePart2(lines: List<String>): Int {
    val segments = readSegments(lines)
    val numCovers = Array(1024) { IntArray(1024) }

    for (segment in segments) {
      val rowStep = (segment.r2 - segment.r1).sign
      val colStep = (segment.c2 - segment.c1).sign
      var r = segment.r1
      var c = segment.c1
      numCovers[r][c] += 1
      do {
        r += rowStep
        c += colStep
        numCovers[r][c] += 1
      } while (r != segment.r2 || c != segment.c2)
    }
    return numCovers.sumOf { row -> row.count { it > 1 } }
  }

  private fun readSegments(lines: List<String>): List<Segment> {
    return lines.map { line ->
      val (c1, r1, c2, r2) = line.split(regex = Pattern.compile(",|( \\-\\> )"))
        .map { it.toInt() }
        .toList()
      Segment(r1, c1, r2, c2)
    }.toList()
  }

  data class Segment(val r1: Int, val c1: Int, val r2: Int, val c2: Int)
}

fun main() {
  val lines = readLines("Day5")
  println("Part 1 solution = ${Day5.solvePart1(lines)}")
  println("Part 2 solution = ${Day5.solvePart2(lines)}")
}
