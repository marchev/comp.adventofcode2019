import java.util.regex.Pattern
import kotlin.math.max

/**
https:adventofcode.com/2021, Day 21

Task: https:adventofcode.com/2021/day/21/
Date: 2022-01-04
Author: Svilen Marchev

Status: works for both parts

Phase 1 answer: 913560
Phase 2 answer: 110271560863819

The second part is nice.

Part 1:
-------------
It can be simply simulated.

Part 2:
-------------
I use Dynamic Programming. The state is:

numWinning(pos1, pos2, score1, score2) gives a pair of numbers: the first is the number of
universes in which the player at pos1 with score score1 will win. The second is the number of
universes in which the same player will lose.

When computing numWinning(pos1, pos2, score1, score2), assume that you are making a turn
on behalf of the player at pos1 who has score score1. In the implementation we can flip the
order of players on each recursive call. This simplifies the implementation, since we don't
need to pass to the function or remember in the state whose turn is it.

There is no loop in the recursion, since the sum of score1 and score2 will strictly increase
when we are progressing towards the bottom of the recursion.

Tags: DP, memoization, game theory, simulation, sequence
 */

object Day21 {
  private val inputPattern = Pattern.compile("^Player \\d starting position: (\\d+)$")

  fun solvePart1(lines: List<String>): Int {
    val (start1, start2) = readStartPositions(lines)

    val die: Iterator<Int> = iterator {
      var cur = 0
      while (true) {
        yield(cur + 1)
        cur = (cur + 1) % 100
      }
    }

    var pos1 = start1
    var pos2 = start2
    var score1 = 0
    var score2 = 0
    var numRolls = 0
    while (true) {
      pos1 = (pos1 - 1 + die.next() + die.next() + die.next()) % 10 + 1
      score1 += pos1
      numRolls += 3
      if (score1 >= 1000) {
        return score2 * numRolls
      }
      pos2 = (pos2 - 1 + die.next() + die.next() + die.next()) % 10 + 1
      score2 += pos2
      numRolls += 3
      if (score2 >= 1000) {
        return score1 * numRolls
      }
    }
    throw AssertionError()
  }

  private val invalidPair = Pair(-1L, -1L)
  private val numWinning =
    List(10) { List(10) { List(21) { MutableList(21) { invalidPair } } } }

  fun solvePart2(lines: List<String>): Long {
    val (start1, start2) = readStartPositions(lines)
    val winnersByPlayer = calculateWinners(start1 - 1, start2 - 1, 0, 0)
    return max(winnersByPlayer.first, winnersByPlayer.second)
  }

  private fun calculateWinners(p1: Int, p2: Int, s1: Int, s2: Int): Pair<Long, Long> {
    check(s1 < 21)
    if (s2 >= 21) {
      return Pair(0, 1)
    }
    if (numWinning[p1][p2][s1][s2] !== invalidPair) {
      return numWinning[p1][p2][s1][s2]
    }
    var wins = 0L
    var losses = 0L

    for (d1 in 1..3) {
      for (d2 in 1..3) {
        for (d3 in 1..3) {
          val newP = (p1 + d1 + d2 + d3) % 10
          val (newLosses, newWins) = calculateWinners(p2, newP, s2, s1 + newP + 1)
          losses += newLosses
          wins += newWins
        }
      }
    }

    numWinning[p1][p2][s1][s2] = Pair(wins, losses)
    return numWinning[p1][p2][s1][s2]
  }

  private fun readStartPositions(lines: List<String>): Pair<Int, Int> {
    val m1 = inputPattern.matcher(lines[0])
    val m2 = inputPattern.matcher(lines[1])
    check(m1.matches())
    check(m2.matches())
    return Pair(m1.group(1).toInt(), m2.group(1).toInt())
  }
}

fun main() {
  val lines = readLines("Day21")
  println("Part 1 solution = ${Day21.solvePart1(lines)}")
  println("Part 2 solution = ${Day21.solvePart2(lines)}")
}
