/// https://adventofcode.com/2021, Day 13
///
/// Task: https://adventofcode.com/2021/day/13/
/// Date: 2021-12-13, 2021-12-14
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 693
/// Phase 2 answer: UCLZRAZU
///
/// Tags: geometry

object Day13 {
  fun solvePart1(lines: List<String>): Int {
    val points = readPoints(lines).toSet()
    // Execute the first fold instruction from the input: "fold along x=655"
    val foldC = 655
    val newPoints = points.map { p ->
      if (p.c > foldC) Point(p.r, foldC - (p.c - foldC))
      else p
    }.toSet()
    return newPoints.size
  }

  fun solvePart2(lines: List<String>): Unit {
    var points = readPoints(lines).toSet()
    val foldLines = readFoldLines(lines)

    for (foldLine in foldLines) {
      points = if (foldLine.c != null) {
        points.map { p ->
          if (p.c > foldLine.c) Point(p.r, foldLine.c - (p.c - foldLine.c))
          else p
        }.toSet()
      } else {
        points.map { p ->
          if (p.r > foldLine.r!!) Point(foldLine.r!! - (p.r - foldLine.r!!), p.c)
          else p
        }.toSet()
      }
    }

    for (r in points.minOf { it.r }..points.maxOf { it.r }) {
      for (c in points.minOf { it.c }..points.maxOf { it.c }) {
        print(if (points.contains(Point(r, c))) "#" else " ")
      }
      println()
    }
  }

  private fun readPoints(lines: List<String>): List<Point> {
    return lines.subList(0, lines.indexOf("")).map {
      val (c, r) = it.split(",").map { it.toInt() }
      Point(r, c)
    }
  }

  private fun readFoldLines(lines: List<String>): List<FoldLine> {
    return lines.drop(lines.indexOf("") + 1).map {
      val coord = it.substring(it.indexOf("=") + 1).toInt()
      if (it.contains("x=")) FoldLine(null, coord)
      else FoldLine(coord, null)
    }
  }

  data class Point(val r: Int, val c: Int)
  data class FoldLine(val r: Int?, val c: Int?)
}

fun main() {
  val lines = readLines("Day13")
  println("Part 1 solution = ${Day13.solvePart1(lines)}")
  println("Part 2 solution:")
  Day13.solvePart2(lines)
}
