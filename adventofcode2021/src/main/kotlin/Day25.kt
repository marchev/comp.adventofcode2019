/**
https:adventofcode.com/2021, Day 25

Task: https:adventofcode.com/2021/day/25/
Date: 2021-12-?
Author: Svilen Marchev

Status: works for ?

Phase 1 answer: ?
Phase 2 answer: ?

Tags: ?
 */

object Day25 {
  fun solvePart1(lines: List<String>): Int {
    return -1
  }
}

fun main() {
  val lines = readLines("Day25")
  println("Part 1 solution = ${Day25.solvePart1(lines)}")
}
