/**
https:adventofcode.com/2021, Day 20

Task: https:adventofcode.com/2021/day/20/
Date: 2021-12-22
Author: Svilen Marchev

Status: works for both parts

Nice one.

The process can be simply simulated, but one must notice that the example differs
from the real input in one major regard - the real input does map completely dark
regions to light regions, and vice versa. That is, on odd iterations it maps an infinite
number of dark pixels into light pixels. This obviously cannot be simulated. But we
can just keep a map of all pixels that we have ever computed and their current light state,
and the current light state of the "background", i.e. of all other pixels that are not
in the map. Then, *at each iteration we can examine all 3x3 rectangles that include
a pixel with a color that differs from the background color*, and use it to compute the pixels
for the next iteration. At the end of each iteration we might have to invert the
background color, depending on the value of TEMPLATE[000000000].

Phase 1 answer: 5619
Phase 2 answer: 20122

Tags: simulation, grid, game of life, infinite grid
 */

object Day20 {
  fun solvePart1(lines: List<String>): Int = runImageEnhancement(lines, 2)

  fun solvePart2(lines: List<String>): Int = runImageEnhancement(lines, 50)

  private fun runImageEnhancement(lines: List<String>, iterations: Int): Int {
    val template = lines[0]
    check(template.length == 512)
    var pixels = readLightPixels(lines.drop(2))

    var isBackgroundLit = false
    for (iter in 0 until iterations) {
      val newPixels = mutableMapOf<Point, Boolean>()
      for (p in pixels.filterValues { it != isBackgroundLit }.keys) {
        for (startR in (p.r - 2)..p.r) {
          for (startC in (p.c - 2)..p.c) {
            val templateIx = computeTemplateIndex(startR, startC, pixels, isBackgroundLit)
            val point = Point(startR + 1, startC + 1)
            newPixels[point] = template[templateIx] == '#'
          }
        }
      }

      pixels = newPixels
      isBackgroundLit = if (template[0] == '#') !isBackgroundLit else isBackgroundLit
    }

    return pixels.count { it.value }
  }

  private fun computeTemplateIndex(
    startR: Int,
    startC: Int,
    pixels: Map<Point, Boolean>,
    isBackgroundLit: Boolean,
  ): Int {
    var ix = 0
    for (off in 0..8) {
      val r = startR + off / 3
      val c = startC + off % 3
      val value = if (pixels.getOrDefault(Point(r, c), isBackgroundLit)) 1 else 0
      ix = (ix shl 1) or value
    }
    return ix
  }

  private fun readLightPixels(lines: List<String>): Map<Point, Boolean> {
    val pixels = mutableMapOf<Point, Boolean>()
    for (r in lines.indices) {
      for (c in lines[r].indices) {
        if (lines[r][c] == '#') {
          pixels[Point(r, c)] = true
        }
      }
    }
    return pixels
  }

  data class Point(val r: Int, val c: Int)
}

fun main() {
  val lines = readLines("Day20")
  println("Part 1 solution = ${Day20.solvePart1(lines)}")
  println("Part 2 solution = ${Day20.solvePart2(lines)}")
}
