/// https://adventofcode.com/2021, Day 6
///
/// Task: https://adventofcode.com/2021/day/6/
/// Date: 2021-12-06, 2021-12-12
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 374994
/// Phase 2 answer: 1686252324092
///
/// Very neat one, esp. the second part.
/// The first part can be simply simulated, but for the second part one has to observe
/// that the order of elements in the sequence doesn't matter. This allows us to simply
/// keep the distribution of the values in the sequence, as opposed to the complete sequence.
///
/// Tags: sequence, compression, simulation

object Day6 {
  fun solvePart1(lines: List<String>): Int {
    var population = lines[0].split(',').map { it.toInt() }.toList()
    for (day in 1..80) {
      var numAdditions = population.count { it == 0 }
      population = population.map { if (it == 0) 6 else it - 1 } + List(numAdditions) { 8 }
    }
    return population.size
  }

  fun solvePart2(lines: List<String>): Long {
    var numbers = lines[0].split(',').map { it.toInt() }.toList()
    var population = List(9) { i -> numbers.count { it == i }.toLong() }
    for (day in 1..256) {
      val nextPopulation = MutableList(9) { if (it <= 7) population[it + 1] else 0L }
      nextPopulation[6] += population[0]
      nextPopulation[8] = population[0]
      population = nextPopulation
    }
    return population.sum()
  }
}

fun main() {
  val lines = readLines("Day6")
  println("Part 1 solution = ${Day6.solvePart1(lines)}")
  println("Part 2 solution = ${Day6.solvePart2(lines)}")
}
