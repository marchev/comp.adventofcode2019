import java.lang.IllegalArgumentException
import kotlin.math.absoluteValue

/// https://adventofcode.com/2021, Day 2
///
/// Task: https://adventofcode.com/2021/day/2/
/// Date: 2021-12-03
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 1580000
/// Phase 2 answer: 1251263225
///
/// Tags: simulation

object Day2 {
  fun solvePart1(instructions: List<Instruction>): Int {
    var x = 0
    var y = 0
    instructions.forEach {
      when (it.type) {
        InstructionType.Forward -> x += it.offset
        InstructionType.Down -> y -= it.offset
        InstructionType.Up -> y += it.offset
      }
    }
    return x * y.absoluteValue
  }

  fun solvePart2(instructions: List<Instruction>): Int {
    var x = 0
    var y = 0
    var aim = 0
    instructions.forEach {
      when (it.type) {
        InstructionType.Forward -> {
          x += it.offset
          y += aim * it.offset
        }
        InstructionType.Down -> aim += it.offset
        InstructionType.Up -> aim -= it.offset
      }
    }
    return x * y.absoluteValue
  }

  fun readInstructions(): List<Instruction> {
    return readLines("Day2").map {
      val (typeString, offset) = it.split(' ', limit = 2)
      val type = when (typeString) {
        "forward" -> InstructionType.Forward
        "up" -> InstructionType.Up
        "down" -> InstructionType.Down
        else -> throw IllegalArgumentException()
      }
      Instruction(type, offset.toInt())
    }
  }

  data class Instruction(val type: InstructionType, val offset: Int)
  enum class InstructionType { Forward, Up, Down }
}

fun main() {
  val instructions = Day2.readInstructions()
  println("Part 1 solution = ${Day2.solvePart1(instructions)}")
  println("Part 2 solution = ${Day2.solvePart2(instructions)}")
}
