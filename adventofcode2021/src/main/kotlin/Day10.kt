/// https://adventofcode.com/2021, Day 10
///
/// Task: https://adventofcode.com/2021/day/10/
/// Date: 2021-12-26
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 316851
/// Phase 2 answer: 2182912364
///
/// Tags: expression validation, brackets, stacks

object Day10 {
  fun solvePart1(lines: List<String>): Int {
    return lines.map { validate(it) }
      .filter { it.status == ValidationStatus.CORRUPTED }
      .sumOf { getPointsForIncorrectClosingBracket(it.incorrectlyClosingBracket!!) }
  }

  fun solvePart2(lines: List<String>): Long {
    val scores = lines.map { validate(it) }
      .filter { it.status == ValidationStatus.INCOMPLETE }
      .map {
        it.closingSequence!!.fold(0L) { acc, closing ->
          acc * 5L + getPointsForAutocompletedClosingBracket(closing)
        }
      }
      .sorted()
    return scores[scores.size / 2]
  }

  private fun validate(line: String): Validation {
    val stack = mutableListOf<Char>()
    for (i in line.indices) {
      if (isOpening(line[i])) {
        stack.add(line[i])
      } else {
        check(stack.isNotEmpty()) { "Guaranteed to not happen by the problem statement" }
        val opening = findOpeningCounterpart(line[i])
        if (opening != stack.removeLast()) {
          return Validation(ValidationStatus.CORRUPTED, line[i], null)
        }
      }
    }

    val closingSequence =
      stack.map { findClosingCounterpart(it) }.reversed().joinToString(separator = "")
    return Validation(ValidationStatus.INCOMPLETE, null, closingSequence)
  }

  private fun isOpening(c: Char) = c in listOf('(', '[', '{', '<')

  private fun findOpeningCounterpart(closing: Char) = when (closing) {
    ')' -> '('
    ']' -> '['
    '}' -> '{'
    '>' -> '<'
    else -> throw AssertionError()
  }

  private fun findClosingCounterpart(opening: Char) = when (opening) {
    '(' -> ')'
    '[' -> ']'
    '{' -> '}'
    '<' -> '>'
    else -> throw AssertionError()
  }

  private fun getPointsForIncorrectClosingBracket(closing: Char) = when (closing) {
    ')' -> 3
    ']' -> 57
    '}' -> 1197
    '>' -> 25137
    else -> throw AssertionError()
  }

  private fun getPointsForAutocompletedClosingBracket(closing: Char) = when (closing) {
    ')' -> 1
    ']' -> 2
    '}' -> 3
    '>' -> 4
    else -> throw AssertionError()
  }

  data class Validation(
    val status: ValidationStatus,
    val incorrectlyClosingBracket: Char?,
    val closingSequence: String?,
  )

  enum class ValidationStatus { INCOMPLETE, CORRUPTED }
}

fun main() {
  val lines = readLines("Day10")
  println("Part 1 solution = ${Day10.solvePart1(lines)}")
  println("Part 2 solution = ${Day10.solvePart2(lines)}")
}
