/// https://adventofcode.com/2021, Day 1
///
/// Task: https://adventofcode.com/2021/day/1/
/// Date: 2021-12-03
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 1451
/// Phase 2 answer: 1395
///
/// Trivial sweeping over a sequence.
///
/// Tags: sequences, sweeping

object Day1 {
  fun solvePart1(numbers: List<Int>): Int {
    var increasingCount = 0
    for (i in 1 until numbers.size) {
      if (numbers[i] > numbers[i - 1]) {
        increasingCount += 1
      }
    }
    return increasingCount
  }

  fun solvePart2(numbers: List<Int>): Int {
    var increasingCount = 0
    for (i in 3 until numbers.size) {
      val prev = numbers.subList(i - 3, i).sum()
      val cur = numbers.subList(i - 2, i + 1).sum()
      if (cur > prev) {
        increasingCount += 1
      }
    }
    return increasingCount
  }
}

fun main() {
  val numbers = readLines("Day1").map { it.toInt() }
  println("Part 1 solution = ${Day1.solvePart1(numbers)}")
  println("Part 2 solution = ${Day1.solvePart2(numbers)}")
}
