import java.util.regex.Pattern
import kotlin.math.max
import kotlin.math.sqrt

/**
https:adventofcode.com/2021, Day 17

Task: https:adventofcode.com/2021/day/17/
Date: 2022-01-01, 2022-01-02
Author: Svilen Marchev

Status: works for both parts

Phase 1 answer: 2701
Phase 2 answer: 1070

Part 1:
--------------
The implementation assumes that a solution exists (so we can always find an appropriate x value
for the y value we pick), the target area lies entirely in quadrant 4 (which helps us focus on
the particular use case), and we can throw high enough, so that y > 0.

Having y > 0 means that the probe will certainly touch the X-axis once again and its next
position will be at PosY=-(y-1). (1)

The higher the y we pick, the higher the probe would shoot up, so we need to maximize y.
But we also need to make sure it falls within the target area, i.e. PosY>=TargetMinY. (2)

From (1) and (2) follows that y<=-TargetMinY-1. To maximize y, we can set it y=-TargetMinY-1.
This way the probe will hit the last row of the target zone, on the first move after
the probe drops below the X-axis.

Part 2:
--------------
The idea is for each feasible (x,y) value to compute all coordinates that the
probe passes through and check if any ends up in the target zone. We can greatly narrow down
the search space for the (x,y) values by assuming the target zone lies entirely in quadrant 4
and making a few observations:

x <= TargetMaxX (otherwise the probe would jump over the entire target zone in its first move)
1+2+...+x = (1+x)x/2 >= TargetMinX (otherwise the probe would never reach the start of the target zone)
y >= TargetMinY (otherwise the probe would jump over the entire target zone in its first move)
y <= maxY = -TargetMinY-1 (as computed in part 1)

Tags: math, geometry, computing trajectory
 */

object Day17 {
  private val inputPattern =
    Pattern.compile("^target area: x=(-?\\d+)[.][.](-?\\d+), y=(-?\\d+)[.][.](-?\\d+)$")

  fun solvePart1(lines: List<String>): Int {
    val matcher = inputPattern.matcher(lines[0])
    check(matcher.matches())
    val targetMinY = matcher.group(3).toInt()

    val maxY = computeMaxY(targetMinY)
    return (1 + maxY) * maxY / 2
  }

  fun solvePart2(lines: List<String>): Int {
    val matcher = inputPattern.matcher(lines[0])
    check(matcher.matches())
    val targetMinX = matcher.group(1).toInt()
    val targetMaxX = matcher.group(2).toInt()
    val targetMinY = matcher.group(3).toInt()
    val targetMaxY = matcher.group(4).toInt()

    var numValuesHitTarget = 0
    for (x in computeMinX(targetMinX)..targetMaxX) {
      for (y in targetMinY..computeMaxY(targetMinY)) {
        if (hitsTarget(x, y, targetMinX, targetMaxX, targetMinY, targetMaxY)) {
          numValuesHitTarget += 1
        }
      }
    }
    return numValuesHitTarget
  }

  /** Computes the min x at which the target zone could be reached. */
  private fun computeMinX(targetMinX: Int): Int {
    var x = sqrt(2.0 * targetMinX).toInt()
    while ((1 + x) * x / 2 < targetMinX) x += 1
    return x
  }

  /** Computes the max y at which the target zone could be reached. */
  private fun computeMaxY(targetMinY: Int): Int {
    return -targetMinY - 1
  }

  private fun hitsTarget(
    x: Int,
    y: Int,
    targetMinX: Int,
    targetMaxX: Int,
    targetMinY: Int,
    targetMaxY: Int,
  ): Boolean {
    var offX = x
    var offY = y
    var posX = 0
    var posY = 0
    while (posX <= targetMaxX && posY >= targetMinY) {
      posX += offX
      posY += offY
      if (posX in targetMinX..targetMaxX && posY in targetMinY..targetMaxY) {
        return true
      }
      offX = max(0, offX - 1)
      offY -= 1
    }
    return false
  }
}

fun main() {
  val lines = readLines("Day17")
  println("Part 1 solution = ${Day17.solvePart1(lines)}")
  println("Part 2 solution = ${Day17.solvePart2(lines)}")
}
