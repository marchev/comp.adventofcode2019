/**
https:adventofcode.com/2021, Day 19

Task: https:adventofcode.com/2021/day/19/
Date: 2021-12-?
Author: Svilen Marchev

Status: works for ?

Phase 1 answer: ?
Phase 2 answer: ?

Tags: ?
 */

object Day19 {
  fun solvePart1(lines: List<String>): Int {
    return -1
  }
}

fun main() {
  val lines = readLines("Day19")
  println("Part 1 solution = ${Day19.solvePart1(lines)}")
}
