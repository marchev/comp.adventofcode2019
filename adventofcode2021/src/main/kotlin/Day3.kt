/// https://adventofcode.com/2021, Day 3
///
/// Task: https://adventofcode.com/2021/day/3/
/// Date: 2021-12-04
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 2967914
/// Phase 2 answer: 7041258
///
/// Tags: bitwise operations

object Day3 {
  fun solvePart1(lines: List<String>): Int {
    val len = lines.first().length
    var gammaValue = 0
    var epsilonValue = 0
    for (i in 0 until len) {
      val numOnes = lines.map { it[i] }.count { it == '1' }
      val numZeros = lines.size - numOnes
      if (numOnes > numZeros) {
        gammaValue = gammaValue or (1 shl (len - 1 - i))
      } else {
        epsilonValue = epsilonValue or (1 shl (len - 1 - i))
      }
    }
    return gammaValue * epsilonValue
  }

  fun solvePart2(lines: List<String>): Int {
    val oxygenValue =
      filterOutEntries(lines) { numOnes: Int, numZeros: Int -> if (numOnes >= numZeros) '1' else '0' }
    val co2Value =
      filterOutEntries(lines) { numOnes: Int, numZeros: Int -> if (numZeros <= numOnes) '0' else '1' }
    return oxygenValue.toInt(2) * co2Value.toInt(2)
  }

  private fun filterOutEntries(lines: List<String>, whichDigitToKeep: (Int, Int) -> Char): String {
    val len = lines.first().length
    var remainingEntries = lines
    var i = 0
    while (remainingEntries.size > 1 && i < len) {
      val numOnes = remainingEntries.map { it[i] }.count { it == '1' }
      val numZeros = remainingEntries.size - numOnes
      remainingEntries =
        remainingEntries.filter { it[i] == whichDigitToKeep(numOnes, numZeros) }.toList()
      i += 1
    }
    assert(remainingEntries.size == 1)
    return remainingEntries.first()
  }
}

fun main() {
  val lines = readLines("Day3")
  println("Part 1 solution = ${Day3.solvePart1(lines)}")
  println("Part 2 solution = ${Day3.solvePart2(lines)}")
}
