import java.util.regex.Pattern

/**
https:adventofcode.com/2021, Day 22

Task: https:adventofcode.com/2021/day/22/
Date: 2021-12-23, 2021-12-24
Author: Svilen Marchev

Status: works for both parts

Phase 1 answer: 647076
Phase 2 answer: 1233304599156793

Finding the sum of the volumes of a set of cuboids is a well-known problem, but in this problem
some cuboids contribute to the sum and others subtract from it, and the order in which they
appear in the input is significant, as they cancel each other. This makes it a whole
different beast.

My solution uses 3 levels of sweeping. We fist sweep over z-coordinate events. An event is
characterized with a time when it happens (its z-coordinate), sign (if a new cuboid starts
or ends at that time), and the cuboid associated with the event. For each cuboid we generate
two events at times Z1 (when it starts) and Z2+1 (when it ends), then we traverse the events in
increasing time (z-coordinate), with end events coming first in case of equal times, and
maintain a set of "active cuboids". In a subtask we need to calculate the total *area* of
all active cuboids, considering only their XY coordinates. This is going to be the area of
all active cuboids until the next z-coordinate event (there is no way the area can change
in-between). So at each z-event we increase the total volume by
`areaOfActiveRectanglesFromPreviousToCurrentEvent * (currentZEvent.time - previousZEvent.time)`.

Now how do we solve the subtask of finding the cumulative *area* of a set of cuboids using
their only XY coordinates (i.e. this way treating them as rectangles). Well, the same way.
For each cuboid we generate two events at times Y1 (when it starts) and Y2+1 (when it ends),
then we sort and traverse in order. We maintain a set of "active cuboids" here as well.
In a subtask we need to calculate the total *length* of all active cuboids, considering only
their X coordinates. This length is going to be the cumulative covered length of all active
cuboids until the next y-coordinate event. So at each y-event we increase the total area by
`lengthOfActiveSegmentsFromPreviousToCurrentEvent * (currentYEvent.time - previousYEvent.time)`.

Finally, we need some procedure to compute the cumulative length of a set of cuboids using
only their X coordinates (i.e. this way treating them as segments). We do it in a similar way -
for each cuboid we generate two events - at times X1 and X2+1, we go through the events
ordered by time, and maintain a set of "active cuboids". At each x-event we need to know
if the segment between it and the previous x-event was covered or not (in regard to the
x-coordinates of the cuboids), so that we know if we should count it in the total covered
length. Note that each such segment can be covered by multiple cuboids, some of them might
be turning on or off the segment, so ultimately we need to know which is the cuboid that
determines the end result. In other words, which is the one that comes last in the input.
This is achieved by just finding the active cuboid with a max "input order".
Depending on whether that cuboid is an "off" or "on" one, we either skip the segment,
or increase the total length by `currentXEvent.time - previousXEvent.time`.

The runtime complexity of the solution is O(N^4). This could be optimized to O(N^3 log N) by
optimizing the handling of active cuboids in the subtask that sweeps by x-coordinate.

An alternative solution would be sweeping over the z-coordinate, and adding and removing
rectangles using a quad-tree. Runtime complexity would be O(N^2 log C), with a big constant,
where C is the coordinates span. This could be reduced to O(N^2 log N) by compressing
the coordinates.

Tags: sweeping, cuboids, 3d, geometry, triple sweeping
 */

object Day22 {
  private val LINE_PATTERN =
    Pattern.compile("^(on|off) x=([-0-9]+)[.][.]([-0-9]+),y=([-0-9]+)[.][.]([-0-9]+),z=([-0-9]+)[.][.]([-0-9]+)$")

  fun solvePart1(lines: List<String>): Long {
    val cuboids = readCuboids(lines).filter { c ->
      listOf(c.x1,
             c.x2,
             c.y1,
             c.y2,
             c.z1,
             c.z2).all { inSmallRange(it) }
    }
    return computeVolume(cuboids)
  }

  fun solvePart2(lines: List<String>): Long {
    val cuboids = readCuboids(lines)
    return computeVolume(cuboids)
  }

  /** Sweeps by the z-coordinate. */
  private fun computeVolume(cuboids: List<Cuboid>): Long {
    val zEvents = cuboids.flatMap {
      listOf(
        Event(it.z1, +1, it),
        Event(it.z2 + 1, -1, it),
      )
    }.sortedBy { it.sign }.sortedBy { it.time }

    var volume = 0L
    var lastZ = 0
    var currentArea = 0L
    val activeCuboids = mutableListOf<Cuboid>()
    for (zEvent in zEvents) {
      volume += (zEvent.time - lastZ) * currentArea

      if (zEvent.sign == 1) {
        activeCuboids.add(zEvent.cuboid)
      } else {
        activeCuboids.remove(zEvent.cuboid)
      }
      currentArea = computeAreaXY(activeCuboids)
      lastZ = zEvent.time
    }

    return volume
  }

  /** Sweeps by the y-coordinate. */
  private fun computeAreaXY(cuboids: List<Cuboid>): Long {
    val yEvents = cuboids.flatMap {
      listOf(
        Event(it.y1, +1, it),
        Event(it.y2 + 1, -1, it),
      )
    }.sortedBy { it.sign }.sortedBy { it.time }

    var area = 0L
    var lastY = 0
    var currentLength = 0L
    val activeCuboids = mutableListOf<Cuboid>()
    for (yEvent in yEvents) {
      area += (yEvent.time - lastY) * currentLength

      if (yEvent.sign == 1) {
        activeCuboids.add(yEvent.cuboid)
      } else {
        activeCuboids.remove(yEvent.cuboid)
      }
      currentLength = computeLengthX(activeCuboids)
      lastY = yEvent.time
    }

    return area
  }

  /** Sweeps by the x-coordinate. */
  private fun computeLengthX(cuboids: List<Cuboid>): Long {
    val xEvents = cuboids.flatMap {
      listOf(
        Event(it.x1, +1, it),
        Event(it.x2 + 1, -1, it),
      )
    }.sortedBy { it.sign }.sortedBy { it.time }

    var length = 0L
    var lastX = 0
    var segmentMultiplier = 0
    val activeCuboids = mutableListOf<Cuboid>()
    for (xEvent in xEvents) {
      length += (xEvent.time - lastX) * segmentMultiplier

      if (xEvent.sign == 1) {
        activeCuboids.add(xEvent.cuboid)
      } else {
        activeCuboids.remove(xEvent.cuboid)
      }
      val latestActiveCuboid = activeCuboids.maxByOrNull { it.order }
      segmentMultiplier =
        if ((latestActiveCuboid?.type ?: CuboidType.OFF) == CuboidType.ON) 1 else 0
      lastX = xEvent.time
    }

    return length
  }

  private fun inSmallRange(coord: Int): Boolean = -50 <= coord && coord <= 50

  private fun readCuboids(lines: List<String>): List<Cuboid> {
    return lines.mapIndexed { i, line ->
      val m = LINE_PATTERN.matcher(line)
      check(m.matches())
      Cuboid(
        i,
        if (m.group(1) == "on") CuboidType.ON else CuboidType.OFF,
        m.group(2).toInt(),
        m.group(3).toInt(),
        m.group(4).toInt(),
        m.group(5).toInt(),
        m.group(6).toInt(),
        m.group(7).toInt(),
      )
    }
  }

  data class Event(val time: Int, val sign: Int, val cuboid: Cuboid)

  data class Cuboid(
    val order: Int,
    val type: CuboidType,
    val x1: Int,
    val x2: Int,
    val y1: Int,
    val y2: Int,
    val z1: Int,
    val z2: Int,
  )

  enum class CuboidType { ON, OFF }
}

fun main() {
  val lines = readLines("Day22")
  println("Part 1 solution = ${Day22.solvePart1(lines)}")
  println("Part 2 solution = ${Day22.solvePart2(lines)}")
}
