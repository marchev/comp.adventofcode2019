import kotlin.math.absoluteValue

/// https://adventofcode.com/2021, Day 7
///
/// Task: https://adventofcode.com/2021/day/7/
/// Date: 2021-12-12
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 352331
/// Phase 2 answer: 99266250
///
/// Tags: simulation

object Day7 {
  fun solvePart1(lines: List<String>): Int {
    val nums = lines[0].split(",").map { it.toInt() }.toList()
    return (nums.minOf { it }..nums.maxOf { it }).minOf { calculateFuelAtConstantRate(nums, it) }
  }

  fun solvePart2(lines: List<String>): Int {
    val nums = lines[0].split(",").map { it.toInt() }.toList()
    return (nums.minOf { it }..nums.maxOf { it }).minOf { calculateFuelAtGrowingRate(nums, it) }
  }

  private fun calculateFuelAtConstantRate(nums: List<Int>, x: Int): Int {
    return nums.sumOf { (it - x).absoluteValue }
  }

  private fun calculateFuelAtGrowingRate(nums: List<Int>, x: Int): Int {
    return nums.sumOf {
      val diff = (it - x).absoluteValue
      (1 + diff) * diff / 2
    }
  }
}

fun main() {
  val lines = readLines("Day7")
  println("Part 1 solution = ${Day7.solvePart1(lines)}")
  println("Part 2 solution = ${Day7.solvePart2(lines)}")
}
