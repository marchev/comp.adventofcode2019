import java.util.PriorityQueue
import kotlin.math.absoluteValue

/**
https:adventofcode.com/2021, Day 23

Task: https:adventofcode.com/2021/day/23/
Date: 2022-01-08, 2022-01-09
Author: Svilen Marchev

Status: works for both parts

Phase 1 answer: 16489
Phase 2 answer: 43413

Nice one. Classic shortest path problem using Dijkstra's algorithm, but one has to be careful
with the state representation, so that the implementation remains sane.

Tags: shortest path, dijkstra, graph theory, grid
 */

object Day23 {
  fun solvePart1(lines: List<String>): Int {
    val initialState = readInitialState(lines)
    val endState =
      State(".".repeat(initialState.hall.length), ('A'..'D').map { it.toString().repeat(2) })
    return computeShortestPath(initialState, endState)
  }

  fun solvePart2(lines: List<String>): Int {
    val s = readInitialState(lines)
    // Apply the special "unfolding", described in the problem statement.
    val initialState = State(s.hall,
                             listOf(
                               s.rooms[0].first() + "DD" + s.rooms[0].last(),
                               s.rooms[1].first() + "BC" + s.rooms[1].last(),
                               s.rooms[2].first() + "AB" + s.rooms[2].last(),
                               s.rooms[3].first() + "CA" + s.rooms[3].last(),
                             ))
    val endState =
      State(".".repeat(initialState.hall.length), ('A'..'D').map { it.toString().repeat(4) })
    return computeShortestPath(initialState, endState)
  }

  private fun computeShortestPath(initialState: State, endState: State): Int {
    val minDist = mutableMapOf(Pair(initialState, 0))
    val queue = PriorityQueue<StateWithDistance> { a, b -> a.dist.compareTo(b.dist) }
    queue.add(StateWithDistance(initialState, 0))

    while (queue.isNotEmpty()) {
      val cur = queue.poll()
      if (minDist[cur.state] != cur.dist) {
        continue
      }
      if (cur.state == endState) {
        return cur.dist
      }
      for (nextState in findNextStates(cur)) {
        if (minDist.getOrDefault(nextState.state, Int.MAX_VALUE) > nextState.dist) {
          minDist[nextState.state] = nextState.dist
          queue.add(nextState)
        }
      }
    }

    throw AssertionError("No solution")
  }

  private fun findNextStates(curWithDistance: StateWithDistance): List<StateWithDistance> {
    return findNextStatesByMovingFromHallwayToRooms(curWithDistance) + findNextStatesByMovingFromRoomsToHallway(
      curWithDistance)
  }

  fun findNextStatesByMovingFromRoomsToHallway(curWithDistance: StateWithDistance): List<StateWithDistance> {
    val (state, dist) = curWithDistance
    val states = mutableListOf<StateWithDistance>()

    for (roomIx in state.rooms.indices) {
      val room = state.rooms[roomIx]

      val targetRoomPiece = 'A' + roomIx
      val roomContainsMisplacedPiece = room.any { it != '.' && it != targetRoomPiece }
      if (roomContainsMisplacedPiece) {
        val movedPiece = room.last { it != '.' }
        val doorIx = roomIx * 2 + 2
        for (i in state.hall.indices) {
          if (!isHallPositionInFrontOfDoor(i) &&
            isDoorToHallwayPositionFree(doorIx, state.hall, i)
          ) {
            val newStateHall = state.hall.substring(0, i) + movedPiece + state.hall.substring(i + 1)
            val newStateRooms = state.rooms.mapIndexed { index, room ->
              if (index == roomIx) {
                val p = room.indexOfLast { it != '.' }
                room.substring(0, p) + '.' + room.substring(p + 1)
              } else {
                room
              }
            }
            val numMoves = (doorIx - i).absoluteValue + (room.count { it == '.' } + 1)
            val newDist = dist + energyOf[movedPiece]!! * numMoves
            states.add(StateWithDistance(State(newStateHall, newStateRooms), newDist))
          }
        }
      }
    }

    return states
  }

  private fun isHallPositionInFrontOfDoor(hallIx: Int) = hallIx in listOf(2, 4, 6, 8)

  fun findNextStatesByMovingFromHallwayToRooms(curWithDistance: StateWithDistance): List<StateWithDistance> {
    val (state, dist) = curWithDistance
    val states = mutableListOf<StateWithDistance>()

    // Move a piece from the hallway to a room, whenever possible.
    for (i in state.hall.indices) {
      val movedPiece = state.hall[i]
      if (movedPiece in 'A'..'D') {
        val targetDoorIx = (movedPiece - 'A') * 2 + 2
        val targetRoomIx = movedPiece - 'A'

        val canMoveToRoom = state.rooms[targetRoomIx].all { it == '.' || it == movedPiece }
        if (isHallwayPositionToDoorFree(state.hall, i, targetDoorIx) && canMoveToRoom) {
          val newStateHall = state.hall.substring(0, i) + "." + state.hall.substring(i + 1)
          val newStateRooms = state.rooms.mapIndexed { index, room ->
            if (index == targetRoomIx) {
              val roomPieces = room.trimEnd('.') + movedPiece
              roomPieces + ".".repeat(room.length - roomPieces.length)
            } else {
              room
            }
          }
          val numMoves =
            (targetDoorIx - i).absoluteValue + state.rooms[targetRoomIx].count { it == '.' }
          val newDist = dist + energyOf[movedPiece]!! * numMoves
          states.add(StateWithDistance(State(newStateHall, newStateRooms), newDist))
        }
      }
    }

    return states
  }

  private fun isHallwayPositionToDoorFree(hall: String, hallIx: Int, targetDoorIx: Int): Boolean {
    val seq =
      if (hallIx < targetDoorIx) hallIx + 1 until targetDoorIx else targetDoorIx + 1 until hallIx
    return seq.all { hall[it] == '.' }
  }

  private fun isDoorToHallwayPositionFree(doorIx: Int, hall: String, targetHallIx: Int): Boolean {
    val seq = if (targetHallIx < doorIx) targetHallIx until doorIx else doorIx + 1..targetHallIx
    return seq.all { hall[it] == '.' }
  }

  private val energyOf = mapOf(Pair('A', 1), Pair('B', 10), Pair('C', 100), Pair('D', 1000))

  private fun readInitialState(lines: List<String>): State {
    val hall = lines[1].substring(1, lines[1].length - 1)
    val rooms = (3 until 10 step 2).map { "" + lines[3][it] + lines[2][it] }.toList()
    check(hall.all { it == '.' })
    return State(hall, rooms)
  }

  data class State(val hall: String, val rooms: List<String>)

  data class StateWithDistance(val state: State, val dist: Int)
}

fun main() {
  val lines = readLines("Day23")
  println("Part 1 solution = ${Day23.solvePart1(lines)}")
  println("Part 2 solution = ${Day23.solvePart2(lines)}")
}
