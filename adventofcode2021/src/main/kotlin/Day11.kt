import kotlin.streams.toList

/// https://adventofcode.com/2021, Day 11
///
/// Task: https://adventofcode.com/2021/day/11/
/// Date: 2021-12-30
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 1608
/// Phase 2 answer: 214
///
/// Tags: simulation, grid

object Day11 {
  private val dr = listOf(0, 1, 0, -1, 1, -1, 1, -1)
  private val dc = listOf(1, 0, -1, 0, 1, -1, -1, 1)

  fun solvePart1(lines: List<String>): Int {
    var grid = readGrid(lines)
    var sum = 0
    for (step in 0 until 100) {
      grid = iterate(grid)
      sum += countZeroes(grid)
    }
    return sum
  }

  fun solvePart2(lines: List<String>): Int {
    var grid = readGrid(lines)
    for (step in 1..Int.MAX_VALUE) {
      grid = iterate(grid)
      if (countZeroes(grid) == grid.size * grid[0].size) {
        return step
      }
    }
    throw AssertionError()
  }


  private fun countZeroes(grid: List<List<Int>>): Int {
    return grid.flatten().count { it == 0 }
  }

  fun iterate(grid: List<List<Int>>): List<List<Int>> {
    val updatedGrid = List(grid.size) { r ->
      grid[r].map { it + 1 }.toMutableList()
    }
    val cellProcessed = List(grid.size) { MutableList(grid[0].size) { false } }
    while (true) {
      var wasAnyProcessed = false
      for (r in updatedGrid.indices) {
        for (c in updatedGrid[0].indices) {
          if (!cellProcessed[r][c] && updatedGrid[r][c] > 9) {
            cellProcessed[r][c] = true
            for (i in dr.indices) {
              val nr = dr[i] + r
              val nc = dc[i] + c
              if (nr in updatedGrid.indices && nc in updatedGrid[0].indices) {
                updatedGrid[nr][nc] += 1
              }
            }
            wasAnyProcessed = true
          }
        }
      }
      if (!wasAnyProcessed) break
    }
    for (r in grid.indices) {
      for (c in grid[0].indices) {
        if (cellProcessed[r][c]) {
          updatedGrid[r][c] = 0
        }
      }
    }
    return updatedGrid
  }

  private fun readGrid(lines: List<String>): List<List<Int>> {
    return lines.map { line ->
      line.chars().map { it - '0'.code }.toList()
    }
  }
}

fun main() {
  val lines = readLines("Day11")
  println("Part 1 solution = ${Day11.solvePart1(lines)}")
  println("Part 2 solution = ${Day11.solvePart2(lines)}")
}
