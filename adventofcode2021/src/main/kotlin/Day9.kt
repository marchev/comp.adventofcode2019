import kotlin.streams.toList

/// https://adventofcode.com/2021, Day 9
///
/// Task: https://adventofcode.com/2021/day/9/
/// Date: 2021-12-26
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 591
/// Phase 2 answer: 1113424
///
/// Tags: grid, simulation

object Day9 {
  private val dr = listOf(0, 1, 0, -1)
  private val dc = listOf(1, 0, -1, 0)

  fun solvePart1(lines: List<String>): Int {
    val grid = readGrid(lines)
    val n = grid.size
    val m = grid[0].size
    var sum = 0
    for (r in 0 until n) {
      for (c in 0 until m) {
        var isLow = true
        for (i in dr.indices) {
          val nr = dr[i] + r
          val nc = dc[i] + c
          if (nr in 0 until n && nc in 0 until m && grid[nr][nc] <= grid[r][c]) {
            isLow = false
          }
        }
        sum += if (isLow) 1 + grid[r][c] else 0
      }
    }
    return sum
  }

  fun solvePart2(lines: List<String>): Int {
    val grid = readGrid(lines)
    val n = grid.size
    val m = grid[0].size
    val visited = List(n) { MutableList(m) { false } }
    val basins = mutableListOf<Int>()
    for (r in 0 until n) {
      for (c in 0 until m) {
        if (grid[r][c] != 9 && !visited[r][c]) {
          basins.add(findBasinSize(grid, visited, r, c))
        }
      }
    }
    return basins.sorted().takeLast(3).fold(1) { acc, a -> acc * a }
  }

  private fun findBasinSize(
    grid: List<List<Int>>,
    visited: List<MutableList<Boolean>>,
    r: Int,
    c: Int,
  ): Int {
    visited[r][c] = true
    var count = 1
    for (i in dr.indices) {
      val nr = dr[i] + r
      val nc = dc[i] + c
      if (nr in grid.indices && nc in grid[0].indices && grid[nr][nc] != 9 && !visited[nr][nc]) {
        count += findBasinSize(grid, visited, nr, nc)
      }
    }
    return count
  }

  private fun readGrid(lines: List<String>): List<List<Int>> {
    return lines.map { line -> line.chars().map { it - '0'.code }.toList() }
  }
}

fun main() {
  val lines = readLines("Day9")
  println("Part 1 solution = ${Day9.solvePart1(lines)}")
  println("Part 2 solution = ${Day9.solvePart2(lines)}")
}
