/**
https://adventofcode.com/2021, Day 12

Task: https://adventofcode.com/2021/day/12/
Date: 2021-12-19
Author: Svilen Marchev

Status: works for both parts

Phase 1 answer: 5104
Phase 2 answer: 149220

Part 2 is based on part 1, but required to rethink the way we keep visited nodes.
Instead of just a set of visited nodes, we use a map that keeps how many times a node
was visited (it will be always between 0 and 2 times, since visiting a node more than
2 times doesn't make sense).

Tags: graph, recursion, simulation
 */

object Day12 {
  private const val START = "start"
  private const val END = "end"

  fun solvePart1(lines: List<String>): Int {
    val graph = readGraph(lines)
    return countPaths(graph, START, mapOf(), false)
  }

  fun solvePart2(lines: List<String>): Int {
    val graph = readGraph(lines)
    return countPaths(graph, START, mapOf(), true)
  }

  private fun countPaths(
    graph: Map<String, MutableSet<String>>,
    node: String,
    visited: Map<String, Int>,
    isVisitingTwiceEnabled: Boolean,
  ): Int {
    if (node == END) return 1
    var count = 0

    for (kid in graph[node]!!.filter { isBigCave(it) }) {
      count += countPaths(graph, kid, visited, isVisitingTwiceEnabled)
    }

    val canVisitANodeTwice = isVisitingTwiceEnabled && (visited.values.maxOrNull() ?: 0) <= 1
    for (kid in graph[node]!!.filter { !isBigCave(it) }) {
      val numVisitsSoFar = visited.getOrDefault(kid, 0)
      if (numVisitsSoFar == 0 || (numVisitsSoFar == 1 && canVisitANodeTwice)) {
        val newVisited = visited.toMutableMap()
        newVisited[kid] = numVisitsSoFar + 1
        count += countPaths(graph, kid, newVisited, isVisitingTwiceEnabled)
      }
    }
    return count
  }

  private fun isBigCave(node: String) = node[0] in 'A'..'Z'

  private fun readGraph(lines: List<String>): Map<String, MutableSet<String>> {
    val graph = mutableMapOf<String, MutableSet<String>>()
    lines.map { it.split("-") }.forEach { (a, b) ->
      if (b != START) graph.getOrPut(a) { mutableSetOf() }.add(b)
      if (a != START) graph.getOrPut(b) { mutableSetOf() }.add(a)
    }
    return graph
  }
}

fun main() {
  val lines = readLines("Day12")
  println("Part 1 solution = ${Day12.solvePart1(lines)}")
  println("Part 2 solution = ${Day12.solvePart2(lines)}")
}
