/// https://adventofcode.com/2021, Day 16
///
/// Task: https://adventofcode.com/2021/day/16/
/// Date: 2021-12-17, 2021-12-18
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 923
/// Phase 2 answer: 258888628940
///
/// Nice one.
/// Deserializing a tree structure from a binary sequence.
///
/// Tags: parsing, serialization, decoding, hex, binary, recursion, trees, graph traversal

object Day16 {
  fun solvePart1(lines: List<String>): Long {
    val stream = BinaryStream(hexToBinary(lines[0]))
    val rootPackage = parsePackage(stream)
    return sumVersions(rootPackage)
  }

  fun solvePart2(lines: List<String>): Long {
    val stream = BinaryStream(hexToBinary(lines[0]))
    val rootPackage = parsePackage(stream)
    return rootPackage.value
  }

  private fun sumVersions(root: Package): Long {
    return root.version + root.subpackages.sumOf { sumVersions(it) }
  }

  fun hexToBinary(hex: String): String =
    hex.chunked(1).joinToString(separator = "") { it.toInt(16).toString(2).padStart(4, '0') }

  private fun parsePackages(stream: BinaryStream, n: Int): List<Package> {
    return (1..n).map { parsePackage(stream) }.toList()
  }

  private fun parseAllPackages(stream: BinaryStream): List<Package> {
    val list = mutableListOf<Package>()
    while (stream.remaining() > 6) {
      list += parsePackage(stream)
    }
    return list
  }

  fun parsePackage(stream: BinaryStream): Package {
    val version = stream.take(3).toInt(2)
    val type = stream.take(3).toInt(2)
    return if (type == 4) {
      val value = parseValue(stream)
      Package(version, type, value, emptyList())
    } else {
      val lengthTypeId = stream.take(1)
      if (lengthTypeId == "0") {
        val lengthInBits = stream.take(15).toInt(2)
        val subpackageBinary = stream.take(lengthInBits)
        val subpackages = parseAllPackages(BinaryStream(subpackageBinary))
        Package(version, type, computeValue(type, subpackages), subpackages)
      } else {
        val numSubpackages = stream.take(11).toInt(2)
        val subpackages = parsePackages(stream, numSubpackages)
        Package(version, type, computeValue(type, subpackages), subpackages)
      }
    }
  }

  private fun computeValue(type: Int, subpackages: List<Package>): Long {
    return when (type) {
      0 -> subpackages.sumOf { it.value }
      1 -> subpackages.fold(1L) { product, p -> product * p.value }
      2 -> subpackages.minOf { it.value }
      3 -> subpackages.maxOf { it.value }
      5 -> if (subpackages[0].value > subpackages[1].value) 1 else 0
      6 -> if (subpackages[0].value < subpackages[1].value) 1 else 0
      7 -> if (subpackages[0].value == subpackages[1].value) 1 else 0
      else -> throw AssertionError()
    }
  }

  private fun parseValue(stream: BinaryStream): Long {
    var value = 0L
    while (true) {
      val fragment = stream.take(5)
      value = (value shl 4) + fragment.substring(1).toInt(2)
      if (fragment[0] == '0') break
    }
    return value
  }

  class BinaryStream(private val binary: String) {
    private var i = 0

    fun take(n: Int): String {
      val next = binary.substring(i, i + n)
      i += n
      return next
    }

    fun remaining(): Int = binary.length - i
  }

  data class Package(
    val version: Int,
    val type: Int,
    val value: Long,
    val subpackages: List<Package>,
  )
}

fun main() {
  val lines = readLines("Day16")
  println("Part 1 solution = ${Day16.solvePart1(lines)}")
  println("Part 2 solution = ${Day16.solvePart2(lines)}")
}
