/// https://adventofcode.com/2022, Day 18
///
/// Task: https://adventofcode.com/2022/day/18/
/// Date: 2022-12-26
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 3500
/// Phase 2 answer: 2048
///
/// Tags: simulation, geometry, bfs

object Day18 {
  val maxCoord = 100

  fun solvePart1(lines: List<String>): Int {
    val hasCubeAt = Array(maxCoord + 1) { Array(maxCoord + 1) { BooleanArray(maxCoord + 1) } }
    val coords = readCoordinates(lines)

    coords.forEach { c ->
      hasCubeAt[c.x][c.y][c.z] = true
    }
    fun hasCubeAt(x: Int, y: Int, z: Int): Boolean = hasCubeAt[x][y][z]
    var surface = 0
    coords.forEach { c ->
      surface += if (hasCubeAt(c.x + 1, c.y, c.z)) 0 else 1
      surface += if (hasCubeAt(c.x - 1, c.y, c.z)) 0 else 1
      surface += if (hasCubeAt(c.x, c.y + 1, c.z)) 0 else 1
      surface += if (hasCubeAt(c.x, c.y - 1, c.z)) 0 else 1
      surface += if (hasCubeAt(c.x, c.y, c.z + 1)) 0 else 1
      surface += if (hasCubeAt(c.x, c.y, c.z - 1)) 0 else 1
    }
    return surface
  }

  /** Reads the coordinates, shifting them by +1, so that they are all >0. */
  fun readCoordinates(lines: List<String>): List<Coordinate> {
    return lines.map { line ->
      val coords = line.split(',').map { it.toInt() }
      Coordinate(coords[0] + 1, coords[1] + 1, coords[2] + 1)
    }
  }

  data class Coordinate(val x: Int, val y: Int, val z: Int)

  fun solvePart2(lines: List<String>): Int {
    val hasCubeAt = Array(maxCoord + 1) { Array(maxCoord + 1) { BooleanArray(maxCoord + 1) } }
    val coords = readCoordinates(lines)

    coords.forEach { c ->
      hasCubeAt[c.x][c.y][c.z] = true
    }
    val exteriorCoordinates = computeExteriorCoordinates(hasCubeAt)

    fun hasNoCubeAndIsExterior(x: Int, y: Int, z: Int): Boolean =
      !hasCubeAt[x][y][z] && exteriorCoordinates.contains(Coordinate(x, y, z))

    var surface = 0
    coords.forEach { c ->
      surface += if (hasNoCubeAndIsExterior(c.x + 1, c.y, c.z)) 1 else 0
      surface += if (hasNoCubeAndIsExterior(c.x - 1, c.y, c.z)) 1 else 0
      surface += if (hasNoCubeAndIsExterior(c.x, c.y + 1, c.z)) 1 else 0
      surface += if (hasNoCubeAndIsExterior(c.x, c.y - 1, c.z)) 1 else 0
      surface += if (hasNoCubeAndIsExterior(c.x, c.y, c.z + 1)) 1 else 0
      surface += if (hasNoCubeAndIsExterior(c.x, c.y, c.z - 1)) 1 else 0
    }
    return surface
  }

  private fun computeExteriorCoordinates(hasCubeAt: Array<Array<BooleanArray>>): Set<Coordinate> {
    val queue = ArrayDeque<Coordinate>()
    val start = Coordinate(0, 0, 0)
    val reachable = HashSet<Coordinate>()
    queue.add(start)
    reachable.add(start)

    fun maybeEnqueueIfNotACube(x: Int, y: Int, z: Int) {
      val next = Coordinate(x, y, z)
      if (x in 0..maxCoord && y in 0..maxCoord && z in 0..maxCoord
        && !hasCubeAt[x][y][z]
        && reachable.add(next)
      ) {
        queue.add(next)
      }
    }

    while (queue.isNotEmpty()) {
      val c = queue.removeLast();
      maybeEnqueueIfNotACube(c.x + 1, c.y, c.z)
      maybeEnqueueIfNotACube(c.x - 1, c.y, c.z)
      maybeEnqueueIfNotACube(c.x, c.y + 1, c.z)
      maybeEnqueueIfNotACube(c.x, c.y - 1, c.z)
      maybeEnqueueIfNotACube(c.x, c.y, c.z + 1)
      maybeEnqueueIfNotACube(c.x, c.y, c.z - 1)
    }
    return reachable
  }
}

fun main() {
  val lines = readLines("Day18")
  println("Part 1 solution = ${Day18.solvePart1(lines)}")
  println("Part 2 solution = ${Day18.solvePart2(lines)}")
}
