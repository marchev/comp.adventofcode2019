/**
 * https://adventofcode.com/2022, Day X
 *
 * Task: https://adventofcode.com/2022/day/X/
 * Date:
 * Author: Svilen Marchev
 *
 * Status: works for ?
 *
 * Phase 1 answer: ?
 * Phase 2 answer: ?
 *
 * Tags: ?
 */

object DayX {
  fun solvePart1(lines: List<String>): Int {
    return 0
  }

  fun solvePart2(lines: List<String>): Int {
    return 0
  }
}

fun main() {
  val lines = readLines("DayX")
  println("Part 1 solution = ${DayX.solvePart1(lines)}")
  println("Part 2 solution = ${DayX.solvePart2(lines)}")
}
