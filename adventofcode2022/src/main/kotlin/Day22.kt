/// https://adventofcode.com/2022, Day 22
///
/// Task: https://adventofcode.com/2022/day/22/
/// Date: 2022-12-27, 2022-12-28
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 159034
/// Phase 2 answer: 147245
///
/// Tags: grid, simulation, 3d grid traversal
///
/// The solution to part 2 is tied to the concrete input and there is a lot of hardcoding for that.
/// The way I approached part 2 is by grabbing a real cube and marking each side with
/// the orientation at which it is described on the map (marked the top right corner)
/// and the coordinates at which a given side appears on the map. Then I described in code all
/// rules for wrapping around the cube by rotating the cube in hands and looking at the markings.

object Day22 {
  // R, D, L, U
  val dr = listOf(0, 1, 0, -1)
  val dc = listOf(1, 0, -1, 0)
  val RIGHT = 0
  val DOWN = 1
  val LEFT = 2
  val UP = 3

  fun solvePart1(lines: List<String>): Int {
    val b = lines.takeWhile { it.isNotEmpty() }
    val seq = lines.last()

    var r = 0
    var c = 0
    var dir = RIGHT
    while (b[r][c] != '.') c += 1

    fun cell(r: Int, c: Int): Char = if (r in b.indices && c in b[r].indices) b[r][c] else ' '

    data class Position(val r: Int, val c: Int)

    fun findNextFreePosition(): Position? {
      var cr = r
      var cc = c
      if (dir == RIGHT || dir == LEFT) {
        while (true) {
          cc = (cc + dc[dir] + b[cr].length) % b[cr].length
          if (cell(cr, cc) == '#') return null
          else if (cell(cr, cc) == '.') return Position(cr, cc)
        }
      } else { // U or D
        while (true) {
          cr = (cr + dr[dir] + b.size) % b.size
          if (cell(cr, cc) == '#') return null
          else if (cell(cr, cc) == '.') return Position(cr, cc)
        }
      }
    }

    fun move(numMoves: Int) {
      for (movesExecuted in 0 until numMoves) {
        val nextPosition = findNextFreePosition() ?: break
        r = nextPosition.r
        c = nextPosition.c
      }
    }

    // fun printBoard() {
    //   for (i in b.indices) {
    //     val row = b[i].toCharArray()
    //     for (j in row.indices) {
    //       print(if (i!=r || j!=c) row[j] else 'X')
    //     }
    //     println()
    //   }
    // }

    var curNumMoves = 0
    for (s in seq.toCharArray()) {
      if (s == 'L' || s == 'R') {
        move(curNumMoves)
        curNumMoves = 0
      }

      when (s) {
        'L' -> dir = (dir + 3) % 4
        'R' -> dir = (dir + 1) % 4
        else -> curNumMoves = 10 * curNumMoves + (s - '0')
      }
    }
    move(curNumMoves)

    return 1000 * (r + 1) + 4 * (c + 1) + dir
  }

  fun solvePart2(lines: List<String>): Int {
    val SIDE = 50

    val b = lines.takeWhile { it.isNotEmpty() }
    val seq = lines.last()

    var r = 0
    var c = 0
    var dir = RIGHT
    while (b[r][c] != '.') c += 1

    fun cell(r: Int, c: Int): Char = if (r in b.indices && c in b[r].indices) b[r][c] else ' '

    data class PositionWithDir(val r: Int, val c: Int, val dir: Int)

    fun findNextFreePosition(r_: Int, c_: Int, dir_: Int): PositionWithDir? {
      var r = r_
      var c = c_
      var dir = dir_

      while (true) {
        var nr = r + dr[dir]
        var nc = c + dc[dir]

        if (cell(nr, nc) == ' ') {
          val quadrantR = r / SIDE
          val quadrantC = c / SIDE

          if (dir == RIGHT || dir == LEFT) {
            if (quadrantR == 0 && quadrantC == 1) {
              check(dir == LEFT)
              nr = 2 * SIDE + (SIDE - 1 - r % SIDE); nc = 0 * SIDE + 0; dir = RIGHT
            } else if (quadrantR == 0 && quadrantC == 2) {
              check(dir == RIGHT)
              nr = 2 * SIDE + (SIDE - 1 - r % SIDE); nc = 1 * SIDE + (SIDE - 1); dir = LEFT
            } else if (quadrantR == 1) {
              check(quadrantC == 1)
              if (dir == RIGHT) {
                nr = 0 * SIDE + (SIDE - 1); nc = 2 * SIDE + r % SIDE; dir = UP
              } else {
                nr = 2 * SIDE + 0; nc = 0 * SIDE + r % SIDE; dir = DOWN
              }
            } else if (quadrantR == 2 && quadrantC == 0) {
              check(dir == LEFT)
              nr = 0 * SIDE + (SIDE - 1 - r % SIDE); nc = 1 * SIDE + 0; dir = RIGHT
            } else if (quadrantR == 2 && quadrantC == 1) {
              check(dir == RIGHT)
              nr = 0 * SIDE + (SIDE - 1 - r % SIDE); nc = 2 * SIDE + (SIDE - 1); dir = LEFT
            } else if (quadrantR == 3) {
              check(quadrantC == 0)
              if (dir == RIGHT) {
                nr = 2 * SIDE + (SIDE - 1); nc = 1 * SIDE + r % SIDE; dir = UP
              } else {
                nr = 0 * SIDE + 0; nc = 1 * SIDE + r % SIDE; dir = DOWN
              }
            } else {
              throw AssertionError()
            }
          } else { // U or D
            if (quadrantR == 0 && quadrantC == 1) {
              check(dir == UP)
              nr = 3 * SIDE + c % SIDE; nc = 0 * SIDE + 0; dir = RIGHT
            } else if (quadrantR == 0 && quadrantC == 2) {
              if (dir == UP) {
                nr = 3 * SIDE + (SIDE - 1); nc = 0 * SIDE + c % SIDE; dir = UP
              } else {
                nr = 1 * SIDE + c % SIDE; nc = 1 * SIDE + (SIDE - 1); dir = LEFT
              }
            } else if (quadrantR == 2 && quadrantC == 0) {
              check(dir == UP)
              nr = 1 * SIDE + c % SIDE; nc = 1 * SIDE + 0; dir = RIGHT
            } else if (quadrantR == 2 && quadrantC == 1) {
              check(dir == DOWN)
              nr = 3 * SIDE + c % SIDE; nc = 0 * SIDE + (SIDE - 1); dir = LEFT
            } else if (quadrantR == 3) {
              check(quadrantC == 0)
              check(dir == DOWN)
              nr = 0 * SIDE + 0; nc = 2 * SIDE + c % SIDE; dir = DOWN
            } else {
              throw AssertionError()
            }
          }
        }

        if (cell(nr, nc) == '#') return null
        else if (cell(nr, nc) == '.') return PositionWithDir(nr, nc, dir)

        r = nr
        c = nc
      }
    }

    fun move(numMoves: Int) {
      for (movesExecuted in 0 until numMoves) {
        val nextPosition = findNextFreePosition(r, c, dir) ?: break
        r = nextPosition.r
        c = nextPosition.c
        dir = nextPosition.dir
      }
    }

    var curNumMoves = 0
    for (s in seq.toCharArray()) {
      if (s == 'L' || s == 'R') {
        move(curNumMoves)
        curNumMoves = 0
      }

      when (s) {
        'L' -> dir = (dir + 3) % 4
        'R' -> dir = (dir + 1) % 4
        else -> curNumMoves = 10 * curNumMoves + (s - '0')
      }
    }
    move(curNumMoves)

    return 1000 * (r + 1) + 4 * (c + 1) + dir
  }
}

fun main() {
  val lines = readLines("Day22")
  println("Part 1 solution = ${Day22.solvePart1(lines)}")
  println("Part 2 solution = ${Day22.solvePart2(lines)}")
}
