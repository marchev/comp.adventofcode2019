import java.util.regex.Pattern
import kotlin.math.max
import kotlin.math.min

/**
 * https://adventofcode.com/2022, Day 16
 *
 * Task: https://adventofcode.com/2022/day/16/
 * Date: 2023-01-01
 * Author: Svilen Marchev
 *
 * Status: works for both parts
 *
 * Phase 1 answer: 2114
 * Phase 2 answer: 2666
 *
 * Tags: DP
 *
 * Includes two solutions to part 2 - one very slow (~15 min, ~15 GB RAM) and one acceptable
 * (~1 min and ~1 GB RAM), which could be optimized even further by sticking to arrays and lists
 * instead of maps for the graph lookup operations.
 */

object Day16 {
  val LINE_PATTERN =
    Pattern.compile("Valve ([A-Z]+) has flow rate=([0-9]+); tunnels? leads? to valves? ([A-Z ,]+)")

  fun solvePart1(lines: List<String>): Int {
    val graph = Graph(lines)

    data class State(val v: String, val openValves: Set<String>, val remMinutes: Int)

    val cache = mutableMapOf<State, Int>()

    fun calcMaxPressure(v: String, openValves: Set<String>, remMinutes: Int): Int {
      if (remMinutes == 0) return 0
      val state = State(v, openValves, remMinutes)
      if (state in cache) return cache[state]!!

      var maxPressure = Int.MIN_VALUE

      if (v !in openValves && graph.rateOf[v]!! > 0) {
        maxPressure = graph.rateOf[v]!! * (remMinutes - 1) + calcMaxPressure(v,
                                                                             openValves + setOf(v),
                                                                             remMinutes - 1)
      }
      for (kid in graph.kidsOf[v]!!) {
        maxPressure = max(maxPressure, calcMaxPressure(kid, openValves, remMinutes - 1))
      }

      cache[state] = maxPressure
      return maxPressure
    }

    return calcMaxPressure("AA", setOf(), 30)
  }

  /** Works for a minute or so. */
  fun solvePart2(lines: List<String>): Int {
    val TOTAL_MINUTES = 26

    val graph = Graph(lines)
    val n = graph.indexOf.size
    val nNonZero = graph.indexOfValveWithNonZeroRate.size

    val dp =
      Array(2) { Array(n + 1) { Array(n + 1) { IntArray(1.shl(nNonZero)) { Int.MIN_VALUE } } } }
    dp[TOTAL_MINUTES % 2][graph.indexOf["AA"]!!][graph.indexOf["AA"]!!][0] = 0

    fun isBitSet(bitset: Int, i: Int) = (bitset.shr(i) and 1) > 0
    fun setBit(bitset: Int, i: Int) = bitset or 1.shl(i)

    fun isValveOpen(openValves: Int, valve: String) =
      isBitSet(openValves, graph.indexOfValveWithNonZeroRate[valve]!!)

    fun markValveAsOpen(openValves: Int, valve: String) =
      setBit(openValves, graph.indexOfValveWithNonZeroRate[valve]!!)

    fun canOpen(openValves: Int, valve: String) =
      graph.rateOf[valve]!! > 0 && !isValveOpen(openValves, valve)

    for (remMinutes in TOTAL_MINUTES downTo 1) {
      println("remMinutes = $remMinutes")
      val dpCur = dp[remMinutes % 2]
      val dpNext = dp[(remMinutes + 1) % 2]
      dpNext.forEach { it.forEach { it.fill(Int.MIN_VALUE) } }

      fun maybeUpdateNext(v1: String, v2: String, openValves: Int, newValue: Int) {
        val i1_ = graph.indexOf[v1]!!
        val i2_ = graph.indexOf[v2]!!
        val i1 = min(i1_, i2_)
        val i2 = max(i1_, i2_)
        if (dpNext[i1][i2][openValves] < newValue) {
          dpNext[i1][i2][openValves] = newValue
        }
      }

      for (i1 in 0 until n) {
        for (i2 in 0 until n) {
          for (openValves in 0 until 1.shl(nNonZero)) {
            val curVal = dpCur[i1][i2][openValves]
            if (curVal == Int.MIN_VALUE) continue

            val v1 = graph.vertexAtIndex[i1]!!
            val v2 = graph.vertexAtIndex[i2]!!

            // Case 1: open both valves (which is never worth doing when they are the same)
            if (canOpen(openValves, v1) && canOpen(openValves, v2) && v1 != v2) {
              val pressure =
                curVal + graph.rateOf[v1]!! * (remMinutes - 1) + graph.rateOf[v2]!! * (remMinutes - 1)
              maybeUpdateNext(v1,
                              v2,
                              markValveAsOpen(markValveAsOpen(openValves, v1), v2),
                              pressure)
            }
            // Case 2: open v1 and go to a successor of v2
            if (canOpen(openValves, v1)) {
              for (kid in graph.kidsOf[v2]!!) {
                val pressure = curVal + graph.rateOf[v1]!! * (remMinutes - 1)
                maybeUpdateNext(v1, kid, markValveAsOpen(openValves, v1), pressure)
              }
            }
            // Case 3: open v2 and go to a successor of v1
            if (canOpen(openValves, v2)) {
              for (kid in graph.kidsOf[v1]!!) {
                val pressure = curVal + graph.rateOf[v2]!! * (remMinutes - 1)
                maybeUpdateNext(kid, v2, markValveAsOpen(openValves, v2), pressure)
              }
            }
            // Case 4: go to successors both of v1 and v2
            for (kid1 in graph.kidsOf[v1]!!) {
              for (kid2 in graph.kidsOf[v2]!!) {
                maybeUpdateNext(kid1, kid2, openValves, curVal)
              }
            }
          }
        }
      }
    }

    var maxPressure = Int.MIN_VALUE
    for (i1 in 0 until n) {
      for (i2 in 0 until n) {
        maxPressure = max(maxPressure, dp[0][i1][i2].maxOrNull()!!)
      }
    }
    return maxPressure
  }

  /** Working solution, but very slow (15 min or so) and memory hungry (15 GB or so). */
  fun solvePart2Slow(lines: List<String>): Int {
    val graph = Graph(lines)
    val cache = mutableMapOf<Long, Int>()

    fun isBitSet(bitset: Int, i: Int) = (bitset.shr(i) and 1) > 0
    fun setBit(bitset: Int, i: Int) = bitset or 1.shl(i)

    fun isValveOpen(openValves: Int, valve: String) =
      isBitSet(openValves, graph.indexOfValveWithNonZeroRate[valve]!!)

    fun markValveAsOpen(openValves: Int, valve: String) =
      setBit(openValves, graph.indexOfValveWithNonZeroRate[valve]!!)

    fun calcMaxPressure(v1: String, v2: String, openValves: Int, remMinutes: Int): Int {
      fun encodeState(): Long {
        val n = graph.indexOf.size
        val i1 = graph.indexOf[v1]!!
        val i2 = graph.indexOf[v2]!!

        var c = 0L
        c = c * (n + 1) + min(i1, i2)
        c = c * (n + 1) + max(i1, i2)
        c = c * 1.shl(graph.indexOfValveWithNonZeroRate.size) + openValves
        c = c * 32 + remMinutes
        return c
      }

      if (remMinutes == 0) return 0
      val state = encodeState()
      if (state in cache) return cache[state]!!

      var maxPressure = Int.MIN_VALUE

      fun canOpen(valve: String) = graph.rateOf[valve]!! > 0 && !isValveOpen(openValves, valve)

      // Case 1: open both valves (which is never worth doing when they are the same)
      if (canOpen(v1) && canOpen(v2) && v1 != v2) {
        maxPressure = max(maxPressure,
                          graph.rateOf[v1]!! * (remMinutes - 1) + graph.rateOf[v2]!! * (remMinutes - 1) + calcMaxPressure(
                            v1,
                            v2,
                            markValveAsOpen(markValveAsOpen(openValves, v1), v2),
                            remMinutes - 1))
      }
      // Case 2: open v1 and go to a successor of v2
      if (canOpen(v1)) {
        for (kid in graph.kidsOf[v2]!!) {
          maxPressure = max(maxPressure,
                            graph.rateOf[v1]!! * (remMinutes - 1) + calcMaxPressure(v1,
                                                                                    kid,
                                                                                    markValveAsOpen(
                                                                                      openValves,
                                                                                      v1),
                                                                                    remMinutes - 1))
        }
      }
      // Case 3: open v2 and go to a successor of v1
      if (canOpen(v2)) {
        for (kid in graph.kidsOf[v1]!!) {
          maxPressure = max(maxPressure,
                            graph.rateOf[v2]!! * (remMinutes - 1) + calcMaxPressure(kid,
                                                                                    v2,
                                                                                    markValveAsOpen(
                                                                                      openValves,
                                                                                      v2),
                                                                                    remMinutes - 1))
        }
      }
      // Case 4: go to successors both of v1 and v2
      for (kid1 in graph.kidsOf[v1]!!) {
        for (kid2 in graph.kidsOf[v2]!!) {
          maxPressure = max(maxPressure,
                            calcMaxPressure(kid1,
                                            kid2,
                                            openValves,
                                            remMinutes - 1))
        }
      }

      cache[state] = maxPressure
      return maxPressure
    }

    return calcMaxPressure("AA", "AA", 0, 26)
  }

  class Graph {
    val kidsOf = mutableMapOf<String, MutableList<String>>()
    val rateOf = mutableMapOf<String, Int>()
    val indexOfValveWithNonZeroRate = mutableMapOf<String, Int>()
    val indexOf = mutableMapOf<String, Int>()
    val vertexAtIndex = mutableMapOf<Int, String>()

    constructor(lines: List<String>) {
      lines.forEach { line ->
        val m = LINE_PATTERN.matcher(line)
        check(m.matches()) { println(line) }

        val from = m.group(1)
        kidsOf.getOrPut(from) { mutableListOf() }

        val rate = m.group(2).toInt()
        rateOf[from] = rate
        if (rate > 0) {
          val ix = indexOfValveWithNonZeroRate.size
          indexOfValveWithNonZeroRate[from] = ix
        }

        val ix = indexOf.size
        indexOf[from] = ix
        vertexAtIndex[ix] = from

        m.group(3).split(", ").forEach { to ->
          kidsOf.getOrPut(to) { mutableListOf() }
          kidsOf[from]!!.add(to)
          kidsOf[to]!!.add(from)
        }
      }
    }
  }
}

fun main() {
  val lines = readLines("Day16")
  println("Part 1 solution = ${Day16.solvePart1(lines)}")
  println("Part 2 solution = ${Day16.solvePart2(lines)}")
}
