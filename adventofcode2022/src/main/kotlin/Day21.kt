import java.util.regex.Pattern

/// https://adventofcode.com/2022, Day 21
///
/// Task: https://adventofcode.com/2022/day/21/
/// Date: 2022-12-26
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 41857219607906
/// Phase 2 answer: 3916936880448
///
/// Tags: graph theory, topological sorting, dfs, memoization, evaluating math expression

object Day21 {
  val literalPattern = Pattern.compile("^([a-z]+): (-?[0-9]+)$")
  val complexExpressionPattern = Pattern.compile("^([a-z]+): ([a-z]+) ([+/*-]) ([a-z]+)$")

  fun computePart1(
    m: String,
    computed: MutableMap<String, Long>,
    monkeyToExpression: Map<String, Expression>,
  ): Long {
    if (m in computed) return computed[m]!!
    val e = monkeyToExpression[m]!!
    if (e.isLiteral) {
      return e.value
    } else {
      val value = e.operation(computePart1(e.m1, computed, monkeyToExpression),
                              computePart1(e.m2, computed, monkeyToExpression))
      computed[m] = value
      return value
    }
  }

  fun solvePart1(lines: List<String>): Long {
    val exp = readExpressions(lines)
    val monkeyToExpression = mutableMapOf<String, Expression>()
    exp.forEach { monkeyToExpression[it.m] = it }
    val computed = mutableMapOf<String, Long>()
    return computePart1("root", computed, monkeyToExpression)
  }

  fun computePart2(
    m: String,
    computed: MutableMap<String, DelayedLong>,
    monkeyToExpression: Map<String, Expression>,
  ): DelayedLong {
    // Return a special value for the node that requires delayed calculation
    if (m == "humn") return DelayedLong(false, 0, null, null, null)
    if (m in computed) return computed[m]!!
    val e = monkeyToExpression[m]!!
    if (e.isLiteral) {
      return DelayedLong(true,e.value,  null, null, null)
    } else {
      val dv1 = computePart2(e.m1, computed, monkeyToExpression)
      val dv2 = computePart2(e.m2, computed, monkeyToExpression)
      if (dv1.isComputed && dv2.isComputed) {
        val value = e.operation(dv1.value!!, dv2.value!!)
        computed[m] = DelayedLong(true, value,  null, null, null)
      } else {
        computed[m] = DelayedLong(false, null, dv1, dv2, e.operationString)
      }
      return computed[m]!!
    }
  }

  /**
   * Represents either an already computed value, or a delayed computation by applying an
   * operation to two other {@link DelayedLong}s.
   */
  data class DelayedLong(
    val isComputed: Boolean,
    val value: Long?,
    val dv1: DelayedLong?,
    val dv2: DelayedLong?,
    val delayedOperationString: String?,
  )

  /**
   * Resolves the missing value for the "humn" input, which boils down to solving one linear
   * equation with one unknown.
   *
   * <p>It assumes that the right-hand side is always known and that the left-hand
   * side has a special form - one operand is a known constant and the other one is either the
   * unknown we are looking for or another {@link DelayedLong} of the same form.
   */
  fun computeX(dv: DelayedLong, rightValue: Long): Long {
    if (!dv.isComputed && dv.dv1 == null) {
      // Special case: we reach the unknown X
      return rightValue
    }
    if (dv.dv1!!.isComputed) {
      when (dv.delayedOperationString) {
        "+" -> return computeX(dv.dv2!!, rightValue - dv.dv1.value!!)
        "-" -> return computeX(dv.dv2!!, -(rightValue - dv.dv1.value!!))
        "*" -> return computeX(dv.dv2!!, rightValue / dv.dv1.value!!)
        "/" -> return computeX(dv.dv2!!, dv.dv1.value!! / rightValue)
      }
    } else {
      check(dv.dv2!!.isComputed)
      when (dv.delayedOperationString) {
        "+" -> return computeX(dv.dv1!!, rightValue - dv.dv2.value!!)
        "-" -> return computeX(dv.dv1!!, rightValue + dv.dv2.value!!)
        "*" -> return computeX(dv.dv1!!, rightValue / dv.dv2.value!!)
        "/" -> return computeX(dv.dv1!!, rightValue * dv.dv2.value!!)
      }
    }
    throw AssertionError()
  }

  fun solvePart2(lines: List<String>): Long {
    val exp = readExpressions(lines)
    val monkeyToExpression = mutableMapOf<String, Expression>()
    exp.forEach { monkeyToExpression[it.m] = it }

    val computed = mutableMapOf<String, DelayedLong>()
    var dv1 = computePart2(monkeyToExpression["root"]!!.m1, computed, monkeyToExpression)
    var dv2 = computePart2(monkeyToExpression["root"]!!.m2, computed, monkeyToExpression)

    // Assume that at least one of the subtrees is computed
    check(dv1.isComputed || dv2.isComputed)
    // Make sure that the right subtree is the one that is computed
    if (dv1.isComputed) {
      var tmp = dv1
      dv1 = dv2
      dv2 = tmp
    }

    return computeX(dv1, dv2.value!!)
  }

  fun readExpressions(lines: List<String>): List<Expression> {
    return lines.map { line ->
      var m = literalPattern.matcher(line)
      if (m.matches()) {
        Expression(m.group(1), true, m.group(2).toLong(), "", "", this::operationAdd, "+")
      } else {
        m = complexExpressionPattern.matcher(line)
        check(m.matches())
        Expression(m.group(1), false, -1, m.group(2), m.group(4), chooseOperation(m.group(3)), m.group(3))
      }
    }
  }

  fun chooseOperation(op: String): (Long, Long) -> Long {
    return when (op) {
      "*" -> this::operationMult
      "/" -> this::operationDiv
      "+" -> this::operationAdd
      "-" -> this::operationSub
      else -> throw AssertionError()
    }
  }

  data class Expression(
    val m: String,
    val isLiteral: Boolean,
    val value: Long,
    val m1: String,
    val m2: String,
    val operation: (Long, Long) -> Long,
    val operationString: String,
  )

  fun operationMult(a: Long, b: Long) = a * b
  fun operationSub(a: Long, b: Long) = a - b
  fun operationDiv(a: Long, b: Long): Long {
    check(b != 0L)
    check(a % b == 0L)
    return a / b
  }

  fun operationAdd(a: Long, b: Long) = a + b
}

fun main() {
  val lines = readLines("Day21")
  println("Part 1 solution = ${Day21.solvePart1(lines)}")
  println("Part 2 solution = ${Day21.solvePart2(lines)}")
}
