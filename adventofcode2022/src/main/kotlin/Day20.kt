/// https://adventofcode.com/2022, Day 20
///
/// Task: https://adventofcode.com/2022/day/20/
/// Date: 2022-12-26
/// Author: Svilen Marchev
///
/// Status: works for both parts
///
/// Phase 1 answer: 8764
/// Phase 2 answer: 535648840980
///
/// Tags: simulation, math, sequences

object Day20 {
  fun solvePart1(lines: List<String>): Long {
    val numbers = readNumbers(lines)
    val order = numbers.mapIndexed { ix, n -> IndexedNumber(ix, n) }.toMutableList()
    mix(order)
    return sumInterestingPositions(order)
  }

  fun solvePart2(lines: List<String>): Long {
    val numbers = readNumbers(lines).map { it * 811589153L }
    val order = numbers.mapIndexed { ix, n -> IndexedNumber(ix, n) }.toMutableList()
    repeat(10) { mix(order) }
    return sumInterestingPositions(order)
  }

  data class IndexedNumber(val ix: Int, val n: Long)

  private fun readNumbers(lines: List<String>): List<Long> = lines.map { it.toLong() }

  fun mix(order: MutableList<IndexedNumber>) {
    val n = order.size
    for (nextToShift in 0 until n) {
      val ix = order.indexOfFirst { it.ix == nextToShift }

      // The element gets excluded from the sequence before finding its new position
      val nExcludingElement = n - 1
      val rightShifts =
        ((order[ix].n % nExcludingElement + nExcludingElement) % nExcludingElement).toInt()

      val saved = order[ix]
      for (k in 0 until rightShifts) {
        order[(ix + k) % n] = order[(ix + k + 1) % n]
      }
      order[(ix + rightShifts) % n] = saved
    }
  }

  private fun sumInterestingPositions(order: MutableList<IndexedNumber>): Long {
    val zeroIx = order.indexOfFirst { it.n == 0L }
    return listOf(1000, 2000, 3000).map { offset -> order[(zeroIx + offset) % order.size].n }
      .reduce { a, b -> a + b }
  }
}

fun main() {
  val lines = readLines("Day20")
  println("Part 1 solution = ${Day20.solvePart1(lines)}")
  println("Part 2 solution = ${Day20.solvePart2(lines)}")
}
