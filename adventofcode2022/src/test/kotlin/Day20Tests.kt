import kotlin.test.Test
import kotlin.test.assertEquals

class Day20Tests {
  private val sampleInput = listOf(
    "1",
    "2",
    "-3",
    "3",
    "-2",
    "0",
    "4",
  )

  @Test
  fun testPart1_sample() {
    val ans = Day20.solvePart1(sampleInput)
    assertEquals(3, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day20")
    val ans = Day20.solvePart1(lines)
    assertEquals(8764, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day20.solvePart2(sampleInput)
    assertEquals(1623178306L, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day20")
    val ans = Day20.solvePart2(lines)
    assertEquals(535648840980, ans)
  }
}
