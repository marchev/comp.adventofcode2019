import kotlin.test.Test
import kotlin.test.assertEquals

class Day21Tests {
  private val sampleInput = listOf(
    "root: pppw + sjmn",
    "dbpl: 5",
    "cczh: sllz + lgvd",
    "zczc: 2",
    "ptdq: humn - dvpt",
    "dvpt: 3",
    "lfqf: 4",
    "humn: 5",
    "ljgn: 2",
    "sjmn: drzm * dbpl",
    "sllz: 4",
    "pppw: cczh / lfqf",
    "lgvd: ljgn * ptdq",
    "drzm: hmdt - zczc",
    "hmdt: 32",
  )

  @Test
  fun testPart1_sample() {
    val ans = Day21.solvePart1(sampleInput)
    assertEquals(152, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day21")
    val ans = Day21.solvePart1(lines)
    assertEquals(41857219607906, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day21.solvePart2(sampleInput)
    assertEquals(301, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day21")
    val ans = Day21.solvePart2(lines)
    assertEquals(3916936880448, ans)
  }
}
