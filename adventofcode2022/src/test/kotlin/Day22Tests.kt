import kotlin.test.Test
import kotlin.test.assertEquals

class Day22Tests {
  private val sampleInput = listOf(
    "        ...#  ",
    "        .#..  ",
    "        #...",
    "        ....",
    "...#.......#    ",
    "........#...",
    "..#....#....",
    "..........#.",
    "        ...#....",
    "        .....#..",
    "        .#......",
    "        ......#.",
    "",
    "10R5L5R10L4R5L5",
  )

  @Test
  fun testPart1_sample() {
    val ans = Day22.solvePart1(sampleInput)
    assertEquals(6032, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day22")
    val ans = Day22.solvePart1(lines)
    assertEquals(159034, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day22")
    val ans = Day22.solvePart2(lines)
    assertEquals(147245, ans)
  }
}
