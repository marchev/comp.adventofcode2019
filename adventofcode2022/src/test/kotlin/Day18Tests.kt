import kotlin.test.Test
import kotlin.test.assertEquals

class Day18Tests {
  private val sampleInput = listOf(
    "2,2,2",
    "1,2,2",
    "3,2,2",
    "2,1,2",
    "2,3,2",
    "2,2,1",
    "2,2,3",
    "2,2,4",
    "2,2,6",
    "1,2,5",
    "3,2,5",
    "2,1,5",
    "2,3,5",
  )

  @Test
  fun testPart1_sample() {
    val ans = Day18.solvePart1(sampleInput)
    assertEquals(64, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day18")
    val ans = Day18.solvePart1(lines)
    assertEquals(3500, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day18.solvePart2(sampleInput)
    assertEquals(58, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day18")
    val ans = Day18.solvePart2(lines)
    assertEquals(2048, ans)
  }
}
