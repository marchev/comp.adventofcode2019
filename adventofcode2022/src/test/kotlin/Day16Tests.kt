import kotlin.test.Test
import kotlin.test.assertEquals

class Day16Tests {
  private val sampleInput = listOf(
    "Valve AA has flow rate=0; tunnels lead to valves DD, II, BB",
    "Valve BB has flow rate=13; tunnels lead to valves CC, AA",
    "Valve CC has flow rate=2; tunnels lead to valves DD, BB",
    "Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE",
    "Valve EE has flow rate=3; tunnels lead to valves FF, DD",
    "Valve FF has flow rate=0; tunnels lead to valves EE, GG",
    "Valve GG has flow rate=0; tunnels lead to valves FF, HH",
    "Valve HH has flow rate=22; tunnel leads to valve GG",
    "Valve II has flow rate=0; tunnels lead to valves AA, JJ",
    "Valve JJ has flow rate=21; tunnel leads to valve II",
  )

  @Test
  fun testPart1_sample() {
    val ans = Day16.solvePart1(sampleInput)
    assertEquals(1651, ans)
  }

  @Test
  fun testPart1_real() {
    val lines = readLines("Day16")
    val ans = Day16.solvePart1(lines)
    assertEquals(2114, ans)
  }

  @Test
  fun testPart2_sample() {
    val ans = Day16.solvePart2(sampleInput)
    assertEquals(1707, ans)
  }

  @Test
  fun testPart2_real() {
    val lines = readLines("Day16")
    val ans = Day16.solvePart2(lines)
    assertEquals(2666, ans)
  }
}
