import kotlin.test.Test
import kotlin.test.assertEquals

class TemplateTests {
  private val sampleInput = listOf(
    "input goes here",
  )

  @Test
  fun testPart1_sample() {
    val ans = DayX.solvePart1(sampleInput)
    assertEquals(0, ans)
  }

  // @Test
  // fun testPart1_real() {
  //   val lines = readLines("DayX")
  //   val ans = DayX.solvePart1(lines)
  //   assertEquals(0, ans)
  // }

  @Test
  fun testPart2_sample() {
    val ans = DayX.solvePart2(sampleInput)
    assertEquals(0, ans)
  }

  // @Test
  // fun testPart2_real() {
  //   val lines = readLines("DayX")
  //   val ans = DayX.solvePart2(lines)
  //   assertEquals(0, ans)
  // }
}
